 -- SET FOREIGN_KEY_CHECKS = 0;
 -- truncate table fatture;
 -- truncate table fatture_stato_fattura;
 -- truncate table fatture_statilav_note;
 -- truncate table fatture_fastweb_csv;
 -- truncate table fatture_fastweb_csv_anomalie;
 -- truncate table fatture_fastweb_statilavnote_csv;
 -- truncate table fatture_fastweb_statilavnote_csv_anomalie;
 -- SET FOREIGN_KEY_CHECKS = 1; 

truncate table fatture_fastweb_csv;
LOAD DATA INFILE 
'C:\\xampp\\htdocs\\gecosql\\Load\\Fastweb\\Files\\csv\\Fastweb_20180301.csv' 
INTO TABLE geco.fatture_fastweb_csv
FIELDS TERMINATED BY ';' 
lines terminated by '\r\n' 
ignore 1 lines 
(ft_zona,
 ft_deb_cod,
 ft_deb_denom,
 ft_deb_citta,
 ft_deb_prov,
 ft_num_fat,
 ft_attribuzione,
 ft_td,
 ft_data_fat,
 ft_data_scad,
 @ft_imp_fat,
 ft_testo,
 ft_stato_lav)
set ft_imp_fat = REPLACE(@ft_imp_fat, ',', '.');
select * from fatture_fastweb_csv;
select sum(ifnull(ft_imp_fat,0)),count(1) from fatture_fastweb_csv;
call upd_fatture_fastweb_csv('Fastweb_20180301.csv');
update fatture_fastweb_csv 
   set ft_id_stato_lav = 20;
   
update fatture_fastweb_csv 
   set ft_td_id = 15 where ft_td_id is null ;
   
   
select * 
  from fatture_fastweb_csv
 where ft_soc             is null or
       ft_id_csv          is null or
       ft_ced_id          is null or
       ft_deb_id          is null or
       ft_num_fat         is null or
       ft_attribuzione    is null or
       ft_td_id           is null or
       ft_id_data_fat     is null or
       ft_imp_fat         is null or
       ft_id_stato_lav    is null;
       
select *
 from fatture_fastweb_csv_anomalie;

call fatturefastwebcsv_2_fatture('Fastweb_20180301.csv');
commit;
select ifnull(b.impano,0),
       ifnull(c.impfatstat,0) + ifnull(b.impano,0) as impnetto,
       ifnull(c.impfatstat,0) as impfatstat
 from (select sum(ifnull(ft_imp_fat,0))  as impano
         from (select distinct ft_id_csv ,
                               ifnull(ft_imp_fat,0) as ft_imp_fat 
                 from fatture_fastweb_csv_anomalie 
                where ft_nomefile_csv = 'Fastweb_20180301.csv') as aa) as b,
      (select sum(ifnull(imp_fat,0)) as impfatstat
         from fatture_v
        where cod_stato_fat in (1,3)) as c;
        
select * from debitori order by deb_id desc;

        
select * 
  from fatture_stato_fattura
 where ft_nomefile_csv = '20171213.csv';    
        
        
        
select 
       ft.deb_cod,
       sum(ifnull(ft.imp_fat,0)) as importo
  from fatture_v as ft
 where cod_stato_fat in (1,3)
 group by  ft.cod_deb
          ;

select * 
  from fatture_fastweb_csv_anomalie;
  
select * from fatture where ft_nomefile_csv = '201711.csv';  
select * from fatture_statilav_note where ftstlav_nomefile_csv = 'Fastweb_20180301.csv';  
select * from fatture_stato_fattura where fsf_nomefile_csv = 'Fastweb_20180301.csv';  

delete from fatture_stato_fattura where fsf_nomefile_csv = '201711.csv';

delete from fatture_fastweb_csv_anomalie where ft_nomefile_csv = '201711.csv';  
  