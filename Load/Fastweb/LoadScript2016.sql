truncate table fatture_fastweb_csv;
LOAD DATA INFILE 'C:\\dropbox\\Dropbox\\GecoDocumenti\\StoricoEstrattiConto\\file originali\\csv\\201601.csv' INTO TABLE geco.fatture_fastweb_csv
FIELDS TERMINATED BY ';' 
lines terminated by '\r\n' 
ignore 1 lines 
(ft_zona,
 ft_deb_cod,
 ft_deb_denom,
 ft_deb_citta,
 ft_deb_prov,
 ft_num_fat,
 ft_attribuzione,
 ft_td,
 ft_data_fat,
 ft_data_scad,
 @ft_imp_fat,
 ft_testo,
 ft_stato_lav)
set ft_imp_fat = REPLACE(@ft_imp_fat, ',', '.'); 
call upd_fatture_fastweb_csv('201601.csv');
update fatture_fastweb_csv 
   set ft_id_stato_lav = 20;
call fatturefastwebcsv_2_fatture('201601.csv');

truncate table fatture_fastweb_csv;
LOAD DATA INFILE 'C:\\dropbox\\Dropbox\\GecoDocumenti\\StoricoEstrattiConto\\file originali\\csv\\201602.csv' INTO TABLE geco.fatture_fastweb_csv
FIELDS TERMINATED BY ';' 
lines terminated by '\r\n' 
ignore 1 lines 
(ft_zona,
 ft_deb_cod,
 ft_deb_denom,
 ft_deb_citta,
 ft_deb_prov,
 ft_num_fat,
 ft_attribuzione,
 ft_td,
 ft_data_fat,
 ft_data_scad,
 @ft_imp_fat,
 ft_testo,
 ft_stato_lav)
set ft_imp_fat = REPLACE(@ft_imp_fat, ',', '.'); 
call upd_fatture_fastweb_csv('201602.csv');
update fatture_fastweb_csv 
   set ft_id_stato_lav = 20;
call fatturefastwebcsv_2_fatture('201602.csv');

truncate table fatture_fastweb_csv;
LOAD DATA INFILE 'C:\\dropbox\\Dropbox\\GecoDocumenti\\StoricoEstrattiConto\\file originali\\csv\\201603.csv' INTO TABLE geco.fatture_fastweb_csv
FIELDS TERMINATED BY ';' 
lines terminated by '\r\n' 
ignore 1 lines 
(ft_zona,
 ft_deb_cod,
 ft_deb_denom,
 ft_deb_citta,
 ft_deb_prov,
 ft_num_fat,
 ft_attribuzione,
 ft_td,
 ft_data_fat,
 ft_data_scad,
 @ft_imp_fat,
 ft_testo,
 ft_stato_lav)
set ft_imp_fat = REPLACE(@ft_imp_fat, ',', '.'); 
call upd_fatture_fastweb_csv('201603.csv');
update fatture_fastweb_csv 
   set ft_id_stato_lav = 20;
call fatturefastwebcsv_2_fatture('201603.csv');

truncate table fatture_fastweb_csv;
LOAD DATA INFILE 'C:\\dropbox\\Dropbox\\GecoDocumenti\\StoricoEstrattiConto\\file originali\\csv\\201604.csv' INTO TABLE geco.fatture_fastweb_csv
FIELDS TERMINATED BY ';' 
lines terminated by '\r\n' 
ignore 1 lines 
(ft_zona,
 ft_deb_cod,
 ft_deb_denom,
 ft_deb_citta,
 ft_deb_prov,
 ft_num_fat,
 ft_attribuzione,
 ft_td,
 ft_data_fat,
 ft_data_scad,
 @ft_imp_fat,
 ft_testo,
 ft_stato_lav)
set ft_imp_fat = REPLACE(@ft_imp_fat, ',', '.'); 
call upd_fatture_fastweb_csv('201604.csv');
update fatture_fastweb_csv 
   set ft_id_stato_lav = 20;
call fatturefastwebcsv_2_fatture('201604.csv');

truncate table fatture_fastweb_csv;
LOAD DATA INFILE 'C:\\dropbox\\Dropbox\\GecoDocumenti\\StoricoEstrattiConto\\file originali\\csv\\201605.csv' INTO TABLE geco.fatture_fastweb_csv
FIELDS TERMINATED BY ';' 
lines terminated by '\r\n' 
ignore 1 lines 
(ft_zona,
 ft_deb_cod,
 ft_deb_denom,
 ft_deb_citta,
 ft_deb_prov,
 ft_num_fat,
 ft_attribuzione,
 ft_td,
 ft_data_fat,
 ft_data_scad,
 @ft_imp_fat,
 ft_testo,
 ft_stato_lav)
set ft_imp_fat = REPLACE(@ft_imp_fat, ',', '.'); 
call upd_fatture_fastweb_csv('201605.csv');
update fatture_fastweb_csv 
   set ft_id_stato_lav = 20;
call fatturefastwebcsv_2_fatture('201605.csv');

truncate table fatture_fastweb_csv;
LOAD DATA INFILE 'C:\\dropbox\\Dropbox\\GecoDocumenti\\StoricoEstrattiConto\\file originali\\csv\\201606.csv' INTO TABLE geco.fatture_fastweb_csv
FIELDS TERMINATED BY ';' 
lines terminated by '\r\n' 
ignore 1 lines 
(ft_zona,
 ft_deb_cod,
 ft_deb_denom,
 ft_deb_citta,
 ft_deb_prov,
 ft_num_fat,
 ft_attribuzione,
 ft_td,
 ft_data_fat,
 ft_data_scad,
 @ft_imp_fat,
 ft_testo,
 ft_stato_lav)
set ft_imp_fat = REPLACE(@ft_imp_fat, ',', '.'); 
call upd_fatture_fastweb_csv('201606.csv');
update fatture_fastweb_csv 
   set ft_id_stato_lav = 20;
call fatturefastwebcsv_2_fatture('201606.csv');

truncate table fatture_fastweb_csv;
LOAD DATA INFILE 'C:\\dropbox\\Dropbox\\GecoDocumenti\\StoricoEstrattiConto\\file originali\\csv\\201607.csv' INTO TABLE geco.fatture_fastweb_csv
FIELDS TERMINATED BY ';' 
lines terminated by '\r\n' 
ignore 1 lines 
(ft_zona,
 ft_deb_cod,
 ft_deb_denom,
 ft_deb_citta,
 ft_deb_prov,
 ft_num_fat,
 ft_attribuzione,
 ft_td,
 ft_data_fat,
 ft_data_scad,
 @ft_imp_fat,
 ft_testo,
 ft_stato_lav)
set ft_imp_fat = REPLACE(@ft_imp_fat, ',', '.'); 
call upd_fatture_fastweb_csv('201607.csv');
update fatture_fastweb_csv 
   set ft_id_stato_lav = 20;
call fatturefastwebcsv_2_fatture('201607.csv');

truncate table fatture_fastweb_csv;
LOAD DATA INFILE 'C:\\dropbox\\Dropbox\\GecoDocumenti\\StoricoEstrattiConto\\file originali\\csv\\201608.csv' INTO TABLE geco.fatture_fastweb_csv
FIELDS TERMINATED BY ';' 
lines terminated by '\r\n' 
ignore 1 lines 
(ft_zona,
 ft_deb_cod,
 ft_deb_denom,
 ft_deb_citta,
 ft_deb_prov,
 ft_num_fat,
 ft_attribuzione,
 ft_td,
 ft_data_fat,
 ft_data_scad,
 @ft_imp_fat,
 ft_testo,
 ft_stato_lav)
set ft_imp_fat = REPLACE(@ft_imp_fat, ',', '.'); 
call upd_fatture_fastweb_csv('201608.csv');
update fatture_fastweb_csv 
   set ft_id_stato_lav = 20;
call fatturefastwebcsv_2_fatture('201608.csv');

truncate table fatture_fastweb_csv;
LOAD DATA INFILE 'C:\\dropbox\\Dropbox\\GecoDocumenti\\StoricoEstrattiConto\\file originali\\csv\\201609.csv' INTO TABLE geco.fatture_fastweb_csv
FIELDS TERMINATED BY ';' 
lines terminated by '\r\n' 
ignore 1 lines 
(ft_zona,
 ft_deb_cod,
 ft_deb_denom,
 ft_deb_citta,
 ft_deb_prov,
 ft_num_fat,
 ft_attribuzione,
 ft_td,
 ft_data_fat,
 ft_data_scad,
 @ft_imp_fat,
 ft_testo,
 ft_stato_lav)
set ft_imp_fat = REPLACE(@ft_imp_fat, ',', '.'); 
call upd_fatture_fastweb_csv('201609.csv');
update fatture_fastweb_csv 
   set ft_id_stato_lav = 20;
call fatturefastwebcsv_2_fatture('201609.csv');
