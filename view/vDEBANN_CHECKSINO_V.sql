drop view if exists debann_checksino_v;
create view debann_checksino_v as
select debann_debid   as debann_debid,
       count(*)       as debann_numann
  from debitori_annotazioni
 group by debann_debid;