drop view if exists fatture_notecrgest_v;                          
create view fatture_notecrgest_v as
select a.ftnote_id_nota as ftnote_id_nota,
       a.ftnote_id_fattura,
       a.ftnote_nota_crgest
  from fatture_notecrgest as a
 where a.ftnote_id_nota = (select max(b.ftnote_id_nota) 
                             from fatture_notecrgest as b
                            where b.ftnote_id_fattura = a.ftnote_id_fattura);
 