drop view if exists webapp_selsoc_v;
create or replace view webapp_selsoc_v as
select fug.fosuser_username               as username,
       a.ges_id                           as ges_id,
       concat(a1.ges_cognome,a1.ges_nome) as nominativo,
       c.soc_cod                          as soc_cod,
       c.soc_des                          as soc_des
  from fosuser_gestori as fug 
  join gest_debitore_v as a   on (a.ges_id = fug.fosuser_gestid)
  join gestori         as a1  on (a1.ges_id = a.ges_id)
  join debitori        as b   on (b.deb_id  = a.deb_id)
  join societa         as c   on (c.soc_cod = b.deb_soc) 
 group by fug.fosuser_username,
          a.ges_id,
          concat(a1.ges_cognome,a1.ges_nome),
          c.soc_cod,
          c.soc_des;