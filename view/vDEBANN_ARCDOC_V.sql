drop view if exists debann_arcdoc_v;
create view debann_arcdoc_v as
select a.debandoc_id                                as debandoc_id,
       a.debandoc_idann                             as debandoc_idann,
       b.tbarcdoc_id                                as debandoc_iddoc,
       b.tbarcdoc_estid                             as debandoc_estid,
       b.tbarcdoc_estcod                            as debandoc_estcod,
       b.tbarcdoc_estdes                            as debandoc_estdes,
       b.tbarcdoc_tipoid                            as debandoc_tipoid,
       b.tbarcdoc_tipocod                           as debandoc_tipocod,
       b.tbarcdoc_tipodes                           as debandoc_tipodes,
       b.tbarcdoc_desc                              as debandoc_desc,
       b.tbarcdoc_size                              as debandoc_size,
       b.tbarcdoc_nomefile                          as debandoc_nomefile,
       a.debandoc_ges_id                            as debandoc_ges_id,
       utypkg_gestid2gestnomin(b.tbarcdoc_ges_id)   as debandoc_ges_nominativo,
       a.debandoc_modifica                          as debandoc_modifica,
       utypkg_datanum2databar(a.debandoc_modifica)  as debandoc_modbar
  from debann_arcdoc as a
  join tab_arcdoc_v  as b on (b.tbarcdoc_id = a.debandoc_iddoc);