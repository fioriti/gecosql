drop view if exists tab_arcdoc_v;
create view tab_arcdoc_v as
select a.tbarcdoc_id        as tbarcdoc_id,
       a.tbarcdoc_soc       as tbarcdoc_soc,
       a.tbarcdoc_est       as tbarcdoc_estid,
       b.estarcdoc_cod      as tbarcdoc_estcod,
       b.estarcdoc_des      as tbarcdoc_estdes, 
       a.tbarcdoc_tipo      as tbarcdoc_tipoid,
       c.tparcdoc_cod       as tbarcdoc_tipocod,
       c.tparcdoc_des       as tbarcdoc_tipodes,
       a.tbarcdoc_size      as tbarcdoc_size,
       a.tbarcdoc_nomefile  as tbarcdoc_nomefile,
       a.tbarcdoc_desc      as tbarcdoc_desc,
       a.tbarcdoc_blob      as tbarcdoc_blob,
       a.tbarcdoc_ges_id    as tbarcdoc_ges_id,
       concat(d.ges_cognome,
              ' ',
              d.ges_nome)   as tbarcdoc_ges_nominativo,
       a.tbarcdoc_modifica  as tbarcdoc_modifica
  from tab_arcdoc         as a
  join estensioni_arcdoc  as b on (b.estarcdoc_id = a.tbarcdoc_est)
  join tipi_arcdoc        as c on (c.tparcdoc_id  = a.tbarcdoc_tipo)
  join gestori            as d on (d.ges_id       = tbarcdoc_ges_id)