drop view if exists filtro0005_elearcdoc_v;
create view filtro0005_elearcdoc_v as
select b.ftarcdoc_id              as elearcdoc_id,
       b.ftarcdoc_idfatt          as elearcdoc_idfatt,
       b.ftarcdoc_iddoc           as elearcdoc_iddoc,
       b.ftarcdoc_estid           as elearcdoc_estid,
       b.ftarcdoc_estcod          as elearcdoc_estcod,
       b.ftarcdoc_estdes          as elearcdoc_estdes,
       b.ftarcdoc_tipoid          as elearcdoc_tipoid,
       b.ftarcdoc_tipocod         as elearcdoc_tipocod,
       b.ftarcdoc_tipodes         as elearcdoc_tipodes,
       b.ftarcdoc_desc            as elearcdoc_desc,
       b.ftarcdoc_size            as elearcdoc_size,
       b.ftarcdoc_nomefile        as elearcdoc_nomefile,
       b.ftarcdoc_ges_id          as elearcdoc_ges_id,
       b.ftarcdoc_ges_nominativo  as elearcdoc_ges_nominativo,
       b.ftarcdoc_modifica        as elearcdoc_modifica,
       b.ftarcdoc_modifica        as elearcdoc_modbar
  from filtro0005_parconn_v as a
  join fatture_arcdoc_v     as b on (b.ftarcdoc_idfatt = a.par_idfatt);
