drop view if exists fatture_stato_fattura_v;
create view fatture_stato_fattura_v as
select a.fsf_id,
       a.fsf_soc,
       a.fsf_cod,
       a.fsf_id_fattura,
       a.fsf_id_stato,
       case a.fsf_id_stato 
           when 1 then 0
           when 2 then 1
           when 3 then 0
           when 4 then 1
       end as fsf_ac,    
       a.fsf_desc,
       a.fsf_ges_id,
       a.fsf_modifica,
       a.fsf_nomefile_csv
  from fatture_stati_fattura_v as a
 where (a.fsf_id_fattura,a.fsf_id) in (select b.fsf_id_fattura,
                                             max(b.fsf_id) 
                                        from fatture_stato_fattura as b
                                       group by b.fsf_id_fattura);