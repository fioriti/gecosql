drop view if exists fatture_arcdoc_sino_v;
create view fatture_arcdoc_sino_v as
select ft.id                                                   as ftarcdoc_idfatt,
       case ifnull(min(fa.ftarcdoc_iddoc),0) when 0 then 0 else 1 end as ftarcdoc_sino
  from fatture                   as ft
  left outer join fatture_arcdoc as fa on (fa.ftarcdoc_idfatt = ft.id) 
 group by ft.id;