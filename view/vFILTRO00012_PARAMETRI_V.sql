drop view if exists filtro0012_parametri_v;
create view filtro0012_parametri_v as
select b.ges_id      as ges_id,
       c.ges_nome    as ges_nome,
       a.id_filtro   as id_filtro,
       a.descrizione as des_filtro,
       b.par_conn    as par_conn,
       b.parint01    as par_id_arcdoc
  from filtri                     as a
  join filtri_parametri           as b on (b.id_filtro = a.id_filtro)
  join gestori                    as c on (c.ges_id    = b.ges_id)
 where a.id_filtro = 12;