drop view if exists GESTIONE_SCADENZIARIO_V;
create view GESTIONE_SCADENZIARIO_V AS
select c.id                             as id,
       c.ft_soc                         as ft_soc,
       c.ft_deb_id                      as ft_deb_id,
       fsf.fsf_ac                       as ft_ac,
       d.deb_id                         as ft_id_debitore,
       d.deb_cod                        as ft_deb_cod,
       d.deb_des                        as ft_deb_des,
       d.deb_citta			                as ft_deb_citta,
       c.ft_ced_id                      as ft_ced_id,
       substr(ft_attribuzione,
          2,length(ft_attribuzione))    as ft_attribuzione,
       c.ft_num_fat                     as ft_num_fat,
       c.ft_td_id                       as ft_td_id,
       td.td_cod                        as ft_td,
       fsln.ftstlav_id_stato_lav        as ft_id_stato_lav,
       fsln.ftstlav_sldes               as ft_des_stato_lav, 
       concat(fsln.ftstlav_slcod,
              '-',
              fsln.ftstlav_sldes )      as ft_sl_coddes,
       c.ft_testo                       as ft_testo,
       df.databar                       as ft_data_fat,
       ds.databar                       as ft_data_scad,
       c.ft_imp_fat                     as ft_imp_fat, 
       c.ft_imp_aperto                  as ft_imp_aperto,
       c.ft_data_ins                    as ft_data_ins,
       c.ft_id_data_fat                 as ft_id_data_fat,
       c.ft_id_data_chius               as ft_id_data_chius,
       fsln.ftstlav_nota_crgest         as ft_nota_crgest,
       fsln.ftstlav_data_bar            as ft_data_nota,
       ifnull(c.ft_arcdoc_check_sino,0) as ft_arcdoc_sino,
       ifnull(c.ft_tabsr_check_sino,0)  as ft_tabsr_sino,
       ft_forecast_mese			            as ft_forecast_mese,
       ft_forecast_anno			            as ft_forecast_anno,
       case ft_forecast_anno 
                    when null then null
                    else concat(ft_forecast_mese,'/',ft_forecast_anno)            
       end                              as ft_forecast_scad
  from filtro0001_parametri_v   as a 
  join gest_debitore_attivi_v   as b   on (b.gd_ges_id    = a.ges_id)
  join debitori                 as d   on (d.deb_id       = b.gd_deb_id                     and
                                           d.deb_id       = ifnull(a.parint03,d.deb_id))
  join societa                  as soc on (soc.soc_cod    = ifnull(a.parint01,soc.soc_cod))
  join tipo_doc                 as td  on (td.td_soc      = ifnull(a.parint01,soc.soc_cod)  and
                                           td.td_id       = ifnull(a.parint02,td.td_id))
  join stato_lavorazione        as sl  on (sl.id          = ifnull(a.parint04,sl.id))
  join calendario               as df  on (df.id between ifnull(a.parint05,0) and ifnull(a.parint06,9999999))
  join calendario               as ds  on (ds.id between ifnull(a.parint07,0) and ifnull(a.parint08,9999999))
  join fatture                  as c   on (c.ft_deb_id        = d.deb_id                      and 
                                           c.ft_soc           = soc.soc_cod                   and 
                                           c.ft_td_id         = td.td_id                      and                                        
                                           c.ft_id_data_fat   = df.id                         and
                                           c.ft_id_data_scad  = ds.id                         and
                                           c.ft_num_fat       = ifnull(parvar09,c.ft_num_fat) and
                                           ifnull(c.ft_arcdoc_check_sino,0) = case ifnull(parint11,0)
                                                                                when 0 then ifnull(c.ft_arcdoc_check_sino,0)
                                                                                else        ifnull(parint11,0)
                                                                              end)
  join fatture_stato_fattura_v  as fsf  on (fsf.fsf_id_fattura = c.id and 
                                            fsf.fsf_ac = case ifnull(parint10,0) when 0 then 0 else fsf.fsf_ac end)
  join fatture_statilavnote_v   as fsln on (ftstlav_id_fattura   = c.id and 
                                            ftstlav_id_stato_lav = sl.id);
                                         