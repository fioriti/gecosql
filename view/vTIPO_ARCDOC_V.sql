drop view if exists tipo_arcdoc_v;
create view tipo_arcdoc_v as 
select a.tparcdoc_id                        as tparcdoc_id,
       a.tparcdoc_soc                       as tparcdoc_soc,
       c.soc_des                            as tparcdoc_socdes,
       a.tparcdoc_cod                       as tparcdoc_cod,
       a.tparcdoc_des                       as tparcdoc_des,
       a.tparcdoc_ges_id                    as tparcdoc_ges_id,
       concat(b.ges_cognome,' ',b.ges_nome) as tparcdoc_gesnome,
       a.tparcdoc_modifica                  as tparcdoc_modifica
  from tipi_arcdoc  as a
  join gestori      as b on (b.ges_id  = a.tparcdoc_ges_id)
  join societa      as c on (c.soc_cod = a.tparcdoc_soc);