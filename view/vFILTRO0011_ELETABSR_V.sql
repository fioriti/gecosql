drop view if exists FILTRO0011_ELETABSR_V;
create view FILTRO0011_ELETABSR_V as
select b.fttbsr_numero                            as fttbsr_numero,
       b.fttbsr_nota                              as fttbsr_nota,
       b.fttbsr_ges_nominativo                    as fttbsr_ges_nominativo,
       b.fttbsr_modifica                          as fttbsr_modifica,
       b.fttbsr_modbar                            as fttbsr_modbar
  from filtro0011_PARCONN_V as a
  join fatture_tabsr_v      as b on (b.fttbsr_idfatt = a.par_idfatt);
  