drop view if exists debann_arcdoc_checksino_v;
create view debann_arcdoc_checksino_v as
select debandoc_idann as debandoc_idann,
       count(*)       as debandoc_numdoc
  from debann_arcdoc
 group by debandoc_idann;
