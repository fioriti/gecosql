drop view if exists fatture_tabsr_v;
create view fatture_tabsr_v as
select a.fttbsr_id                                as fttbsr_id,
       a.fttbsr_idfatt                            as fttbsr_idfatt,
       a.fttbsr_idsr                              as fttbsr_idsr,  
       b.tbsr_numero                              as fttbsr_numero,
       b.tbsr_nota                                as fttbsr_nota,
       UTYPKG_GESTID2GESTNOMIN(a.fttbsr_ges_id)   as fttbsr_ges_nominativo,
       a.fttbsr_modifica                          as fttbsr_modifica,
       UTYPKG_DATADATE2DATABAR(a.fttbsr_modifica) as fttbsr_modbar
  from fatture_tabsr  as a
  join tab_sr         as b on (b.tbsr_id   = a.fttbsr_idsr)
  join tabsr_stati    as c on (c.tbsrst_id = b.tbsr_stato);

