drop view if exists tab_procedure_parametri_v;
create view tab_procedure_parametri_v as
select a.id_proc      as id_proc,
       b.descrizione  as des_proc,
       a.id_host      as id_host,
       c.descrizione  as des_host,
       a.parcar1      as directory_file_load
  from tab_procedure_parametri as a
  join tab_procedure           as b on (b.id_proc = a.id_proc)
  join tab_procedure_host      as c on (c.id_host = a.id_host);
  
  select * from tab_procedure_parametri_v;