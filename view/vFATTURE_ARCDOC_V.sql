drop view if exists fatture_arcdoc_v;
create view fatture_arcdoc_v as
select a.ftarcdoc_id                                as ftarcdoc_id,
       a.ftarcdoc_idfatt                            as ftarcdoc_idfatt,
       b.tbarcdoc_id                                as ftarcdoc_iddoc,
       b.tbarcdoc_estid                             as ftarcdoc_estid,
       b.tbarcdoc_estcod                            as ftarcdoc_estcod,
       b.tbarcdoc_estdes                            as ftarcdoc_estdes,
       b.tbarcdoc_tipoid                            as ftarcdoc_tipoid,
       b.tbarcdoc_tipocod                           as ftarcdoc_tipocod,
       b.tbarcdoc_tipodes                           as ftarcdoc_tipodes,
       b.tbarcdoc_desc                              as ftarcdoc_desc,
       b.tbarcdoc_size                              as ftarcdoc_size,
       b.tbarcdoc_nomefile                          as ftarcdoc_nomefile,
       b.tbarcdoc_ges_id                            as ftarcdoc_ges_id,
       b.tbarcdoc_ges_nominativo                    as ftarcdoc_ges_nominativo,
       b.tbarcdoc_modifica                          as ftarcdoc_modifica,
       utypkg_datanum2databar(b.tbarcdoc_modifica)  as ftarcdoc_modbar
  from fatture_arcdoc as a
  join tab_arcdoc_v   as b on (b.tbarcdoc_id = a.ftarcdoc_iddoc);