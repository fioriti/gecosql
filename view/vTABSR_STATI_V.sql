drop view if exists tabsr_stati_v;
create view tabsr_stati_v as
select a.tbsrst_id                                as tbsrst_id,
       a.tbsrst_soc                               as tbsrst_soccod,
       b.soc_des                                  as tbsrst_socdes,
       a.tbsrst_cod                               as tbsrst_cod,
       a.tbsrst_desc                              as tbsrst_desc,
       a.tbsrst_ges_id                            as tbsrst_ges_id,
       UTYPKG_GESTID2GESTNOMIN(a.tbsrst_ges_id)   as tbsrst_ges_nominativo,
       a.tbsrst_modifica                          as tbsrst_modifica,
       UTYPKG_DATADATE2DATABAR(a.tbsrst_modifica) as tbsrst_modbar
  from tabsr_stati as a
  join societa     as b on (b.soc_cod = a.tbsrst_soc);
  
  select * from tabsr_stati_v;
