drop view if exists filtro0001_debitori_v;
create view filtro0001_debitori_v as 
select 0	      as deb_id,
       'Tutti'	as deb_des
 union all 
select b.gd_deb_id                                            as deb_id,
       concat(ifnull(c.deb_cod,' '),'-',ifnull(deb_des,' '))  as deb_des 
  from filtro0001_parametri_v as a 
       join gest_debitore_attivi_v     	as b on (b.gd_ges_id = a.ges_id)
       join debitori          		as c on (c.deb_soc   = ifnull(a.parint01,c.deb_soc) and
                                       		 c.deb_id    = b.gd_deb_id)
 where a.par_conn = connection_id();