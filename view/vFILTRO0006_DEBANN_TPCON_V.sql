drop view if exists filtro0006_debann_tpcon_v;
create view filtro0006_debann_tpcon_v as
select a.dantpcon_id                                 as dantpcon_id,
       a.dantpcon_idsoc                              as dantpcon_idsoc,
       a.dantpcon_cod                                as dantpcon_cod,
       a.dantpcon_des                                as dantpcon_des,
       a.dantpcon_ges_id                             as dantpcon_gesid,
       utypkg_gestid2gestnomin(a.dantpcon_ges_id)    as dantpcon_nominativo,
       a.dantpcon_modifica                           as dantpcon_modifica,
       utypkg_datadate2databar(a.dantpcon_modifica)  as dantpcon_modbar        
  from debann_tipi_contatto as a
 where dantpcon_idsoc = filtro0001_get_societa();
 