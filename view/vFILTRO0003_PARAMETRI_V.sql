drop view if exists filtro0003_parametri_v;
create view filtro0003_parametri_v as
select b.ges_id      as ges_id,
       c.ges_nome    as ges_nome,
       a.id_filtro   as id_filtro,
       a.descrizione as des_filtro,
       b.par_conn    as par_conn,
       d.id          as idrecperpag,
       case d.valore when 0 
               then case b.parint02 when b.parint02 <= e.maxrecperpag then b.parint02 else e.maxrecperpag end
               else d.valore end    as valrecperpag,
       d.descrizione as desrecperpag, 
       b.parint02    as numrectot, 
       b.parint03    as numpagtot,
       b.parint04    as numpagcorr,
       b.parint05    as numpagesp,
       b.parint06    as limini,
       b.parint07    as limfin,
       b.parint08    as numposcorr
  from filtri                     as a
  join filtri_parametri           as b on (b.id_filtro = a.id_filtro)
  join gestori                    as c on (c.ges_id    = b.ges_id)
  join filtro0003_valori_default  as e
  join filtro0003_elementi_pagina as d on (d.id = ifnull(b.parint09,e.idrecperpag))
 where a.id_filtro = 3;