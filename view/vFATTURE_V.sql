drop view if exists fatture_v;
create or replace view fatture_v as
select a.id                        as id,
       a.ft_soc                    as soc_cod,
       b.soc_des                   as soc_des,
       c.deb_id                    as deb_id,
       a.ft_deb_id                 as cod_deb,
       c.deb_cod                   as deb_cod,
       c.deb_des                   as deb_des,
       a.ft_ced_id                 as ced_id,
       d.ced_des                   as ced_des,
       a.ft_num_fat                as num_fat,
       a.ft_attribuzione           as attribuzione,
       a.ft_td                     as tipo_doc_cod,
       e.td_des                    as tipo_doc_des,
       a.ft_testo                  as testo,
       a.ft_data_fat               as data_fat,
       a.ft_id_data_fat            as id_data_fat,
       a.ft_data_scad              as data_scad,
       a.ft_id_data_scad           as id_data_scad,
       a.ft_imp_fat                as imp_fat,
       a.ft_imp_aperto             as imp_aperto,
       a.ft_data_ins               as data_ins,
       a.ft_id_stato_lav           as id_stato_lav_cod,
       ifnull(g.fs_cod,0)          as cod_stato_fat,
       ifnull(g.fs_desc,'Nessuno') as des_stato_fat,
       ft_data_chius               as data_chius,
       ft_id_data_chius            as id_data_chius,
       ft_nomefile_csv             as ft_nomefile_csv,
       fsf_nomefile_csv            as fsf_nomefile_csv
  from fatture                            as a 
  join societa                            as b on (b.soc_cod = a.ft_soc)
  join debitori                           as c on (c.deb_id  = a.ft_deb_id)
  join cedenti                            as d on (d.ced_id  = a.ft_ced_id)
  join tipo_doc                           as e on (e.td_id   = a.ft_td_id)
  left outer join fatture_stato_fattura_v as f on (f.fsf_id_fattura = a.id)
  left outer join fatture_stati           as g on (g.fs_id = f.fsf_id_stato);