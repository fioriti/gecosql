drop view if exists filtro0001_get_tipodoc_v;
create view filtro0001_get_tipodoc_v as
select b.td_id  as td_id,
       b.td_des	as td_des
  from filtro0001_parametri_v as a
  join filtro0001_tipodoc_v  as b on (b.td_id = ifnull(a.parint02,0));