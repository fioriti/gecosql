drop view if exists filtro0008_elearcdoc_v;
create view filtro0008_elearcdoc_v as
select b.debandoc_id              as elearcdoc_id,
       b.debandoc_idann           as elearcdoc_idann,
       b.debandoc_iddoc           as elearcdoc_iddoc,
       b.debandoc_estid           as elearcdoc_estid,
       b.debandoc_estcod          as elearcdoc_estcod,
       b.debandoc_estdes          as elearcdoc_estdes,
       b.debandoc_tipoid          as elearcdoc_tipoid,
       b.debandoc_tipocod         as elearcdoc_tipocod,
       b.debandoc_tipodes         as elearcdoc_tipodes,
       b.debandoc_desc            as elearcdoc_desc,
       b.debandoc_size            as elearcdoc_size,
       b.debandoc_nomefile        as elearcdoc_nomefile,
       b.debandoc_ges_id          as elearcdoc_ges_id,
       b.debandoc_ges_nominativo  as elearcdoc_ges_nominativo,
       b.debandoc_modifica        as elearcdoc_modifica,
       b.debandoc_modifica        as elearcdoc_modbar
  from filtro0008_parconn_v as a
  join debann_arcdoc_v      as b on (b.debandoc_idann = a.par_debannid);
  
  
  
  
