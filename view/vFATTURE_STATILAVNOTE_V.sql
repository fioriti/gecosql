drop view if exists fatture_statilavnote_v;
create view fatture_statilavnote_v as
select a.ftstlav_id           as ftstlav_id,
       a.ftstlav_id_fattura   as ftstlav_id_fattura,
       a.ftstlav_id_stato_lav as ftstlav_id_stato_lav,
       b.sl_cod               as ftstlav_slcod,
       b.sl_descrizione       as ftstlav_sldes,
       a.ftstlav_nota_crgest  as ftstlav_nota_crgest
  from fatture_statilav_note as a
  join stato_lavorazione     as b on (b.id = a.ftstlav_id_stato_lav)
 where a.ftstlav_id = (select max(c.ftstlav_id)
                              from fatture_statilav_note as c
                             where c.ftstlav_id_fattura = a.ftstlav_id_fattura);