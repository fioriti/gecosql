drop view if exists filtro0006_debann_tpann_v;
create view filtro0006_debann_tpann_v as
select a.dantpan_id                                 as dantpan_id,
       a.dantpan_idsoc                              as dantpan_idsoc,
       a.dantpan_cod                                as dantpan_cod,
       a.dantpan_des                                as dantpan_des,
       a.dantpan_ges_id                             as dantpan_gesid,
       utypkg_gestid2gestnomin(a.dantpan_ges_id)    as dantpan_nominativo,
       a.dantpan_modifica                           as dantpan_modifica,
       utypkg_datadate2databar(a.dantpan_modifica)  as dantpan_modbar        
  from debann_tipi_annotazione as a
 where dantpan_idsoc = filtro0001_get_societa();
 