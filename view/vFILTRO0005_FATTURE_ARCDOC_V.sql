drop view if exists filtro0005_fatture_arcdoc_v;
create view filtro0005_fatture_arcdoc_v as
select b.ftarcdoc_id              as ftarcdoc_id,
       b.ftarcdoc_idfatt          as ftarcdoc_idfatt,
       b.ftarcdoc_iddoc           as ftarcdoc_iddoc,
       b.ftarcdoc_estid           as ftarcdoc_estid,
       b.ftarcdoc_estcod          as ftarcdoc_estcod,
       b.ftarcdoc_estdes          as ftarcdoc_estdes,
       b.ftarcdoc_tipoid          as ftarcdoc_tipoid,
       b.ftarcdoc_tipocod         as ftarcdoc_tipocod,
       b.ftarcdoc_tipodes         as ftarcdoc_tipodes,
       b.ftarcdoc_desc            as ftarcdoc_desc,
       b.ftarcdoc_size            as ftarcdoc_size,
       b.ftarcdoc_nomefile        as ftarcdoc_nomefile,
       b.ftarcdoc_ges_id          as ftarcdoc_ges_id,
       b.ftarcdoc_ges_nominativo  as ftarcdoc_ges_nominativo,
       b.ftarcdoc_modifica        as ftarcdoc_modifica,
       b.ftarcdoc_modifica        as ftarcdoc_modbar
  from filtro0005_parconn_v as a
  join fatture_arcdoc_v     as b on (b.ftarcdoc_idfatt = a.par_idfatt);
