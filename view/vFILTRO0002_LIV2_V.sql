drop view if exists filtro0002_liv2_v;
create view filtro0002_liv2_v as 
select 0	          as lv2_id,
       'Tutti'	    as lv2_des
 union all 
select a.eleord_id  as lv2_id,
       a.eleord_des as lv2_des
  from filtro0002_eleord as a;