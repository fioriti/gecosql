drop view if exists filtro0003_elenco_pagine_v;
create view filtro0003_elenco_pagine_v as
--  select 1          as seq,
--         '&laquo;'  as valore
--  union 
-- select 2          as seq,
--        '&lsaquo;' as valore
-- union
select 3          as seq,
       '1'        as valore
union     
select 4          as seq,
       '2'        as valore
union     
select 5          as seq,
       '3'        as valore
union     
select 6          as seq,
       '4'        as valore
union     
select 7          as seq,
       '5'        as valore       ;
-- union     
-- select 8          as seq,
--       '&rsaquo;' as valore
-- union     
-- select 9          as seq,
--       '&raquo;'  as valore;
