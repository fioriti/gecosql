drop view if exists gest_debitori_periodi_v;
create view gest_debitori_periodi_v as
select a.gd_ges_id        as gd_ges_id,
       b.ges_cognome      as gd_ges_cognome,
       b.ges_nome         as gd_ges_nome,
       a.gd_deb_id        as gd_deb_id,
       c.deb_cod          as gd_deb_cod,
       c.deb_des          as gd_deb_des,
       a.gd_id_dataassoc  as gd_id_dataassoc,
       d.data             as gd_dataassoc,
       d.databar          as gd_datbarassoc,
       a.gd_id_datadissoc as gd_id_datadissoc,
       e.data             as gd_datadissoc,
       e.databar          as gd_datbardissoc
  from gest_debitori_periodi as a
  join gestori               as b on (b.ges_id = a.gd_ges_id)
  join debitori              as c on (c.deb_id = a.gd_deb_id)
  join calendario            as d on (d.id     = a.gd_id_dataassoc)
  join calendario            as e on (e.id     = a.gd_id_datadissoc);