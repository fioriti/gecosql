drop view if exists filtro0001_tipoarcdoc_v;
create view filtro0001_tipoarcdoc_v as 
select tparcdoc_id,
       tparcdoc_soc,
       tparcdoc_socdes,
       tparcdoc_cod,
       tparcdoc_des,
       tparcdoc_ges_id,
       tparcdoc_gesnome,
       tparcdoc_modifica
  from tipo_arcdoc_v  as a
  where a.tparcdoc_soc = filtro0001_get_societa();  