drop view if exists filtro0001_societa_v;
create or replace view filtro0001_societa_v as
select c.soc_cod                          as soc_cod,
       c.soc_des                          as soc_des
  from filtro0001_parametri_v as upd
  join fosuser_gestori        as fug on (fug.fosuser_gestid = upd.ges_id) 
  join gest_debitore          as a   on (a.gd_ges_id = fug.fosuser_gestid)
  join debitori               as b   on (b.deb_id    = a.gd_deb_id)
  join societa                as c   on (c.soc_cod   = b.deb_soc) 
 group by c.soc_cod,
          c.soc_des;