drop view if exists filtro0012_orig_arcdoc_griglia_v;
create view filtro0012_orig_arcdoc_griglia_v as
select a.orig_arcdoc_id                 as orig_arcdoc_id, 
       a.orig_arcdoc_des                as orig_arcdoc_des, 
       a.orig_arcdoc_idori              as orig_arcdoc_idori, 
       a.orig_arcdoc_debid              as orig_arcdoc_debid, 
       c.deb_cod                        as orig_arcdoc_debcod,
       c.deb_des                        as orig_arcdoc_debdes,
       a.orig_arcdoc_ele                as orig_arcdoc_ele, 
       a.orig_arcdoc_eledata            as orig_arcdoc_eledata, 
       d.databar                        as orig_arcdoc_eledatabar,
       a.orig_arcdoc_iddoc              as orig_arcdoc_iddoc, 
       a.orig_arcdoc_gesid              as orig_arcdoc_gesid, 
       a.orig_arcdoc_ges_nominativo     as orig_arcdoc_ges_nominativo, 
       a.orig_arcdoc_modifica           as orig_arcdoc_modifica,
       a.orig_arcdoc_modbar             as orig_arcdoc_modbar,
       b.tbarcdoc_estid                 as orig_estid, 
       b.tbarcdoc_estcod                as orig_estcod, 
       b.tbarcdoc_estdes                as orig_estdes, 
       b.tbarcdoc_tipoid                as orig_tipoid, 
       b.tbarcdoc_tipocod               as orig_tipocod, 
       b.tbarcdoc_tipodes               as orig_tipodes, 
       b.tbarcdoc_size                  as orig_size, 
       b.tbarcdoc_nomefile              as orig_nomefile, 
       b.tbarcdoc_desc                  as orig_desc
  from filtro0012_orig_arcdoc_v as a
  join tab_arcdoc_v             as b on (b.tbarcdoc_id = a.orig_arcdoc_iddoc)
  join debitori                 as c on (c.deb_id      = a.orig_arcdoc_debid)
  join calendario               as d on (d.id          = a.orig_arcdoc_eledata);