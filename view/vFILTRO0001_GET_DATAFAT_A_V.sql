drop view if exists filtro0001_get_datafat_a_v;
create view filtro0001_get_datafat_a_v as
select ifnull(b.data,'gg/mm/aaaa') as datafat_a_v
  from filtro0001_parametri_v as a
  join calendario  as b on (b.id = ifnull(a.parint06,0));
