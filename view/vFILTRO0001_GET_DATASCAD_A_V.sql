drop view if exists filtro0001_get_datascad_a_v;
create view filtro0001_get_datascad_a_v as
select ifnull(b.data,'gg/mm/aaaa') as datascad_a_v
  from filtro0001_parametri_v as a
  join calendario  as b on (b.id = ifnull(a.parint08,0));
