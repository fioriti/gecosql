drop view if exists filtro0002_liv3_v;
create view filtro0002_liv3_v as 
select 0	          as lv3_id,
       'Tutti'	    as lv3_des
 union all 
select a.eleord_id  as lv3_id,
       a.eleord_des as lv3_des
  from filtro0002_eleord as a;