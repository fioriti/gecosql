drop view if exists filtro0002_liv1_v;
create view filtro0002_liv1_v as 
select 0	          as lv1_id,
       'Tutti'	    as lv1_des
 union all 
select a.eleord_id  as lv1_id,
       a.eleord_des as lv1_des
  from filtro0002_eleord as a;