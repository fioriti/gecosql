drop view if exists filtro0008_debann_arcdoc_v;
create view filtro0008_debann_arcdoc_v as
select b.debandoc_id              as debandoc_id,
       b.debandoc_idann           as debandoc_idann,
       b.debandoc_iddoc           as debandoc_iddoc,
       b.debandoc_estid           as debandoc_estid,
       b.debandoc_estcod          as debandoc_estcod,
       b.debandoc_estdes          as debandoc_estdes,
       b.debandoc_tipoid          as debandoc_tipoid,
       b.debandoc_tipocod         as debandoc_tipocod,
       b.debandoc_tipodes         as debandoc_tipodes,
       b.debandoc_desc            as debandoc_desc,
       b.debandoc_size            as debandoc_size,
       b.debandoc_nomefile        as debandoc_nomefile,
       b.debandoc_ges_id          as debandoc_ges_id,
       b.debandoc_ges_nominativo  as debandoc_ges_nominativo,
       b.debandoc_modifica        as debandoc_modifica,
       b.debandoc_modifica        as debandoc_modbar
  from filtro0008_parconn_v as a
  join debann_arcdoc_v      as b on (b.debandoc_idann = a.par_debannid);
  
  
  
  
