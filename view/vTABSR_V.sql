drop view if exists tabsr_v;
create view tabsr_v as 
select a.tbsr_id          as tbsr_id,
       a.tbsr_numero      as tbsr_numero,
       a.tbsr_nota        as tbsr_nota,
       a.tbsr_stato       as tbsr_idstato,
       b.tbsrst_cod       as tbsr_codstato,
       b.tbsrst_desc      as tbsr_desstato,
       a.tbsr_ges_id      as tbsr_ges_id,
       a.tbsr_modifica    as tbsr_modifica
  from tab_sr      as a
  join tabsr_stati as b on (b.tbsrst_id = a.tbsr_stato);
  
  select * from tabsr_v;