drop view if exists filtro0001_get_debitore_v;
create view filtro0001_get_debitore_v as
select b.deb_id  as deb_id,
       b.deb_des as deb_des
  from filtro0001_parametri_v as a
  join filtro0001_debitori_v  as b on (b.deb_id = ifnull(a.parint03,0));
