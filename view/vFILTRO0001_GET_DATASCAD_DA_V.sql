drop view if exists filtro0001_get_datascad_da_v;
create view filtro0001_get_datascad_da_v as
select ifnull(b.data,'gg/mm/aaaa') as datascad_da_v
  from filtro0001_parametri_v as a
  join calendario  as b on (b.id = ifnull(a.parint07,0));