drop view if exists filtro0009_debitori_v;
create view filtro0009_debitori_v as
select b.deb_id     as deb_id,
       b.deb_cod    as deb_cod,
       b.deb_des    as deb_des,
       b.deb_citta  as deb_citta,
       b.deb_pro    as deb_pro,
       b.deb_tel    as deb_tel,
       b.deb_mail   as deb_mail,
       ifnull(c.debann_numann,0)  as deb_numann,
       case ifnull(c.debann_numann,0)
         when 0 then 'N'
         else        'S'
       end                        as deb_ann_checksino
  from filtro0009_parconn_v           as a
  join gest_debitore_attivi_v         as gd on (gd.gd_ges_id = a.ges_id and
                                                gd.gd_deb_id = ifnull(a.par_debid,gd.gd_deb_id))
  join debitori                       as b  on (b.deb_soc    = filtro0001_get_societa() and 
                                                b.deb_id     = gd.gd_deb_id)
  left outer join debann_checksino_v  as c  on (c.debann_debid = b.deb_id)
  order by deb_cod;