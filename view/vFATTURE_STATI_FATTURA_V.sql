drop view if exists fatture_stati_fattura_v;
create view fatture_stati_fattura_v as
select a.fsf_id           as fsf_id,
       b.fs_soc           as fsf_soc,
       a.fsf_id_fattura   as fsf_id_fattura,
       a.fsf_id_stato     as fsf_id_stato,
       b.fs_cod           as fsf_cod,  
       b.fs_desc          as fsf_desc,
       a.fsf_ges_id       as fsf_ges_id,
       a.fsf_modifica     as fsf_modifica,
       a.fsf_nomefile_csv as fsf_nomefile_csv
  from fatture_stato_fattura as a
  join fatture_stati         as b on (b.fs_id = a.fsf_id_stato);