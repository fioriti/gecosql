drop view if exists filtro0012_orig_arcdoc_v;
create view filtro0012_orig_arcdoc_v as
select 'FTAC'                                       as orig_arcdoc_id,
       'FattureArcDoc'                              as orig_arcdoc_des,
       a.ftarcdoc_idfatt                            as orig_arcdoc_idori,
       b.ft_deb_id                                  as orig_arcdoc_debid,
       b.ft_num_fat                                 as orig_arcdoc_ele,
       b.ft_id_data_fat                             as orig_arcdoc_eledata,
       a.ftarcdoc_iddoc                             as orig_arcdoc_iddoc,
       a.ftarcdoc_ges_id                            as orig_arcdoc_gesid,
       utypkg_gestid2gestnomin(a.ftarcdoc_ges_id)   as orig_arcdoc_ges_nominativo,
       a.ftarcdoc_modifica                          as orig_arcdoc_modifica,
       utypkg_datadate2databar(a.ftarcdoc_modifica) as orig_arcdoc_modbar
  from fatture_arcdoc   as a
  join fatture          as b on (b.id = a.ftarcdoc_idfatt)
union
select 'DAAC'                                       as orig_arcdoc_id,
       'DebannArcDoc'                               as orig_arcdoc_des,
       a.debandoc_idann                             as orig_arcdoc_idori,       
       b.debann_debid                               as orig_arcdoc_debid,
       b.debann_idtpann                             as orig_arcdoc_ele,
       b.debann_iddata                              as orig_arcdoc_eledata,   
       a.debandoc_iddoc                             as orig_arcdoc_iddoc,
       a.debandoc_ges_id                            as orig_arcdoc_gesid,
       utypkg_gestid2gestnomin(a.debandoc_ges_id)   as orig_arcdoc_ges_nominativo,
       a.debandoc_modifica                          as orig_arcdoc_modifica,
       utypkg_datadate2databar(a.debandoc_modifica) as orig_arcdoc_modbar
  from debann_arcdoc        as a
  join debitori_annotazioni as b on (b.debann_id = a.debandoc_idann);