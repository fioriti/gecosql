 create or replace view gest_debitore_v as  
 select a.ges_id      as ges_id,
        a.ges_cognome as ges_cognome,
        a.ges_nome    as ges_nome, 
        a.ges_ruolo   as ges_ruolo,
        c.deb_id      as deb_id,
        c.deb_soc     as deb_soc,
        c.deb_cod     as beb_cod,
        c.deb_des     as des_des
   from gestori    as a
   left outer join gest_debitore as b on (b.gd_ges_id = a.ges_id)
              join debitori      as c on (c.deb_id    = case a.ges_ruolo when 'admin' then c.deb_id else b.gd_deb_id end);