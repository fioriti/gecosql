drop view if exists filtro0006_debann_v;
create view filtro0006_debann_v as
select a.debann_id	                as debann_id,
       a.debann_debid	              as deann_debid,
       a.debann_idsoc	              as debann_idsoc,
       a.debann_debcod              as debann_debcod,
       a.debann_debdes              as debann_debdes,
       a.debann_idtpann             as debann_idtpann,
       a.debann_anncod              as debann_anncod,
       a.debann_anndes              as debann_anndes,
       a.debann_idtpcon             as debann_idtpcon,
       a.debann_tpcod               as debann_tpcod,
       a.debann_tpdes               as debann_tpdes,
       a.debann_iddata              as debann_iddata,
       a.debann_datanum             as debann_datanum,
       a.debann_databar             as debann_databar,
       a.debann_annot               as debann_annot,
       a.debann_inter               as debann_inter,
       a.debann_ges_id              as debann_ges_id,
       a.debanno_nominativo         as debanno_nominativo,
       a.debann_modifica            as debann_modifica,
       a.debann_modbar              as debann_modbar,
       ifnull(b.debandoc_numdoc,0)  as debann_numdoc,
       case ifnull(b.debandoc_numdoc,0)
         when 0 then 'N'
         else        'S'
       end                                   as debann_doc_checkdocsino
  from filtro0006_parconn_v                  as par
  join societa                               as soc on (soc.soc_cod    = ifnull(filtro0001_get_societa(),soc.soc_cod))
  join gest_debitore                         as gd  on (gd.gd_ges_id   = filtro0000_get_gesid())
  join debitori                              as d   on (d.deb_soc      = soc.soc_cod  and
                                                        d.deb_id       = gd.gd_deb_id and
                                                        d.deb_id       = ifnull(par.par_debid,d.deb_id)) 
  join debitori_annotazioni_v                as a on (a.debann_idsoc = soc.soc_cod and
                                                      a.debann_debid = d.deb_id)
  left outer join debann_arcdoc_checksino_v  as b on (b.debandoc_idann = a.debann_id)
  order by a.debann_iddata desc;
