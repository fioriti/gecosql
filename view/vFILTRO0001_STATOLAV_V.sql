drop view if exists filtro0001_statolav_v;
create view filtro0001_statolav_v as 
select 0	                                   as sl_id,
       'Tutti'	                             as sl_des
 union all 
select b.id                                  as sl_id,
       substr(concat(b.sl_cod,'-',b.sl_descrizione),1,25) as sl_des 
  from (filtro0001_parametri_v a 
        join stato_lavorazione as b on(b.sl_soc = ifnull(a.parint01,b.sl_soc)))
 where b.sl_ins = 1;