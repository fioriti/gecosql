drop view if exists filtro0001_tipodoc_v;
create view filtro0001_tipodoc_v as 
select 0	      as td_id,
       'Tutti'	as td_des
 union all 
select b.td_id  as td_id,
       b.td_cod as td_des 
  from (filtro0001_parametri_v a 
        join tipo_doc b on(b.td_soc = ifnull(a.parint01,b.td_soc)));