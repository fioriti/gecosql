drop view if exists debitori_annotazioni_v;
create view debitori_annotazioni_v as
select a.debann_id            as debann_id,
       a.debann_debid         as debann_debid,
       b.deb_soc              as debann_idsoc,
       b.deb_cod              as debann_debcod,
       b.deb_des              as debann_debdes,
       a.debann_idtpann       as debann_idtpann,
       c.dantpan_cod          as debann_anncod,
       c.dantpan_des          as debann_anndes,
       a.debann_idtpcon       as debann_idtpcon,
       d.dantpcon_cod         as debann_tpcod,
       d.dantpcon_des         as debann_tpdes,
       a.debann_iddata        as debann_iddata,
       e.datanum              as debann_datanum,
       e.databar              as debann_databar,
       a.debann_annot         as debann_annot,
       a.debann_interlocutore as debann_inter,
       a.debann_ges_id        as debann_ges_id,
       utypkg_gestid2gestnomin(a.debann_ges_id) as debanno_nominativo,
       a.debann_modifica      as debann_modifica,
       utypkg_datadate2databar(debann_modifica) as debann_modbar 
  from debitori_annotazioni    as a
  join debitori                as b on (b.deb_id      = a.debann_debid)
  join debann_tipi_annotazione as c on (c.dantpan_id  = a.debann_idtpann)
  join debann_tipi_contatto    as d on (d.dantpcon_id = a.debann_idtpcon)
  join calendario              as e on (e.id          = a.debann_iddata);

select * from debitori_annotazioni_v;
