drop view if exists STATIST_MOVIMENTI_V;
create view STATIST_MOVIMENTI_V as
select a.stmov_gesid      as stmov_gesid,
       f.fosuser_username as stmov_username,
       g.ges_cognome      as cognome,
       g.ges_nome         as ges_nome,
       a.stmov_opeid      as stmov_opeid,
       b.statope_desc     as stmov_des,
       a.stmod_datetime   as stmod_datetime,
       d.username         as operatore,
       a.modifica         as modifica
  from statist_movimenti  as a
  join statist_operazioni as b on (b.statope_id     = a.stmov_id)
  join gestori            as g on (g.ges_id         = a.stmov_gesid)
  join fosuser_gestori    as f on (f.fosuser_gestid = g.ges_id)
  join fos_user           as d on (d.id             = a.operatore);
  
  
  
  select * from statist_movimenti_v;
