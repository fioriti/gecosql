drop view if exists gest_debitore_attivi_v;
create view gest_debitore_attivi_v as 
select gd_ges_id as gd_ges_id,
       gd_deb_id as gd_deb_id
  from gest_debitore
 where gd_data_dissociazione is null;