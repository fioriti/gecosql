drop view if exists filtro0009_parametri_v;
create view filtro0009_parametri_v as
select b.ges_id      as ges_id,
       utypkg_gestid2gestnomin(b.ges_id) as ges_nominativo,
       a.id_filtro   as id_filtro,
       a.descrizione as des_filtro,
       b.par_conn    as par_conn,
       b.parint01    as par_debid,
       utypkg_gestid2gestnomin(b.operatore) as ope_nominativo,
       UTYPKG_DATADATE2DATABAR(b.modifica)  as modibar
  from filtri                     as a
  join filtri_parametri           as b on (b.id_filtro = a.id_filtro)
 where a.id_filtro = 9;
 
 select * from filtro0009_parametri_v;