drop view if exists filtro0001_get_statolav_v;
create view filtro0001_get_statolav_v as
select b.sl_id  as sl_id,
       b.sl_des	as sl_des
  from filtro0001_parametri_v as a
  join filtro0001_statolav_v  as b on (b.sl_id = ifnull(a.parint04,0));
