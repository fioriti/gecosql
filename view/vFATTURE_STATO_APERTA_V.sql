drop view fatture_stato_aperta_v;
create view fatture_stato_aperta_v as
select a.id           as fsfa_id_fattura,
       a.ft_num_fat   as fsfa_num_fat,
       a.ft_imp_fat   as fsfa_imp_fat,
       b.fsf_id_stato as fsfa_id_stato,
       b.fsf_ac       as fsfa_ac,
       c.fs_cod       as fsfa_cod_stato,
       c.fs_desc      as fsfa_des_stato
  from fatture as a 
  join fatture_stato_fattura_v  as b on(b.fsf_id_fattura = a.id)
  join fatture_stati            as c on(c.fs_id          = b.fsf_id_stato)
 where b.fsf_ac = 'A';