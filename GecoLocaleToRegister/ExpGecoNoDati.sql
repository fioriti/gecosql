-- MySQL dump 10.16  Distrib 10.1.16-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: vs28ahbz_gecotest
-- ------------------------------------------------------
-- Server version	10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `vs28ahbz_gecotest`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `vs28ahbz_gecotest` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `vs28ahbz_gecotest`;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `descrizione` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_debitore` bigint(255) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `stato` int(11) DEFAULT NULL,
  `id_societa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendario`
--

DROP TABLE IF EXISTS `calendario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendario` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `gg` int(2) DEFAULT NULL,
  `mese` int(2) DEFAULT NULL,
  `anno` int(4) DEFAULT NULL,
  `databar` varchar(10) DEFAULT NULL,
  `datanum` int(8) DEFAULT NULL,
  `datachar` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `calendario_1` (`datachar`),
  UNIQUE KEY `calendario_2` (`databar`)
) ENGINE=InnoDB AUTO_INCREMENT=36502 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cedenti`
--

DROP TABLE IF EXISTS `cedenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cedenti` (
  `ced_id` int(10) NOT NULL AUTO_INCREMENT,
  `ced_soc` int(10) NOT NULL,
  `ced_cod` int(10) NOT NULL,
  `ced_des` varchar(50) DEFAULT NULL,
  `ced_citta` varchar(50) DEFAULT NULL,
  `ced_ind` varchar(50) DEFAULT NULL,
  `ced_cap` varchar(5) DEFAULT NULL,
  `ced_pro` varchar(2) DEFAULT NULL,
  `ced_regione` varchar(20) DEFAULT NULL,
  `ced_tel` varchar(10) DEFAULT NULL,
  `ced_mail` varchar(50) DEFAULT NULL,
  `ced_data_affido` date DEFAULT NULL,
  `ced_data_disaffido` date DEFAULT NULL,
  `idold` int(10) DEFAULT NULL,
  PRIMARY KEY (`ced_id`),
  UNIQUE KEY `cedenti_1` (`ced_soc`,`ced_cod`),
  CONSTRAINT `FK_CEDENTI_SOCIETA` FOREIGN KEY (`ced_soc`) REFERENCES `societa` (`soc_cod`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `debann_arcdoc`
--

DROP TABLE IF EXISTS `debann_arcdoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debann_arcdoc` (
  `debandoc_id` int(11) NOT NULL AUTO_INCREMENT,
  `debandoc_idann` int(11) NOT NULL,
  `debandoc_iddoc` int(11) NOT NULL,
  `debandoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `debandoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`debandoc_id`),
  KEY `debann_arcdoc_1` (`debandoc_idann`),
  KEY `fk_debannarcdoc_tabarcdoc` (`debandoc_iddoc`),
  KEY `fk_debannarcdoc_gestori` (`debandoc_ges_id`),
  CONSTRAINT `fk_debannarcdoc_debitoriannotazioni` FOREIGN KEY (`debandoc_idann`) REFERENCES `debitori_annotazioni` (`debann_id`),
  CONSTRAINT `fk_debannarcdoc_gestori` FOREIGN KEY (`debandoc_ges_id`) REFERENCES `gestori` (`ges_id`),
  CONSTRAINT `fk_debannarcdoc_tabarcdoc` FOREIGN KEY (`debandoc_iddoc`) REFERENCES `tab_arcdoc` (`tbarcdoc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `debann_arcdoc_checksino_v`
--

DROP TABLE IF EXISTS `debann_arcdoc_checksino_v`;
/*!50001 DROP VIEW IF EXISTS `debann_arcdoc_checksino_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `debann_arcdoc_checksino_v` (
  `debandoc_idann` tinyint NOT NULL,
  `debandoc_numdoc` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `debann_arcdoc_v`
--

DROP TABLE IF EXISTS `debann_arcdoc_v`;
/*!50001 DROP VIEW IF EXISTS `debann_arcdoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `debann_arcdoc_v` (
  `debandoc_id` tinyint NOT NULL,
  `debandoc_idann` tinyint NOT NULL,
  `debandoc_iddoc` tinyint NOT NULL,
  `debandoc_estid` tinyint NOT NULL,
  `debandoc_estcod` tinyint NOT NULL,
  `debandoc_estdes` tinyint NOT NULL,
  `debandoc_tipoid` tinyint NOT NULL,
  `debandoc_tipocod` tinyint NOT NULL,
  `debandoc_tipodes` tinyint NOT NULL,
  `debandoc_desc` tinyint NOT NULL,
  `debandoc_size` tinyint NOT NULL,
  `debandoc_nomefile` tinyint NOT NULL,
  `debandoc_ges_id` tinyint NOT NULL,
  `debandoc_ges_nominativo` tinyint NOT NULL,
  `debandoc_modifica` tinyint NOT NULL,
  `debandoc_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `debann_checksino_v`
--

DROP TABLE IF EXISTS `debann_checksino_v`;
/*!50001 DROP VIEW IF EXISTS `debann_checksino_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `debann_checksino_v` (
  `debann_debid` tinyint NOT NULL,
  `debann_numann` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `debann_tipi_annotazione`
--

DROP TABLE IF EXISTS `debann_tipi_annotazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debann_tipi_annotazione` (
  `dantpan_id` int(11) NOT NULL AUTO_INCREMENT,
  `dantpan_idsoc` int(11) NOT NULL,
  `dantpan_cod` decimal(5,0) NOT NULL,
  `dantpan_des` varchar(50) NOT NULL,
  `dantpan_ges_id` int(11) NOT NULL DEFAULT '0',
  `dantpan_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dantpan_id`),
  UNIQUE KEY `debanntipiannotazione_1` (`dantpan_idsoc`,`dantpan_cod`),
  KEY `fk_debanntipiannotazione_gestori` (`dantpan_ges_id`),
  CONSTRAINT `fk_debanntipiannotazione_gestori` FOREIGN KEY (`dantpan_ges_id`) REFERENCES `gestori` (`ges_id`),
  CONSTRAINT `fk_debanntipiannotazione_societa` FOREIGN KEY (`dantpan_idsoc`) REFERENCES `societa` (`soc_cod`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `debann_tipi_contatto`
--

DROP TABLE IF EXISTS `debann_tipi_contatto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debann_tipi_contatto` (
  `dantpcon_id` int(11) NOT NULL AUTO_INCREMENT,
  `dantpcon_idsoc` int(11) NOT NULL,
  `dantpcon_cod` decimal(5,0) NOT NULL,
  `dantpcon_des` varchar(50) NOT NULL,
  `dantpcon_ges_id` int(11) NOT NULL DEFAULT '0',
  `dantpcon_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dantpcon_id`),
  UNIQUE KEY `debanntipicontatto_1` (`dantpcon_idsoc`,`dantpcon_cod`),
  KEY `fk_debanntipicontatto_gestori` (`dantpcon_ges_id`),
  CONSTRAINT `fk_debanntipicontatto_gestori` FOREIGN KEY (`dantpcon_ges_id`) REFERENCES `gestori` (`ges_id`),
  CONSTRAINT `fk_debanntipicontatto_societa` FOREIGN KEY (`dantpcon_idsoc`) REFERENCES `societa` (`soc_cod`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `debitori`
--

DROP TABLE IF EXISTS `debitori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debitori` (
  `deb_id` int(10) NOT NULL AUTO_INCREMENT,
  `deb_soc` int(10) NOT NULL,
  `deb_cod` decimal(10,0) NOT NULL,
  `deb_des` varchar(100) DEFAULT NULL,
  `deb_cf_piva` varchar(16) DEFAULT NULL,
  `deb_citta` varchar(50) DEFAULT NULL,
  `deb_ind` varchar(50) DEFAULT NULL,
  `deb_cap` varchar(5) DEFAULT NULL,
  `deb_pro` varchar(2) DEFAULT NULL,
  `deb_regione` varchar(20) DEFAULT NULL,
  `deb_tel` varchar(30) DEFAULT NULL,
  `deb_mail` varchar(50) DEFAULT NULL,
  `deb_data_affido` date DEFAULT NULL,
  `deb_data_disaffido` date DEFAULT NULL,
  PRIMARY KEY (`deb_id`),
  UNIQUE KEY `debitori_1` (`deb_soc`,`deb_cod`),
  CONSTRAINT `FK_debitori_SOCIETA` FOREIGN KEY (`deb_soc`) REFERENCES `societa` (`soc_cod`)
) ENGINE=InnoDB AUTO_INCREMENT=1694 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `debitori_annotazioni`
--

DROP TABLE IF EXISTS `debitori_annotazioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debitori_annotazioni` (
  `debann_id` int(11) NOT NULL AUTO_INCREMENT,
  `debann_debid` int(11) NOT NULL,
  `debann_idtpann` int(11) NOT NULL,
  `debann_idtpcon` int(11) NOT NULL,
  `debann_iddata` int(11) NOT NULL,
  `debann_annot` varchar(2000) DEFAULT NULL,
  `debann_interlocutore` varchar(2000) DEFAULT NULL,
  `debann_ges_id` int(11) NOT NULL DEFAULT '0',
  `debann_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`debann_id`),
  KEY `fk_debitoriannotazioni_calendario` (`debann_iddata`),
  KEY `fk_debitoriannotazioni_debanntipiannotazione` (`debann_idtpann`),
  KEY `fk_debitoriannotazioni_debanntipicontatto` (`debann_idtpcon`),
  KEY `fk_debitoriannotazioni_debitori` (`debann_debid`),
  CONSTRAINT `fk_debitoriannotazioni_calendario` FOREIGN KEY (`debann_iddata`) REFERENCES `calendario` (`id`),
  CONSTRAINT `fk_debitoriannotazioni_debanntipiannotazione` FOREIGN KEY (`debann_idtpann`) REFERENCES `debann_tipi_annotazione` (`dantpan_id`),
  CONSTRAINT `fk_debitoriannotazioni_debanntipicontatto` FOREIGN KEY (`debann_idtpcon`) REFERENCES `debann_tipi_contatto` (`dantpcon_id`),
  CONSTRAINT `fk_debitoriannotazioni_debitori` FOREIGN KEY (`debann_debid`) REFERENCES `debitori` (`deb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `debitori_annotazioni_csv`
--

DROP TABLE IF EXISTS `debitori_annotazioni_csv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debitori_annotazioni_csv` (
  `debann_id` int(11) NOT NULL AUTO_INCREMENT,
  `debann_debcod` varchar(10) DEFAULT NULL,
  `debann_debid` int(11) DEFAULT NULL,
  `debann_idtpann` int(11) DEFAULT NULL,
  `debann_destpann` varchar(50) DEFAULT NULL,
  `debann_idtpcon` int(11) DEFAULT NULL,
  `debann_destpconn` varchar(50) DEFAULT NULL,
  `debann_iddata` int(11) DEFAULT NULL,
  `debann_desdata` varchar(10) DEFAULT NULL,
  `debann_annot` varchar(2000) DEFAULT NULL,
  `debann_interlocutore` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`debann_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2070 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `debitori_annotazioni_v`
--

DROP TABLE IF EXISTS `debitori_annotazioni_v`;
/*!50001 DROP VIEW IF EXISTS `debitori_annotazioni_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `debitori_annotazioni_v` (
  `debann_id` tinyint NOT NULL,
  `debann_debid` tinyint NOT NULL,
  `debann_idsoc` tinyint NOT NULL,
  `debann_debcod` tinyint NOT NULL,
  `debann_debdes` tinyint NOT NULL,
  `debann_idtpann` tinyint NOT NULL,
  `debann_anncod` tinyint NOT NULL,
  `debann_anndes` tinyint NOT NULL,
  `debann_idtpcon` tinyint NOT NULL,
  `debann_tpcod` tinyint NOT NULL,
  `debann_tpdes` tinyint NOT NULL,
  `debann_iddata` tinyint NOT NULL,
  `debann_datanum` tinyint NOT NULL,
  `debann_databar` tinyint NOT NULL,
  `debann_annot` tinyint NOT NULL,
  `debann_inter` tinyint NOT NULL,
  `debann_ges_id` tinyint NOT NULL,
  `debanno_nominativo` tinyint NOT NULL,
  `debann_modifica` tinyint NOT NULL,
  `debann_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `estensioni_arcdoc`
--

DROP TABLE IF EXISTS `estensioni_arcdoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estensioni_arcdoc` (
  `estarcdoc_id` int(11) NOT NULL AUTO_INCREMENT,
  `estarcdoc_cod` varchar(5) NOT NULL,
  `estarcdoc_des` varchar(50) NOT NULL,
  `estarcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `estarcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`estarcdoc_id`),
  UNIQUE KEY `estensioni_arcdoc_1` (`estarcdoc_cod`),
  KEY `fk_estensioni_arcdoc_gestori` (`estarcdoc_ges_id`),
  CONSTRAINT `fk_estensioni_arcdoc_gestori` FOREIGN KEY (`estarcdoc_ges_id`) REFERENCES `gestori` (`ges_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture`
--

DROP TABLE IF EXISTS `fatture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` decimal(10,0) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL,
  `ft_arcdoc_check_sino` decimal(1,0) DEFAULT NULL,
  `ft_tabsr_check_sino` decimal(1,0) DEFAULT NULL,
  `ft_aperta` decimal(1,0) NOT NULL DEFAULT '0',
  `last_ftstlav_id` int(11) DEFAULT NULL,
  `last_ftstlav_id_stato_lav` int(11) NOT NULL DEFAULT '20',
  `last_ftstlav_cod_stato_lav` varchar(4) DEFAULT NULL,
  `last_ftstlav_des_stato_lav` varchar(50) DEFAULT NULL,
  `last_ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `last_fsf_id` int(11) DEFAULT NULL,
  `last_fsf_id_stato` int(11) NOT NULL DEFAULT '1',
  `last_fsf_ac` int(11) NOT NULL DEFAULT '1',
  `ft_forecast_mese` decimal(2,0) DEFAULT NULL,
  `ft_forecast_anno` decimal(4,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fatture_1` (`ft_soc`,`ft_deb_id`,`ft_ced_id`,`ft_num_fat`,`ft_attribuzione`,`ft_id_data_fat`,`ft_imp_fat`),
  UNIQUE KEY `fatture_2` (`ft_nomefile_csv`,`ft_id_csv`),
  KEY `FK_FATDAAFAT_CALENDARIO` (`ft_id_data_fat`),
  KEY `FK_FATDATASCAD_CALENDARIO` (`ft_id_data_scad`),
  KEY `FK_FATTURE_CEDENTI` (`ft_ced_id`),
  KEY `FK_FATTURE_DEBITORI` (`ft_deb_id`),
  KEY `FK_FATTURE_STATOLAV` (`ft_id_stato_lav`),
  KEY `FK_FATTURE_TIPODOC` (`ft_td_id`),
  KEY `fatture_3` (`ft_id_data_chius`),
  KEY `fatture_4` (`ft_data_chius`),
  KEY `ft_gestione_scadenziario` (`ft_soc`,`ft_deb_id`,`ft_ced_id`,`ft_td_id`,`ft_id_data_fat`,`ft_id_data_scad`,`ft_num_fat`,`ft_arcdoc_check_sino`,`ft_aperta`)
) ENGINE=InnoDB AUTO_INCREMENT=300468 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_20170828`
--

DROP TABLE IF EXISTS `fatture_20170828`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_20170828` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` decimal(10,0) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL,
  `ft_arcdoc_check_sino` decimal(1,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_arcdoc`
--

DROP TABLE IF EXISTS `fatture_arcdoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_arcdoc` (
  `ftarcdoc_id` int(11) NOT NULL AUTO_INCREMENT,
  `ftarcdoc_idfatt` int(11) NOT NULL,
  `ftarcdoc_iddoc` int(11) NOT NULL,
  `ftarcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `ftarcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ftarcdoc_id`),
  KEY `fk_fatturearcdoc_gestori` (`ftarcdoc_ges_id`),
  CONSTRAINT `fk_fatturearcdoc_gestori` FOREIGN KEY (`ftarcdoc_ges_id`) REFERENCES `gestori` (`ges_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `fatture_arcdoc_sino_v`
--

DROP TABLE IF EXISTS `fatture_arcdoc_sino_v`;
/*!50001 DROP VIEW IF EXISTS `fatture_arcdoc_sino_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `fatture_arcdoc_sino_v` (
  `ftarcdoc_idfatt` tinyint NOT NULL,
  `ftarcdoc_sino` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `fatture_arcdoc_v`
--

DROP TABLE IF EXISTS `fatture_arcdoc_v`;
/*!50001 DROP VIEW IF EXISTS `fatture_arcdoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `fatture_arcdoc_v` (
  `ftarcdoc_id` tinyint NOT NULL,
  `ftarcdoc_idfatt` tinyint NOT NULL,
  `ftarcdoc_iddoc` tinyint NOT NULL,
  `ftarcdoc_estid` tinyint NOT NULL,
  `ftarcdoc_estcod` tinyint NOT NULL,
  `ftarcdoc_estdes` tinyint NOT NULL,
  `ftarcdoc_tipoid` tinyint NOT NULL,
  `ftarcdoc_tipocod` tinyint NOT NULL,
  `ftarcdoc_tipodes` tinyint NOT NULL,
  `ftarcdoc_desc` tinyint NOT NULL,
  `ftarcdoc_size` tinyint NOT NULL,
  `ftarcdoc_nomefile` tinyint NOT NULL,
  `ftarcdoc_ges_id` tinyint NOT NULL,
  `ftarcdoc_ges_nominativo` tinyint NOT NULL,
  `ftarcdoc_modifica` tinyint NOT NULL,
  `ftarcdoc_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `fatture_fastweb_201708`
--

DROP TABLE IF EXISTS `fatture_fastweb_201708`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_fastweb_201708` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` decimal(10,0) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL,
  `ft_arcdoc_check_sino` decimal(1,0) DEFAULT NULL,
  `ft_tabsr_check_sino` decimal(1,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_fastweb_201709`
--

DROP TABLE IF EXISTS `fatture_fastweb_201709`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_fastweb_201709` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` decimal(10,0) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL,
  `ft_arcdoc_check_sino` decimal(1,0) DEFAULT NULL,
  `ft_tabsr_check_sino` decimal(1,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_fastweb_csv`
--

DROP TABLE IF EXISTS `fatture_fastweb_csv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_fastweb_csv` (
  `ft_id_csv` int(10) NOT NULL AUTO_INCREMENT,
  `ft_zona` int(10) DEFAULT NULL,
  `ft_soc` int(10) NOT NULL DEFAULT '1',
  `ft_ced_cod` int(10) DEFAULT NULL,
  `ft_ced_id` int(10) DEFAULT NULL,
  `ft_ced_denom` varchar(100) DEFAULT NULL,
  `ft_deb_cod` decimal(10,0) DEFAULT NULL,
  `ft_deb_id` int(10) DEFAULT NULL,
  `ft_deb_denom` varchar(100) DEFAULT NULL,
  `ft_deb_citta` varchar(50) DEFAULT NULL,
  `ft_deb_prov` varchar(2) DEFAULT NULL,
  `ft_num_fat` varchar(20) DEFAULT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_td_id` int(10) DEFAULT NULL,
  `ft_data_fat` varchar(10) DEFAULT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_data_scad` varchar(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_stato_lav` varchar(100) DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_data_ins` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ft_id_csv`),
  KEY `fatture_fastweb_csv_1` (`ft_nomefile_csv`),
  KEY `fatture_fastweb_csv_2` (`ft_soc`,`ft_deb_id`,`ft_ced_id`,`ft_num_fat`,`ft_attribuzione`,`ft_imp_fat`,`ft_id_data_fat`)
) ENGINE=InnoDB AUTO_INCREMENT=65536 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_fastweb_csv_anomalie`
--

DROP TABLE IF EXISTS `fatture_fastweb_csv_anomalie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_fastweb_csv_anomalie` (
  `ft_id_csv` int(10) NOT NULL DEFAULT '0',
  `ft_zona` int(10) DEFAULT NULL,
  `ft_soc` int(10) DEFAULT NULL,
  `ft_ced_cod` int(10) DEFAULT NULL,
  `ft_ced_id` int(10) DEFAULT NULL,
  `ft_ced_denom` varchar(100) DEFAULT NULL,
  `ft_deb_cod` int(10) DEFAULT NULL,
  `ft_deb_id` int(10) DEFAULT NULL,
  `ft_deb_denom` varchar(100) DEFAULT NULL,
  `ft_deb_citta` varchar(50) DEFAULT NULL,
  `ft_deb_prov` varchar(2) DEFAULT NULL,
  `ft_num_fat` varchar(20) DEFAULT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_td_id` int(10) DEFAULT NULL,
  `ft_data_fat` varchar(10) DEFAULT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_data_scad` varchar(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_stato_lav` varchar(100) DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_data_ins` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_fastweb_notecrgest_csv`
--

DROP TABLE IF EXISTS `fatture_fastweb_notecrgest_csv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_fastweb_notecrgest_csv` (
  `ftnote_id` int(10) NOT NULL AUTO_INCREMENT,
  `ftnote_idfatt` int(10) DEFAULT NULL,
  `ftnote_societa` int(10) DEFAULT NULL,
  `ftnote_ced_id` int(10) DEFAULT NULL,
  `ftnote_ced_cod` int(10) DEFAULT NULL,
  `ftnote_deb_id` int(10) DEFAULT NULL,
  `ftnote_deb_cod` int(10) DEFAULT NULL,
  `ftnote_id_data_fat` int(10) DEFAULT NULL,
  `ftnote_data_fat` varchar(10) DEFAULT NULL,
  `ftnote_num_fat` varchar(20) DEFAULT NULL,
  `ftnote_attribuzione` varchar(50) DEFAULT NULL,
  `ftnote_imp_fat` decimal(10,2) DEFAULT NULL,
  `ftnote_stato_lav` varchar(100) DEFAULT NULL,
  `ftnote_id_stato_lav` int(10) DEFAULT NULL,
  `ftnote_nota` varchar(1000) DEFAULT NULL,
  `ftnote_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftnote_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ftnote_id`),
  UNIQUE KEY `fatture_fastweb_notecrgest_csv_1` (`ftnote_idfatt`),
  KEY `fatture_fastweb_notecrgest_csv_2` (`ftnote_societa`,`ftnote_ced_id`,`ftnote_deb_id`,`ftnote_data_fat`,`ftnote_num_fat`,`ftnote_attribuzione`,`ftnote_imp_fat`),
  KEY `fk_fatturefastwebnotecrgestcsv_statolav` (`ftnote_id_stato_lav`),
  CONSTRAINT `fk_fatturefastwebnotecrgestcsv_fatture` FOREIGN KEY (`ftnote_idfatt`) REFERENCES `fatture` (`id`),
  CONSTRAINT `fk_fatturefastwebnotecrgestcsv_statolav` FOREIGN KEY (`ftnote_id_stato_lav`) REFERENCES `stato_lavorazione` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67551 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_fastweb_statilavnote_csv`
--

DROP TABLE IF EXISTS `fatture_fastweb_statilavnote_csv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_fastweb_statilavnote_csv` (
  `ftstlav_id` int(10) NOT NULL AUTO_INCREMENT,
  `ftstlav_idfatt` int(10) DEFAULT NULL,
  `ftstlav_societa` int(10) DEFAULT NULL,
  `ftstlav_ced_id` int(10) DEFAULT NULL,
  `ftstlav_ced_cod` decimal(10,0) DEFAULT NULL,
  `ftstlav_deb_id` int(10) DEFAULT NULL,
  `ftstlav_deb_cod` decimal(10,0) DEFAULT NULL,
  `ftstlav_id_data_fat` int(10) DEFAULT NULL,
  `ftstlav_data_fat` varchar(10) DEFAULT NULL,
  `ftstlav_num_fat` varchar(20) DEFAULT NULL,
  `ftstlav_attribuzione` varchar(50) DEFAULT NULL,
  `ftstlav_imp_fat` decimal(10,2) DEFAULT NULL,
  `ftstlav_stato_lav` varchar(100) DEFAULT NULL,
  `ftstlav_id_stato_lav` int(10) DEFAULT NULL,
  `ftstlav_nota` varchar(1000) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ftstlav_id`),
  UNIQUE KEY `fatture_fastweb_statilavnote_csv_1` (`ftstlav_idfatt`),
  KEY `fatture_fastweb_statilavnote_csv_2` (`ftstlav_societa`,`ftstlav_ced_id`,`ftstlav_deb_id`,`ftstlav_data_fat`,`ftstlav_num_fat`,`ftstlav_attribuzione`,`ftstlav_imp_fat`),
  KEY `fk_fatturefastwebstatilavnotecsv_statolav` (`ftstlav_id_stato_lav`),
  CONSTRAINT `fk_fatturefastwebstatilavnotecsv_fatture` FOREIGN KEY (`ftstlav_idfatt`) REFERENCES `fatture` (`id`),
  CONSTRAINT `fk_fatturefastwebstatilavnotecsv_statolav` FOREIGN KEY (`ftstlav_id_stato_lav`) REFERENCES `stato_lavorazione` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=574 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_fastweb_statilavnote_csv_anomalie`
--

DROP TABLE IF EXISTS `fatture_fastweb_statilavnote_csv_anomalie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_fastweb_statilavnote_csv_anomalie` (
  `ftstlav_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_idfatt` int(10) DEFAULT NULL,
  `ftstlav_societa` int(10) DEFAULT NULL,
  `ftstlav_ced_id` int(10) DEFAULT NULL,
  `ftstlav_ced_cod` int(10) DEFAULT NULL,
  `ftstlav_deb_id` int(10) DEFAULT NULL,
  `ftstlav_deb_cod` int(10) DEFAULT NULL,
  `ftstlav_id_data_fat` int(10) DEFAULT NULL,
  `ftstlav_data_fat` varchar(10) DEFAULT NULL,
  `ftstlav_num_fat` varchar(20) DEFAULT NULL,
  `ftstlav_attribuzione` varchar(50) DEFAULT NULL,
  `ftstlav_imp_fat` decimal(10,2) DEFAULT NULL,
  `ftstlav_stato_lav` varchar(100) DEFAULT NULL,
  `ftstlav_id_stato_lav` int(10) DEFAULT NULL,
  `ftstlav_nota` varchar(1000) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_febbraio_temp`
--

DROP TABLE IF EXISTS `fatture_febbraio_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_febbraio_temp` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` int(10) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_mps_csv`
--

DROP TABLE IF EXISTS `fatture_mps_csv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_mps_csv` (
  `ft_id_csv` int(10) NOT NULL AUTO_INCREMENT,
  `ft_ced_cod` int(10) DEFAULT NULL,
  `ft_ced_id` int(10) DEFAULT NULL,
  `ft_ced_denom` varchar(100) DEFAULT NULL,
  `ft_deb_cod` int(10) DEFAULT NULL,
  `ft_deb_id` int(10) DEFAULT NULL,
  `ft_deb_denom` varchar(100) DEFAULT NULL,
  `ft_num_fat` varchar(20) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_td_id` int(10) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` varchar(10) DEFAULT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_data_scad` varchar(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) DEFAULT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ft_id_csv`)
) ENGINE=InnoDB AUTO_INCREMENT=9928 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_notecrgest`
--

DROP TABLE IF EXISTS `fatture_notecrgest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_notecrgest` (
  `ftnote_id_nota` int(10) NOT NULL AUTO_INCREMENT,
  `ftnote_id_fattura` int(10) NOT NULL,
  `ftnote_id_stato_lav` int(10) NOT NULL,
  `ftnote_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftnote_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftnote_id_nota_csv` int(10) DEFAULT NULL,
  `ftnote_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftnote_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ftnote_id_nota`),
  KEY `fatture_notecrgest_1` (`ftnote_id_fattura`),
  KEY `fk_fatturenotegrgest_gestori` (`ftnote_ges_id`),
  CONSTRAINT `fk_fatturenotegrgest_fatture` FOREIGN KEY (`ftnote_id_fattura`) REFERENCES `fatture` (`id`),
  CONSTRAINT `fk_fatturenotegrgest_gestori` FOREIGN KEY (`ftnote_ges_id`) REFERENCES `gestori` (`ges_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_notecrgest_aprile`
--

DROP TABLE IF EXISTS `fatture_notecrgest_aprile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_notecrgest_aprile` (
  `ftnote_id_nota` int(10) NOT NULL DEFAULT '0',
  `ftnote_id_fattura` int(10) NOT NULL,
  `ftnote_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftnote_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftnote_id_nota_csv` int(10) DEFAULT NULL,
  `ftnote_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftnote_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `fatture_notecrgest_v`
--

DROP TABLE IF EXISTS `fatture_notecrgest_v`;
/*!50001 DROP VIEW IF EXISTS `fatture_notecrgest_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `fatture_notecrgest_v` (
  `ftnote_id_nota` tinyint NOT NULL,
  `ftnote_id_fattura` tinyint NOT NULL,
  `ftnote_nota_crgest` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `fatture_stati`
--

DROP TABLE IF EXISTS `fatture_stati`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_stati` (
  `fs_id` int(3) NOT NULL AUTO_INCREMENT,
  `fs_soc` int(10) NOT NULL,
  `fs_cod` int(3) DEFAULT NULL,
  `fs_desc` varchar(50) DEFAULT NULL,
  `fs_ges_id` int(10) NOT NULL DEFAULT '0',
  `fs_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fs_id`),
  UNIQUE KEY `fatture_stati_1` (`fs_soc`,`fs_cod`),
  CONSTRAINT `fk_fatturestati_societa` FOREIGN KEY (`fs_soc`) REFERENCES `societa` (`soc_cod`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `fatture_stati_fattura_v`
--

DROP TABLE IF EXISTS `fatture_stati_fattura_v`;
/*!50001 DROP VIEW IF EXISTS `fatture_stati_fattura_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `fatture_stati_fattura_v` (
  `fsf_id` tinyint NOT NULL,
  `fsf_soc` tinyint NOT NULL,
  `fsf_id_fattura` tinyint NOT NULL,
  `fsf_id_stato` tinyint NOT NULL,
  `fsf_cod` tinyint NOT NULL,
  `fsf_desc` tinyint NOT NULL,
  `fsf_ges_id` tinyint NOT NULL,
  `fsf_modifica` tinyint NOT NULL,
  `fsf_nomefile_csv` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `fatture_statilav_note`
--

DROP TABLE IF EXISTS `fatture_statilav_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_statilav_note` (
  `ftstlav_id` int(10) NOT NULL AUTO_INCREMENT,
  `ftstlav_id_fattura` int(10) NOT NULL,
  `ftstlav_id_stato_lav` int(10) NOT NULL,
  `ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftstlav_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_nota_csv` int(10) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ftstlav_id`),
  KEY `fatture_fatture_statilav_note_1` (`ftstlav_id_fattura`),
  KEY `fatture_fatture_statilav_note_2` (`ftstlav_id_fattura`,`ftstlav_id_stato_lav`),
  KEY `fk_fatturestatilavnote_gestori` (`ftstlav_ges_id`),
  KEY `fk_fatturestatilavnote_statolavorazione` (`ftstlav_id_stato_lav`),
  CONSTRAINT `fk_fatturestatilavnote_fatture` FOREIGN KEY (`ftstlav_id_fattura`) REFERENCES `fatture` (`id`),
  CONSTRAINT `fk_fatturestatilavnote_gestori` FOREIGN KEY (`ftstlav_ges_id`) REFERENCES `gestori` (`ges_id`),
  CONSTRAINT `fk_fatturestatilavnote_statolavorazione` FOREIGN KEY (`ftstlav_id_stato_lav`) REFERENCES `stato_lavorazione` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=333286 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_statilav_note_20170828`
--

DROP TABLE IF EXISTS `fatture_statilav_note_20170828`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_statilav_note_20170828` (
  `ftstlav_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_fattura` int(10) NOT NULL,
  `ftstlav_id_stato_lav` int(10) NOT NULL,
  `ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftstlav_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_nota_csv` int(10) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_statilav_note_fastweb_201708`
--

DROP TABLE IF EXISTS `fatture_statilav_note_fastweb_201708`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_statilav_note_fastweb_201708` (
  `ftstlav_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_fattura` int(10) NOT NULL,
  `ftstlav_id_stato_lav` int(10) NOT NULL,
  `ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftstlav_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_nota_csv` int(10) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_statilav_note_fastweb_201709`
--

DROP TABLE IF EXISTS `fatture_statilav_note_fastweb_201709`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_statilav_note_fastweb_201709` (
  `ftstlav_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_fattura` int(10) NOT NULL,
  `ftstlav_id_stato_lav` int(10) NOT NULL,
  `ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftstlav_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_nota_csv` int(10) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `fatture_statilavnote_v`
--

DROP TABLE IF EXISTS `fatture_statilavnote_v`;
/*!50001 DROP VIEW IF EXISTS `fatture_statilavnote_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `fatture_statilavnote_v` (
  `ftstlav_id` tinyint NOT NULL,
  `ftstlav_id_fattura` tinyint NOT NULL,
  `ftstlav_id_stato_lav` tinyint NOT NULL,
  `ftstlav_slcod` tinyint NOT NULL,
  `ftstlav_sldes` tinyint NOT NULL,
  `ftstlav_nota_crgest` tinyint NOT NULL,
  `ftstlav_data_bar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `fatture_stato_aperta_v`
--

DROP TABLE IF EXISTS `fatture_stato_aperta_v`;
/*!50001 DROP VIEW IF EXISTS `fatture_stato_aperta_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `fatture_stato_aperta_v` (
  `fsfa_id_fattura` tinyint NOT NULL,
  `fsfa_num_fat` tinyint NOT NULL,
  `fsfa_imp_fat` tinyint NOT NULL,
  `fsfa_id_stato` tinyint NOT NULL,
  `fsfa_ac` tinyint NOT NULL,
  `fsfa_cod_stato` tinyint NOT NULL,
  `fsfa_des_stato` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `fatture_stato_fattura`
--

DROP TABLE IF EXISTS `fatture_stato_fattura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_stato_fattura` (
  `fsf_id` int(3) NOT NULL AUTO_INCREMENT,
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`fsf_id`),
  KEY `fatture_stato_fattura_2` (`fsf_id_fattura`),
  KEY `fatture_stato_fattura_3` (`fsf_id_stato`),
  KEY `fatture_stato_fattura_1` (`fsf_id_fattura`,`fsf_id_stato`),
  CONSTRAINT `fk_fatturestatofatture_fatture` FOREIGN KEY (`fsf_id_fattura`) REFERENCES `fatture` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=951123 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_stato_fattura_20170828`
--

DROP TABLE IF EXISTS `fatture_stato_fattura_20170828`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_stato_fattura_20170828` (
  `fsf_id` int(3) NOT NULL DEFAULT '0',
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_stato_fattura_fastweb_201708`
--

DROP TABLE IF EXISTS `fatture_stato_fattura_fastweb_201708`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_stato_fattura_fastweb_201708` (
  `fsf_id` int(3) NOT NULL DEFAULT '0',
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_stato_fattura_fastweb_201709`
--

DROP TABLE IF EXISTS `fatture_stato_fattura_fastweb_201709`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_stato_fattura_fastweb_201709` (
  `fsf_id` int(3) NOT NULL DEFAULT '0',
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_stato_fattura_fastweb_csv`
--

DROP TABLE IF EXISTS `fatture_stato_fattura_fastweb_csv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_stato_fattura_fastweb_csv` (
  `fsf_id` int(3) NOT NULL AUTO_INCREMENT,
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`fsf_id`),
  KEY `fatture_stato_fattura_fastweb_csv_1` (`fsf_id_fattura`,`fsf_id_stato`),
  KEY `fatture_stato_fattura_fastweb_csv_2` (`fsf_id_fattura`),
  KEY `fatture_stato_fattura_fastweb_csv_3` (`fsf_id_stato`),
  KEY `fatture_stato_fattura_fastweb_csv_4` (`fsf_nomefile_csv`),
  CONSTRAINT `fk_fatturestatofatturefastweb_csv_fatture` FOREIGN KEY (`fsf_id_fattura`) REFERENCES `fatture` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=876 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_stato_fattura_febbraio_temp`
--

DROP TABLE IF EXISTS `fatture_stato_fattura_febbraio_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_stato_fattura_febbraio_temp` (
  `fsf_id` int(3) NOT NULL DEFAULT '0',
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `fatture_stato_fattura_v`
--

DROP TABLE IF EXISTS `fatture_stato_fattura_v`;
/*!50001 DROP VIEW IF EXISTS `fatture_stato_fattura_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `fatture_stato_fattura_v` (
  `fsf_id` tinyint NOT NULL,
  `fsf_soc` tinyint NOT NULL,
  `fsf_cod` tinyint NOT NULL,
  `fsf_id_fattura` tinyint NOT NULL,
  `fsf_id_stato` tinyint NOT NULL,
  `fsf_ac` tinyint NOT NULL,
  `fsf_desc` tinyint NOT NULL,
  `fsf_ges_id` tinyint NOT NULL,
  `fsf_modifica` tinyint NOT NULL,
  `fsf_nomefile_csv` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `fatture_stato_fatura_maggio`
--

DROP TABLE IF EXISTS `fatture_stato_fatura_maggio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_stato_fatura_maggio` (
  `fsf_id` int(3) NOT NULL DEFAULT '0',
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fatture_tabsr`
--

DROP TABLE IF EXISTS `fatture_tabsr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fatture_tabsr` (
  `fttbsr_id` int(11) NOT NULL AUTO_INCREMENT,
  `fttbsr_idfatt` int(11) NOT NULL,
  `fttbsr_idsr` int(11) NOT NULL,
  `fttbsr_ges_id` int(11) NOT NULL DEFAULT '0',
  `fttbsr_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fttbsr_id`),
  UNIQUE KEY `fatture_tabsr_idx` (`fttbsr_idfatt`,`fttbsr_idsr`),
  KEY `fk_fatturetabsr_tabsr` (`fttbsr_idsr`),
  KEY `fk_fatturetabsr_gestori` (`fttbsr_ges_id`),
  CONSTRAINT `fk_fatturetabsr_fatture` FOREIGN KEY (`fttbsr_idfatt`) REFERENCES `fatture` (`id`),
  CONSTRAINT `fk_fatturetabsr_gestori` FOREIGN KEY (`fttbsr_ges_id`) REFERENCES `gestori` (`ges_id`),
  CONSTRAINT `fk_fatturetabsr_tabsr` FOREIGN KEY (`fttbsr_idsr`) REFERENCES `tab_sr` (`tbsr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `fatture_tabsr_v`
--

DROP TABLE IF EXISTS `fatture_tabsr_v`;
/*!50001 DROP VIEW IF EXISTS `fatture_tabsr_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `fatture_tabsr_v` (
  `fttbsr_id` tinyint NOT NULL,
  `fttbsr_idfatt` tinyint NOT NULL,
  `fttbsr_idsr` tinyint NOT NULL,
  `fttbsr_numero` tinyint NOT NULL,
  `fttbsr_nota` tinyint NOT NULL,
  `fttbsr_ges_nominativo` tinyint NOT NULL,
  `fttbsr_modifica` tinyint NOT NULL,
  `fttbsr_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `fatture_v`
--

DROP TABLE IF EXISTS `fatture_v`;
/*!50001 DROP VIEW IF EXISTS `fatture_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `fatture_v` (
  `id` tinyint NOT NULL,
  `soc_cod` tinyint NOT NULL,
  `soc_des` tinyint NOT NULL,
  `deb_id` tinyint NOT NULL,
  `cod_deb` tinyint NOT NULL,
  `deb_cod` tinyint NOT NULL,
  `deb_des` tinyint NOT NULL,
  `ced_id` tinyint NOT NULL,
  `ced_des` tinyint NOT NULL,
  `num_fat` tinyint NOT NULL,
  `attribuzione` tinyint NOT NULL,
  `tipo_doc_cod` tinyint NOT NULL,
  `tipo_doc_des` tinyint NOT NULL,
  `testo` tinyint NOT NULL,
  `data_fat` tinyint NOT NULL,
  `id_data_fat` tinyint NOT NULL,
  `data_scad` tinyint NOT NULL,
  `imp_fat` tinyint NOT NULL,
  `imp_aperto` tinyint NOT NULL,
  `data_ins` tinyint NOT NULL,
  `id_stato_lav_cod` tinyint NOT NULL,
  `cod_stato_fat` tinyint NOT NULL,
  `des_stato_fat` tinyint NOT NULL,
  `data_chius` tinyint NOT NULL,
  `id_data_chius` tinyint NOT NULL,
  `ft_nomefile_csv` tinyint NOT NULL,
  `fsf_nomefile_csv` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `filtri`
--

DROP TABLE IF EXISTS `filtri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filtri` (
  `id_filtro` int(10) NOT NULL,
  `descrizione` varchar(50) DEFAULT NULL,
  `operatore` int(11) NOT NULL DEFAULT '0',
  `modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_filtro`),
  KEY `fk_filtri_fosuser` (`operatore`),
  CONSTRAINT `fk_filtri_fosuser` FOREIGN KEY (`operatore`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `filtri_parametri`
--

DROP TABLE IF EXISTS `filtri_parametri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filtri_parametri` (
  `ges_id` int(10) NOT NULL,
  `id_filtro` int(10) NOT NULL,
  `par_conn` int(10) DEFAULT NULL,
  `parvar01` varchar(50) DEFAULT NULL,
  `parvar02` varchar(50) DEFAULT NULL,
  `parvar03` varchar(50) DEFAULT NULL,
  `parvar04` varchar(50) DEFAULT NULL,
  `parvar05` varchar(50) DEFAULT NULL,
  `parvar06` varchar(50) DEFAULT NULL,
  `parvar07` varchar(50) DEFAULT NULL,
  `parvar08` varchar(50) DEFAULT NULL,
  `parvar09` varchar(50) DEFAULT NULL,
  `parvar10` varchar(50) DEFAULT NULL,
  `parvar11` varchar(50) DEFAULT NULL,
  `parvar12` varchar(50) DEFAULT NULL,
  `parvar13` varchar(50) DEFAULT NULL,
  `parvar14` varchar(50) DEFAULT NULL,
  `parvar15` varchar(50) DEFAULT NULL,
  `parvar16` varchar(50) DEFAULT NULL,
  `parvar17` varchar(50) DEFAULT NULL,
  `parvar18` varchar(50) DEFAULT NULL,
  `parvar19` varchar(50) DEFAULT NULL,
  `parvar20` varchar(50) DEFAULT NULL,
  `parvar21` varchar(50) DEFAULT NULL,
  `parvar22` varchar(50) DEFAULT NULL,
  `parvar23` varchar(50) DEFAULT NULL,
  `parvar24` varchar(50) DEFAULT NULL,
  `parvar25` varchar(50) DEFAULT NULL,
  `parvar26` varchar(50) DEFAULT NULL,
  `parvar27` varchar(50) DEFAULT NULL,
  `parvar28` varchar(50) DEFAULT NULL,
  `parvar29` varchar(50) DEFAULT NULL,
  `parvar30` varchar(50) DEFAULT NULL,
  `parint01` decimal(10,0) DEFAULT NULL,
  `parint02` decimal(10,0) DEFAULT NULL,
  `parint03` decimal(10,0) DEFAULT NULL,
  `parint04` decimal(10,0) DEFAULT NULL,
  `parint05` decimal(10,0) DEFAULT NULL,
  `parint06` decimal(10,0) DEFAULT NULL,
  `parint07` decimal(10,0) DEFAULT NULL,
  `parint08` decimal(10,0) DEFAULT NULL,
  `parint09` decimal(10,0) DEFAULT NULL,
  `parint10` decimal(10,0) DEFAULT NULL,
  `parint11` decimal(10,0) DEFAULT NULL,
  `parint12` decimal(10,0) DEFAULT NULL,
  `parint13` decimal(10,0) DEFAULT NULL,
  `parint14` decimal(10,0) DEFAULT NULL,
  `parint15` decimal(10,0) DEFAULT NULL,
  `parint16` decimal(10,0) DEFAULT NULL,
  `parint17` decimal(10,0) DEFAULT NULL,
  `parint18` decimal(10,0) DEFAULT NULL,
  `parint19` decimal(10,0) DEFAULT NULL,
  `parint20` decimal(10,0) DEFAULT NULL,
  `parint21` decimal(10,0) DEFAULT NULL,
  `parint22` decimal(10,0) DEFAULT NULL,
  `parint23` decimal(10,0) DEFAULT NULL,
  `parint24` decimal(10,0) DEFAULT NULL,
  `parint25` decimal(10,0) DEFAULT NULL,
  `parint26` decimal(10,0) DEFAULT NULL,
  `parint27` decimal(10,0) DEFAULT NULL,
  `parint28` decimal(10,0) DEFAULT NULL,
  `parint29` decimal(10,0) DEFAULT NULL,
  `parint30` decimal(10,0) DEFAULT NULL,
  `pardat01` date DEFAULT NULL,
  `pardat02` date DEFAULT NULL,
  `pardat03` date DEFAULT NULL,
  `pardat04` date DEFAULT NULL,
  `pardat05` date DEFAULT NULL,
  `pardat06` date DEFAULT NULL,
  `pardat07` date DEFAULT NULL,
  `pardat08` date DEFAULT NULL,
  `pardat09` date DEFAULT NULL,
  `pardat10` date DEFAULT NULL,
  `operatore` int(11) NOT NULL DEFAULT '0',
  `modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ges_id`,`id_filtro`),
  KEY `fk_filtriparametri2_gestori` (`operatore`),
  CONSTRAINT `fk_filtriparametri_gestori` FOREIGN KEY (`ges_id`) REFERENCES `gestori` (`ges_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `filtri_parametri_v`
--

DROP TABLE IF EXISTS `filtri_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtri_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtri_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `parvar01` tinyint NOT NULL,
  `parvar02` tinyint NOT NULL,
  `parvar03` tinyint NOT NULL,
  `parvar04` tinyint NOT NULL,
  `parvar05` tinyint NOT NULL,
  `parvar06` tinyint NOT NULL,
  `parvar07` tinyint NOT NULL,
  `parvar08` tinyint NOT NULL,
  `parvar09` tinyint NOT NULL,
  `parvar10` tinyint NOT NULL,
  `parvar11` tinyint NOT NULL,
  `parvar12` tinyint NOT NULL,
  `parvar13` tinyint NOT NULL,
  `parvar14` tinyint NOT NULL,
  `parvar15` tinyint NOT NULL,
  `parvar16` tinyint NOT NULL,
  `parvar17` tinyint NOT NULL,
  `parvar18` tinyint NOT NULL,
  `parvar19` tinyint NOT NULL,
  `parvar20` tinyint NOT NULL,
  `parvar21` tinyint NOT NULL,
  `parvar22` tinyint NOT NULL,
  `parvar23` tinyint NOT NULL,
  `parvar24` tinyint NOT NULL,
  `parvar25` tinyint NOT NULL,
  `parvar26` tinyint NOT NULL,
  `parvar27` tinyint NOT NULL,
  `parvar28` tinyint NOT NULL,
  `parvar29` tinyint NOT NULL,
  `parvar30` tinyint NOT NULL,
  `parint01` tinyint NOT NULL,
  `parint02` tinyint NOT NULL,
  `parint03` tinyint NOT NULL,
  `parint04` tinyint NOT NULL,
  `parint05` tinyint NOT NULL,
  `parint06` tinyint NOT NULL,
  `parint07` tinyint NOT NULL,
  `parint08` tinyint NOT NULL,
  `parint09` tinyint NOT NULL,
  `parint10` tinyint NOT NULL,
  `parint11` tinyint NOT NULL,
  `parint12` tinyint NOT NULL,
  `parint13` tinyint NOT NULL,
  `parint14` tinyint NOT NULL,
  `parint15` tinyint NOT NULL,
  `parint16` tinyint NOT NULL,
  `parint17` tinyint NOT NULL,
  `parint18` tinyint NOT NULL,
  `parint19` tinyint NOT NULL,
  `parint20` tinyint NOT NULL,
  `parint21` tinyint NOT NULL,
  `parint22` tinyint NOT NULL,
  `parint23` tinyint NOT NULL,
  `parint24` tinyint NOT NULL,
  `parint25` tinyint NOT NULL,
  `parint26` tinyint NOT NULL,
  `parint27` tinyint NOT NULL,
  `parint28` tinyint NOT NULL,
  `parint29` tinyint NOT NULL,
  `parint30` tinyint NOT NULL,
  `pardat01` tinyint NOT NULL,
  `pardat02` tinyint NOT NULL,
  `pardat03` tinyint NOT NULL,
  `pardat04` tinyint NOT NULL,
  `pardat05` tinyint NOT NULL,
  `pardat06` tinyint NOT NULL,
  `pardat07` tinyint NOT NULL,
  `pardat08` tinyint NOT NULL,
  `pardat09` tinyint NOT NULL,
  `pardat10` tinyint NOT NULL,
  `operatore` tinyint NOT NULL,
  `modifica` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0000_parametri_v`
--

DROP TABLE IF EXISTS `filtro0000_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0000_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0000_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `parvar01` tinyint NOT NULL,
  `parvar02` tinyint NOT NULL,
  `parvar03` tinyint NOT NULL,
  `parvar04` tinyint NOT NULL,
  `parvar05` tinyint NOT NULL,
  `parvar06` tinyint NOT NULL,
  `parvar07` tinyint NOT NULL,
  `parvar08` tinyint NOT NULL,
  `parvar09` tinyint NOT NULL,
  `parvar10` tinyint NOT NULL,
  `parint01` tinyint NOT NULL,
  `parint02` tinyint NOT NULL,
  `parint03` tinyint NOT NULL,
  `parint04` tinyint NOT NULL,
  `parint05` tinyint NOT NULL,
  `parint06` tinyint NOT NULL,
  `parint07` tinyint NOT NULL,
  `parint08` tinyint NOT NULL,
  `parint09` tinyint NOT NULL,
  `parint10` tinyint NOT NULL,
  `pardat01` tinyint NOT NULL,
  `pardat02` tinyint NOT NULL,
  `pardat03` tinyint NOT NULL,
  `pardat04` tinyint NOT NULL,
  `pardat05` tinyint NOT NULL,
  `pardat06` tinyint NOT NULL,
  `pardat07` tinyint NOT NULL,
  `pardat08` tinyint NOT NULL,
  `pardat09` tinyint NOT NULL,
  `pardat10` tinyint NOT NULL,
  `operatore` tinyint NOT NULL,
  `modifica` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_debitori_v`
--

DROP TABLE IF EXISTS `filtro0001_debitori_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_debitori_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_debitori_v` (
  `deb_id` tinyint NOT NULL,
  `deb_des` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_get_datafat_a_v`
--

DROP TABLE IF EXISTS `filtro0001_get_datafat_a_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_get_datafat_a_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_get_datafat_a_v` (
  `datafat_a_v` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_get_datafat_da_v`
--

DROP TABLE IF EXISTS `filtro0001_get_datafat_da_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_get_datafat_da_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_get_datafat_da_v` (
  `datafat_da_v` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_get_datascad_a_v`
--

DROP TABLE IF EXISTS `filtro0001_get_datascad_a_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_get_datascad_a_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_get_datascad_a_v` (
  `datascad_a_v` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_get_datascad_da_v`
--

DROP TABLE IF EXISTS `filtro0001_get_datascad_da_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_get_datascad_da_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_get_datascad_da_v` (
  `datascad_da_v` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_get_tipodoc_v`
--

DROP TABLE IF EXISTS `filtro0001_get_tipodoc_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_get_tipodoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_get_tipodoc_v` (
  `td_id` tinyint NOT NULL,
  `td_des` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_parametri_v`
--

DROP TABLE IF EXISTS `filtro0001_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `parvar01` tinyint NOT NULL,
  `parvar02` tinyint NOT NULL,
  `parvar03` tinyint NOT NULL,
  `parvar04` tinyint NOT NULL,
  `parvar05` tinyint NOT NULL,
  `parvar06` tinyint NOT NULL,
  `parvar07` tinyint NOT NULL,
  `parvar08` tinyint NOT NULL,
  `parvar09` tinyint NOT NULL,
  `parvar10` tinyint NOT NULL,
  `parvar11` tinyint NOT NULL,
  `parvar12` tinyint NOT NULL,
  `parvar13` tinyint NOT NULL,
  `parvar14` tinyint NOT NULL,
  `parvar15` tinyint NOT NULL,
  `parvar16` tinyint NOT NULL,
  `parvar17` tinyint NOT NULL,
  `parvar18` tinyint NOT NULL,
  `parvar19` tinyint NOT NULL,
  `parvar20` tinyint NOT NULL,
  `parvar21` tinyint NOT NULL,
  `parvar22` tinyint NOT NULL,
  `parvar23` tinyint NOT NULL,
  `parvar24` tinyint NOT NULL,
  `parvar25` tinyint NOT NULL,
  `parvar26` tinyint NOT NULL,
  `parvar27` tinyint NOT NULL,
  `parvar28` tinyint NOT NULL,
  `parvar29` tinyint NOT NULL,
  `parvar30` tinyint NOT NULL,
  `parint01` tinyint NOT NULL,
  `parint02` tinyint NOT NULL,
  `parint03` tinyint NOT NULL,
  `parint04` tinyint NOT NULL,
  `parint05` tinyint NOT NULL,
  `parint06` tinyint NOT NULL,
  `parint07` tinyint NOT NULL,
  `parint08` tinyint NOT NULL,
  `parint09` tinyint NOT NULL,
  `parint10` tinyint NOT NULL,
  `parint11` tinyint NOT NULL,
  `parint12` tinyint NOT NULL,
  `parint13` tinyint NOT NULL,
  `parint14` tinyint NOT NULL,
  `parint15` tinyint NOT NULL,
  `parint16` tinyint NOT NULL,
  `parint17` tinyint NOT NULL,
  `parint18` tinyint NOT NULL,
  `parint19` tinyint NOT NULL,
  `parint20` tinyint NOT NULL,
  `parint21` tinyint NOT NULL,
  `parint22` tinyint NOT NULL,
  `parint23` tinyint NOT NULL,
  `parint24` tinyint NOT NULL,
  `parint25` tinyint NOT NULL,
  `parint26` tinyint NOT NULL,
  `parint27` tinyint NOT NULL,
  `parint28` tinyint NOT NULL,
  `parint29` tinyint NOT NULL,
  `parint30` tinyint NOT NULL,
  `pardat01` tinyint NOT NULL,
  `pardat02` tinyint NOT NULL,
  `pardat03` tinyint NOT NULL,
  `pardat04` tinyint NOT NULL,
  `pardat05` tinyint NOT NULL,
  `pardat06` tinyint NOT NULL,
  `pardat07` tinyint NOT NULL,
  `pardat08` tinyint NOT NULL,
  `pardat09` tinyint NOT NULL,
  `pardat10` tinyint NOT NULL,
  `operatore` tinyint NOT NULL,
  `modifica` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_societa_v`
--

DROP TABLE IF EXISTS `filtro0001_societa_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_societa_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_societa_v` (
  `soc_cod` tinyint NOT NULL,
  `soc_des` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_statolav_v`
--

DROP TABLE IF EXISTS `filtro0001_statolav_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_statolav_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_statolav_v` (
  `sl_id` tinyint NOT NULL,
  `sl_des` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_tipoarcdoc_v`
--

DROP TABLE IF EXISTS `filtro0001_tipoarcdoc_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_tipoarcdoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_tipoarcdoc_v` (
  `tparcdoc_id` tinyint NOT NULL,
  `tparcdoc_soc` tinyint NOT NULL,
  `tparcdoc_socdes` tinyint NOT NULL,
  `tparcdoc_cod` tinyint NOT NULL,
  `tparcdoc_des` tinyint NOT NULL,
  `tparcdoc_ges_id` tinyint NOT NULL,
  `tparcdoc_gesnome` tinyint NOT NULL,
  `tparcdoc_modifica` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0001_tipodoc_v`
--

DROP TABLE IF EXISTS `filtro0001_tipodoc_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0001_tipodoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0001_tipodoc_v` (
  `td_id` tinyint NOT NULL,
  `td_des` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `filtro0002_eleord`
--

DROP TABLE IF EXISTS `filtro0002_eleord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filtro0002_eleord` (
  `eleord_id` int(2) NOT NULL,
  `eleord_cod` int(2) NOT NULL,
  `eleord_des` varchar(50) DEFAULT NULL,
  `eleord_campo` varchar(50) NOT NULL,
  `eleord_ope` varchar(100) NOT NULL,
  `operatore` varchar(50) NOT NULL DEFAULT 'SYSTEM',
  `modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`eleord_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `filtro0002_liv1_v`
--

DROP TABLE IF EXISTS `filtro0002_liv1_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0002_liv1_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0002_liv1_v` (
  `lv1_id` tinyint NOT NULL,
  `lv1_des` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0002_liv2_v`
--

DROP TABLE IF EXISTS `filtro0002_liv2_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0002_liv2_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0002_liv2_v` (
  `lv2_id` tinyint NOT NULL,
  `lv2_des` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0002_liv3_v`
--

DROP TABLE IF EXISTS `filtro0002_liv3_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0002_liv3_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0002_liv3_v` (
  `lv3_id` tinyint NOT NULL,
  `lv3_des` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `filtro0003_elementi_pagina`
--

DROP TABLE IF EXISTS `filtro0003_elementi_pagina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filtro0003_elementi_pagina` (
  `id` int(3) NOT NULL,
  `valore` int(3) DEFAULT NULL,
  `descrizione` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `filtro0003_elenco_pagine_v`
--

DROP TABLE IF EXISTS `filtro0003_elenco_pagine_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_elenco_pagine_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_elenco_pagine_v` (
  `seq` tinyint NOT NULL,
  `valore` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_get_idrecperpag_v`
--

DROP TABLE IF EXISTS `filtro0003_get_idrecperpag_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_idrecperpag_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_get_idrecperpag_v` (
  `idrecperpag` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_get_limfin_v`
--

DROP TABLE IF EXISTS `filtro0003_get_limfin_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_limfin_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_get_limfin_v` (
  `numpagesp` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_get_limini_v`
--

DROP TABLE IF EXISTS `filtro0003_get_limini_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_limini_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_get_limini_v` (
  `limini` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_get_numpagcorr_v`
--

DROP TABLE IF EXISTS `filtro0003_get_numpagcorr_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_numpagcorr_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_get_numpagcorr_v` (
  `numpagcorr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_get_numpagesp_v`
--

DROP TABLE IF EXISTS `filtro0003_get_numpagesp_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_numpagesp_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_get_numpagesp_v` (
  `numpagesp` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_get_numpagtot_v`
--

DROP TABLE IF EXISTS `filtro0003_get_numpagtot_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_numpagtot_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_get_numpagtot_v` (
  `numpagtot` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_get_numposcorr_v`
--

DROP TABLE IF EXISTS `filtro0003_get_numposcorr_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_numposcorr_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_get_numposcorr_v` (
  `numposcorr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_get_numrectot_v`
--

DROP TABLE IF EXISTS `filtro0003_get_numrectot_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_numrectot_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_get_numrectot_v` (
  `numrectot` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_get_recperpag_v`
--

DROP TABLE IF EXISTS `filtro0003_get_recperpag_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_recperpag_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_get_recperpag_v` (
  `valrecperpag` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_get_valrecperpag_v`
--

DROP TABLE IF EXISTS `filtro0003_get_valrecperpag_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_valrecperpag_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_get_valrecperpag_v` (
  `valrecperpag` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `filtro0003_pagine`
--

DROP TABLE IF EXISTS `filtro0003_pagine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filtro0003_pagine` (
  `id_pagina` int(10) NOT NULL,
  PRIMARY KEY (`id_pagina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `filtro0003_parametri_v`
--

DROP TABLE IF EXISTS `filtro0003_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `idrecperpag` tinyint NOT NULL,
  `valrecperpag` tinyint NOT NULL,
  `desrecperpag` tinyint NOT NULL,
  `numrectot` tinyint NOT NULL,
  `numpagtot` tinyint NOT NULL,
  `numpagcorr` tinyint NOT NULL,
  `numpagesp` tinyint NOT NULL,
  `limini` tinyint NOT NULL,
  `limfin` tinyint NOT NULL,
  `numposcorr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_parconn_v`
--

DROP TABLE IF EXISTS `filtro0003_parconn_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_parconn_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_parconn_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `idrecperpag` tinyint NOT NULL,
  `valrecperpag` tinyint NOT NULL,
  `desrecperpag` tinyint NOT NULL,
  `numrectot` tinyint NOT NULL,
  `numpagtot` tinyint NOT NULL,
  `numpagcorr` tinyint NOT NULL,
  `numpagesp` tinyint NOT NULL,
  `limini` tinyint NOT NULL,
  `limfin` tinyint NOT NULL,
  `numposcorr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0003_range_pagine_v`
--

DROP TABLE IF EXISTS `filtro0003_range_pagine_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0003_range_pagine_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0003_range_pagine_v` (
  `valore` tinyint NOT NULL,
  `numpagcorr` tinyint NOT NULL,
  `numposcorr` tinyint NOT NULL,
  `sinistra` tinyint NOT NULL,
  `destra` tinyint NOT NULL,
  `valsin` tinyint NOT NULL,
  `valdes` tinyint NOT NULL,
  `numpagtot` tinyint NOT NULL,
  `numpagesp` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `filtro0003_valori_default`
--

DROP TABLE IF EXISTS `filtro0003_valori_default`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filtro0003_valori_default` (
  `idrecperpag` decimal(4,0) NOT NULL,
  `numpagesp` decimal(4,0) NOT NULL,
  `numpagcorr` decimal(4,0) NOT NULL,
  `numposcorr` decimal(4,0) NOT NULL,
  `maxrecperpag` decimal(9,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `filtro0004_parametri_v`
--

DROP TABLE IF EXISTS `filtro0004_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0004_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0004_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_lastid` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0004_parconn_v`
--

DROP TABLE IF EXISTS `filtro0004_parconn_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0004_parconn_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0004_parconn_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_lastid` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0005_elearcdoc_v`
--

DROP TABLE IF EXISTS `filtro0005_elearcdoc_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0005_elearcdoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0005_elearcdoc_v` (
  `elearcdoc_id` tinyint NOT NULL,
  `elearcdoc_idfatt` tinyint NOT NULL,
  `elearcdoc_iddoc` tinyint NOT NULL,
  `elearcdoc_estid` tinyint NOT NULL,
  `elearcdoc_estcod` tinyint NOT NULL,
  `elearcdoc_estdes` tinyint NOT NULL,
  `elearcdoc_tipoid` tinyint NOT NULL,
  `elearcdoc_tipocod` tinyint NOT NULL,
  `elearcdoc_tipodes` tinyint NOT NULL,
  `elearcdoc_desc` tinyint NOT NULL,
  `elearcdoc_size` tinyint NOT NULL,
  `elearcdoc_nomefile` tinyint NOT NULL,
  `elearcdoc_ges_id` tinyint NOT NULL,
  `elearcdoc_ges_nominativo` tinyint NOT NULL,
  `elearcdoc_modifica` tinyint NOT NULL,
  `elearcdoc_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0005_fatture_arcdoc_v`
--

DROP TABLE IF EXISTS `filtro0005_fatture_arcdoc_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0005_fatture_arcdoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0005_fatture_arcdoc_v` (
  `ftarcdoc_id` tinyint NOT NULL,
  `ftarcdoc_idfatt` tinyint NOT NULL,
  `ftarcdoc_iddoc` tinyint NOT NULL,
  `ftarcdoc_estid` tinyint NOT NULL,
  `ftarcdoc_estcod` tinyint NOT NULL,
  `ftarcdoc_estdes` tinyint NOT NULL,
  `ftarcdoc_tipoid` tinyint NOT NULL,
  `ftarcdoc_tipocod` tinyint NOT NULL,
  `ftarcdoc_tipodes` tinyint NOT NULL,
  `ftarcdoc_desc` tinyint NOT NULL,
  `ftarcdoc_size` tinyint NOT NULL,
  `ftarcdoc_nomefile` tinyint NOT NULL,
  `ftarcdoc_ges_id` tinyint NOT NULL,
  `ftarcdoc_ges_nominativo` tinyint NOT NULL,
  `ftarcdoc_modifica` tinyint NOT NULL,
  `ftarcdoc_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0005_parametri_v`
--

DROP TABLE IF EXISTS `filtro0005_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0005_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0005_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_idfatt` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0005_parconn_v`
--

DROP TABLE IF EXISTS `filtro0005_parconn_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0005_parconn_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0005_parconn_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_idfatt` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0006_debann_tpann_v`
--

DROP TABLE IF EXISTS `filtro0006_debann_tpann_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0006_debann_tpann_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0006_debann_tpann_v` (
  `dantpan_id` tinyint NOT NULL,
  `dantpan_idsoc` tinyint NOT NULL,
  `dantpan_cod` tinyint NOT NULL,
  `dantpan_des` tinyint NOT NULL,
  `dantpan_gesid` tinyint NOT NULL,
  `dantpan_nominativo` tinyint NOT NULL,
  `dantpan_modifica` tinyint NOT NULL,
  `dantpan_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0006_debann_tpcon_v`
--

DROP TABLE IF EXISTS `filtro0006_debann_tpcon_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0006_debann_tpcon_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0006_debann_tpcon_v` (
  `dantpcon_id` tinyint NOT NULL,
  `dantpcon_idsoc` tinyint NOT NULL,
  `dantpcon_cod` tinyint NOT NULL,
  `dantpcon_des` tinyint NOT NULL,
  `dantpcon_gesid` tinyint NOT NULL,
  `dantpcon_nominativo` tinyint NOT NULL,
  `dantpcon_modifica` tinyint NOT NULL,
  `dantpcon_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0006_debann_v`
--

DROP TABLE IF EXISTS `filtro0006_debann_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0006_debann_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0006_debann_v` (
  `debann_id` tinyint NOT NULL,
  `deann_debid` tinyint NOT NULL,
  `debann_idsoc` tinyint NOT NULL,
  `debann_debcod` tinyint NOT NULL,
  `debann_debdes` tinyint NOT NULL,
  `debann_idtpann` tinyint NOT NULL,
  `debann_anncod` tinyint NOT NULL,
  `debann_anndes` tinyint NOT NULL,
  `debann_idtpcon` tinyint NOT NULL,
  `debann_tpcod` tinyint NOT NULL,
  `debann_tpdes` tinyint NOT NULL,
  `debann_iddata` tinyint NOT NULL,
  `debann_datanum` tinyint NOT NULL,
  `debann_databar` tinyint NOT NULL,
  `debann_annot` tinyint NOT NULL,
  `debann_inter` tinyint NOT NULL,
  `debann_ges_id` tinyint NOT NULL,
  `debanno_nominativo` tinyint NOT NULL,
  `debann_modifica` tinyint NOT NULL,
  `debann_modbar` tinyint NOT NULL,
  `debann_numdoc` tinyint NOT NULL,
  `debann_doc_checkdocsino` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0006_parametri_v`
--

DROP TABLE IF EXISTS `filtro0006_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0006_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0006_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nominativo` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_debid` tinyint NOT NULL,
  `par_lastid` tinyint NOT NULL,
  `ope_nominativo` tinyint NOT NULL,
  `modibar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0006_parconn_v`
--

DROP TABLE IF EXISTS `filtro0006_parconn_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0006_parconn_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0006_parconn_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nominativo` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_debid` tinyint NOT NULL,
  `par_lastid` tinyint NOT NULL,
  `ope_nominativo` tinyint NOT NULL,
  `modibar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0007_parametri_v`
--

DROP TABLE IF EXISTS `filtro0007_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0007_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0007_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nominativo` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_debid` tinyint NOT NULL,
  `ope_nominativo` tinyint NOT NULL,
  `modibar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0007_parconn_v`
--

DROP TABLE IF EXISTS `filtro0007_parconn_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0007_parconn_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0007_parconn_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nominativo` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_debid` tinyint NOT NULL,
  `ope_nominativo` tinyint NOT NULL,
  `modibar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0008_debann_arcdoc_v`
--

DROP TABLE IF EXISTS `filtro0008_debann_arcdoc_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0008_debann_arcdoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0008_debann_arcdoc_v` (
  `debandoc_id` tinyint NOT NULL,
  `debandoc_idann` tinyint NOT NULL,
  `debandoc_iddoc` tinyint NOT NULL,
  `debandoc_estid` tinyint NOT NULL,
  `debandoc_estcod` tinyint NOT NULL,
  `debandoc_estdes` tinyint NOT NULL,
  `debandoc_tipoid` tinyint NOT NULL,
  `debandoc_tipocod` tinyint NOT NULL,
  `debandoc_tipodes` tinyint NOT NULL,
  `debandoc_desc` tinyint NOT NULL,
  `debandoc_size` tinyint NOT NULL,
  `debandoc_nomefile` tinyint NOT NULL,
  `debandoc_ges_id` tinyint NOT NULL,
  `debandoc_ges_nominativo` tinyint NOT NULL,
  `debandoc_modifica` tinyint NOT NULL,
  `debandoc_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0008_elearcdoc_v`
--

DROP TABLE IF EXISTS `filtro0008_elearcdoc_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0008_elearcdoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0008_elearcdoc_v` (
  `elearcdoc_id` tinyint NOT NULL,
  `elearcdoc_idann` tinyint NOT NULL,
  `elearcdoc_iddoc` tinyint NOT NULL,
  `elearcdoc_estid` tinyint NOT NULL,
  `elearcdoc_estcod` tinyint NOT NULL,
  `elearcdoc_estdes` tinyint NOT NULL,
  `elearcdoc_tipoid` tinyint NOT NULL,
  `elearcdoc_tipocod` tinyint NOT NULL,
  `elearcdoc_tipodes` tinyint NOT NULL,
  `elearcdoc_desc` tinyint NOT NULL,
  `elearcdoc_size` tinyint NOT NULL,
  `elearcdoc_nomefile` tinyint NOT NULL,
  `elearcdoc_ges_id` tinyint NOT NULL,
  `elearcdoc_ges_nominativo` tinyint NOT NULL,
  `elearcdoc_modifica` tinyint NOT NULL,
  `elearcdoc_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0008_parametri_v`
--

DROP TABLE IF EXISTS `filtro0008_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0008_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0008_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nominativo` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_debannid` tinyint NOT NULL,
  `ope_nominativo` tinyint NOT NULL,
  `modibar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0008_parconn_v`
--

DROP TABLE IF EXISTS `filtro0008_parconn_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0008_parconn_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0008_parconn_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nominativo` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_debannid` tinyint NOT NULL,
  `ope_nominativo` tinyint NOT NULL,
  `modibar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0009_debitori_v`
--

DROP TABLE IF EXISTS `filtro0009_debitori_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0009_debitori_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0009_debitori_v` (
  `deb_id` tinyint NOT NULL,
  `deb_cod` tinyint NOT NULL,
  `deb_des` tinyint NOT NULL,
  `deb_citta` tinyint NOT NULL,
  `deb_pro` tinyint NOT NULL,
  `deb_tel` tinyint NOT NULL,
  `deb_mail` tinyint NOT NULL,
  `deb_numann` tinyint NOT NULL,
  `deb_ann_checksino` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0009_parametri_v`
--

DROP TABLE IF EXISTS `filtro0009_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0009_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0009_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nominativo` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_debid` tinyint NOT NULL,
  `ope_nominativo` tinyint NOT NULL,
  `modibar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0009_parconn_v`
--

DROP TABLE IF EXISTS `filtro0009_parconn_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0009_parconn_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0009_parconn_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nominativo` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_debid` tinyint NOT NULL,
  `ope_nominativo` tinyint NOT NULL,
  `modibar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0010_parametri_v`
--

DROP TABLE IF EXISTS `filtro0010_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0010_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0010_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_lastid` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0010_parconn_v`
--

DROP TABLE IF EXISTS `filtro0010_parconn_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0010_parconn_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0010_parconn_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_lastid` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0011_eletabsr_v`
--

DROP TABLE IF EXISTS `filtro0011_eletabsr_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0011_eletabsr_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0011_eletabsr_v` (
  `fttbsr_numero` tinyint NOT NULL,
  `fttbsr_nota` tinyint NOT NULL,
  `fttbsr_ges_nominativo` tinyint NOT NULL,
  `fttbsr_modifica` tinyint NOT NULL,
  `fttbsr_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0011_parametri_v`
--

DROP TABLE IF EXISTS `filtro0011_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0011_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0011_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_idfatt` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0011_parconn_v`
--

DROP TABLE IF EXISTS `filtro0011_parconn_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0011_parconn_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0011_parconn_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_idfatt` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0012_orig_arcdoc_griglia_v`
--

DROP TABLE IF EXISTS `filtro0012_orig_arcdoc_griglia_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0012_orig_arcdoc_griglia_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0012_orig_arcdoc_griglia_v` (
  `orig_arcdoc_id` tinyint NOT NULL,
  `orig_arcdoc_des` tinyint NOT NULL,
  `orig_arcdoc_idori` tinyint NOT NULL,
  `orig_arcdoc_debid` tinyint NOT NULL,
  `orig_arcdoc_debcod` tinyint NOT NULL,
  `orig_arcdoc_debdes` tinyint NOT NULL,
  `orig_arcdoc_ele` tinyint NOT NULL,
  `orig_arcdoc_eledata` tinyint NOT NULL,
  `orig_arcdoc_eledatabar` tinyint NOT NULL,
  `orig_arcdoc_iddoc` tinyint NOT NULL,
  `orig_arcdoc_gesid` tinyint NOT NULL,
  `orig_arcdoc_ges_nominativo` tinyint NOT NULL,
  `orig_arcdoc_modifica` tinyint NOT NULL,
  `orig_arcdoc_modbar` tinyint NOT NULL,
  `orig_estid` tinyint NOT NULL,
  `orig_estcod` tinyint NOT NULL,
  `orig_estdes` tinyint NOT NULL,
  `orig_tipoid` tinyint NOT NULL,
  `orig_tipocod` tinyint NOT NULL,
  `orig_tipodes` tinyint NOT NULL,
  `orig_size` tinyint NOT NULL,
  `orig_nomefile` tinyint NOT NULL,
  `orig_desc` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0012_orig_arcdoc_v`
--

DROP TABLE IF EXISTS `filtro0012_orig_arcdoc_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0012_orig_arcdoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0012_orig_arcdoc_v` (
  `orig_arcdoc_id` tinyint NOT NULL,
  `orig_arcdoc_des` tinyint NOT NULL,
  `orig_arcdoc_idori` tinyint NOT NULL,
  `orig_arcdoc_debid` tinyint NOT NULL,
  `orig_arcdoc_ele` tinyint NOT NULL,
  `orig_arcdoc_eledata` tinyint NOT NULL,
  `orig_arcdoc_iddoc` tinyint NOT NULL,
  `orig_arcdoc_gesid` tinyint NOT NULL,
  `orig_arcdoc_ges_nominativo` tinyint NOT NULL,
  `orig_arcdoc_modifica` tinyint NOT NULL,
  `orig_arcdoc_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0012_parametri_v`
--

DROP TABLE IF EXISTS `filtro0012_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0012_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0012_parametri_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_id_arcdoc` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro0012_parconn_v`
--

DROP TABLE IF EXISTS `filtro0012_parconn_v`;
/*!50001 DROP VIEW IF EXISTS `filtro0012_parconn_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro0012_parconn_v` (
  `ges_id` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `id_filtro` tinyint NOT NULL,
  `des_filtro` tinyint NOT NULL,
  `par_conn` tinyint NOT NULL,
  `par_id_arcdoc` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `filtro003_elenco_pagine_v`
--

DROP TABLE IF EXISTS `filtro003_elenco_pagine_v`;
/*!50001 DROP VIEW IF EXISTS `filtro003_elenco_pagine_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `filtro003_elenco_pagine_v` (
  `pag` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fos_user` (
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fosuser_gestori`
--

DROP TABLE IF EXISTS `fosuser_gestori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fosuser_gestori` (
  `fosuser_username` varchar(50) NOT NULL,
  `fosuser_gestid` int(10) NOT NULL,
  PRIMARY KEY (`fosuser_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vs28ahbz_gecotestlog`
--

DROP TABLE IF EXISTS `vs28ahbz_gecotestlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vs28ahbz_gecotestlog` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `orario` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descrizione` varchar(100) DEFAULT NULL,
  `ges_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1402 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gest_debitore`
--

DROP TABLE IF EXISTS `gest_debitore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gest_debitore` (
  `gd_ges_id` int(10) NOT NULL,
  `gd_deb_id` int(10) NOT NULL,
  `gd_data_associazione` date DEFAULT NULL,
  `gd_data_dissociazione` date DEFAULT NULL,
  PRIMARY KEY (`gd_ges_id`,`gd_deb_id`),
  KEY `FK_GESTDEBITORE_DEBITORI` (`gd_deb_id`),
  CONSTRAINT `FK_GESTDEBITORE_DEBITORI` FOREIGN KEY (`gd_deb_id`) REFERENCES `debitori` (`deb_id`),
  CONSTRAINT `FK_GESTEDBITORI_GESTORI` FOREIGN KEY (`gd_ges_id`) REFERENCES `gestori` (`ges_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `gest_debitore_v`
--

DROP TABLE IF EXISTS `gest_debitore_v`;
/*!50001 DROP VIEW IF EXISTS `gest_debitore_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `gest_debitore_v` (
  `ges_id` tinyint NOT NULL,
  `ges_cognome` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `ges_ruolo` tinyint NOT NULL,
  `deb_id` tinyint NOT NULL,
  `deb_soc` tinyint NOT NULL,
  `beb_cod` tinyint NOT NULL,
  `des_des` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `gestdebitore_fastweb_csv`
--

DROP TABLE IF EXISTS `gestdebitore_fastweb_csv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gestdebitore_fastweb_csv` (
  `gd_id` int(11) NOT NULL AUTO_INCREMENT,
  `gd_ges_cod` decimal(10,0) NOT NULL,
  `gd_ges_id` int(11) NOT NULL,
  `gd_deb_cod` decimal(10,0) NOT NULL,
  `gd_deb_id` int(11) NOT NULL,
  `gd_data_associazione` varchar(10) NOT NULL,
  `gd_id_dataassoc` int(11) NOT NULL,
  `gd_data_dissociazione` varchar(10) DEFAULT NULL,
  `gd_id_datadissoc` int(11) NOT NULL,
  `gd_operatore` int(11) NOT NULL DEFAULT '0',
  `gd_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`gd_id`),
  KEY `fk_gestdebitorefastwebcsv_gestori1` (`gd_ges_id`),
  KEY `fk_gestdebitorefastwebcsv_debitori` (`gd_deb_id`),
  KEY `fk_gestdebitorefastwebcsv_gestori2` (`gd_operatore`),
  KEY `fk_gestdebitorefastwebcsv_calendario1` (`gd_id_dataassoc`),
  KEY `fk_gestdebitorefastwebcsv_calendario2` (`gd_id_datadissoc`)
) ENGINE=InnoDB AUTO_INCREMENT=1831 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `gestione_scadenziario_v`
--

DROP TABLE IF EXISTS `gestione_scadenziario_v`;
/*!50001 DROP VIEW IF EXISTS `gestione_scadenziario_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `gestione_scadenziario_v` (
  `id` tinyint NOT NULL,
  `ft_soc` tinyint NOT NULL,
  `ft_deb_id` tinyint NOT NULL,
  `ft_ac` tinyint NOT NULL,
  `ft_id_debitore` tinyint NOT NULL,
  `ft_deb_cod` tinyint NOT NULL,
  `ft_deb_des` tinyint NOT NULL,
  `ft_deb_citta` tinyint NOT NULL,
  `ft_ced_id` tinyint NOT NULL,
  `ft_attribuzione` tinyint NOT NULL,
  `ft_num_fat` tinyint NOT NULL,
  `ft_td_id` tinyint NOT NULL,
  `ft_td` tinyint NOT NULL,
  `ft_id_stato_lav` tinyint NOT NULL,
  `ft_des_stato_lav` tinyint NOT NULL,
  `ft_sl_coddes` tinyint NOT NULL,
  `ft_testo` tinyint NOT NULL,
  `ft_data_fat` tinyint NOT NULL,
  `ft_data_scad` tinyint NOT NULL,
  `ft_imp_fat` tinyint NOT NULL,
  `ft_imp_aperto` tinyint NOT NULL,
  `ft_data_ins` tinyint NOT NULL,
  `ft_id_data_fat` tinyint NOT NULL,
  `ft_id_data_chius` tinyint NOT NULL,
  `ft_nota_crgest` tinyint NOT NULL,
  `ft_data_nota` tinyint NOT NULL,
  `ft_arcdoc_sino` tinyint NOT NULL,
  `ft_tabsr_sino` tinyint NOT NULL,
  `ft_forecast_mese` tinyint NOT NULL,
  `ft_forecast_anno` tinyint NOT NULL,
  `ft_forecast_scad` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `gestori`
--

DROP TABLE IF EXISTS `gestori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gestori` (
  `ges_id` int(10) NOT NULL,
  `ges_cognome` varchar(50) DEFAULT NULL,
  `ges_nome` varchar(50) DEFAULT NULL,
  `ges_mail` varchar(50) DEFAULT NULL,
  `ges_tel` varchar(50) DEFAULT NULL,
  `ges_ind` varchar(50) DEFAULT NULL,
  `ges_citta` varchar(30) DEFAULT NULL,
  `ges_regione` varchar(50) DEFAULT NULL,
  `ges_pwd` varchar(10) NOT NULL,
  `ges_ruolo` varchar(10) NOT NULL,
  PRIMARY KEY (`ges_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parametri_procedura`
--

DROP TABLE IF EXISTS `parametri_procedura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametri_procedura` (
  `sistema_operativo` char(50) DEFAULT NULL,
  `directory_fatture` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pippo`
--

DROP TABLE IF EXISTS `pippo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pippo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `pippo_v`
--

DROP TABLE IF EXISTS `pippo_v`;
/*!50001 DROP VIEW IF EXISTS `pippo_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `pippo_v` (
  `pippo` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `sess_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `sess_data` blob NOT NULL,
  `sess_time` int(10) unsigned NOT NULL,
  `sess_lifetime` mediumint(9) NOT NULL,
  PRIMARY KEY (`sess_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `societa`
--

DROP TABLE IF EXISTS `societa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `societa` (
  `soc_cod` int(10) NOT NULL,
  `soc_des` varchar(50) DEFAULT NULL,
  `soc_data_ini` date DEFAULT NULL,
  `soc_indirizzo` varchar(50) DEFAULT NULL,
  `soc_citta` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`soc_cod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statist_movimenti`
--

DROP TABLE IF EXISTS `statist_movimenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statist_movimenti` (
  `stmov_id` int(11) NOT NULL AUTO_INCREMENT,
  `stmov_gesid` int(10) NOT NULL,
  `stmov_opeid` int(10) NOT NULL,
  `stmod_datetime` datetime DEFAULT NULL,
  `operatore` int(11) NOT NULL DEFAULT '0',
  `modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`stmov_id`),
  KEY `fk_stmovimenti_fosuser` (`operatore`),
  KEY `fk_stmovimenti_gestori` (`stmov_gesid`),
  KEY `fk_stmovimenti_stoperazioni` (`stmov_opeid`),
  CONSTRAINT `fk_stmovimenti_fosuser` FOREIGN KEY (`operatore`) REFERENCES `fos_user` (`id`),
  CONSTRAINT `fk_stmovimenti_gestori` FOREIGN KEY (`stmov_gesid`) REFERENCES `gestori` (`ges_id`),
  CONSTRAINT `fk_stmovimenti_stoperazioni` FOREIGN KEY (`stmov_opeid`) REFERENCES `statist_operazioni` (`statope_id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `statist_movimenti_v`
--

DROP TABLE IF EXISTS `statist_movimenti_v`;
/*!50001 DROP VIEW IF EXISTS `statist_movimenti_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `statist_movimenti_v` (
  `stmov_gesid` tinyint NOT NULL,
  `stmov_username` tinyint NOT NULL,
  `cognome` tinyint NOT NULL,
  `ges_nome` tinyint NOT NULL,
  `stmov_opeid` tinyint NOT NULL,
  `stmov_des` tinyint NOT NULL,
  `stmod_datetime` tinyint NOT NULL,
  `operatore` tinyint NOT NULL,
  `modifica` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `statist_operazioni`
--

DROP TABLE IF EXISTS `statist_operazioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statist_operazioni` (
  `statope_id` int(10) NOT NULL,
  `statope_desc` varchar(50) NOT NULL,
  `operatore` int(11) NOT NULL DEFAULT '0',
  `modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`statope_id`),
  KEY `fk_stoperazioni_fosuser` (`operatore`),
  CONSTRAINT `fk_stoperazioni_fosuser` FOREIGN KEY (`operatore`) REFERENCES `fos_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stato_ft`
--

DROP TABLE IF EXISTS `stato_ft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stato_ft` (
  `st_id` int(10) NOT NULL,
  `st_ft_id` int(10) NOT NULL,
  `st_data_nota` date NOT NULL,
  `st_stato_lavorazione` int(10) NOT NULL,
  `st_note` varchar(1000) DEFAULT NULL,
  `st_operatore` int(10) DEFAULT NULL,
  PRIMARY KEY (`st_id`),
  UNIQUE KEY `stato_ft_1` (`st_ft_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stato_lavorazione`
--

DROP TABLE IF EXISTS `stato_lavorazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stato_lavorazione` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sl_soc` int(10) NOT NULL,
  `sl_cod` varchar(4) NOT NULL,
  `sl_descrizione` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stato_lavorazione_1` (`sl_soc`,`sl_cod`),
  CONSTRAINT `FK_STATOLAVORAZIONE_SOCIETA` FOREIGN KEY (`sl_soc`) REFERENCES `societa` (`soc_cod`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tab_arcdoc`
--

DROP TABLE IF EXISTS `tab_arcdoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_arcdoc` (
  `tbarcdoc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tbarcdoc_soc` int(11) NOT NULL,
  `tbarcdoc_est` int(11) NOT NULL,
  `tbarcdoc_tipo` int(11) NOT NULL,
  `tbarcdoc_size` decimal(10,0) NOT NULL,
  `tbarcdoc_nomefile` varchar(50) DEFAULT NULL,
  `tbarcdoc_desc` varchar(50) DEFAULT NULL,
  `tbarcdoc_blob` blob,
  `tbarcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `tbarcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tbarcdoc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `tab_arcdoc_v`
--

DROP TABLE IF EXISTS `tab_arcdoc_v`;
/*!50001 DROP VIEW IF EXISTS `tab_arcdoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `tab_arcdoc_v` (
  `tbarcdoc_id` tinyint NOT NULL,
  `tbarcdoc_soc` tinyint NOT NULL,
  `tbarcdoc_estid` tinyint NOT NULL,
  `tbarcdoc_estcod` tinyint NOT NULL,
  `tbarcdoc_estdes` tinyint NOT NULL,
  `tbarcdoc_tipoid` tinyint NOT NULL,
  `tbarcdoc_tipocod` tinyint NOT NULL,
  `tbarcdoc_tipodes` tinyint NOT NULL,
  `tbarcdoc_size` tinyint NOT NULL,
  `tbarcdoc_nomefile` tinyint NOT NULL,
  `tbarcdoc_desc` tinyint NOT NULL,
  `tbarcdoc_blob` tinyint NOT NULL,
  `tbarcdoc_ges_id` tinyint NOT NULL,
  `tbarcdoc_ges_nominativo` tinyint NOT NULL,
  `tbarcdoc_modifica` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tab_procedure`
--

DROP TABLE IF EXISTS `tab_procedure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_procedure` (
  `id_proc` int(10) NOT NULL AUTO_INCREMENT,
  `descrizione` char(50) DEFAULT NULL,
  `modifica` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_proc`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tab_procedure_host`
--

DROP TABLE IF EXISTS `tab_procedure_host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_procedure_host` (
  `id_host` int(10) NOT NULL,
  `descrizione` char(50) DEFAULT NULL,
  `modifica` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_host`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tab_procedure_parametri`
--

DROP TABLE IF EXISTS `tab_procedure_parametri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_procedure_parametri` (
  `id_proc` int(10) NOT NULL,
  `id_host` int(2) NOT NULL,
  `parint1` int(10) DEFAULT NULL,
  `parint2` int(10) DEFAULT NULL,
  `parint3` int(10) DEFAULT NULL,
  `parint4` int(10) DEFAULT NULL,
  `parint5` int(10) DEFAULT NULL,
  `parcar1` char(100) DEFAULT NULL,
  `parcar2` char(100) DEFAULT NULL,
  `parcar3` char(100) DEFAULT NULL,
  `parcar4` char(100) DEFAULT NULL,
  `parcar5` char(100) DEFAULT NULL,
  `modifica` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_proc`),
  KEY `fk_tabprochost_tabhost` (`id_host`),
  CONSTRAINT `fk_tabprochost_tabhost` FOREIGN KEY (`id_host`) REFERENCES `tab_procedure_host` (`id_host`),
  CONSTRAINT `fk_tabprocpar_tabprocedure` FOREIGN KEY (`id_proc`) REFERENCES `tab_procedure` (`id_proc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `tab_procedure_parametri_v`
--

DROP TABLE IF EXISTS `tab_procedure_parametri_v`;
/*!50001 DROP VIEW IF EXISTS `tab_procedure_parametri_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `tab_procedure_parametri_v` (
  `id_proc` tinyint NOT NULL,
  `des_proc` tinyint NOT NULL,
  `id_host` tinyint NOT NULL,
  `des_host` tinyint NOT NULL,
  `directory_file_load` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tab_sr`
--

DROP TABLE IF EXISTS `tab_sr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_sr` (
  `tbsr_id` int(11) NOT NULL AUTO_INCREMENT,
  `tbsr_numero` varchar(10) NOT NULL,
  `tbsr_nota` varchar(2000) DEFAULT NULL,
  `tbsr_stato` int(11) NOT NULL,
  `tbsr_ges_id` int(11) NOT NULL DEFAULT '0',
  `tbsr_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tbsr_id`),
  UNIQUE KEY `tab_sr_idx` (`tbsr_numero`),
  KEY `fk_tabsr_gestori` (`tbsr_ges_id`),
  KEY `fk_tabsr_tabsrstati` (`tbsr_stato`),
  CONSTRAINT `fk_tabsr_gestori` FOREIGN KEY (`tbsr_ges_id`) REFERENCES `gestori` (`ges_id`),
  CONSTRAINT `fk_tabsr_tabsrstati` FOREIGN KEY (`tbsr_stato`) REFERENCES `tabsr_stati` (`tbsrst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tabsr_stati`
--

DROP TABLE IF EXISTS `tabsr_stati`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabsr_stati` (
  `tbsrst_id` int(11) NOT NULL AUTO_INCREMENT,
  `tbsrst_soc` int(11) NOT NULL,
  `tbsrst_cod` varchar(1) NOT NULL,
  `tbsrst_desc` varchar(50) NOT NULL,
  `tbsrst_ges_id` int(11) NOT NULL DEFAULT '0',
  `tbsrst_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tbsrst_id`),
  UNIQUE KEY `tabsrstati_idx` (`tbsrst_soc`,`tbsrst_cod`),
  KEY `fk_tabsrstati_gestori` (`tbsrst_ges_id`),
  CONSTRAINT `fk_tabsrstati_gestori` FOREIGN KEY (`tbsrst_ges_id`) REFERENCES `gestori` (`ges_id`),
  CONSTRAINT `fk_tabsrstati_societa` FOREIGN KEY (`tbsrst_soc`) REFERENCES `societa` (`soc_cod`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `tabsr_stati_v`
--

DROP TABLE IF EXISTS `tabsr_stati_v`;
/*!50001 DROP VIEW IF EXISTS `tabsr_stati_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `tabsr_stati_v` (
  `tbsrst_id` tinyint NOT NULL,
  `tbsrst_soccod` tinyint NOT NULL,
  `tbsrst_socdes` tinyint NOT NULL,
  `tbsrst_cod` tinyint NOT NULL,
  `tbsrst_desc` tinyint NOT NULL,
  `tbsrst_ges_id` tinyint NOT NULL,
  `tbsrst_ges_nominativo` tinyint NOT NULL,
  `tbsrst_modifica` tinyint NOT NULL,
  `tbsrst_modbar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `tabsr_v`
--

DROP TABLE IF EXISTS `tabsr_v`;
/*!50001 DROP VIEW IF EXISTS `tabsr_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `tabsr_v` (
  `tbsr_id` tinyint NOT NULL,
  `tbsr_numero` tinyint NOT NULL,
  `tbsr_nota` tinyint NOT NULL,
  `tbsr_idstato` tinyint NOT NULL,
  `tbsr_codstato` tinyint NOT NULL,
  `tbsr_desstato` tinyint NOT NULL,
  `tbsr_ges_id` tinyint NOT NULL,
  `tbsr_modifica` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `temp_fatture_id`
--

DROP TABLE IF EXISTS `temp_fatture_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_fatture_id` (
  `tmpft_id` int(11) NOT NULL,
  PRIMARY KEY (`tmpft_id`),
  CONSTRAINT `fk_tempfattureid_fatture` FOREIGN KEY (`tmpft_id`) REFERENCES `fatture` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `campo` varchar(300) DEFAULT NULL,
  KEY `test_idx` (`campo`(10))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipi_arcdoc`
--

DROP TABLE IF EXISTS `tipi_arcdoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipi_arcdoc` (
  `tparcdoc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tparcdoc_soc` int(11) NOT NULL,
  `tparcdoc_cod` decimal(10,0) NOT NULL,
  `tparcdoc_des` varchar(50) DEFAULT NULL,
  `tparcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `tparcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tparcdoc_id`),
  UNIQUE KEY `tipi_arcdoc_1` (`tparcdoc_soc`,`tparcdoc_cod`),
  KEY `tipi_arcdoc_2` (`tparcdoc_soc`),
  KEY `fk_tipiarcdoc_gestori` (`tparcdoc_ges_id`),
  CONSTRAINT `fk_tipiarcdoc_gestori` FOREIGN KEY (`tparcdoc_ges_id`) REFERENCES `gestori` (`ges_id`),
  CONSTRAINT `fk_tipiarcdoc_societa` FOREIGN KEY (`tparcdoc_soc`) REFERENCES `societa` (`soc_cod`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `tipo_arcdoc_v`
--

DROP TABLE IF EXISTS `tipo_arcdoc_v`;
/*!50001 DROP VIEW IF EXISTS `tipo_arcdoc_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `tipo_arcdoc_v` (
  `tparcdoc_id` tinyint NOT NULL,
  `tparcdoc_soc` tinyint NOT NULL,
  `tparcdoc_socdes` tinyint NOT NULL,
  `tparcdoc_cod` tinyint NOT NULL,
  `tparcdoc_des` tinyint NOT NULL,
  `tparcdoc_ges_id` tinyint NOT NULL,
  `tparcdoc_gesnome` tinyint NOT NULL,
  `tparcdoc_modifica` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `tipo_doc`
--

DROP TABLE IF EXISTS `tipo_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_doc` (
  `td_id` int(10) NOT NULL AUTO_INCREMENT,
  `td_soc` int(10) NOT NULL,
  `td_cod` varchar(5) NOT NULL,
  `td_des` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`td_id`),
  KEY `FK_TIPODOC_SOCIETA` (`td_soc`),
  CONSTRAINT `FK_TIPODOC_SOCIETA` FOREIGN KEY (`td_soc`) REFERENCES `societa` (`soc_cod`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'vs28ahbz_gecotest'
--
/*!50003 DROP FUNCTION IF EXISTS `FILTRO0000_GET_GESID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `filtro0000_get_gesid`() RETURNS int(10)
begin
declare pgesid int(10);
select ges_id
  into pgesid
  from filtro0000_parametri_v;
return pgesid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `FILTRO0001_GET_ARCDOC_SINO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FILTRO0001_GET_ARCDOC_SINO`() RETURNS int(11)
begin
  declare pfatt_arcdoc_sino int(10);
   select parint11
     into pfatt_arcdoc_sino
     from filtri_parametri
    where par_conn = connection_id()
      and id_filtro = 1;
  return pfatt_arcdoc_sino;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `FILTRO0001_GET_SOCIETA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `filtro0001_get_societa`() RETURNS int(11)
begin
  declare psoc_cod int(10);
   select parint01 
     into psoc_cod
     from filtri_parametri
   where par_conn = connection_id()
     and id_filtro = 1;
  return psoc_cod;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `FILTRO0001_GET_TIPODOC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FILTRO0001_GET_TIPODOC`() RETURNS int(11)
begin
  declare ptd_id int(10);
   select parint02 
     into ptd_id
     from filtri_parametri
    where par_conn = connection_id()
      and id_filtro = 1;
  return ptd_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `filtro0004_find_idest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `filtro0004_find_idest`(pparest varchar(5)) RETURNS int(11)
begin
declare ppar_estid int;
select estarcdoc_id
  into ppar_estid
  from estensioni_arcdoc
 where lower(estarcdoc_cod) = lower(ifnull(pparest,'err'));
return ppar_estid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `filtro0004_GET_LASTID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `filtro0004_GET_LASTID`() RETURNS int(11)
begin
declare ppar_lastid int;
select par_lastid
  into ppar_lastid
  from filtro0004_parconn_v;
return ppar_lastid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `FILTRO0005_GET_IDFATTURA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FILTRO0005_GET_IDFATTURA`() RETURNS int(11)
begin
declare pid_fatt int;
select par_idfatt
  into pid_fatt
  from filtro0005_parconn_v;
return pid_fatt;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `FILTRO0006_GET_DEBID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FILTRO0006_GET_DEBID`() RETURNS int(11)
begin
  declare pdebid int(10);
   select parint01
     into pdebid
     from filtri_parametri
    where par_conn = connection_id()
      and id_filtro = 6;
  return pdebid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `filtro0006_GET_LASTID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `filtro0006_GET_LASTID`() RETURNS int(11)
begin
declare ppar_lastid int;
select par_lastid
  into ppar_lastid
  from filtro0006_parconn_v;
return ppar_lastid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `FILTRO0007_GET_DEBID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FILTRO0007_GET_DEBID`() RETURNS int(11)
begin
  declare pdebid int(10);
   select parint01
     into pdebid
     from filtri_parametri
    where par_conn = connection_id()
      and id_filtro = 7;
  return pdebid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `FILTRO0008_GET_DEBANNID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FILTRO0008_GET_DEBANNID`() RETURNS int(11)
begin
declare pdebannid int;
select par_debannid
  into pdebannid
  from filtro0008_parconn_v;
return pdebannid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `FILTRO0009_GET_DEBID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FILTRO0009_GET_DEBID`() RETURNS int(11)
begin
  declare pdebid int(10);
   select parint01
     into pdebid
     from filtri_parametri
    where par_conn = connection_id()
      and id_filtro = 9;
  return pdebid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `filtro0010_GET_LASTID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `filtro0010_GET_LASTID`() RETURNS int(11)
begin
declare ppar_lastid int;
select par_lastid
  into ppar_lastid
  from filtro0010_parconn_v;
return ppar_lastid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `FILTRO0011_GET_IDFATTURA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FILTRO0011_GET_IDFATTURA`() RETURNS int(11)
begin
declare pid_fatt int;
select par_idfatt
  into pid_fatt
  from filtro0011_parconn_v;
return pid_fatt;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `getnewdebid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `getnewdebid`(pdeb_soc int) RETURNS int(11)
begin
declare pnewdebid int(10);
select ifnull(max(deb_id),pdeb_soc*1000000000)+1
  into pnewdebid
  from debitori
 where deb_soc = pdeb_soc;
return pnewdebid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `UTYPKG_DATADATE2DATABAR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `utypkg_datadate2databar`(pdate datetime) RETURNS char(19) CHARSET latin1
begin
declare pdata    numeric(14);
declare pdatabar varchar(19);
select date_format(pdate,'%Y%m%d%H%i%s') 
  into pdata;
 select concat(substr(pdata,7,2),'/',substr(pdata,5,2),'/',substr(pdata,1,4),'-',substr(pdata,9,2),':',substr(pdata,11,2),':',substr(pdata,13,2)) as aaa
   into pdatabar;
return pdatabar;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `UTYPKG_DATANUM2DATABAR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `utypkg_datanum2databar`(pdata numeric(14)) RETURNS char(19) CHARSET latin1
begin
declare pdatabar varchar(19);
 select concat(substr(pdata,7,2),'/',substr(pdata,5,2),'/',substr(pdata,1,4),'-',substr(pdata,9,2),':',substr(pdata,11,2),':',substr(pdata,13,2)) as aaa
   into pdatabar;
return pdatabar;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `UTYPKG_DATANUM2DATAID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `UTYPKG_DATANUM2DATAID`(pdata char(8)) RETURNS int(11)
begin
declare pid integer;
 select id 
   into pid
   from calendario
  where datachar = pdata;
return pid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `UTYPKG_GESTID2GESTNOMIN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `utypkg_gestid2gestnomin`(pges_id integer) RETURNS char(100) CHARSET latin1
begin
declare pnominativo varchar(100);
 select concat(rtrim(ifnull(ges_cognome,' ')),
              ' ',
              rtrim(ifnull(ges_nome,' ')))
   into pnominativo
   from gestori where ges_id = pges_id;
return pnominativo;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `AGG_DEBITORI_FASTWEB` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `AGG_DEBITORI_FASTWEB`()
begin
  declare psocieta     int(10);
  declare pdebitore    varchar(50);
  declare pdescrizione varchar(100);
  declare pcitta       varchar(100);
  declare pprovincia   varchar(2);
  declare exit_loop BOOLEAN; 
  declare leggi_temp_fastweb cursor for 
  select distinct 1           as societa,
                  conto       as debitore
    from temp_fastweb
    where conto   is not null
      and conto != 0
      and length(conto) > 0
      and (1,conto) not in (select deb_soc,deb_cod 
                            from debitori);
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET exit_loop = TRUE;
 OPEN  leggi_temp_fastweb;
 read_loop: LOOP
  FETCH leggi_temp_fastweb INTO psocieta,pdebitore;
  IF exit_loop THEN
         CLOSE leggi_temp_fastweb;
         LEAVE read_loop;
  END IF;
  insert 
    into debitori (deb_id,         deb_soc,  deb_cod) 
           values (getnewdebid(1), psocieta, pdebitore);
  END LOOP read_loop;
  update debitori as a
   set a.deb_des = (select min(b.descrizione) 
                      from temp_fastweb as b
                     where b.conto = a.deb_cod
                       and b.descrizione is not null)
 where a.deb_soc = 1;
 update debitori as a
   set a.deb_citta = (select min(b.citta) 
                        from temp_fastweb as b
                       where b.conto = a.deb_cod
                         and b.citta is not null)
 where a.deb_soc = 1;
 update debitori as a
    set a.deb_pro = (select min(b.provincia) 
                       from temp_fastweb as b
                      where b.conto = a.deb_cod
                        and b.provincia is not null)
 where a.deb_soc = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `agg_gest_debitore` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `agg_gest_debitore`()
begin
  delete from gest_debitore
   where gd_ges_id in (select b.ges_id from gestori as b where b.ges_ruolo = 'admin');
  insert into gest_debitore 
  select b.ges_id,
         a.deb_id,
         null,
         null
    from debitori as a
    join gestori  as b
   where b.ges_ruolo = 'admin'
     and b.ges_id   != 0;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `crea_calendario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `crea_calendario`()
begin
declare pgg         int(10);
declare pnumgg      int(10);
set pgg = 0;
set pnumgg = 36500;
truncate table calendario;
 label1: LOOP
    set pgg = pgg+1;
    insert 
      into calendario (data,gg,mese,anno,databar,datachar,datanum) 
    select date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%m'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%Y'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d/%m/%Y'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d%m%Y'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d%m%Y');
    if pgg > pnumgg
       then LEAVE label1;
    end if;
  END LOOP label1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fatturefastwebcsv_2_fatture` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fatturefastwebcsv_2_fatture`(in pnomefile_csv varchar(100))
begin
-- ********   RIAPERTURA DELLE FATTURE INIZIO                    **********
-- A) Imposto a Riaperto le fatture chiuse che sono da riaprire, a prescindere dal valore che hanno attualmente in fatture_stato_fatture. Uso la stessa select della fase B
 call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseA Inizio'));   
insert into fatture_stato_fattura 
      (fsf_id_fattura, 
       fsf_id_stato, 
       fsf_ges_id, 
       fsf_modifica,
       fsf_nomefile_csv)
select a.id,       
       3,  
       ifnull(FILTRO0000_GET_GESID(),0),
       now(),
       pnomefile_csv
  from fatture as a
  join fatture_stato_fattura_v as b on (b.fsf_id_fattura = a.id)
 where (b.fsf_cod = 2 or b.fsf_cod = 4)
    and (a.ft_soc,a.ft_deb_id,a.ft_ced_id,a.ft_num_fat,a.ft_attribuzione,a.ft_id_data_fat,a.ft_imp_fat) in
 (select b.ft_soc,b.ft_deb_id,b.ft_ced_id,b.ft_num_fat,b.ft_attribuzione,b.ft_id_data_fat,b.ft_imp_fat
   from fatture_fastweb_csv as b
  where ft_nomefile_csv = pnomefile_csv);
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseA Fine'));   

-- B) Riapro i record che ci sono sul file fatture che hanno la data di chiusura e sono presenti sul nuovo file
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseB Inizio'));   
update fatture as a
   set a.ft_id_data_chius = null,
       a.ft_data_chius    = null
 where a.ft_soc = 1
   and a.ft_id_data_chius is not null 
   and (a.ft_soc,a.ft_deb_id,a.ft_ced_id,a.ft_num_fat,a.ft_attribuzione,a.ft_id_data_fat,a.ft_imp_fat) in
   (select b.ft_soc,b.ft_deb_id,b.ft_ced_id,b.ft_num_fat,b.ft_attribuzione,b.ft_id_data_fat,b.ft_imp_fat
      from fatture_fastweb_csv as b
     where ft_nomefile_csv = pnomefile_csv);
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseB Fine'));   
-- ********   RIAPERTURA DELLE FATTURE FINE                      **********

-- ********   CHIUSURA/RICHIUSURA DELLE FATTURE APERTE INIZIO    **********
-- C) Imposto a Chiusa/Richiusa in base al valore in fatture_stato_fattura. Uso la stessa select della fase D
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseC Inizio'));   
insert into fatture_stato_fattura 
      (fsf_id_fattura, 
       fsf_id_stato, 
       fsf_ges_id, 
       fsf_modifica,
       fsf_nomefile_csv)
select a.id,       
       case ifnull(b.fsf_id_stato,1) when 1 then 2  
                                     when 3 then 4
                                     else 2
       end, 
       ifnull(FILTRO0000_GET_GESID(),0),
       now(),
       pnomefile_csv
  from fatture as a
  left outer join fatture_stato_fattura_v as b on (b.fsf_id_fattura =a.id)
where a.ft_soc = 1
   and ifnull(b.fsf_id_stato,1) in (1,3)
   -- OldVersion a.ft_id_data_chius is null 
   and not exists (select null
                     from fatture_fastweb_csv as b
                    where b.ft_soc          = a.ft_soc
                      and b.ft_deb_id       = a.ft_deb_id
                      and b.ft_ced_id       = a.ft_ced_id
                      and b.ft_num_fat      = a.ft_num_fat
                      and b.ft_attribuzione = a.ft_attribuzione
                      and b.ft_id_data_fat  = a.ft_id_data_fat
                      and b.ft_imp_fat      = a.ft_imp_fat
                      and b.ft_nomefile_csv = pnomefile_csv);
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseC Fine'));                         
                      
-- D) Chiudo i record che non sono presenti sul nuovo file 
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseD Inizio'));                         
update fatture as a
   set a.ft_data_chius = now()
 where a.ft_soc = 1
   and a.ft_id_data_chius is null 
   and not exists (select null
                     from fatture_fastweb_csv as b
                    where b.ft_soc          = a.ft_soc
                      and b.ft_deb_id       = a.ft_deb_id
                      and b.ft_ced_id       = a.ft_ced_id
                      and b.ft_num_fat      = a.ft_num_fat
                      and b.ft_attribuzione = a.ft_attribuzione
                      and b.ft_id_data_fat  = a.ft_id_data_fat
                      and b.ft_imp_fat      = a.ft_imp_fat
                      and b.ft_nomefile_csv = pnomefile_csv);
update fatture as a
   set a.ft_id_data_chius = (select b.id from calendario as b where b.databar=date_format(a.ft_data_chius,'%d/%m/%Y'))
   where a.ft_data_chius is not null;
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseD Fine'));                            
-- ********   CHIUSURA/RICHIUSURA DELLE FATTURE APERTE FINE      **********

-- ********   APERTURA DELLE FATTURE NUOVE INIZIO                **********                     
-- E) Carico i record del nuovo file che non sono presenti in fatture
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseE Inizio'));                         
insert into fatture (
  ft_soc
  ,ft_deb_id
  ,ft_ced_id
  ,ft_num_fat
  ,ft_attribuzione
  ,ft_td
  ,ft_testo
  ,ft_data_fat
  ,ft_data_scad
  ,ft_imp_fat
  ,ft_data_ins
  ,ft_id_stato_lav
  ,ft_td_id
  ,ft_id_data_fat
  ,ft_id_data_scad
  ,ft_nomefile_csv
  ,ft_id_csv
  ,ft_datetime_csv) 
select 
   a.ft_soc
  ,a.ft_deb_id
  ,a.ft_ced_id
  ,a.ft_num_fat
  ,a.ft_attribuzione
  ,a.ft_td
  ,a.ft_testo
  ,a.ft_data_fat
  ,a.ft_data_scad
  ,a.ft_imp_fat
  ,now()
  ,a.ft_id_stato_lav
  ,a.ft_td_id
  ,a.ft_id_data_fat
  ,a.ft_id_data_scad 
  ,a.ft_nomefile_csv
  ,a.ft_id_csv
  ,now()
 from fatture_fastweb_csv as a
where a.ft_nomefile_csv = pnomefile_csv
  and a.ft_ced_id       is not null
  and a.ft_deb_id       is not null
  and a.ft_td_id        is not null
  and a.ft_id_data_fat  is not null
  and a.ft_id_data_scad is not null
  and a.ft_id_stato_lav is not null
  and not exists (select null
                    from fatture as b
                   where b.ft_soc          = a.ft_soc
                     and b.ft_deb_id       = a.ft_deb_id
                     and b.ft_ced_id       = a.ft_ced_id
                     and b.ft_num_fat      = a.ft_num_fat
                     and b.ft_attribuzione = a.ft_attribuzione
                     and b.ft_id_data_fat  = a.ft_id_data_fat
                     and b.ft_imp_fat      = a.ft_imp_fat)
  and not exists (select null
                    from (select ft_soc,ft_deb_id,ft_ced_id,ft_num_fat,ft_attribuzione,ft_id_data_fat,ft_imp_fat
                            from fatture_fastweb_csv
                           where ft_nomefile_csv = pnomefile_csv
                           group by ft_soc,ft_deb_id,ft_ced_id,ft_num_fat,ft_attribuzione,ft_id_data_fat,ft_imp_fat
                           having count(1) > 1) as c
                   where c.ft_soc          = a.ft_soc
                     and c.ft_deb_id       = a.ft_deb_id
                     and c.ft_ced_id       = a.ft_ced_id
                     and c.ft_num_fat      = a.ft_num_fat
                     and c.ft_attribuzione = a.ft_attribuzione
                     and c.ft_id_data_fat  = a.ft_id_data_fat
                     and c.ft_imp_fat      = a.ft_imp_fat);
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseE Fine'));                                              
                     
-- F) Imposto con Stato Aperta tutte le fatture inserite.                     
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseF Inizio'));                                              
insert into fatture_stato_fattura 
      (fsf_id_fattura, 
       fsf_id_stato, 
       fsf_ges_id, 
       fsf_modifica,
       fsf_nomefile_csv)
select a.id,
        1,  
        ifnull(FILTRO0000_GET_GESID(),0),
        now(),
        b.ft_nomefile_csv
   from fatture as a
   join fatture_fastweb_csv as b on (    b.ft_soc          = a.ft_soc
                                     and b.ft_deb_id       = a.ft_deb_id
                                     and b.ft_ced_id       = a.ft_ced_id
                                     and b.ft_num_fat      = a.ft_num_fat
                                     and b.ft_attribuzione = a.ft_attribuzione
                                     and b.ft_id_data_fat  = a.ft_id_data_fat
                                     and b.ft_imp_fat      = a.ft_imp_fat
                                     and b.ft_nomefile_csv = pnomefile_csv)
  where a.id not in (select fsf_id_fattura
                       from fatture_stato_fattura);              
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseF Fine'));                                                  
-- ********   APERTURA DELLE FATTURE NUOVE FINE                  **********
-- ********   IMPOSTAZIONE DELLO STATO LAVORAZIONE A 20 PER LE FATTURE  NUOVE INIZIO **********
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseG Inizio'));                                                  
insert into fatture_statilav_note
(ftstlav_id_fattura, 
 ftstlav_id_stato_lav, 
 ftstlav_nota_crgest, 
 ftstlav_ges_id, 
 ftstlav_id_nota_csv, 
 ftstlav_nomefile_csv, 
 ftstlav_modifica)             
select a.id,
       20,
       null,
       ifnull(FILTRO0000_GET_GESID(),0),
       b.ft_id_csv,
       b.ft_nomefile_csv,
       now()
  from fatture             as a 
  join fatture_fastweb_csv as b on (    b.ft_soc          = a.ft_soc
                                    and b.ft_deb_id       = a.ft_deb_id
                                    and b.ft_ced_id       = a.ft_ced_id
                                    and b.ft_num_fat      = a.ft_num_fat
                                    and b.ft_attribuzione = a.ft_attribuzione
                                    and b.ft_id_data_fat  = a.ft_id_data_fat
                                    and b.ft_imp_fat      = a.ft_imp_fat
                                    and b.ft_nomefile_csv = pnomefile_csv)
  where a.id not in (select ftstlav_id_fattura
                       from fatture_statilav_note);


call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseG Fine'));                                                  
-- ********   IMPOSTAZIONE DELLO STATO LAVORAZIONE A 20 PER LE FATTURE  NUOVE FINE   **********


end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fatturefastwebcsv_2_gestdebitore` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fatturefastwebcsv_2_gestdebitore`(in pnomefile_csv varchar(100))
begin
insert into gest_debitore (
  gd_ges_id,
  gd_deb_id)
select 
  ft_zona,
  ft_deb_id
 from fatture_fastweb_csv
where ft_nomefile_csv = pnomefile_csv and
      ft_ced_id       is not null and
      ft_deb_id       is not null and
      ft_td_id        is not null and
      ft_id_data_fat  is not null and
      ft_id_data_scad is not null
  and (ft_zona,ft_deb_id) not in (select gd_ges_id,gd_deb_id from gest_debitore)
  group by ft_zona,ft_deb_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fatturefastwebcsv_delcaricate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fatturefastwebcsv_delcaricate`(in pnomefile_csv varchar(100))
begin
delete 
  from fatture_fastweb_csv
 where ft_nomefile_csv = pnomefile_csv
   and (ft_soc,ft_ced_id,ft_deb_id,ft_num_fat,ft_attribuzione,ft_id_data_fat,ft_imp_fat) in (select ft_soc,ft_ced_id,ft_deb_id,ft_num_fat,ft_attribuzione,ft_id_data_fat,ft_imp_fat from fatture);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fatturefastwebnotecrgest_2_fatturenotecrgest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fatturefastwebnotecrgest_2_fatturenotecrgest`(in pnomefile_csv varchar(100))
begin   
-- Carico i record del nuovo file che non sono presenti in fatture_notecrgest
delete 
 from fatture_notecrgest 
where ftnote_nomefile_csv = pnomefile_csv;

insert 
  into fatture_notecrgest (ftnote_id_fattura,ftnote_nota_crgest,ftnote_id_nota_csv,ftnote_nomefile_csv)
 select ftnote_idfatt,ftnote_nota,ftnote_id,ftnote_nomefile_csv
   from fatture_fastweb_notecrgest_csv
  where ftnote_nomefile_csv = pnomefile_csv
    and ftnote_nota is not null
    and ftnote_nota != ' ';
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fatturefastwebstatilavnote_2_fatturestatilavnote` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fatturefastwebstatilavnote_2_fatturestatilavnote`(in pnomefile_csv varchar(100))
begin   
-- Carico i record del nuovo file che non sono presenti in fatture_notecrgest;

select * 
  from fatture_fastweb_statilavnote_csv;


insert 
  into fatture_notecrgest (ftnote_id_fattura,ftnote_nota_crgest,ftnote_id_nota_csv,ftnote_nomefile_csv)
 select ftnote_idfatt,ftnote_nota,ftnote_id,ftnote_nomefile_csv
   from fatture_fastweb_notecrgest_csv
  where ftnote_nomefile_csv = pnomefile_csv
    and ftnote_nota is not null
    and ftnote_nota != ' ';
    
    
    
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fatturempscsv_2_fatture` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fatturempscsv_2_fatture`(in pnomefile_csv varchar(100))
begin
insert into fatture (
  ft_soc
  ,ft_deb_id
  ,ft_ced_id
  ,ft_num_fat
  ,ft_attribuzione
  ,ft_td
  ,ft_testo
  ,ft_data_fat
  ,ft_data_scad
  ,ft_imp_fat
  ,ft_imp_aperto
  ,ft_data_ins
  ,ft_id_stato_lav
  ,ft_td_id
  ,ft_id_data_fat
  ,ft_id_data_scad
  ,ft_nomefile_csv
  ,ft_id_csv
  ,ft_datetime_csv) 
select 
   2
  ,ft_deb_id
  ,ft_ced_id
  ,ft_num_fat
  ,ft_attribuzione
  ,ft_td
  ,null
  ,ft_data_fat
  ,ft_data_scad
  ,ft_imp_fat
  ,ft_imp_aperto
  ,now()
  ,9
  ,ft_td_id
  ,ft_id_data_fat
  ,ft_id_data_scad 
  ,ft_nomefile_csv
  ,ft_id_csv
  ,now()
 from fatture_mps_csv
where ft_nomefile_csv = pnomefile_csv and
      ft_ced_id       is not null and
      ft_deb_id       is not null and
      ft_td_id        is not null and
      ft_id_data_fat  is not null and
      ft_id_data_scad is not null
  and (2,ft_ced_id,ft_deb_id,ft_num_fat,ft_id_data_fat) not in (select ft_soc,ft_ced_id,ft_deb_id,ft_num_fat,ft_id_data_fat from fatture);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fatture_delete_recdoppi` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fatture_delete_recdoppi`()
begin
delete b 
   from fatture as a,
        fatture as b
  where b.ft_soc      = a.ft_soc      and
        b.ft_deb_id   = a.ft_deb_id   and 
        b.ft_ced_id   = a.ft_ced_id   and
        b.ft_num_fat  = a.ft_num_fat  and
        b.ft_data_fat = a.ft_data_fat and 
        b.ft_imp_fat  = a.ft_imp_fat  and
        b.id          < a.id;  
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0000_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0000_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 0;
if (pexists = 0)
then 
insert 
  into filtri 
select 0,'Login ',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0000_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0000_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 0;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 1;
if (pexists = 0)
then 
insert 
  into filtri 
select 1,'Gestione scadenzario',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_POPOLA_TEMP` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_POPOLA_TEMP`(in porder int,in pperpag int,in pnumpag int)
begin
  declare pmaxpag int(10);
  declare pinipag int(10);
  declare pexist  int(10);
-- Verifico l'esistenza della tabella temporanea TEMP_GESTIONE_SCADENZIARIO
  select count(1) 
    into pexist
    from information_schema.tables
   where upper(table_name) = 'TEMP_GESTIONE_SCADENZIARIO';
-- Se la tabella TEMP_GESTIONE_SCADENZIARIO non esiste la creo
  if pexist = 0 then
     create   table temp_gestione_scadenziario as select * from gestione_scadenziario_v where id = 0;
     alter    table temp_gestione_scadenziario add idord int(10) not null;
     alter    table temp_gestione_scadenziario add index idord_1(idord);
     alter    table temp_gestione_scadenziario CHANGE idord idord INT(10) AUTO_INCREMENT NOT NULL;
  end if;
  truncate table temp_gestione_scadenziario;
  insert into temp_gestione_scadenziario (
  id
  ,ft_soc
  ,ft_deb_id
  ,ft_ced_id
  ,ft_num_fat
  ,ft_attribuzione
  ,ft_td
  ,ft_testo
  ,ft_data_fat
  ,ft_data_scad
  ,ft_imp_fat
  ,ft_imp_aperto
  ,ft_data_ins
  ,ft_id_stato_lav
  ,ft_td_id)
    select 
  b.id
  ,b.ft_soc
  ,b.ft_deb_id
  ,b.ft_ced_id
  ,b.ft_num_fat
  ,b.ft_attribuzione
  ,b.ft_td
  ,b.ft_testo
  ,b.ft_data_fat
  ,b.ft_data_scad
  ,b.ft_imp_fat
  ,b.ft_imp_aperto
  ,b.ft_data_ins
  ,b.ft_id_stato_lav
  ,b.ft_td_id 
    from gestione_scadenziario_v as b 
   order by case ifnull(porder,1) when 1  then lpad(b.id,10,0)
                                  when 2  then lpad(b.ft_soc,10,0)
                                  when 3  then b.ft_num_fat
                                  else lpad(b.id,10,0) end;
  select * from temp_gestione_scadenziario
  where idord between ((pnumpag-1)*pperpag)+1 and (pperpag*pnumpag)
  order by idord;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_ALL`()
begin
call FILTRO0001_RESET_TIPODOC();
call FILTRO0001_RESET_DEBITORE();
call FILTRO0001_RESET_STATOLAV();
call FILTRO0001_RESET_DATAFAT_DA();
call FILTRO0001_RESET_DATAFAT_A();
call FILTRO0001_RESET_DATASCAD_DA();
call FILTRO0001_RESET_DATASCAD_A();
call FILTRO0001_RESET_NUMFAT();
call FILTRO0001_RESET_CHIUSE();
call FILTRO0001_RESET_ARCDOC_SINO();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_ARCDOC_SINO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_ARCDOC_SINO`()
begin
  update filtri_parametri as a
     set a.parint11 = 0
   where par_conn   = connection_id()
     and id_filtro  = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_CHIUSE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_CHIUSE`()
begin
  update filtri_parametri as a
     set a.parint10 = 0
   where par_conn   = connection_id()
     and id_filtro  = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_DATAFAT_A` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_DATAFAT_A`()
begin
  update filtri_parametri as a
     set a.parint06 = null
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_DATAFAT_DA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_DATAFAT_DA`()
begin
  update filtri_parametri as a
     set a.parint05 = null
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_DATASCAD_A` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_DATASCAD_A`()
begin
  update filtri_parametri as a
     set a.parint08 = null
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_DATASCAD_DA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_DATASCAD_DA`()
begin
  update filtri_parametri as a
     set a.parint07 = null
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_DEBITORE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_DEBITORE`()
begin
  update filtri_parametri 
     set parint03  = null
   where par_conn  = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_NUMFAT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_NUMFAT`()
begin
  update filtri_parametri as a
     set a.parvar09 = null
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_SOCIETA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_SOCIETA`()
begin
  update filtri_parametri 
     set parint01  = null
   where par_conn  = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_STATOLAV` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_STATOLAV`()
begin
  update filtri_parametri 
     set parint04  = null
   where par_conn  = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_TIPODOC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_RESET_TIPODOC`()
begin
  update filtri_parametri 
     set parint02 = null
   where par_conn = connection_id()
    and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_ARCDOC_SINO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_ARCDOC_SINO`()
begin
  update filtri_parametri as a
     set a.parint11 = 1
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_CHIUSE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_CHIUSE`()
begin
  update filtri_parametri as a
     set a.parint10 = 1
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_DATAFAT_A` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_DATAFAT_A`(in pdatafat_a varchar(10))
begin
  update filtri_parametri as a
     set a.parint06 = (select ifnull(b.id,0) 
                         from calendario as b 
                        where b.datachar = case substring(pdatafat_a,3,1) when '/' then concat(substring(pdatafat_a,1,2),substring(pdatafat_a,4,2),substring(pdatafat_a,7,4))
                                                                                   else concat(substring(pdatafat_a,9,2),substring(pdatafat_a,6,2),substring(pdatafat_a,1,4)) end)
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_DATAFAT_DA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_DATAFAT_DA`(in pdatafat_da varchar(10))
begin
  update filtri_parametri as a
     set a.parint05 = (select ifnull(b.id,0) 
                         from calendario as b 
                        where b.datachar = case substring(pdatafat_da,3,1) when '/' then concat(substring(pdatafat_da,1,2),substring(pdatafat_da,4,2),substring(pdatafat_da,7,4))
                                                                                    else concat(substring(pdatafat_da,9,2),substring(pdatafat_da,6,2),substring(pdatafat_da,1,4)) end)
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_DATASCAD_A` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_DATASCAD_A`(in pdatascad_a varchar(10))
begin
  update filtri_parametri as a
     set a.parint08 = (select ifnull(b.id,0) 
                         from calendario as b 
                        where b.datachar = case substring(pdatascad_a,3,1) when '/' then concat(substring(pdatascad_a,1,2),substring(pdatascad_a,4,2),substring(pdatascad_a,7,4))
                                                                                    else concat(substring(pdatascad_a,9,2),substring(pdatascad_a,6,2),substring(pdatascad_a,1,4)) end)
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_DATASCAD_DA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_DATASCAD_DA`(in pdatascad_da varchar(10))
begin
  update filtri_parametri as a
     set a.parint07 = (select ifnull(b.id,0) 
                         from calendario as b 
                        where b.datachar = case substring(pdatascad_da,3,1) when '/' then concat(substring(pdatascad_da,1,2),substring(pdatascad_da,4,2),substring(pdatascad_da,7,4))
                                                                                     else concat(substring(pdatascad_da,9,2),substring(pdatascad_da,6,2),substring(pdatascad_da,1,4)) end)
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_DEBITORE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_DEBITORE`(in pdebitore int)
begin
  update filtri_parametri 
     set parint03 = pdebitore
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_FATTURE_NOTECRGEST` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_FATTURE_NOTECRGEST`( in pid_fattura int(10),
                                                    in pid_stato_lav int(10),
                                                    in pid_nota varchar(1000) )
begin
  insert
    into fatture_notecrgest 
   (
    ftnote_id_fattura,
    ftnote_id_stato_lav,
    ftnote_nota_crgest,
    ftnote_ges_id,
    ftnote_modifica)
    select 
    pid_fattura,
    pid_stato_lav,
    pid_nota,
    filtro0000_get_gesid(),
    now();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_FATTURE_STATILAVNOTE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_FATTURE_STATILAVNOTE`( in pid_fattura int(10),
                                                      in pid_stato_lav int(10),
                                                      in pid_nota varchar(1000) )
begin
  insert
    into fatture_statilav_note
   (
    ftstlav_id_fattura,
    ftstlav_id_stato_lav,
    ftstlav_nota_crgest,
    ftstlav_ges_id,
    ftstlav_modifica)
    select 
    pid_fattura,
    case ifnull(pid_stato_lav,0)
         when 0 then a.ftstlav_id_stato_lav
         else pid_stato_lav
    end,     
    pid_nota,
    filtro0000_get_gesid(),
    now()
 from fatture_statilavnote_v as a
where a.ftstlav_id_fattura = pid_fattura;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_NUMFAT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_NUMFAT`(in pnumfat varchar(10))
begin
  update filtri_parametri as a
     set a.parvar09 = pnumfat
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_SOCIETA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_SOCIETA`(in psocieta int)
begin
  update filtri_parametri 
     set parint01 = psocieta
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_STATOLAV` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_STATOLAV`(in pstatolav int)
begin
  update filtri_parametri 
     set parint04 = pstatolav
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_SET_TIPODOC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_SET_TIPODOC`(in ptd_id int)
begin
  update filtri_parametri 
     set parint02 = ptd_id
   where par_conn = connection_id()
     and id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0001_UNLOCK` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0001_UNLOCK`()
begin
update filtri_parametri set par_conn = null where id_filtro = 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0002_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0002_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 2;
if (pexists = 0)
then 
insert 
  into filtri 
select 2,'Gestione ordinamenti',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0002_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0002_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0002_reset_liv1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0002_reset_liv1`()
begin
  update filtri_parametri 
     set parint01  = null
   where par_conn  = connection_id()
     and id_filtro = 2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0002_reset_liv2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0002_reset_liv2`()
begin
  update filtri_parametri 
     set parint02  = null
   where par_conn  = connection_id()
     and id_filtro = 2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0002_reset_liv3` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0002_reset_liv3`()
begin
  update filtri_parametri 
     set parint03  = null
   where par_conn  = connection_id()
     and id_filtro = 2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0002_reset_liv4` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0002_reset_liv4`()
begin
  update filtri_parametri 
     set parint04  = null
   where par_conn  = connection_id()
     and id_filtro = 2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0002_set_liv1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0002_set_liv1`(in pliv1 int)
begin
  update filtri_parametri 
     set parint1  = pliv1
   where par_conn  = connection_id()
     and id_filtro = 2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0002_set_liv2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0002_set_liv2`(in pliv2 int)
begin
  update filtri_parametri 
     set parint02  = pliv2
   where par_conn  = connection_id()
     and id_filtro = 2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0002_set_liv3` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0002_set_liv3`(in pliv3 int)
begin
  update filtri_parametri 
     set parint03  = pliv3
   where par_conn  = connection_id()
     and id_filtro = 2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0002_set_liv4` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0002_set_liv4`(in pliv4 int)
begin
  update filtri_parametri 
     set parint04  = pliv4
   where par_conn  = connection_id()
     and id_filtro = 2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_CHECK_DEFAULT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_CHECK_DEFAULT`()
begin
declare pnumpagesp   int(10);
declare pnumpagcorr  int(10);
declare pnumposcorr  int(10);
declare pidrecperpag int(10);
select ifnull(numpagesp,0), ifnull(numpagcorr,0), ifnull(numposcorr,0), ifnull(idrecperpag,0)
  into pnumpagesp,          pnumpagcorr,          pnumposcorr,          pidrecperpag
  from filtro0003_parconn_v;
if (pnumpagesp    = 0 or
    pnumpagcorr   = 0 or
    pnumposcorr   = 0 or
    pidrecperpag  = 0)
    then select numpagesp,  numpagcorr,  numposcorr,  idrecperpag
           into pnumpagesp, pnumpagcorr, pnumposcorr, pidrecperpag
           from filtro0003_valori_default;
         update filtri_parametri 
            set parint05 = pnumpagesp,
                parint04 = pnumpagcorr,
                parint08 = pnumposcorr,
                parint09 = pidrecperpag
         where par_conn  = connection_id()
           and id_filtro = 3;               
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 3;
if (pexists = 0)
then 
insert 
  into filtri 
select 3,'Paginazione',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_IDRECPERPAG` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_IDRECPERPAG`(in pidrecperpag int)
begin
update filtri_parametri 
   set parint09  = pidrecperpag
 where par_conn  = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_LIMFIN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_LIMFIN`()
begin
update filtri_parametri 
   set parint07 = (ifnull(parint04,1)*ifnull(parint01,25))
 where par_conn = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_LIMINI` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_LIMINI`()
begin
declare pvalrecperpag int(10);
select valrecperpag
  into pvalrecperpag
  from filtro0003_parconn_v;
update filtri_parametri
   set parint06 = ((ifnull(parint04,1)-1)*pvalrecperpag)
 where par_conn = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGCORR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_NUMPAGCORR`(in pidpagcorr int)
begin
declare poldpos int(10);
declare poldpag int(10);
declare pnewpos int(10);
declare ppagtot int(10);
declare ppagesp int(10);
select a.numpagcorr,a.numposcorr, a.numpagtot, a.numpagesp
  into poldpag,     poldpos,      ppagtot, ppagesp
  from filtro0003_parconn_v as a;
select poldpag, poldpos,ppagtot,ppagesp,pidpagcorr;
if (pidpagcorr > poldpag) then set pnewpos = poldpos + (pidpagcorr-poldpag); end if;
if (pidpagcorr < poldpag) then set pnewpos = poldpos - (poldpag-pidpagcorr); end if;
if (pidpagcorr = ppagtot) then set pnewpos = ppagesp; end if;
if (pidpagcorr = 1)       then set pnewpos = 1; end if;
update filtri_parametri 
   set parint08 = pnewpos,
       parint04 = pidpagcorr                
 where par_conn = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGESP` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_NUMPAGESP`(in pnumpagesp int)
begin
update filtri_parametri 
   set parint05 = pnumpagesp
 where par_conn = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGPREC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_NUMPAGPREC`()
begin
update filtri_parametri 
   set parint04 = case ifnull(parint04,1) when 1 then 1 else ifnull(parint04,1)-1 end,
       parint08 = case ifnull(parint08,1) when 1 then 1 else ifnull(parint08,1)-1 end
 where par_conn = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGPRI` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_NUMPAGPRI`()
begin
update filtri_parametri 
   set parint04 = 1,
       parint08 = 1
 where par_conn = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGSUCC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_NUMPAGSUCC`()
begin
update filtri_parametri 
   set parint04 = case ifnull(parint04,1) when parint03 then parint03 else ifnull(parint04,1)+1 end,
       parint08 = case ifnull(parint08,1) when parint05 then parint05 else ifnull(parint08,1)+1 end
 where par_conn = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGTOT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_NUMPAGTOT`()
begin
update filtri_parametri as a
  join filtro0003_parconn_v as b
   set a.parint03 = case truncate((a.parint02/b.valrecperpag)+0.999999,0) when 0 then 1 else truncate((a.parint02/b.valrecperpag)+0.999999,0) end
 where a.par_conn = connection_id()
   and a.id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGULT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_NUMPAGULT`()
begin
declare pultpag int(10);
declare pnumpagesp int(10);
select numpagtot 
  into pultpag
  from filtro0003_get_numpagtot_v;
select numpagesp 
  into pnumpagesp
  from filtro0003_get_numpagesp_v;  
update filtri_parametri 
   set parint04 = pultpag,
       parint08 = pnumpagesp
 where par_conn = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPOSCORR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_NUMPOSCORR`(in pnumposcorr int)
begin
update filtri_parametri 
   set parint08 = pnumposcorr
 where par_conn = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMRECTOT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0003_SET_NUMRECTOT`()
begin
declare pnumrectot int(10);
select count(1) 
  into pnumrectot
  from gestione_scadenziario_v;
update filtri_parametri 
   set parint02 = pnumrectot
 where par_conn = connection_id()
   and id_filtro = 3;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0004_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0004_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 4;
if (pexists = 0)
then 
insert 
  into filtri 
select 4,'Tabella Tab_arcdoc',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0004_INS_TABARCDOCID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0004_INS_TABARCDOCID`(in pestcod   varchar(5),
                                             in ptipo     int,
                                             in psize     numeric,
                                             in pnomefile varchar(50),
                                             in pdesc     varchar(50))
begin
start transaction;
insert into tab_arcdoc (
tbarcdoc_soc,
tbarcdoc_est,
tbarcdoc_tipo,
tbarcdoc_size,
tbarcdoc_nomefile,
tbarcdoc_desc,
tbarcdoc_blob,
tbarcdoc_ges_id
)
values
(
filtro0001_get_societa(),
ifnull(filtro0004_find_idest(pestcod),0),
ptipo,
psize,
pnomefile,
pdesc,
null,
filtro0000_get_gesid()
);
commit;
call filtro0004_SET_LASTID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0004_INS_TABSR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0004_INS_TABSR`(in ptbsr_numero varchar(10),
                                       in ptbsr_nota   varchar(2000))
begin
start transaction;
insert into tab_sr (
tbsr_numero,
tbsr_nota,
tbsr_stato,
tbarcdoc_ges_id
)
values
(ptbsr_numero,
 ptbsr_nota,
 0,
 filtro0000_get_gesid()
);
commit;
call filtro0010_SET_LASTID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0004_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0004_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 4;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0004_RESET_LASTID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0004_RESET_LASTID`()
begin
update filtri_parametri 
   set parint01 = null
 where id_filtro = 4
   and par_conn  = connection_ID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0004_SET_LASTID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0004_SET_LASTID`()
begin
update filtri_parametri 
   set parint01 = LAST_INSERT_ID()
 where id_filtro = 4
   and par_conn  = connection_ID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0004_UPD_BLOB` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0004_UPD_BLOB`(in pblob MEDIUMBLOB)
begin
update tab_arcdoc
   set tbarcdoc_blob = pblob
 where tbarcdoc_id = filtro0004_GET_LASTID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0005_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0005_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 5;
if (pexists = 0)
then 
insert 
  into filtri 
select 5,'Tabella Fatture_arcdoc',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0005_INS_FATARCDOC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0005_INS_FATARCDOC`(in pidfatt int,
                                           in piddoc  int)
begin
insert into fatture_arcdoc 
(
ftarcdoc_idfatt,
ftarcdoc_iddoc,
ftarcdoc_ges_id
)
values
(
pidfatt,
piddoc,
filtro0000_get_gesid()
);
update fatture 
   set ft_arcdoc_check_sino = 1
 where id = pidfatt;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0005_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0005_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 5;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0005_SET_IDFATTURA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0005_SET_IDFATTURA`(in pidfatt int)
begin
update filtri_parametri
   set parint01 = pidfatt
 where ges_id     = FILTRO0000_GET_GESID()
   and id_filtro  = 5;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0006_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0006_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 6;
if (pexists = 0)
then 
insert 
  into filtri 
select 6,'Gestione attivita'' debitori',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0006_INS_ANNOTAZIONE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0006_INS_ANNOTAZIONE`(in ptpann integer,
                                            in ptpcon integer,
                                            in pdata  char(10),
                                            in pnota  TEXT,
                                            in pinter TEXT)
begin
start transaction;
insert into debitori_annotazioni (
debann_debid, 
debann_idtpann, 
debann_idtpcon, 
debann_iddata, 
debann_annot, 
debann_interlocutore, 
debann_ges_id
)
select 
filtro0006_get_debid(),
ptpann,
ptpcon,
id,
pnota,
pinter,
filtro0000_get_gesid()
 from calendario as a
where a.databar = pdata ;
commit;
call filtro0006_SET_LASTID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0006_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0006_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 6;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0006_RESET_DEBID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0006_RESET_DEBID`()
begin
update filtri_parametri 
   set parint01  = null
 where par_conn  = connection_id()
   and id_filtro = 6;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0006_RESET_LASTID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0006_RESET_LASTID`()
begin
update filtri_parametri 
   set parint02 = null
 where id_filtro = 6
   and par_conn  = connection_ID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0006_SET_DEBID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0006_SET_DEBID`(in pdebid integer)
begin
update filtri_parametri 
   set parint01  = pdebid
 where par_conn  = connection_id()
   and id_filtro = 6;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0006_SET_LASTID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0006_SET_LASTID`()
begin
update filtri_parametri 
   set parint02 = LAST_INSERT_ID()
 where id_filtro = 6
   and par_conn  = connection_ID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0007_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0007_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 7;
if (pexists = 0)
then 
insert 
  into filtri 
select 7,'Gestione documenti attivita'' debitori',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0007_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0007_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 7;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0007_RESET_DEBID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0007_RESET_DEBID`()
begin
update filtri_parametri 
   set parint01  = null
 where par_conn  = connection_id()
   and id_filtro = 7;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0007_SET_DEBID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0007_SET_DEBID`(in pdebid integer)
begin
update filtri_parametri 
   set parint01  = pdebid
 where par_conn  = connection_id()
   and id_filtro = 7;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0008_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0008_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 8;
if (pexists = 0)
then 
insert 
  into filtri 
select 8,'Gestione anagrafica debitori',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0008_INS_DEBANNARCDOC` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0008_INS_DEBANNARCDOC`(in pdebannid int,
                                              in piddoc    int)
begin
insert into debann_arcdoc
(
debandoc_idann,
debandoc_iddoc,
debandoc_ges_id
)
values
(
pdebannid,
piddoc,
filtro0000_get_gesid()
);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0008_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0008_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 8;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0008_RESET_DEBANNID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0008_RESET_DEBANNID`()
begin
update filtri_parametri
   set parint01 = null
 where ges_id     = FILTRO0000_GET_GESID()
   and id_filtro  = 8;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0008_SET_DEBANNID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0008_SET_DEBANNID`(in pdebannid int)
begin
update filtri_parametri
   set parint01 = pdebannid
 where ges_id     = FILTRO0000_GET_GESID()
   and id_filtro  = 8;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0009_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0009_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 9;
if (pexists = 0)
then 
insert 
  into filtri 
select 9,'Gestione generica debitori',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0009_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0009_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 9;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0009_RESET_DEBID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0009_RESET_DEBID`()
begin
update filtri_parametri 
   set parint01  = null
 where par_conn  = connection_id()
   and id_filtro = 9;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0009_SET_DEBID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0009_SET_DEBID`(in pdebid integer)
begin
update filtri_parametri 
   set parint01  = pdebid
 where par_conn  = connection_id()
   and id_filtro = 9;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0010_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0010_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 10;
if (pexists = 0)
then 
insert 
  into filtri 
select 10,'Tabella TAB_SR',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0010_INS_TABSR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0010_INS_TABSR`(in ptbsr_numero varchar(20),
                                       in ptbsr_nota   varchar(2000))
begin
start transaction;
insert into tab_sr (
tbsr_numero,
tbsr_nota,
tbsr_stato,
tbsr_ges_id
)
values
(
 ptbsr_numero,
 ptbsr_nota,
 1,
 filtro0000_get_gesid()
);
commit;
call filtro0010_SET_LASTID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0010_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0010_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 10;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0010_RESET_LASTID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0010_RESET_LASTID`()
begin
update filtri_parametri 
   set parint01 = null
 where id_filtro = 10
   and par_conn  = connection_ID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `filtro0010_SET_LASTID` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `filtro0010_SET_LASTID`()
begin
update filtri_parametri 
   set parint01 = LAST_INSERT_ID()
 where id_filtro = 10
   and par_conn  = connection_ID();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0011_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0011_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 11;
if (pexists = 0)
then 
insert 
  into filtri 
select 11,'Tabella Fatture_tabsr',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0011_INS_FATTABSR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0011_INS_FATTABSR`(in pidfatt int,
                                          in pidsr  int)
begin
insert into fatture_tabsr
(
fttbsr_idfatt,
fttbsr_idsr,
fttbsr_ges_id
)
values
(
pidfatt,
pidsr,
filtro0000_get_gesid()
);
update fatture 
   set ft_tabsr_check_sino = 1
 where id = pidfatt;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0011_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0011_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 11;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0011_SET_IDFATTURA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0011_SET_IDFATTURA`(in pidfatt int)
begin
update filtri_parametri
   set parint01 = pidfatt
 where ges_id     = FILTRO0000_GET_GESID()
   and id_filtro  = 11;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0012_CHECK_EXISTS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0012_CHECK_EXISTS`()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 12;
if (pexists = 0)
then 
insert 
  into filtri 
select 12,'Forecast',0,now();
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0012_DELFORECAST` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0012_DELFORECAST`(in pid_fattura integer)
begin
update fatture 
   set ft_forecast_mese = null,
       ft_forecast_anno = null
 where id = pid_fattura;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0012_RESET_ALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0012_RESET_ALL`()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 12;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FILTRO0012_UPDFORECAST` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FILTRO0012_UPDFORECAST`(in pfrcst_mese numeric(2),
                                         in pfrcst_anno numeric(4),
                                         in pid_fattura integer)
begin
update fatture 
   set ft_forecast_mese = pfrcst_mese,
       ft_forecast_anno = pfrcst_anno
 where id = pid_fattura;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FOSUPD_FILTRI_PARAMALL` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FOSUPD_FILTRI_PARAMALL`(in pfosuser_username varchar(50))
begin
  declare pgest_id int(10);
  select fosuser_gestid
    into pgest_id
    from fosuser_gestori
   where fosuser_username = pfosuser_username;
  call upd_filtri_parametri(pgest_id, 0);
  call upd_filtri_parametri(pgest_id, 1);
  call upd_filtri_parametri(pgest_id, 2);
  call upd_filtri_parametri(pgest_id, 3);
  call upd_filtri_parametri(pgest_id, 4);
  call upd_filtri_parametri(pgest_id, 5);
  call upd_filtri_parametri(pgest_id, 6);
  call upd_filtri_parametri(pgest_id, 7);
  call upd_filtri_parametri(pgest_id, 8);
  call upd_filtri_parametri(pgest_id, 9);
  call upd_filtri_parametri(pgest_id,10);
  call upd_filtri_parametri(pgest_id,11);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FOSUPD_FILTRI_PARAMETRI` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FOSUPD_FILTRI_PARAMETRI`(in pfosuser_username varchar(50), in pfiltro int)
begin
  declare pgest_id int(10);
  select fosuser_gestid
    into pgest_id
    from fosuser_gestori
   where fosuser_username = pfosuser_username;
  call upd_filtri_parametri(pgest_id,pfiltro);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `IMPORTA_FATTURE_MPS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `IMPORTA_FATTURE_MPS`(in phost int,in pnomefile char(50))
begin
  declare pexist_file int(1);
  declare pfilename   char(200);
  DECLARE result CHAR(255);
  select concat(directory_file_load,pnomefile,'.csv')
    into pfilename
    from tab_procedure_parametri_v
   where id_proc = 1
     and id_host = phost; 
  -- select pfilename;
  -- LOAD DATA INFILE 'C:\\xampp\\htdocs\\vs28ahbz_gecotest\\sql\\Access2Mysql\\Fatture.csv' INTO TABLE vs28ahbz_gecotest.fatture
  -- LOAD DATA INFILE pfilename INTO TABLE temp_fatture_mps
  -- FIELDS TERMINATED BY ';' 
  -- lines terminated by '\r\n' 
  -- ignore 1 lines 
  -- (ft_ced_id);
  set result = mysql.sys_exec('pippo.bat'); 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `INS_GECOLOG` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `INS_GECOLOG`(in pdescrizione varchar(100))
begin
insert into vs28ahbz_gecotestlog(descrizione,ges_id) values (pdescrizione,ifnull(filtro0000_get_gesid(),0));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `load_data_SP` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `load_data_SP`(in_filepath varchar(100),in_db_table varchar(100))
BEGIN
declare exec_str varchar(500);
declare ret_val int;
set exec_str=concat("sh /tmp/load.sh ",in_filepath," ", in_db_table);
set ret_val=sys_exec(exec_str);
if ret_val=0 then
select "Success" as Result;
else
select "Please check file permissions and paths" as Result;
end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updins_gestdebitore_fastweb` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updins_gestdebitore_fastweb`()
begin
update gestdebitore_fastweb_csv set gd_ges_id = gd_ges_cod;
update gestdebitore_fastweb_csv set gd_deb_id = (select deb_id from debitori where deb_soc = 1 and deb_cod = gd_deb_cod);
update gestdebitore_fastweb_csv set gd_id_dataassoc = (select id from calendario where databar = gd_data_associazione) where gd_data_associazione is not null;
update gestdebitore_fastweb_csv set gd_id_datadissoc = (select id from calendario where databar = gd_data_dissociazione) where gd_data_dissociazione is not null;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upd_fatture_fastweb_csv` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_fatture_fastweb_csv`(in pnomefile_csv varchar(100))
begin
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase1 Inizio'));
update fatture_fastweb_csv 
   set ft_nomefile_csv = pnomefile_csv
 where ft_nomefile_csv is null;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase1 Fine'));


call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase2 Inizio'));
update fatture_fastweb_csv 
   set ft_ced_id = (select min(ced_id) from cedenti where ced_soc = 1) 
 where ft_nomefile_csv = pnomefile_csv;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase2 Fine')); 

call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase3 Inizio'));
-- Aggiorno la tabella dei debitori con eventuali nuovi
insert into debitori (deb_soc,deb_cod,deb_des,deb_citta,deb_pro)
select 1,ft_deb_cod,min(ft_deb_denom),min(ft_deb_citta),min(ft_deb_prov) 
  from fatture_fastweb_csv
 where ft_nomefile_csv = pnomefile_csv
   and (1,ft_deb_cod) not in (select b.deb_soc,b.deb_cod from debitori as b)
 group by ft_deb_cod;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase3 Fine'));
 
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase4 Inizio'));
-- Assegno l'id tipo_documento in base a societ� e codice
update fatture_fastweb_csv 
   set ft_td_id = (select b.td_id from tipo_doc as b where b.td_soc = 1 and b.td_cod = ft_td)
 where ft_nomefile_csv = pnomefile_csv;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase4 Fine'));

call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase5 Inizio'));
-- Assegno l'id debitore in base a societ� e codice
update fatture_fastweb_csv 
   set ft_deb_id = (select b.deb_id from debitori as b where b.deb_soc = 1 and b.deb_cod = ft_deb_cod)
 where ft_nomefile_csv = pnomefile_csv;    
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase5 Fine')); 
   
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase6 Inizio'));   
-- Assegno l'id data fattura
update fatture_fastweb_csv 
   set ft_id_data_fat = (select b.id from calendario as b where b.databar=ft_data_fat)
   where ft_nomefile_csv = pnomefile_csv;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase6 Fine'));   
   
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase7 Inizio'));   
-- Assegno l'id data scadenza
update fatture_fastweb_csv 
   set ft_id_data_scad = (select b.id from calendario as b where b.databar=ft_data_scad)
 where ft_nomefile_csv = pnomefile_csv;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase7 Fine')); 
   
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase8 Inizio'));   
-- Assegno l'id stato lavorazione
update fatture_fastweb_csv 
   set ft_id_stato_lav = (select b.id
                            from stato_lavorazione as b 
                           where b.sl_soc = 1 
                             and upper(sl_descrizione) = upper(ft_stato_lav))
 where ft_nomefile_csv = pnomefile_csv;
 call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase8 Fine'));
 
 
 call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase9 Inizio'));
 insert into fatture_fastweb_csv_anomalie
 select b.*
 from fatture_fastweb_csv  as a,
        fatture_fastweb_csv  as b
  where b.ft_soc          = a.ft_soc
    and b.ft_ced_id       = a.ft_ced_id
    and b.ft_deb_id       = a.ft_deb_id
    and b.ft_data_fat     = a.ft_data_fat
    and b.ft_num_fat      = a.ft_num_fat
    and b.ft_attribuzione = a.ft_attribuzione
    and b.ft_imp_fat      = a.ft_imp_fat
    and b.ft_id_csv       < a.ft_id_csv;
 call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase9 Fine'));
    
 call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase10 Inizio'));   
 delete b 
   from fatture_fastweb_csv  as a,
        fatture_fastweb_csv  as b
  where b.ft_soc          = a.ft_soc
    and b.ft_ced_id       = a.ft_ced_id
    and b.ft_deb_id       = a.ft_deb_id
    and b.ft_data_fat     = a.ft_data_fat
    and b.ft_num_fat      = a.ft_num_fat
    and b.ft_attribuzione = a.ft_attribuzione
    and b.ft_imp_fat      = a.ft_imp_fat
    and b.ft_id_csv       < a.ft_id_csv;
 call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase10 Fine'));       
 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upd_fatture_fastweb_notecrgest_csv` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_fatture_fastweb_notecrgest_csv`(in pnomefile_csv varchar(100))
begin

update fatture_fastweb_statilavnote_csv set ftstlav_nomefile_csv = pnomefile_csv;

update fatture_fastweb_statilavnote_csv set ftstlav_ced_id = (select min(ced_id) from cedenti where ced_soc = 1);

update fatture_fastweb_statilavnote_csv set ftstlav_societa = 1;

-- Assegno l'id debitore in base a societ� e codice
update fatture_fastweb_statilavnote_csv 
   set ftstlav_deb_id = (select b.deb_id from debitori as b where b.deb_soc = 1 and b.deb_cod = ftstlav_deb_cod);    
   
-- Assegno l'id data fattura
update fatture_fastweb_statilavnote_csv 
   set ftstlav_id_data_fat = (select b.id from calendario as b where b.databar=ftstlav_data_fat);

-- Assegno l'id stato lavorazione
update fatture_fastweb_statilavnote_csv 
   set ftstlav_id_stato_lav = (select b.id
                                 from stato_lavorazione as b 
                                where b.sl_soc = 1 
                                  and upper(sl_descrizione) = upper(ftstlav_stato_lav))  
 where ftstlav_nomefile_csv = pnomefile_csv;

-- Inserisco nella tabella delle anomali le fatture doppie
 insert into fatture_fastweb_statilavnote_csv_anomalie
 select b.*
   from fatture_fastweb_statilavnote_csv  as a,
        fatture_fastweb_statilavnote_csv  as b
  where b.ftstlav_societa      = a.ftstlav_societa
    and b.ftstlav_ced_id       = a.ftstlav_ced_id
    and b.ftstlav_deb_id       = a.ftstlav_deb_id
    and b.ftstlav_data_fat     = a.ftstlav_data_fat
    and b.ftstlav_num_fat      = a.ftstlav_num_fat
    and b.ftstlav_attribuzione = a.ftstlav_attribuzione
    and b.ftstlav_imp_fat      = a.ftstlav_imp_fat
    and b.ftstlav_id           < a.ftstlav_id;
    
-- Elimino le fatture doppie
delete b 
   from fatture_fastweb_statilavnote_csv  as a,
        fatture_fastweb_statilavnote_csv  as b
    where b.ftstlav_societa    = a.ftstlav_societa
    and b.ftstlav_ced_id       = a.ftstlav_ced_id
    and b.ftstlav_deb_id       = a.ftstlav_deb_id
    and b.ftstlav_data_fat     = a.ftstlav_data_fat
    and b.ftstlav_num_fat      = a.ftstlav_num_fat
    and b.ftstlav_attribuzione = a.ftstlav_attribuzione
    and b.ftstlav_imp_fat      = a.ftstlav_imp_fat
    and b.ftstlav_id           < a.ftstlav_id;
    
-- Assegno l'id fattura
update fatture_fastweb_statilavnote_csv as a
   set a.ftstlav_idfatt = (select b.id 
                            from fatture as b 
                           where b.ft_soc              = a.ftstlav_societa 
                             and b.ft_ced_id           = a.ftstlav_ced_id
                             and b.ft_deb_id           = a.ftstlav_deb_id
                             and b.ft_id_data_fat      = a.ftstlav_id_data_fat
                             and b.ft_num_fat          = a.ftstlav_num_fat
                             and b.ft_attribuzione     = a.ftstlav_attribuzione
                             and b.ft_imp_fat          = a.ftstlav_imp_fat);    
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upd_fatture_mps_csv` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_fatture_mps_csv`(in pnomefile_csv varchar(100))
begin

update fatture_mps_csv set ft_nomefile_csv = pnomefile_csv;

-- Aggiorno la tabella dei cedenti con eventuali nuovi
insert into cedenti (ced_soc,ced_cod,ced_des )
select 2,ft_ced_cod,min(ft_ced_denom) 
  from fatture_mps_csv
 where (2,ft_ced_cod) not in (select b.ced_soc,b.ced_cod from cedenti as b)
 group by ft_ced_cod;

-- Aggiorno la tabella dei debitori con eventuali nuovi
insert into debitori (deb_soc,deb_cod,deb_des)
select 2,ft_deb_cod,min(ft_deb_denom) 
  from fatture_mps_csv
 where (2,ft_deb_cod) not in (select b.deb_soc,b.deb_cod from debitori as b)
 group by ft_deb_cod;

-- Assegno l'id tipo_documento in base a societ� e codice
update fatture_mps_csv 
   set ft_td_id = (select b.td_id from tipo_doc as b where b.td_soc = 2 and b.td_cod = ft_td);

-- Assegno l'id cedente in base a societ� e codice
update fatture_mps_csv 
   set ft_ced_id = (select b.ced_id from cedenti as b where b.ced_soc = 2 and b.ced_cod = ft_ced_cod);

-- Assegno l'id debitore in base a societ� e codice
update fatture_mps_csv 
   set ft_deb_id = (select b.deb_id from debitori as b where b.deb_soc = 2 and b.deb_cod = ft_deb_cod);    
   
-- Assegno l'id data fattura
update fatture_mps_csv 
   set ft_id_data_fat = (select b.id from calendario as b where b.databar=ft_data_fat);
   
-- Assegno l'id data scadenza
update fatture_mps_csv 
   set ft_id_data_scad = (select b.id from calendario as b where b.databar=ft_data_scad);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UPD_FILTRI_PARAMETRI` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `UPD_FILTRI_PARAMETRI`(in pges_id int,in pid_filtro int)
begin
  declare pexist_idfiltro int(1);
  declare pexist_gesid    int(1);
  declare pexist_filpara  int(1);
  if (pid_filtro = 0)
     then call filtro0000_check_exists();
  end if;
  if (pid_filtro =  1)
     then call filtro0001_check_exists();
  end if;
  if (pid_filtro =  2)
     then call filtro0002_check_exists();
  end if;  
  if (pid_filtro =  3)
     then call filtro0003_check_exists();
  end if;  
  if (pid_filtro =  4)
     then call filtro0004_check_exists();
  end if;
  if (pid_filtro =  5)
     then call filtro0005_check_exists();
  end if;
  if (pid_filtro =  6)
     then call filtro0006_check_exists();
  end if;
  if (pid_filtro =  7)
     then call filtro0007_check_exists();
  end if;
  if (pid_filtro =  8)
     then call filtro0008_check_exists();
  end if;  
  if (pid_filtro =  9)
     then call filtro0009_check_exists();
  end if;
  if (pid_filtro = 10)
     then call filtro0010_check_exists();
  end if;
  if (pid_filtro = 11)
     then call filtro0011_check_exists();
  end if;
  select case count(*) when 0 then 0 else 1 end
    into pexist_idfiltro
    from filtri 
   where id_filtro = pid_filtro;
  select case count(*) when 0 then 0 else 1 end
    into pexist_gesid
    from gestori
   where ges_id = pges_id;
  select case count(*) when 0 then 0 else 1 end
    into pexist_filpara
    from filtri_parametri
   where id_filtro = pid_filtro
     and ges_id    = pges_id;
  if (pexist_idfiltro = 1 and
      pexist_gesid    = 1 and
      pexist_filpara  = 0)
      then 
       insert 
         into filtri_parametri (ges_id,id_filtro,par_conn,modifica)
       select pges_id,pid_filtro,null,now();
  end if;
  update filtri_parametri 
     set par_conn   = connection_id(),
         modifica   = now()
   where ges_id     = pges_id
     and id_filtro  = pid_filtro;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UPD_STATIST_MOVIMENTI` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `UPD_STATIST_MOVIMENTI`(in pstatope_id int)
begin
  insert 
    into statist_movimenti 
        (stmov_gesid,
         stmov_opeid,
         stmod_datetime) 
  select a.ges_id,
         pstatope_id,
         now()
    from filtro0000_parametri_v as a;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `vs28ahbz_gecotest`
--

USE `vs28ahbz_gecotest`;

--
-- Final view structure for view `debann_arcdoc_checksino_v`
--

/*!50001 DROP TABLE IF EXISTS `debann_arcdoc_checksino_v`*/;
/*!50001 DROP VIEW IF EXISTS `debann_arcdoc_checksino_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `debann_arcdoc_checksino_v` AS select `debann_arcdoc`.`debandoc_idann` AS `debandoc_idann`,count(0) AS `debandoc_numdoc` from `debann_arcdoc` group by `debann_arcdoc`.`debandoc_idann` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `debann_arcdoc_v`
--

/*!50001 DROP TABLE IF EXISTS `debann_arcdoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `debann_arcdoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `debann_arcdoc_v` AS select `a`.`debandoc_id` AS `debandoc_id`,`a`.`debandoc_idann` AS `debandoc_idann`,`b`.`tbarcdoc_id` AS `debandoc_iddoc`,`b`.`tbarcdoc_estid` AS `debandoc_estid`,`b`.`tbarcdoc_estcod` AS `debandoc_estcod`,`b`.`tbarcdoc_estdes` AS `debandoc_estdes`,`b`.`tbarcdoc_tipoid` AS `debandoc_tipoid`,`b`.`tbarcdoc_tipocod` AS `debandoc_tipocod`,`b`.`tbarcdoc_tipodes` AS `debandoc_tipodes`,`b`.`tbarcdoc_desc` AS `debandoc_desc`,`b`.`tbarcdoc_size` AS `debandoc_size`,`b`.`tbarcdoc_nomefile` AS `debandoc_nomefile`,`a`.`debandoc_ges_id` AS `debandoc_ges_id`,`utypkg_gestid2gestnomin`(`b`.`tbarcdoc_ges_id`) AS `debandoc_ges_nominativo`,`a`.`debandoc_modifica` AS `debandoc_modifica`,`utypkg_datanum2databar`(`a`.`debandoc_modifica`) AS `debandoc_modbar` from (`debann_arcdoc` `a` join `tab_arcdoc_v` `b` on((`b`.`tbarcdoc_id` = `a`.`debandoc_iddoc`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `debann_checksino_v`
--

/*!50001 DROP TABLE IF EXISTS `debann_checksino_v`*/;
/*!50001 DROP VIEW IF EXISTS `debann_checksino_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `debann_checksino_v` AS select `debitori_annotazioni`.`debann_debid` AS `debann_debid`,count(0) AS `debann_numann` from `debitori_annotazioni` group by `debitori_annotazioni`.`debann_debid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `debitori_annotazioni_v`
--

/*!50001 DROP TABLE IF EXISTS `debitori_annotazioni_v`*/;
/*!50001 DROP VIEW IF EXISTS `debitori_annotazioni_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `debitori_annotazioni_v` AS select `a`.`debann_id` AS `debann_id`,`a`.`debann_debid` AS `debann_debid`,`b`.`deb_soc` AS `debann_idsoc`,`b`.`deb_cod` AS `debann_debcod`,`b`.`deb_des` AS `debann_debdes`,`a`.`debann_idtpann` AS `debann_idtpann`,`c`.`dantpan_cod` AS `debann_anncod`,`c`.`dantpan_des` AS `debann_anndes`,`a`.`debann_idtpcon` AS `debann_idtpcon`,`d`.`dantpcon_cod` AS `debann_tpcod`,`d`.`dantpcon_des` AS `debann_tpdes`,`a`.`debann_iddata` AS `debann_iddata`,`e`.`datanum` AS `debann_datanum`,`e`.`databar` AS `debann_databar`,`a`.`debann_annot` AS `debann_annot`,`a`.`debann_interlocutore` AS `debann_inter`,`a`.`debann_ges_id` AS `debann_ges_id`,`utypkg_gestid2gestnomin`(`a`.`debann_ges_id`) AS `debanno_nominativo`,`a`.`debann_modifica` AS `debann_modifica`,`utypkg_datadate2databar`(`a`.`debann_modifica`) AS `debann_modbar` from ((((`debitori_annotazioni` `a` join `debitori` `b` on((`b`.`deb_id` = `a`.`debann_debid`))) join `debann_tipi_annotazione` `c` on((`c`.`dantpan_id` = `a`.`debann_idtpann`))) join `debann_tipi_contatto` `d` on((`d`.`dantpcon_id` = `a`.`debann_idtpcon`))) join `calendario` `e` on((`e`.`id` = `a`.`debann_iddata`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fatture_arcdoc_sino_v`
--

/*!50001 DROP TABLE IF EXISTS `fatture_arcdoc_sino_v`*/;
/*!50001 DROP VIEW IF EXISTS `fatture_arcdoc_sino_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `fatture_arcdoc_sino_v` AS select `ft`.`id` AS `ftarcdoc_idfatt`,(case ifnull(min(`fa`.`ftarcdoc_iddoc`),0) when 0 then 0 else 1 end) AS `ftarcdoc_sino` from (`fatture` `ft` left join `fatture_arcdoc` `fa` on((`fa`.`ftarcdoc_idfatt` = `ft`.`id`))) group by `ft`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fatture_arcdoc_v`
--

/*!50001 DROP TABLE IF EXISTS `fatture_arcdoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `fatture_arcdoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `fatture_arcdoc_v` AS select `a`.`ftarcdoc_id` AS `ftarcdoc_id`,`a`.`ftarcdoc_idfatt` AS `ftarcdoc_idfatt`,`b`.`tbarcdoc_id` AS `ftarcdoc_iddoc`,`b`.`tbarcdoc_estid` AS `ftarcdoc_estid`,`b`.`tbarcdoc_estcod` AS `ftarcdoc_estcod`,`b`.`tbarcdoc_estdes` AS `ftarcdoc_estdes`,`b`.`tbarcdoc_tipoid` AS `ftarcdoc_tipoid`,`b`.`tbarcdoc_tipocod` AS `ftarcdoc_tipocod`,`b`.`tbarcdoc_tipodes` AS `ftarcdoc_tipodes`,`b`.`tbarcdoc_desc` AS `ftarcdoc_desc`,`b`.`tbarcdoc_size` AS `ftarcdoc_size`,`b`.`tbarcdoc_nomefile` AS `ftarcdoc_nomefile`,`b`.`tbarcdoc_ges_id` AS `ftarcdoc_ges_id`,`b`.`tbarcdoc_ges_nominativo` AS `ftarcdoc_ges_nominativo`,`b`.`tbarcdoc_modifica` AS `ftarcdoc_modifica`,`utypkg_datanum2databar`(`b`.`tbarcdoc_modifica`) AS `ftarcdoc_modbar` from (`fatture_arcdoc` `a` join `tab_arcdoc_v` `b` on((`b`.`tbarcdoc_id` = `a`.`ftarcdoc_iddoc`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fatture_notecrgest_v`
--

/*!50001 DROP TABLE IF EXISTS `fatture_notecrgest_v`*/;
/*!50001 DROP VIEW IF EXISTS `fatture_notecrgest_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `fatture_notecrgest_v` AS select `a`.`ftnote_id_nota` AS `ftnote_id_nota`,`a`.`ftnote_id_fattura` AS `ftnote_id_fattura`,`a`.`ftnote_nota_crgest` AS `ftnote_nota_crgest` from `fatture_notecrgest` `a` where (`a`.`ftnote_id_nota` = (select max(`b`.`ftnote_id_nota`) from `fatture_notecrgest` `b` where (`b`.`ftnote_id_fattura` = `a`.`ftnote_id_fattura`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fatture_stati_fattura_v`
--

/*!50001 DROP TABLE IF EXISTS `fatture_stati_fattura_v`*/;
/*!50001 DROP VIEW IF EXISTS `fatture_stati_fattura_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `fatture_stati_fattura_v` AS select `a`.`fsf_id` AS `fsf_id`,`b`.`fs_soc` AS `fsf_soc`,`a`.`fsf_id_fattura` AS `fsf_id_fattura`,`a`.`fsf_id_stato` AS `fsf_id_stato`,`b`.`fs_cod` AS `fsf_cod`,`b`.`fs_desc` AS `fsf_desc`,`a`.`fsf_ges_id` AS `fsf_ges_id`,`a`.`fsf_modifica` AS `fsf_modifica`,`a`.`fsf_nomefile_csv` AS `fsf_nomefile_csv` from (`fatture_stato_fattura` `a` join `fatture_stati` `b` on((`b`.`fs_id` = `a`.`fsf_id_stato`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fatture_statilavnote_v`
--

/*!50001 DROP TABLE IF EXISTS `fatture_statilavnote_v`*/;
/*!50001 DROP VIEW IF EXISTS `fatture_statilavnote_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `fatture_statilavnote_v` AS select `a`.`ftstlav_id` AS `ftstlav_id`,`a`.`ftstlav_id_fattura` AS `ftstlav_id_fattura`,`a`.`ftstlav_id_stato_lav` AS `ftstlav_id_stato_lav`,`b`.`sl_cod` AS `ftstlav_slcod`,`b`.`sl_descrizione` AS `ftstlav_sldes`,`a`.`ftstlav_nota_crgest` AS `ftstlav_nota_crgest`,date_format(`a`.`ftstlav_modifica`,'%d/%m/%Y') AS `ftstlav_data_bar` from (`fatture_statilav_note` `a` join `stato_lavorazione` `b` on((`b`.`id` = `a`.`ftstlav_id_stato_lav`))) where (`a`.`ftstlav_id` = (select max(`c`.`ftstlav_id`) from `fatture_statilav_note` `c` where (`c`.`ftstlav_id_fattura` = `a`.`ftstlav_id_fattura`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fatture_stato_aperta_v`
--

/*!50001 DROP TABLE IF EXISTS `fatture_stato_aperta_v`*/;
/*!50001 DROP VIEW IF EXISTS `fatture_stato_aperta_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `fatture_stato_aperta_v` AS select `a`.`id` AS `fsfa_id_fattura`,`a`.`ft_num_fat` AS `fsfa_num_fat`,`a`.`ft_imp_fat` AS `fsfa_imp_fat`,`b`.`fsf_id_stato` AS `fsfa_id_stato`,`b`.`fsf_ac` AS `fsfa_ac`,`c`.`fs_cod` AS `fsfa_cod_stato`,`c`.`fs_desc` AS `fsfa_des_stato` from ((`fatture` `a` join `fatture_stato_fattura_v` `b` on((`b`.`fsf_id_fattura` = `a`.`id`))) join `fatture_stati` `c` on((`c`.`fs_id` = `b`.`fsf_id_stato`))) where (`b`.`fsf_ac` = 'A') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fatture_stato_fattura_v`
--

/*!50001 DROP TABLE IF EXISTS `fatture_stato_fattura_v`*/;
/*!50001 DROP VIEW IF EXISTS `fatture_stato_fattura_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `fatture_stato_fattura_v` AS select `a`.`fsf_id` AS `fsf_id`,`a`.`fsf_soc` AS `fsf_soc`,`a`.`fsf_cod` AS `fsf_cod`,`a`.`fsf_id_fattura` AS `fsf_id_fattura`,`a`.`fsf_id_stato` AS `fsf_id_stato`,(case `a`.`fsf_id_stato` when 1 then 0 when 2 then 1 when 3 then 0 when 4 then 1 end) AS `fsf_ac`,`a`.`fsf_desc` AS `fsf_desc`,`a`.`fsf_ges_id` AS `fsf_ges_id`,`a`.`fsf_modifica` AS `fsf_modifica`,`a`.`fsf_nomefile_csv` AS `fsf_nomefile_csv` from `fatture_stati_fattura_v` `a` where (`a`.`fsf_id_fattura`,`a`.`fsf_id`) in (select `b`.`fsf_id_fattura`,max(`b`.`fsf_id`) from `fatture_stato_fattura` `b` group by `b`.`fsf_id_fattura`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fatture_tabsr_v`
--

/*!50001 DROP TABLE IF EXISTS `fatture_tabsr_v`*/;
/*!50001 DROP VIEW IF EXISTS `fatture_tabsr_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `fatture_tabsr_v` AS select `a`.`fttbsr_id` AS `fttbsr_id`,`a`.`fttbsr_idfatt` AS `fttbsr_idfatt`,`a`.`fttbsr_idsr` AS `fttbsr_idsr`,`b`.`tbsr_numero` AS `fttbsr_numero`,`b`.`tbsr_nota` AS `fttbsr_nota`,`UTYPKG_GESTID2GESTNOMIN`(`a`.`fttbsr_ges_id`) AS `fttbsr_ges_nominativo`,`a`.`fttbsr_modifica` AS `fttbsr_modifica`,`UTYPKG_DATADATE2DATABAR`(`a`.`fttbsr_modifica`) AS `fttbsr_modbar` from ((`fatture_tabsr` `a` join `tab_sr` `b` on((`b`.`tbsr_id` = `a`.`fttbsr_idsr`))) join `tabsr_stati` `c` on((`c`.`tbsrst_id` = `b`.`tbsr_stato`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fatture_v`
--

/*!50001 DROP TABLE IF EXISTS `fatture_v`*/;
/*!50001 DROP VIEW IF EXISTS `fatture_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `fatture_v` AS select `a`.`id` AS `id`,`a`.`ft_soc` AS `soc_cod`,`b`.`soc_des` AS `soc_des`,`c`.`deb_id` AS `deb_id`,`a`.`ft_deb_id` AS `cod_deb`,`c`.`deb_cod` AS `deb_cod`,`c`.`deb_des` AS `deb_des`,`a`.`ft_ced_id` AS `ced_id`,`d`.`ced_des` AS `ced_des`,`a`.`ft_num_fat` AS `num_fat`,`a`.`ft_attribuzione` AS `attribuzione`,`a`.`ft_td` AS `tipo_doc_cod`,`e`.`td_des` AS `tipo_doc_des`,`a`.`ft_testo` AS `testo`,`a`.`ft_data_fat` AS `data_fat`,`a`.`ft_id_data_fat` AS `id_data_fat`,`a`.`ft_data_scad` AS `data_scad`,`a`.`ft_imp_fat` AS `imp_fat`,`a`.`ft_imp_aperto` AS `imp_aperto`,`a`.`ft_data_ins` AS `data_ins`,`a`.`ft_id_stato_lav` AS `id_stato_lav_cod`,ifnull(`g`.`fs_cod`,0) AS `cod_stato_fat`,ifnull(`g`.`fs_desc`,'Nessuno') AS `des_stato_fat`,`a`.`ft_data_chius` AS `data_chius`,`a`.`ft_id_data_chius` AS `id_data_chius`,`a`.`ft_nomefile_csv` AS `ft_nomefile_csv`,`f`.`fsf_nomefile_csv` AS `fsf_nomefile_csv` from ((((((`fatture` `a` join `societa` `b` on((`b`.`soc_cod` = `a`.`ft_soc`))) join `debitori` `c` on((`c`.`deb_id` = `a`.`ft_deb_id`))) join `cedenti` `d` on((`d`.`ced_id` = `a`.`ft_ced_id`))) join `tipo_doc` `e` on((`e`.`td_id` = `a`.`ft_td_id`))) left join `fatture_stato_fattura_v` `f` on((`f`.`fsf_id_fattura` = `a`.`id`))) left join `fatture_stati` `g` on((`g`.`fs_id` = `f`.`fsf_id_stato`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtri_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtri_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtri_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtri_parametri_v` AS select `filtri_parametri`.`ges_id` AS `ges_id`,`filtri_parametri`.`id_filtro` AS `id_filtro`,`filtri_parametri`.`par_conn` AS `par_conn`,`filtri_parametri`.`parvar01` AS `parvar01`,`filtri_parametri`.`parvar02` AS `parvar02`,`filtri_parametri`.`parvar03` AS `parvar03`,`filtri_parametri`.`parvar04` AS `parvar04`,`filtri_parametri`.`parvar05` AS `parvar05`,`filtri_parametri`.`parvar06` AS `parvar06`,`filtri_parametri`.`parvar07` AS `parvar07`,`filtri_parametri`.`parvar08` AS `parvar08`,`filtri_parametri`.`parvar09` AS `parvar09`,`filtri_parametri`.`parvar10` AS `parvar10`,`filtri_parametri`.`parvar11` AS `parvar11`,`filtri_parametri`.`parvar12` AS `parvar12`,`filtri_parametri`.`parvar13` AS `parvar13`,`filtri_parametri`.`parvar14` AS `parvar14`,`filtri_parametri`.`parvar15` AS `parvar15`,`filtri_parametri`.`parvar16` AS `parvar16`,`filtri_parametri`.`parvar17` AS `parvar17`,`filtri_parametri`.`parvar18` AS `parvar18`,`filtri_parametri`.`parvar19` AS `parvar19`,`filtri_parametri`.`parvar20` AS `parvar20`,`filtri_parametri`.`parvar21` AS `parvar21`,`filtri_parametri`.`parvar22` AS `parvar22`,`filtri_parametri`.`parvar23` AS `parvar23`,`filtri_parametri`.`parvar24` AS `parvar24`,`filtri_parametri`.`parvar25` AS `parvar25`,`filtri_parametri`.`parvar26` AS `parvar26`,`filtri_parametri`.`parvar27` AS `parvar27`,`filtri_parametri`.`parvar28` AS `parvar28`,`filtri_parametri`.`parvar29` AS `parvar29`,`filtri_parametri`.`parvar30` AS `parvar30`,`filtri_parametri`.`parint01` AS `parint01`,`filtri_parametri`.`parint02` AS `parint02`,`filtri_parametri`.`parint03` AS `parint03`,`filtri_parametri`.`parint04` AS `parint04`,`filtri_parametri`.`parint05` AS `parint05`,`filtri_parametri`.`parint06` AS `parint06`,`filtri_parametri`.`parint07` AS `parint07`,`filtri_parametri`.`parint08` AS `parint08`,`filtri_parametri`.`parint09` AS `parint09`,`filtri_parametri`.`parint10` AS `parint10`,`filtri_parametri`.`parint11` AS `parint11`,`filtri_parametri`.`parint12` AS `parint12`,`filtri_parametri`.`parint13` AS `parint13`,`filtri_parametri`.`parint14` AS `parint14`,`filtri_parametri`.`parint15` AS `parint15`,`filtri_parametri`.`parint16` AS `parint16`,`filtri_parametri`.`parint17` AS `parint17`,`filtri_parametri`.`parint18` AS `parint18`,`filtri_parametri`.`parint19` AS `parint19`,`filtri_parametri`.`parint20` AS `parint20`,`filtri_parametri`.`parint21` AS `parint21`,`filtri_parametri`.`parint22` AS `parint22`,`filtri_parametri`.`parint23` AS `parint23`,`filtri_parametri`.`parint24` AS `parint24`,`filtri_parametri`.`parint25` AS `parint25`,`filtri_parametri`.`parint26` AS `parint26`,`filtri_parametri`.`parint27` AS `parint27`,`filtri_parametri`.`parint28` AS `parint28`,`filtri_parametri`.`parint29` AS `parint29`,`filtri_parametri`.`parint30` AS `parint30`,`filtri_parametri`.`pardat01` AS `pardat01`,`filtri_parametri`.`pardat02` AS `pardat02`,`filtri_parametri`.`pardat03` AS `pardat03`,`filtri_parametri`.`pardat04` AS `pardat04`,`filtri_parametri`.`pardat05` AS `pardat05`,`filtri_parametri`.`pardat06` AS `pardat06`,`filtri_parametri`.`pardat07` AS `pardat07`,`filtri_parametri`.`pardat08` AS `pardat08`,`filtri_parametri`.`pardat09` AS `pardat09`,`filtri_parametri`.`pardat10` AS `pardat10`,`filtri_parametri`.`operatore` AS `operatore`,`filtri_parametri`.`modifica` AS `modifica` from `filtri_parametri` where (`filtri_parametri`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0000_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0000_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0000_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0000_parametri_v` AS select `filtri_parametri_v`.`ges_id` AS `ges_id`,`filtri_parametri_v`.`id_filtro` AS `id_filtro`,`filtri_parametri_v`.`par_conn` AS `par_conn`,`filtri_parametri_v`.`parvar01` AS `parvar01`,`filtri_parametri_v`.`parvar02` AS `parvar02`,`filtri_parametri_v`.`parvar03` AS `parvar03`,`filtri_parametri_v`.`parvar04` AS `parvar04`,`filtri_parametri_v`.`parvar05` AS `parvar05`,`filtri_parametri_v`.`parvar06` AS `parvar06`,`filtri_parametri_v`.`parvar07` AS `parvar07`,`filtri_parametri_v`.`parvar08` AS `parvar08`,`filtri_parametri_v`.`parvar09` AS `parvar09`,`filtri_parametri_v`.`parvar10` AS `parvar10`,`filtri_parametri_v`.`parint01` AS `parint01`,`filtri_parametri_v`.`parint02` AS `parint02`,`filtri_parametri_v`.`parint03` AS `parint03`,`filtri_parametri_v`.`parint04` AS `parint04`,`filtri_parametri_v`.`parint05` AS `parint05`,`filtri_parametri_v`.`parint06` AS `parint06`,`filtri_parametri_v`.`parint07` AS `parint07`,`filtri_parametri_v`.`parint08` AS `parint08`,`filtri_parametri_v`.`parint09` AS `parint09`,`filtri_parametri_v`.`parint10` AS `parint10`,`filtri_parametri_v`.`pardat01` AS `pardat01`,`filtri_parametri_v`.`pardat02` AS `pardat02`,`filtri_parametri_v`.`pardat03` AS `pardat03`,`filtri_parametri_v`.`pardat04` AS `pardat04`,`filtri_parametri_v`.`pardat05` AS `pardat05`,`filtri_parametri_v`.`pardat06` AS `pardat06`,`filtri_parametri_v`.`pardat07` AS `pardat07`,`filtri_parametri_v`.`pardat08` AS `pardat08`,`filtri_parametri_v`.`pardat09` AS `pardat09`,`filtri_parametri_v`.`pardat10` AS `pardat10`,`filtri_parametri_v`.`operatore` AS `operatore`,`filtri_parametri_v`.`modifica` AS `modifica` from `filtri_parametri_v` where (`filtri_parametri_v`.`id_filtro` = 0) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_debitori_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_debitori_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_debitori_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_debitori_v` AS select 0 AS `deb_id`,'Tutti' AS `deb_des` union all select `b`.`gd_deb_id` AS `deb_id`,concat(`c`.`deb_cod`,'-',`c`.`deb_des`) AS `deb_des` from ((`filtro0001_parametri_v` `a` join `gest_debitore` `b` on((`b`.`gd_ges_id` = `a`.`ges_id`))) join `debitori` `c` on(((`c`.`deb_soc` = ifnull(`a`.`parint01`,`c`.`deb_soc`)) and (`c`.`deb_id` = `b`.`gd_deb_id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_get_datafat_a_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_get_datafat_a_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_get_datafat_a_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_get_datafat_a_v` AS select ifnull(`b`.`data`,'gg/mm/aaaa') AS `datafat_a_v` from (`filtro0001_parametri_v` `a` join `calendario` `b` on((`b`.`id` = ifnull(`a`.`parint06`,0)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_get_datafat_da_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_get_datafat_da_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_get_datafat_da_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_get_datafat_da_v` AS select ifnull(`b`.`data`,'gg/mm/aaaa') AS `datafat_da_v` from (`filtro0001_parametri_v` `a` join `calendario` `b` on((`b`.`id` = ifnull(`a`.`parint05`,0)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_get_datascad_a_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_get_datascad_a_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_get_datascad_a_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_get_datascad_a_v` AS select ifnull(`b`.`data`,'gg/mm/aaaa') AS `datascad_a_v` from (`filtro0001_parametri_v` `a` join `calendario` `b` on((`b`.`id` = ifnull(`a`.`parint08`,0)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_get_datascad_da_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_get_datascad_da_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_get_datascad_da_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_get_datascad_da_v` AS select ifnull(`b`.`data`,'gg/mm/aaaa') AS `datascad_da_v` from (`filtro0001_parametri_v` `a` join `calendario` `b` on((`b`.`id` = ifnull(`a`.`parint07`,0)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_get_tipodoc_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_get_tipodoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_get_tipodoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_get_tipodoc_v` AS select `b`.`td_id` AS `td_id`,`b`.`td_des` AS `td_des` from (`filtro0001_parametri_v` `a` join `filtro0001_tipodoc_v` `b` on((`b`.`td_id` = ifnull(`a`.`parint02`,0)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_parametri_v` AS select `filtri_parametri_v`.`ges_id` AS `ges_id`,`filtri_parametri_v`.`id_filtro` AS `id_filtro`,`filtri_parametri_v`.`par_conn` AS `par_conn`,`filtri_parametri_v`.`parvar01` AS `parvar01`,`filtri_parametri_v`.`parvar02` AS `parvar02`,`filtri_parametri_v`.`parvar03` AS `parvar03`,`filtri_parametri_v`.`parvar04` AS `parvar04`,`filtri_parametri_v`.`parvar05` AS `parvar05`,`filtri_parametri_v`.`parvar06` AS `parvar06`,`filtri_parametri_v`.`parvar07` AS `parvar07`,`filtri_parametri_v`.`parvar08` AS `parvar08`,`filtri_parametri_v`.`parvar09` AS `parvar09`,`filtri_parametri_v`.`parvar10` AS `parvar10`,`filtri_parametri_v`.`parvar11` AS `parvar11`,`filtri_parametri_v`.`parvar12` AS `parvar12`,`filtri_parametri_v`.`parvar13` AS `parvar13`,`filtri_parametri_v`.`parvar14` AS `parvar14`,`filtri_parametri_v`.`parvar15` AS `parvar15`,`filtri_parametri_v`.`parvar16` AS `parvar16`,`filtri_parametri_v`.`parvar17` AS `parvar17`,`filtri_parametri_v`.`parvar18` AS `parvar18`,`filtri_parametri_v`.`parvar19` AS `parvar19`,`filtri_parametri_v`.`parvar20` AS `parvar20`,`filtri_parametri_v`.`parvar21` AS `parvar21`,`filtri_parametri_v`.`parvar22` AS `parvar22`,`filtri_parametri_v`.`parvar23` AS `parvar23`,`filtri_parametri_v`.`parvar24` AS `parvar24`,`filtri_parametri_v`.`parvar25` AS `parvar25`,`filtri_parametri_v`.`parvar26` AS `parvar26`,`filtri_parametri_v`.`parvar27` AS `parvar27`,`filtri_parametri_v`.`parvar28` AS `parvar28`,`filtri_parametri_v`.`parvar29` AS `parvar29`,`filtri_parametri_v`.`parvar30` AS `parvar30`,`filtri_parametri_v`.`parint01` AS `parint01`,`filtri_parametri_v`.`parint02` AS `parint02`,`filtri_parametri_v`.`parint03` AS `parint03`,`filtri_parametri_v`.`parint04` AS `parint04`,`filtri_parametri_v`.`parint05` AS `parint05`,`filtri_parametri_v`.`parint06` AS `parint06`,`filtri_parametri_v`.`parint07` AS `parint07`,`filtri_parametri_v`.`parint08` AS `parint08`,`filtri_parametri_v`.`parint09` AS `parint09`,`filtri_parametri_v`.`parint10` AS `parint10`,`filtri_parametri_v`.`parint11` AS `parint11`,`filtri_parametri_v`.`parint12` AS `parint12`,`filtri_parametri_v`.`parint13` AS `parint13`,`filtri_parametri_v`.`parint14` AS `parint14`,`filtri_parametri_v`.`parint15` AS `parint15`,`filtri_parametri_v`.`parint16` AS `parint16`,`filtri_parametri_v`.`parint17` AS `parint17`,`filtri_parametri_v`.`parint18` AS `parint18`,`filtri_parametri_v`.`parint19` AS `parint19`,`filtri_parametri_v`.`parint20` AS `parint20`,`filtri_parametri_v`.`parint21` AS `parint21`,`filtri_parametri_v`.`parint22` AS `parint22`,`filtri_parametri_v`.`parint23` AS `parint23`,`filtri_parametri_v`.`parint24` AS `parint24`,`filtri_parametri_v`.`parint25` AS `parint25`,`filtri_parametri_v`.`parint26` AS `parint26`,`filtri_parametri_v`.`parint27` AS `parint27`,`filtri_parametri_v`.`parint28` AS `parint28`,`filtri_parametri_v`.`parint29` AS `parint29`,`filtri_parametri_v`.`parint30` AS `parint30`,`filtri_parametri_v`.`pardat01` AS `pardat01`,`filtri_parametri_v`.`pardat02` AS `pardat02`,`filtri_parametri_v`.`pardat03` AS `pardat03`,`filtri_parametri_v`.`pardat04` AS `pardat04`,`filtri_parametri_v`.`pardat05` AS `pardat05`,`filtri_parametri_v`.`pardat06` AS `pardat06`,`filtri_parametri_v`.`pardat07` AS `pardat07`,`filtri_parametri_v`.`pardat08` AS `pardat08`,`filtri_parametri_v`.`pardat09` AS `pardat09`,`filtri_parametri_v`.`pardat10` AS `pardat10`,`filtri_parametri_v`.`operatore` AS `operatore`,`filtri_parametri_v`.`modifica` AS `modifica` from `filtri_parametri_v` where (`filtri_parametri_v`.`id_filtro` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_societa_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_societa_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_societa_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_societa_v` AS select `c`.`soc_cod` AS `soc_cod`,`c`.`soc_des` AS `soc_des` from ((((`filtri_parametri_v` `upd` join `fosuser_gestori` `fug` on((`fug`.`fosuser_gestid` = `upd`.`ges_id`))) join `gest_debitore` `a` on((`a`.`gd_ges_id` = `fug`.`fosuser_gestid`))) join `debitori` `b` on((`b`.`deb_id` = `a`.`gd_deb_id`))) join `societa` `c` on((`c`.`soc_cod` = `b`.`deb_soc`))) group by `c`.`soc_cod`,`c`.`soc_des` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_statolav_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_statolav_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_statolav_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_statolav_v` AS select 0 AS `sl_id`,'Tutti' AS `sl_des` union all select `b`.`id` AS `sl_id`,substr(concat(`b`.`sl_cod`,'-',`b`.`sl_descrizione`),1,30) AS `sl_des` from (`filtro0001_parametri_v` `a` join `stato_lavorazione` `b` on((`b`.`sl_soc` = ifnull(`a`.`parint01`,`b`.`sl_soc`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_tipoarcdoc_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_tipoarcdoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_tipoarcdoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_tipoarcdoc_v` AS select `a`.`tparcdoc_id` AS `tparcdoc_id`,`a`.`tparcdoc_soc` AS `tparcdoc_soc`,`a`.`tparcdoc_socdes` AS `tparcdoc_socdes`,`a`.`tparcdoc_cod` AS `tparcdoc_cod`,`a`.`tparcdoc_des` AS `tparcdoc_des`,`a`.`tparcdoc_ges_id` AS `tparcdoc_ges_id`,`a`.`tparcdoc_gesnome` AS `tparcdoc_gesnome`,`a`.`tparcdoc_modifica` AS `tparcdoc_modifica` from `tipo_arcdoc_v` `a` where (`a`.`tparcdoc_soc` = `filtro0001_get_societa`()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0001_tipodoc_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0001_tipodoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0001_tipodoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0001_tipodoc_v` AS select 0 AS `td_id`,'Tutti' AS `td_des` union all select `b`.`td_id` AS `td_id`,`b`.`td_cod` AS `td_des` from (`filtro0001_parametri_v` `a` join `tipo_doc` `b` on((`b`.`td_soc` = ifnull(`a`.`parint01`,`b`.`td_soc`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0002_liv1_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0002_liv1_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0002_liv1_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0002_liv1_v` AS select 0 AS `lv1_id`,'Tutti' AS `lv1_des` union all select `a`.`eleord_id` AS `lv1_id`,`a`.`eleord_des` AS `lv1_des` from `filtro0002_eleord` `a` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0002_liv2_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0002_liv2_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0002_liv2_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0002_liv2_v` AS select 0 AS `lv2_id`,'Tutti' AS `lv2_des` union all select `a`.`eleord_id` AS `lv2_id`,`a`.`eleord_des` AS `lv2_des` from `filtro0002_eleord` `a` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0002_liv3_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0002_liv3_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0002_liv3_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0002_liv3_v` AS select 0 AS `lv3_id`,'Tutti' AS `lv3_des` union all select `a`.`eleord_id` AS `lv3_id`,`a`.`eleord_des` AS `lv3_des` from `filtro0002_eleord` `a` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_elenco_pagine_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_elenco_pagine_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_elenco_pagine_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_elenco_pagine_v` AS select 1 AS `seq`,'&laquo;' AS `valore` union select 2 AS `seq`,'&lsaquo;' AS `valore` union select 3 AS `seq`,'1' AS `valore` union select 4 AS `seq`,'2' AS `valore` union select 5 AS `seq`,'3' AS `valore` union select 6 AS `seq`,'4' AS `valore` union select 7 AS `seq`,'5' AS `valore` union select 8 AS `seq`,'&rsaquo;' AS `valore` union select 9 AS `seq`,'&raquo;' AS `valore` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_get_idrecperpag_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_get_idrecperpag_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_idrecperpag_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_get_idrecperpag_v` AS select `filtro0003_parconn_v`.`idrecperpag` AS `idrecperpag` from `filtro0003_parconn_v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_get_limfin_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_get_limfin_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_limfin_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_get_limfin_v` AS select `filtro0003_parconn_v`.`numpagesp` AS `numpagesp` from `filtro0003_parconn_v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_get_limini_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_get_limini_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_limini_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_get_limini_v` AS select `filtro0003_parconn_v`.`limini` AS `limini` from `filtro0003_parconn_v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_get_numpagcorr_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_get_numpagcorr_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_numpagcorr_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_get_numpagcorr_v` AS select `filtro0003_parconn_v`.`numpagcorr` AS `numpagcorr` from `filtro0003_parconn_v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_get_numpagesp_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_get_numpagesp_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_numpagesp_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_get_numpagesp_v` AS select `filtro0003_parconn_v`.`numpagesp` AS `numpagesp` from `filtro0003_parconn_v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_get_numpagtot_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_get_numpagtot_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_numpagtot_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_get_numpagtot_v` AS select `filtro0003_parconn_v`.`numpagtot` AS `numpagtot` from `filtro0003_parconn_v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_get_numposcorr_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_get_numposcorr_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_numposcorr_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_get_numposcorr_v` AS select `filtro0003_parconn_v`.`numposcorr` AS `numposcorr` from `filtro0003_parconn_v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_get_numrectot_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_get_numrectot_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_numrectot_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_get_numrectot_v` AS select `filtro0003_parconn_v`.`numrectot` AS `numrectot` from `filtro0003_parconn_v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_get_recperpag_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_get_recperpag_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_recperpag_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_get_recperpag_v` AS select `filtro0003_parconn_v`.`valrecperpag` AS `valrecperpag` from `filtro0003_parconn_v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_get_valrecperpag_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_get_valrecperpag_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_get_valrecperpag_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_get_valrecperpag_v` AS select `filtro0003_parconn_v`.`valrecperpag` AS `valrecperpag` from `filtro0003_parconn_v` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_parametri_v` AS select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`d`.`id` AS `idrecperpag`,(case `d`.`valore` when 0 then (case `b`.`parint02` when (`b`.`parint02` <= `e`.`maxrecperpag`) then `b`.`parint02` else `e`.`maxrecperpag` end) else `d`.`valore` end) AS `valrecperpag`,`d`.`descrizione` AS `desrecperpag`,`b`.`parint02` AS `numrectot`,`b`.`parint03` AS `numpagtot`,`b`.`parint04` AS `numpagcorr`,`b`.`parint05` AS `numpagesp`,`b`.`parint06` AS `limini`,`b`.`parint07` AS `limfin`,`b`.`parint08` AS `numposcorr` from ((((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) join `filtro0003_valori_default` `e`) join `filtro0003_elementi_pagina` `d` on((`d`.`id` = ifnull(`b`.`parint09`,`e`.`idrecperpag`)))) where (`a`.`id_filtro` = 3) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_parconn_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_parconn_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_parconn_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_parconn_v` AS select `filtro0003_parametri_v`.`ges_id` AS `ges_id`,`filtro0003_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0003_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0003_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0003_parametri_v`.`par_conn` AS `par_conn`,`filtro0003_parametri_v`.`idrecperpag` AS `idrecperpag`,`filtro0003_parametri_v`.`valrecperpag` AS `valrecperpag`,`filtro0003_parametri_v`.`desrecperpag` AS `desrecperpag`,`filtro0003_parametri_v`.`numrectot` AS `numrectot`,`filtro0003_parametri_v`.`numpagtot` AS `numpagtot`,`filtro0003_parametri_v`.`numpagcorr` AS `numpagcorr`,`filtro0003_parametri_v`.`numpagesp` AS `numpagesp`,`filtro0003_parametri_v`.`limini` AS `limini`,`filtro0003_parametri_v`.`limfin` AS `limfin`,`filtro0003_parametri_v`.`numposcorr` AS `numposcorr` from `filtro0003_parametri_v` where (`filtro0003_parametri_v`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0003_range_pagine_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0003_range_pagine_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0003_range_pagine_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0003_range_pagine_v` AS select `b`.`id_pagina` AS `valore`,`a`.`numpagcorr` AS `numpagcorr`,`a`.`numposcorr` AS `numposcorr`,(`a`.`numposcorr` - 1) AS `sinistra`,(`a`.`numpagesp` - `a`.`numposcorr`) AS `destra`,(`a`.`numpagcorr` - (`a`.`numposcorr` - 1)) AS `valsin`,(`a`.`numpagcorr` + (`a`.`numpagesp` - `a`.`numposcorr`)) AS `valdes`,`a`.`numpagtot` AS `numpagtot`,`a`.`numpagesp` AS `numpagesp` from (`filtro0003_parconn_v` `a` join `filtro0003_pagine` `b` on((`b`.`id_pagina` between (`a`.`numpagcorr` - (`a`.`numposcorr` - 1)) and (`a`.`numpagcorr` + (`a`.`numpagesp` - `a`.`numposcorr`))))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0004_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0004_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0004_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0004_parametri_v` AS select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_lastid` from ((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) where (`a`.`id_filtro` = 4) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0004_parconn_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0004_parconn_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0004_parconn_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0004_parconn_v` AS select `filtro0004_parametri_v`.`ges_id` AS `ges_id`,`filtro0004_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0004_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0004_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0004_parametri_v`.`par_conn` AS `par_conn`,`filtro0004_parametri_v`.`par_lastid` AS `par_lastid` from `filtro0004_parametri_v` where (`filtro0004_parametri_v`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0005_elearcdoc_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0005_elearcdoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0005_elearcdoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0005_elearcdoc_v` AS select `b`.`ftarcdoc_id` AS `elearcdoc_id`,`b`.`ftarcdoc_idfatt` AS `elearcdoc_idfatt`,`b`.`ftarcdoc_iddoc` AS `elearcdoc_iddoc`,`b`.`ftarcdoc_estid` AS `elearcdoc_estid`,`b`.`ftarcdoc_estcod` AS `elearcdoc_estcod`,`b`.`ftarcdoc_estdes` AS `elearcdoc_estdes`,`b`.`ftarcdoc_tipoid` AS `elearcdoc_tipoid`,`b`.`ftarcdoc_tipocod` AS `elearcdoc_tipocod`,`b`.`ftarcdoc_tipodes` AS `elearcdoc_tipodes`,`b`.`ftarcdoc_desc` AS `elearcdoc_desc`,`b`.`ftarcdoc_size` AS `elearcdoc_size`,`b`.`ftarcdoc_nomefile` AS `elearcdoc_nomefile`,`b`.`ftarcdoc_ges_id` AS `elearcdoc_ges_id`,`b`.`ftarcdoc_ges_nominativo` AS `elearcdoc_ges_nominativo`,`b`.`ftarcdoc_modifica` AS `elearcdoc_modifica`,`b`.`ftarcdoc_modifica` AS `elearcdoc_modbar` from (`filtro0005_parconn_v` `a` join `fatture_arcdoc_v` `b` on((`b`.`ftarcdoc_idfatt` = `a`.`par_idfatt`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0005_fatture_arcdoc_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0005_fatture_arcdoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0005_fatture_arcdoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0005_fatture_arcdoc_v` AS select `b`.`ftarcdoc_id` AS `ftarcdoc_id`,`b`.`ftarcdoc_idfatt` AS `ftarcdoc_idfatt`,`b`.`ftarcdoc_iddoc` AS `ftarcdoc_iddoc`,`b`.`ftarcdoc_estid` AS `ftarcdoc_estid`,`b`.`ftarcdoc_estcod` AS `ftarcdoc_estcod`,`b`.`ftarcdoc_estdes` AS `ftarcdoc_estdes`,`b`.`ftarcdoc_tipoid` AS `ftarcdoc_tipoid`,`b`.`ftarcdoc_tipocod` AS `ftarcdoc_tipocod`,`b`.`ftarcdoc_tipodes` AS `ftarcdoc_tipodes`,`b`.`ftarcdoc_desc` AS `ftarcdoc_desc`,`b`.`ftarcdoc_size` AS `ftarcdoc_size`,`b`.`ftarcdoc_nomefile` AS `ftarcdoc_nomefile`,`b`.`ftarcdoc_ges_id` AS `ftarcdoc_ges_id`,`b`.`ftarcdoc_ges_nominativo` AS `ftarcdoc_ges_nominativo`,`b`.`ftarcdoc_modifica` AS `ftarcdoc_modifica`,`b`.`ftarcdoc_modifica` AS `ftarcdoc_modbar` from (`filtro0005_parconn_v` `a` join `fatture_arcdoc_v` `b` on((`b`.`ftarcdoc_idfatt` = `a`.`par_idfatt`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0005_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0005_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0005_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0005_parametri_v` AS select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_idfatt` from ((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) where (`a`.`id_filtro` = 5) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0005_parconn_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0005_parconn_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0005_parconn_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0005_parconn_v` AS select `filtro0005_parametri_v`.`ges_id` AS `ges_id`,`filtro0005_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0005_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0005_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0005_parametri_v`.`par_conn` AS `par_conn`,`filtro0005_parametri_v`.`par_idfatt` AS `par_idfatt` from `filtro0005_parametri_v` where (`filtro0005_parametri_v`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0006_debann_tpann_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0006_debann_tpann_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0006_debann_tpann_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0006_debann_tpann_v` AS select `a`.`dantpan_id` AS `dantpan_id`,`a`.`dantpan_idsoc` AS `dantpan_idsoc`,`a`.`dantpan_cod` AS `dantpan_cod`,`a`.`dantpan_des` AS `dantpan_des`,`a`.`dantpan_ges_id` AS `dantpan_gesid`,`utypkg_gestid2gestnomin`(`a`.`dantpan_ges_id`) AS `dantpan_nominativo`,`a`.`dantpan_modifica` AS `dantpan_modifica`,`utypkg_datadate2databar`(`a`.`dantpan_modifica`) AS `dantpan_modbar` from `debann_tipi_annotazione` `a` where (`a`.`dantpan_idsoc` = `filtro0001_get_societa`()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0006_debann_tpcon_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0006_debann_tpcon_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0006_debann_tpcon_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0006_debann_tpcon_v` AS select `a`.`dantpcon_id` AS `dantpcon_id`,`a`.`dantpcon_idsoc` AS `dantpcon_idsoc`,`a`.`dantpcon_cod` AS `dantpcon_cod`,`a`.`dantpcon_des` AS `dantpcon_des`,`a`.`dantpcon_ges_id` AS `dantpcon_gesid`,`utypkg_gestid2gestnomin`(`a`.`dantpcon_ges_id`) AS `dantpcon_nominativo`,`a`.`dantpcon_modifica` AS `dantpcon_modifica`,`utypkg_datadate2databar`(`a`.`dantpcon_modifica`) AS `dantpcon_modbar` from `debann_tipi_contatto` `a` where (`a`.`dantpcon_idsoc` = `filtro0001_get_societa`()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0006_debann_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0006_debann_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0006_debann_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0006_debann_v` AS select `a`.`debann_id` AS `debann_id`,`a`.`debann_debid` AS `deann_debid`,`a`.`debann_idsoc` AS `debann_idsoc`,`a`.`debann_debcod` AS `debann_debcod`,`a`.`debann_debdes` AS `debann_debdes`,`a`.`debann_idtpann` AS `debann_idtpann`,`a`.`debann_anncod` AS `debann_anncod`,`a`.`debann_anndes` AS `debann_anndes`,`a`.`debann_idtpcon` AS `debann_idtpcon`,`a`.`debann_tpcod` AS `debann_tpcod`,`a`.`debann_tpdes` AS `debann_tpdes`,`a`.`debann_iddata` AS `debann_iddata`,`a`.`debann_datanum` AS `debann_datanum`,`a`.`debann_databar` AS `debann_databar`,`a`.`debann_annot` AS `debann_annot`,`a`.`debann_inter` AS `debann_inter`,`a`.`debann_ges_id` AS `debann_ges_id`,`a`.`debanno_nominativo` AS `debanno_nominativo`,`a`.`debann_modifica` AS `debann_modifica`,`a`.`debann_modbar` AS `debann_modbar`,ifnull(`b`.`debandoc_numdoc`,0) AS `debann_numdoc`,(case ifnull(`b`.`debandoc_numdoc`,0) when 0 then 'N' else 'S' end) AS `debann_doc_checkdocsino` from (((((`filtro0006_parconn_v` `par` join `societa` `soc` on((`soc`.`soc_cod` = ifnull(`filtro0001_get_societa`(),`soc`.`soc_cod`)))) join `gest_debitore` `gd` on((`gd`.`gd_ges_id` = `filtro0000_get_gesid`()))) join `debitori` `d` on(((`d`.`deb_soc` = `soc`.`soc_cod`) and (`d`.`deb_id` = `gd`.`gd_deb_id`) and (`d`.`deb_id` = ifnull(`par`.`par_debid`,`d`.`deb_id`))))) join `debitori_annotazioni_v` `a` on(((`a`.`debann_idsoc` = `soc`.`soc_cod`) and (`a`.`debann_debid` = `d`.`deb_id`)))) left join `debann_arcdoc_checksino_v` `b` on((`b`.`debandoc_idann` = `a`.`debann_id`))) order by `a`.`debann_iddata` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0006_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0006_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0006_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0006_parametri_v` AS select `b`.`ges_id` AS `ges_id`,`utypkg_gestid2gestnomin`(`b`.`ges_id`) AS `ges_nominativo`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_debid`,`b`.`parint02` AS `par_lastid`,`utypkg_gestid2gestnomin`(`b`.`operatore`) AS `ope_nominativo`,`UTYPKG_DATADATE2DATABAR`(`b`.`modifica`) AS `modibar` from (`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) where (`a`.`id_filtro` = 6) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0006_parconn_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0006_parconn_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0006_parconn_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0006_parconn_v` AS select `filtro0006_parametri_v`.`ges_id` AS `ges_id`,`filtro0006_parametri_v`.`ges_nominativo` AS `ges_nominativo`,`filtro0006_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0006_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0006_parametri_v`.`par_conn` AS `par_conn`,`filtro0006_parametri_v`.`par_debid` AS `par_debid`,`filtro0006_parametri_v`.`par_lastid` AS `par_lastid`,`filtro0006_parametri_v`.`ope_nominativo` AS `ope_nominativo`,`filtro0006_parametri_v`.`modibar` AS `modibar` from `filtro0006_parametri_v` where (`filtro0006_parametri_v`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0007_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0007_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0007_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0007_parametri_v` AS select `b`.`ges_id` AS `ges_id`,`utypkg_gestid2gestnomin`(`b`.`ges_id`) AS `ges_nominativo`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_debid`,`utypkg_gestid2gestnomin`(`b`.`operatore`) AS `ope_nominativo`,`UTYPKG_DATADATE2DATABAR`(`b`.`modifica`) AS `modibar` from (`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) where (`a`.`id_filtro` = 7) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0007_parconn_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0007_parconn_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0007_parconn_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0007_parconn_v` AS select `filtro0007_parametri_v`.`ges_id` AS `ges_id`,`filtro0007_parametri_v`.`ges_nominativo` AS `ges_nominativo`,`filtro0007_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0007_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0007_parametri_v`.`par_conn` AS `par_conn`,`filtro0007_parametri_v`.`par_debid` AS `par_debid`,`filtro0007_parametri_v`.`ope_nominativo` AS `ope_nominativo`,`filtro0007_parametri_v`.`modibar` AS `modibar` from `filtro0007_parametri_v` where (`filtro0007_parametri_v`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0008_debann_arcdoc_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0008_debann_arcdoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0008_debann_arcdoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0008_debann_arcdoc_v` AS select `b`.`debandoc_id` AS `debandoc_id`,`b`.`debandoc_idann` AS `debandoc_idann`,`b`.`debandoc_iddoc` AS `debandoc_iddoc`,`b`.`debandoc_estid` AS `debandoc_estid`,`b`.`debandoc_estcod` AS `debandoc_estcod`,`b`.`debandoc_estdes` AS `debandoc_estdes`,`b`.`debandoc_tipoid` AS `debandoc_tipoid`,`b`.`debandoc_tipocod` AS `debandoc_tipocod`,`b`.`debandoc_tipodes` AS `debandoc_tipodes`,`b`.`debandoc_desc` AS `debandoc_desc`,`b`.`debandoc_size` AS `debandoc_size`,`b`.`debandoc_nomefile` AS `debandoc_nomefile`,`b`.`debandoc_ges_id` AS `debandoc_ges_id`,`b`.`debandoc_ges_nominativo` AS `debandoc_ges_nominativo`,`b`.`debandoc_modifica` AS `debandoc_modifica`,`b`.`debandoc_modifica` AS `debandoc_modbar` from (`filtro0008_parconn_v` `a` join `debann_arcdoc_v` `b` on((`b`.`debandoc_idann` = `a`.`par_debannid`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0008_elearcdoc_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0008_elearcdoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0008_elearcdoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0008_elearcdoc_v` AS select `b`.`debandoc_id` AS `elearcdoc_id`,`b`.`debandoc_idann` AS `elearcdoc_idann`,`b`.`debandoc_iddoc` AS `elearcdoc_iddoc`,`b`.`debandoc_estid` AS `elearcdoc_estid`,`b`.`debandoc_estcod` AS `elearcdoc_estcod`,`b`.`debandoc_estdes` AS `elearcdoc_estdes`,`b`.`debandoc_tipoid` AS `elearcdoc_tipoid`,`b`.`debandoc_tipocod` AS `elearcdoc_tipocod`,`b`.`debandoc_tipodes` AS `elearcdoc_tipodes`,`b`.`debandoc_desc` AS `elearcdoc_desc`,`b`.`debandoc_size` AS `elearcdoc_size`,`b`.`debandoc_nomefile` AS `elearcdoc_nomefile`,`b`.`debandoc_ges_id` AS `elearcdoc_ges_id`,`b`.`debandoc_ges_nominativo` AS `elearcdoc_ges_nominativo`,`b`.`debandoc_modifica` AS `elearcdoc_modifica`,`b`.`debandoc_modifica` AS `elearcdoc_modbar` from (`filtro0008_parconn_v` `a` join `debann_arcdoc_v` `b` on((`b`.`debandoc_idann` = `a`.`par_debannid`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0008_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0008_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0008_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0008_parametri_v` AS select `b`.`ges_id` AS `ges_id`,`utypkg_gestid2gestnomin`(`b`.`ges_id`) AS `ges_nominativo`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_debannid`,`utypkg_gestid2gestnomin`(`b`.`operatore`) AS `ope_nominativo`,`UTYPKG_DATADATE2DATABAR`(`b`.`modifica`) AS `modibar` from (`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) where (`a`.`id_filtro` = 8) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0008_parconn_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0008_parconn_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0008_parconn_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0008_parconn_v` AS select `filtro0008_parametri_v`.`ges_id` AS `ges_id`,`filtro0008_parametri_v`.`ges_nominativo` AS `ges_nominativo`,`filtro0008_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0008_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0008_parametri_v`.`par_conn` AS `par_conn`,`filtro0008_parametri_v`.`par_debannid` AS `par_debannid`,`filtro0008_parametri_v`.`ope_nominativo` AS `ope_nominativo`,`filtro0008_parametri_v`.`modibar` AS `modibar` from `filtro0008_parametri_v` where (`filtro0008_parametri_v`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0009_debitori_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0009_debitori_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0009_debitori_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0009_debitori_v` AS select `b`.`deb_id` AS `deb_id`,`b`.`deb_cod` AS `deb_cod`,`b`.`deb_des` AS `deb_des`,`b`.`deb_citta` AS `deb_citta`,`b`.`deb_pro` AS `deb_pro`,`b`.`deb_tel` AS `deb_tel`,`b`.`deb_mail` AS `deb_mail`,ifnull(`c`.`debann_numann`,0) AS `deb_numann`,(case ifnull(`c`.`debann_numann`,0) when 0 then 'N' else 'S' end) AS `deb_ann_checksino` from (((`filtro0009_parconn_v` `a` join `gest_debitore` `gd` on(((`gd`.`gd_ges_id` = `a`.`ges_id`) and (`gd`.`gd_deb_id` = ifnull(`a`.`par_debid`,`gd`.`gd_deb_id`))))) join `debitori` `b` on(((`b`.`deb_soc` = `filtro0001_get_societa`()) and (`b`.`deb_id` = `gd`.`gd_deb_id`)))) left join `debann_checksino_v` `c` on((`c`.`debann_debid` = `b`.`deb_id`))) order by `b`.`deb_cod` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0009_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0009_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0009_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0009_parametri_v` AS select `b`.`ges_id` AS `ges_id`,`utypkg_gestid2gestnomin`(`b`.`ges_id`) AS `ges_nominativo`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_debid`,`utypkg_gestid2gestnomin`(`b`.`operatore`) AS `ope_nominativo`,`UTYPKG_DATADATE2DATABAR`(`b`.`modifica`) AS `modibar` from (`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) where (`a`.`id_filtro` = 9) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0009_parconn_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0009_parconn_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0009_parconn_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0009_parconn_v` AS select `filtro0009_parametri_v`.`ges_id` AS `ges_id`,`filtro0009_parametri_v`.`ges_nominativo` AS `ges_nominativo`,`filtro0009_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0009_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0009_parametri_v`.`par_conn` AS `par_conn`,`filtro0009_parametri_v`.`par_debid` AS `par_debid`,`filtro0009_parametri_v`.`ope_nominativo` AS `ope_nominativo`,`filtro0009_parametri_v`.`modibar` AS `modibar` from `filtro0009_parametri_v` where (`filtro0009_parametri_v`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0010_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0010_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0010_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0010_parametri_v` AS select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_lastid` from ((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) where (`a`.`id_filtro` = 10) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0010_parconn_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0010_parconn_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0010_parconn_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0010_parconn_v` AS select `filtro0010_parametri_v`.`ges_id` AS `ges_id`,`filtro0010_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0010_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0010_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0010_parametri_v`.`par_conn` AS `par_conn`,`filtro0010_parametri_v`.`par_lastid` AS `par_lastid` from `filtro0010_parametri_v` where (`filtro0010_parametri_v`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0011_eletabsr_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0011_eletabsr_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0011_eletabsr_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0011_eletabsr_v` AS select `b`.`fttbsr_numero` AS `fttbsr_numero`,`b`.`fttbsr_nota` AS `fttbsr_nota`,`b`.`fttbsr_ges_nominativo` AS `fttbsr_ges_nominativo`,`b`.`fttbsr_modifica` AS `fttbsr_modifica`,`b`.`fttbsr_modbar` AS `fttbsr_modbar` from (`filtro0011_parconn_v` `a` join `fatture_tabsr_v` `b` on((`b`.`fttbsr_idfatt` = `a`.`par_idfatt`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0011_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0011_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0011_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0011_parametri_v` AS select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_idfatt` from ((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) where (`a`.`id_filtro` = 11) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0011_parconn_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0011_parconn_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0011_parconn_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0011_parconn_v` AS select `filtro0011_parametri_v`.`ges_id` AS `ges_id`,`filtro0011_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0011_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0011_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0011_parametri_v`.`par_conn` AS `par_conn`,`filtro0011_parametri_v`.`par_idfatt` AS `par_idfatt` from `filtro0011_parametri_v` where (`filtro0011_parametri_v`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0012_orig_arcdoc_griglia_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0012_orig_arcdoc_griglia_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0012_orig_arcdoc_griglia_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0012_orig_arcdoc_griglia_v` AS select `a`.`orig_arcdoc_id` AS `orig_arcdoc_id`,`a`.`orig_arcdoc_des` AS `orig_arcdoc_des`,`a`.`orig_arcdoc_idori` AS `orig_arcdoc_idori`,`a`.`orig_arcdoc_debid` AS `orig_arcdoc_debid`,`c`.`deb_cod` AS `orig_arcdoc_debcod`,`c`.`deb_des` AS `orig_arcdoc_debdes`,`a`.`orig_arcdoc_ele` AS `orig_arcdoc_ele`,`a`.`orig_arcdoc_eledata` AS `orig_arcdoc_eledata`,`d`.`databar` AS `orig_arcdoc_eledatabar`,`a`.`orig_arcdoc_iddoc` AS `orig_arcdoc_iddoc`,`a`.`orig_arcdoc_gesid` AS `orig_arcdoc_gesid`,`a`.`orig_arcdoc_ges_nominativo` AS `orig_arcdoc_ges_nominativo`,`a`.`orig_arcdoc_modifica` AS `orig_arcdoc_modifica`,`a`.`orig_arcdoc_modbar` AS `orig_arcdoc_modbar`,`b`.`tbarcdoc_estid` AS `orig_estid`,`b`.`tbarcdoc_estcod` AS `orig_estcod`,`b`.`tbarcdoc_estdes` AS `orig_estdes`,`b`.`tbarcdoc_tipoid` AS `orig_tipoid`,`b`.`tbarcdoc_tipocod` AS `orig_tipocod`,`b`.`tbarcdoc_tipodes` AS `orig_tipodes`,`b`.`tbarcdoc_size` AS `orig_size`,`b`.`tbarcdoc_nomefile` AS `orig_nomefile`,`b`.`tbarcdoc_desc` AS `orig_desc` from (((`filtro0012_orig_arcdoc_v` `a` join `tab_arcdoc_v` `b` on((`b`.`tbarcdoc_id` = `a`.`orig_arcdoc_iddoc`))) join `debitori` `c` on((`c`.`deb_id` = `a`.`orig_arcdoc_debid`))) join `calendario` `d` on((`d`.`id` = `a`.`orig_arcdoc_eledata`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0012_orig_arcdoc_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0012_orig_arcdoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0012_orig_arcdoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0012_orig_arcdoc_v` AS select 'FTAC' AS `orig_arcdoc_id`,'FattureArcDoc' AS `orig_arcdoc_des`,`a`.`ftarcdoc_idfatt` AS `orig_arcdoc_idori`,`b`.`ft_deb_id` AS `orig_arcdoc_debid`,`b`.`ft_num_fat` AS `orig_arcdoc_ele`,`b`.`ft_id_data_fat` AS `orig_arcdoc_eledata`,`a`.`ftarcdoc_iddoc` AS `orig_arcdoc_iddoc`,`a`.`ftarcdoc_ges_id` AS `orig_arcdoc_gesid`,`utypkg_gestid2gestnomin`(`a`.`ftarcdoc_ges_id`) AS `orig_arcdoc_ges_nominativo`,`a`.`ftarcdoc_modifica` AS `orig_arcdoc_modifica`,`utypkg_datadate2databar`(`a`.`ftarcdoc_modifica`) AS `orig_arcdoc_modbar` from (`fatture_arcdoc` `a` join `fatture` `b` on((`b`.`id` = `a`.`ftarcdoc_idfatt`))) union select 'DAAC' AS `orig_arcdoc_id`,'DebannArcDoc' AS `orig_arcdoc_des`,`a`.`debandoc_idann` AS `orig_arcdoc_idori`,`b`.`debann_debid` AS `orig_arcdoc_debid`,`b`.`debann_idtpann` AS `orig_arcdoc_ele`,`b`.`debann_iddata` AS `orig_arcdoc_eledata`,`a`.`debandoc_iddoc` AS `orig_arcdoc_iddoc`,`a`.`debandoc_ges_id` AS `orig_arcdoc_gesid`,`utypkg_gestid2gestnomin`(`a`.`debandoc_ges_id`) AS `orig_arcdoc_ges_nominativo`,`a`.`debandoc_modifica` AS `orig_arcdoc_modifica`,`utypkg_datadate2databar`(`a`.`debandoc_modifica`) AS `orig_arcdoc_modbar` from (`debann_arcdoc` `a` join `debitori_annotazioni` `b` on((`b`.`debann_id` = `a`.`debandoc_idann`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0012_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0012_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0012_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0012_parametri_v` AS select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_id_arcdoc` from ((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) where (`a`.`id_filtro` = 12) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro0012_parconn_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro0012_parconn_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro0012_parconn_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro0012_parconn_v` AS select `filtro0012_parametri_v`.`ges_id` AS `ges_id`,`filtro0012_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0012_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0012_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0012_parametri_v`.`par_conn` AS `par_conn`,`filtro0012_parametri_v`.`par_id_arcdoc` AS `par_id_arcdoc` from `filtro0012_parametri_v` where (`filtro0012_parametri_v`.`par_conn` = connection_id()) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `filtro003_elenco_pagine_v`
--

/*!50001 DROP TABLE IF EXISTS `filtro003_elenco_pagine_v`*/;
/*!50001 DROP VIEW IF EXISTS `filtro003_elenco_pagine_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `filtro003_elenco_pagine_v` AS select 1 AS `pag` union select 2 AS `pag` union select 3 AS `pag` union select 4 AS `pag` union select 5 AS `pag` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `gest_debitore_v`
--

/*!50001 DROP TABLE IF EXISTS `gest_debitore_v`*/;
/*!50001 DROP VIEW IF EXISTS `gest_debitore_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `gest_debitore_v` AS select `a`.`ges_id` AS `ges_id`,`a`.`ges_cognome` AS `ges_cognome`,`a`.`ges_nome` AS `ges_nome`,`a`.`ges_ruolo` AS `ges_ruolo`,`c`.`deb_id` AS `deb_id`,`c`.`deb_soc` AS `deb_soc`,`c`.`deb_cod` AS `beb_cod`,`c`.`deb_des` AS `des_des` from ((`gestori` `a` left join `gest_debitore` `b` on((`b`.`gd_ges_id` = `a`.`ges_id`))) join `debitori` `c` on((`c`.`deb_id` = (case `a`.`ges_ruolo` when 'admin' then `c`.`deb_id` else `b`.`gd_deb_id` end)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `gestione_scadenziario_v`
--

/*!50001 DROP TABLE IF EXISTS `gestione_scadenziario_v`*/;
/*!50001 DROP VIEW IF EXISTS `gestione_scadenziario_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `gestione_scadenziario_v` AS select `c`.`id` AS `id`,`c`.`ft_soc` AS `ft_soc`,`c`.`ft_deb_id` AS `ft_deb_id`,`fsf`.`fsf_ac` AS `ft_ac`,`d`.`deb_id` AS `ft_id_debitore`,`d`.`deb_cod` AS `ft_deb_cod`,`d`.`deb_des` AS `ft_deb_des`,`d`.`deb_citta` AS `ft_deb_citta`,`c`.`ft_ced_id` AS `ft_ced_id`,substr(`c`.`ft_attribuzione`,2,length(`c`.`ft_attribuzione`)) AS `ft_attribuzione`,`c`.`ft_num_fat` AS `ft_num_fat`,`c`.`ft_td_id` AS `ft_td_id`,`td`.`td_cod` AS `ft_td`,`fsln`.`ftstlav_id_stato_lav` AS `ft_id_stato_lav`,`fsln`.`ftstlav_sldes` AS `ft_des_stato_lav`,concat(`fsln`.`ftstlav_slcod`,'-',`fsln`.`ftstlav_sldes`) AS `ft_sl_coddes`,`c`.`ft_testo` AS `ft_testo`,`df`.`databar` AS `ft_data_fat`,`ds`.`databar` AS `ft_data_scad`,`c`.`ft_imp_fat` AS `ft_imp_fat`,`c`.`ft_imp_aperto` AS `ft_imp_aperto`,`c`.`ft_data_ins` AS `ft_data_ins`,`c`.`ft_id_data_fat` AS `ft_id_data_fat`,`c`.`ft_id_data_chius` AS `ft_id_data_chius`,`fsln`.`ftstlav_nota_crgest` AS `ft_nota_crgest`,`fsln`.`ftstlav_data_bar` AS `ft_data_nota`,ifnull(`c`.`ft_arcdoc_check_sino`,0) AS `ft_arcdoc_sino`,ifnull(`c`.`ft_tabsr_check_sino`,0) AS `ft_tabsr_sino`,`c`.`ft_forecast_mese` AS `ft_forecast_mese`,`c`.`ft_forecast_anno` AS `ft_forecast_anno`,(case `c`.`ft_forecast_anno` when NULL then NULL else concat(`c`.`ft_forecast_mese`,'/',`c`.`ft_forecast_anno`) end) AS `ft_forecast_scad` from ((((((((((`filtro0001_parametri_v` `a` join `gest_debitore` `b` on((`b`.`gd_ges_id` = `a`.`ges_id`))) join `debitori` `d` on(((`d`.`deb_id` = `b`.`gd_deb_id`) and (`d`.`deb_id` = ifnull(`a`.`parint03`,`d`.`deb_id`))))) join `societa` `soc` on((`soc`.`soc_cod` = ifnull(`a`.`parint01`,`soc`.`soc_cod`)))) join `tipo_doc` `td` on(((`td`.`td_soc` = ifnull(`a`.`parint01`,`soc`.`soc_cod`)) and (`td`.`td_id` = ifnull(`a`.`parint02`,`td`.`td_id`))))) join `stato_lavorazione` `sl` on((`sl`.`id` = ifnull(`a`.`parint04`,`sl`.`id`)))) join `calendario` `df` on((`df`.`id` between ifnull(`a`.`parint05`,0) and ifnull(`a`.`parint06`,9999999)))) join `calendario` `ds` on((`ds`.`id` between ifnull(`a`.`parint07`,0) and ifnull(`a`.`parint08`,9999999)))) join `fatture` `c` on(((`c`.`ft_deb_id` = `d`.`deb_id`) and (`c`.`ft_soc` = `soc`.`soc_cod`) and (`c`.`ft_td_id` = `td`.`td_id`) and (`c`.`ft_id_data_fat` = `df`.`id`) and (`c`.`ft_id_data_scad` = `ds`.`id`) and (`c`.`ft_num_fat` = ifnull(`a`.`parvar09`,`c`.`ft_num_fat`)) and (ifnull(`c`.`ft_arcdoc_check_sino`,0) = (case ifnull(`a`.`parint11`,0) when 0 then ifnull(`c`.`ft_arcdoc_check_sino`,0) else ifnull(`a`.`parint11`,0) end))))) join `fatture_stato_fattura_v` `fsf` on(((`fsf`.`fsf_id_fattura` = `c`.`id`) and (`fsf`.`fsf_ac` = (case ifnull(`a`.`parint10`,0) when 0 then 0 else `fsf`.`fsf_ac` end))))) join `fatture_statilavnote_v` `fsln` on(((`fsln`.`ftstlav_id_fattura` = `c`.`id`) and (`fsln`.`ftstlav_id_stato_lav` = `sl`.`id`)))) limit 5 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pippo_v`
--

/*!50001 DROP TABLE IF EXISTS `pippo_v`*/;
/*!50001 DROP VIEW IF EXISTS `pippo_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `pippo_v` AS select '\'' AS `pippo` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `statist_movimenti_v`
--

/*!50001 DROP TABLE IF EXISTS `statist_movimenti_v`*/;
/*!50001 DROP VIEW IF EXISTS `statist_movimenti_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `statist_movimenti_v` AS select `a`.`stmov_gesid` AS `stmov_gesid`,`f`.`fosuser_username` AS `stmov_username`,`g`.`ges_cognome` AS `cognome`,`g`.`ges_nome` AS `ges_nome`,`a`.`stmov_opeid` AS `stmov_opeid`,`b`.`statope_desc` AS `stmov_des`,`a`.`stmod_datetime` AS `stmod_datetime`,`d`.`username` AS `operatore`,`a`.`modifica` AS `modifica` from ((((`statist_movimenti` `a` join `statist_operazioni` `b` on((`b`.`statope_id` = `a`.`stmov_id`))) join `gestori` `g` on((`g`.`ges_id` = `a`.`stmov_gesid`))) join `fosuser_gestori` `f` on((`f`.`fosuser_gestid` = `g`.`ges_id`))) join `fos_user` `d` on((`d`.`id` = `a`.`operatore`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `tab_arcdoc_v`
--

/*!50001 DROP TABLE IF EXISTS `tab_arcdoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `tab_arcdoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tab_arcdoc_v` AS select `a`.`tbarcdoc_id` AS `tbarcdoc_id`,`a`.`tbarcdoc_soc` AS `tbarcdoc_soc`,`a`.`tbarcdoc_est` AS `tbarcdoc_estid`,`b`.`estarcdoc_cod` AS `tbarcdoc_estcod`,`b`.`estarcdoc_des` AS `tbarcdoc_estdes`,`a`.`tbarcdoc_tipo` AS `tbarcdoc_tipoid`,`c`.`tparcdoc_cod` AS `tbarcdoc_tipocod`,`c`.`tparcdoc_des` AS `tbarcdoc_tipodes`,`a`.`tbarcdoc_size` AS `tbarcdoc_size`,`a`.`tbarcdoc_nomefile` AS `tbarcdoc_nomefile`,`a`.`tbarcdoc_desc` AS `tbarcdoc_desc`,`a`.`tbarcdoc_blob` AS `tbarcdoc_blob`,`a`.`tbarcdoc_ges_id` AS `tbarcdoc_ges_id`,concat(`d`.`ges_cognome`,' ',`d`.`ges_nome`) AS `tbarcdoc_ges_nominativo`,`a`.`tbarcdoc_modifica` AS `tbarcdoc_modifica` from (((`tab_arcdoc` `a` join `estensioni_arcdoc` `b` on((`b`.`estarcdoc_id` = `a`.`tbarcdoc_est`))) join `tipi_arcdoc` `c` on((`c`.`tparcdoc_id` = `a`.`tbarcdoc_tipo`))) join `gestori` `d` on((`d`.`ges_id` = `a`.`tbarcdoc_ges_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `tab_procedure_parametri_v`
--

/*!50001 DROP TABLE IF EXISTS `tab_procedure_parametri_v`*/;
/*!50001 DROP VIEW IF EXISTS `tab_procedure_parametri_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tab_procedure_parametri_v` AS select `a`.`id_proc` AS `id_proc`,`b`.`descrizione` AS `des_proc`,`a`.`id_host` AS `id_host`,`c`.`descrizione` AS `des_host`,`a`.`parcar1` AS `directory_file_load` from ((`tab_procedure_parametri` `a` join `tab_procedure` `b` on((`b`.`id_proc` = `a`.`id_proc`))) join `tab_procedure_host` `c` on((`c`.`id_host` = `a`.`id_host`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `tabsr_stati_v`
--

/*!50001 DROP TABLE IF EXISTS `tabsr_stati_v`*/;
/*!50001 DROP VIEW IF EXISTS `tabsr_stati_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tabsr_stati_v` AS select `a`.`tbsrst_id` AS `tbsrst_id`,`a`.`tbsrst_soc` AS `tbsrst_soccod`,`b`.`soc_des` AS `tbsrst_socdes`,`a`.`tbsrst_cod` AS `tbsrst_cod`,`a`.`tbsrst_desc` AS `tbsrst_desc`,`a`.`tbsrst_ges_id` AS `tbsrst_ges_id`,`UTYPKG_GESTID2GESTNOMIN`(`a`.`tbsrst_ges_id`) AS `tbsrst_ges_nominativo`,`a`.`tbsrst_modifica` AS `tbsrst_modifica`,`UTYPKG_DATADATE2DATABAR`(`a`.`tbsrst_modifica`) AS `tbsrst_modbar` from (`tabsr_stati` `a` join `societa` `b` on((`b`.`soc_cod` = `a`.`tbsrst_soc`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `tabsr_v`
--

/*!50001 DROP TABLE IF EXISTS `tabsr_v`*/;
/*!50001 DROP VIEW IF EXISTS `tabsr_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tabsr_v` AS select `a`.`tbsr_id` AS `tbsr_id`,`a`.`tbsr_numero` AS `tbsr_numero`,`a`.`tbsr_nota` AS `tbsr_nota`,`a`.`tbsr_stato` AS `tbsr_idstato`,`b`.`tbsrst_cod` AS `tbsr_codstato`,`b`.`tbsrst_desc` AS `tbsr_desstato`,`a`.`tbsr_ges_id` AS `tbsr_ges_id`,`a`.`tbsr_modifica` AS `tbsr_modifica` from (`tab_sr` `a` join `tabsr_stati` `b` on((`b`.`tbsrst_id` = `a`.`tbsr_stato`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `tipo_arcdoc_v`
--

/*!50001 DROP TABLE IF EXISTS `tipo_arcdoc_v`*/;
/*!50001 DROP VIEW IF EXISTS `tipo_arcdoc_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `tipo_arcdoc_v` AS select `a`.`tparcdoc_id` AS `tparcdoc_id`,`a`.`tparcdoc_soc` AS `tparcdoc_soc`,`c`.`soc_des` AS `tparcdoc_socdes`,`a`.`tparcdoc_cod` AS `tparcdoc_cod`,`a`.`tparcdoc_des` AS `tparcdoc_des`,`a`.`tparcdoc_ges_id` AS `tparcdoc_ges_id`,concat(`b`.`ges_cognome`,' ',`b`.`ges_nome`) AS `tparcdoc_gesnome`,`a`.`tparcdoc_modifica` AS `tparcdoc_modifica` from ((`tipi_arcdoc` `a` join `gestori` `b` on((`b`.`ges_id` = `a`.`tparcdoc_ges_id`))) join `societa` `c` on((`c`.`soc_cod` = `a`.`tparcdoc_soc`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-08  9:50:58
