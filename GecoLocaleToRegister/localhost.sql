-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Feb 28, 2018 alle 22:40
-- Versione del server: 5.6.35-cll-lve
-- Versione PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: vs28ahbz_gecotest
--
CREATE DATABASE IF NOT EXISTS vs28ahbz_gecotest DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE vs28ahbz_gecotest;

DELIMITER $$
--
-- Procedure
--
DROP PROCEDURE IF EXISTS `AGG_DEBITORI_FASTWEB`$$
$$

DROP PROCEDURE IF EXISTS `agg_gest_debitore`$$
$$

DROP PROCEDURE IF EXISTS `crea_calendario`$$
$$

DROP PROCEDURE IF EXISTS `fatturefastwebcsv_2_fatture`$$
$$

DROP PROCEDURE IF EXISTS `fatturefastwebcsv_2_gestdebitore`$$
$$

DROP PROCEDURE IF EXISTS `fatturempscsv_2_fatture`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_POPOLA_TEMP`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_ALL`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_DATAFAT_A`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_DATAFAT_DA`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_DATASCAD_A`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_DATASCAD_DA`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_DEBITORE`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_NUMFAT`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_SOCIETA`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_STATOLAV`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_RESET_TIPODOC`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_SET_DATAFAT_A`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_SET_DATAFAT_DA`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_SET_DATASCAD_A`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_SET_DATASCAD_DA`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_SET_DEBITORE`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_SET_NUMFAT`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_SET_SOCIETA`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_SET_STATOLAV`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_SET_TIPODOC`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0001_UNLOCK`$$
$$

DROP PROCEDURE IF EXISTS `filtro0002_reset_liv1`$$
$$

DROP PROCEDURE IF EXISTS `filtro0002_reset_liv2`$$
$$

DROP PROCEDURE IF EXISTS `filtro0002_reset_liv3`$$
$$

DROP PROCEDURE IF EXISTS `filtro0002_reset_liv4`$$
$$

DROP PROCEDURE IF EXISTS `filtro0002_set_liv1`$$
$$

DROP PROCEDURE IF EXISTS `filtro0002_set_liv2`$$
$$

DROP PROCEDURE IF EXISTS `filtro0002_set_liv3`$$
$$

DROP PROCEDURE IF EXISTS `filtro0002_set_liv4`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_RESET_ALL`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_LIMFIN`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_LIMINI`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGCORR`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGESP`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGPREC`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGSUCC`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGTOT`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPAGULT`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMPOSCORR`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_NUMRECTOT`$$
$$

DROP PROCEDURE IF EXISTS `FILTRO0003_SET_RECPERPAG`$$
$$

DROP PROCEDURE IF EXISTS `FOSUPD_FILTRI_PARAMETRI`$$
$$

DROP PROCEDURE IF EXISTS `IMPORTA_FATTURE_MPS`$$
$$

DROP PROCEDURE IF EXISTS `load_data_SP`$$
$$

DROP PROCEDURE IF EXISTS `upd_fatture_fastweb_csv`$$
$$

DROP PROCEDURE IF EXISTS `upd_fatture_mps_csv`$$
$$

DROP PROCEDURE IF EXISTS `UPD_FILTRI_PARAMETRI`$$
$$

DROP PROCEDURE IF EXISTS `UPD_STATIST_MOVIMENTI`$$
$$

--
-- Funzioni
--
DROP FUNCTION IF EXISTS `FILTRO0001_GET_TIPODOC`$$
$$

DROP FUNCTION IF EXISTS `getnewdebid`$$
$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura della tabella agenda
--

DROP TABLE IF EXISTS agenda;
CREATE TABLE agenda (
  id int(11) NOT NULL,
  username varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  descrizione varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  cliente varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  data datetime DEFAULT NULL,
  stato int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella calendario
--

DROP TABLE IF EXISTS calendario;
CREATE TABLE calendario (
  id int(10) NOT NULL,
  data date DEFAULT NULL,
  gg int(2) DEFAULT NULL,
  mese int(2) DEFAULT NULL,
  anno int(4) DEFAULT NULL,
  databar varchar(10) DEFAULT NULL,
  datanum int(8) DEFAULT NULL,
  datachar varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella cedenti
--

DROP TABLE IF EXISTS cedenti;
CREATE TABLE cedenti (
  ced_id int(10) NOT NULL,
  ced_soc int(10) NOT NULL,
  ced_cod int(10) NOT NULL,
  ced_des varchar(50) DEFAULT NULL,
  ced_citta varchar(50) DEFAULT NULL,
  ced_ind varchar(50) DEFAULT NULL,
  ced_cap varchar(5) DEFAULT NULL,
  ced_pro varchar(2) DEFAULT NULL,
  ced_regione varchar(20) DEFAULT NULL,
  ced_tel varchar(10) DEFAULT NULL,
  ced_mail varchar(50) DEFAULT NULL,
  ced_data_affido date DEFAULT NULL,
  ced_data_disaffido date DEFAULT NULL,
  idold int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella debitori
--

DROP TABLE IF EXISTS debitori;
CREATE TABLE debitori (
  deb_id int(10) NOT NULL,
  deb_soc int(10) NOT NULL,
  deb_cod int(10) NOT NULL,
  deb_des varchar(100) DEFAULT NULL,
  deb_cf_piva varchar(16) DEFAULT NULL,
  deb_citta varchar(50) DEFAULT NULL,
  deb_ind varchar(50) DEFAULT NULL,
  deb_cap varchar(5) DEFAULT NULL,
  deb_pro varchar(2) DEFAULT NULL,
  deb_regione varchar(20) DEFAULT NULL,
  deb_tel varchar(10) DEFAULT NULL,
  deb_mail varchar(50) DEFAULT NULL,
  deb_data_affido date DEFAULT NULL,
  deb_data_disaffido date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella fatture
--

DROP TABLE IF EXISTS fatture;
CREATE TABLE fatture (
  id int(10) NOT NULL,
  ft_soc int(10) NOT NULL,
  ft_deb_id int(10) NOT NULL,
  ft_ced_id int(10) NOT NULL,
  ft_num_fat varchar(20) NOT NULL,
  ft_attribuzione varchar(50) DEFAULT NULL,
  ft_td varchar(5) DEFAULT NULL,
  ft_testo varchar(500) DEFAULT NULL,
  ft_data_fat date NOT NULL,
  ft_data_scad date DEFAULT NULL,
  ft_imp_fat decimal(10,2) NOT NULL,
  ft_imp_aperto decimal(10,2) DEFAULT NULL,
  ft_data_ins date DEFAULT NULL,
  ft_id_stato_lav int(10) DEFAULT NULL,
  ft_td_id int(10) NOT NULL,
  ft_id_data_fat int(10) DEFAULT NULL,
  ft_id_data_scad int(10) DEFAULT NULL,
  ft_nomefile_csv varchar(100) DEFAULT NULL,
  ft_id_csv int(10) DEFAULT NULL,
  ft_datetime_csv datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella fatture_fastweb_csv
--

DROP TABLE IF EXISTS fatture_fastweb_csv;
CREATE TABLE fatture_fastweb_csv (
  ft_id_csv int(10) NOT NULL,
  ft_zona int(10) DEFAULT NULL,
  ft_ced_cod int(10) DEFAULT NULL,
  ft_ced_id int(10) DEFAULT NULL,
  ft_ced_denom varchar(100) DEFAULT NULL,
  ft_deb_cod int(10) DEFAULT NULL,
  ft_deb_id int(10) DEFAULT NULL,
  ft_deb_denom varchar(100) DEFAULT NULL,
  ft_deb_citta varchar(50) DEFAULT NULL,
  ft_deb_prov varchar(2) DEFAULT NULL,
  ft_num_fat varchar(20) DEFAULT NULL,
  ft_attribuzione varchar(50) DEFAULT NULL,
  ft_td varchar(5) DEFAULT NULL,
  ft_td_id int(10) DEFAULT NULL,
  ft_data_fat varchar(10) DEFAULT NULL,
  ft_id_data_fat int(10) DEFAULT NULL,
  ft_data_scad varchar(10) DEFAULT NULL,
  ft_id_data_scad int(10) DEFAULT NULL,
  ft_imp_fat decimal(10,2) DEFAULT NULL,
  ft_testo varchar(500) DEFAULT NULL,
  ft_stato_lav varchar(100) DEFAULT NULL,
  ft_id_stato_lav int(10) DEFAULT NULL,
  ft_data_ins date DEFAULT NULL,
  ft_nomefile_csv varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella fatture_mps_csv
--

DROP TABLE IF EXISTS fatture_mps_csv;
CREATE TABLE fatture_mps_csv (
  ft_id_csv int(10) NOT NULL,
  ft_ced_cod int(10) DEFAULT NULL,
  ft_ced_id int(10) DEFAULT NULL,
  ft_ced_denom varchar(100) DEFAULT NULL,
  ft_deb_cod int(10) DEFAULT NULL,
  ft_deb_id int(10) DEFAULT NULL,
  ft_deb_denom varchar(100) DEFAULT NULL,
  ft_num_fat varchar(20) DEFAULT NULL,
  ft_td varchar(5) DEFAULT NULL,
  ft_td_id int(10) DEFAULT NULL,
  ft_testo varchar(500) DEFAULT NULL,
  ft_data_fat varchar(10) DEFAULT NULL,
  ft_id_data_fat int(10) DEFAULT NULL,
  ft_data_scad varchar(10) DEFAULT NULL,
  ft_id_data_scad int(10) DEFAULT NULL,
  ft_imp_fat decimal(10,2) DEFAULT NULL,
  ft_imp_aperto decimal(10,2) DEFAULT NULL,
  ft_data_ins date DEFAULT NULL,
  ft_id_stato_lav int(10) DEFAULT NULL,
  ft_attribuzione varchar(50) DEFAULT NULL,
  ft_nomefile_csv varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella fatture_ori
--

DROP TABLE IF EXISTS fatture_ori;
CREATE TABLE fatture_ori (
  id int(10) NOT NULL DEFAULT '0',
  ft_soc int(10) NOT NULL,
  ft_deb_id int(10) NOT NULL,
  ft_ced_id int(10) NOT NULL,
  ft_num_fat varchar(20) NOT NULL,
  ft_attribuzione varchar(50) DEFAULT NULL,
  ft_td varchar(5) DEFAULT NULL,
  ft_testo varchar(500) DEFAULT NULL,
  ft_data_fat date NOT NULL,
  ft_data_scad date DEFAULT NULL,
  ft_imp_fat decimal(10,2) NOT NULL,
  ft_imp_aperto decimal(10,2) DEFAULT NULL,
  ft_data_ins date DEFAULT NULL,
  ft_id_stato_lav int(10) DEFAULT NULL,
  ft_td_id int(10) NOT NULL,
  ft_id_data_fat int(10) DEFAULT NULL,
  ft_id_data_scad int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste fatture_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `fatture_v`;
CREATE TABLE `fatture_v` (
`id` int(10)
,`soc_cod` int(10)
,`soc_des` varchar(50)
,`deb_id` int(10)
,`cod_deb` int(10)
,`deb_des` varchar(100)
,`ced_cod` int(10)
,`ced_des` varchar(50)
,`num_fat` varchar(20)
,`attribuzione` varchar(50)
,`tipo_doc_cod` varchar(5)
,`tipo_doc_des` varchar(100)
,`testo` varchar(500)
,`data_fat` date
,`data_scad` date
,`imp_fat` decimal(10,2)
,`imp_aperto` decimal(10,2)
,`data_ins` date
,`id_stato_lav_cod` int(10)
);

-- --------------------------------------------------------

--
-- Struttura della tabella filtri
--

DROP TABLE IF EXISTS filtri;
CREATE TABLE filtri (
  id_filtro int(10) NOT NULL,
  descrizione varchar(50) DEFAULT NULL,
  operatore int(11) NOT NULL DEFAULT '0',
  modifica datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella filtri_parametri
--

DROP TABLE IF EXISTS filtri_parametri;
CREATE TABLE filtri_parametri (
  ges_id int(10) NOT NULL,
  id_filtro int(10) NOT NULL,
  par_conn int(10) DEFAULT NULL,
  parvar01 varchar(10) DEFAULT NULL,
  parvar02 varchar(10) DEFAULT NULL,
  parvar03 varchar(10) DEFAULT NULL,
  parvar04 varchar(10) DEFAULT NULL,
  parvar05 varchar(10) DEFAULT NULL,
  parvar06 varchar(10) DEFAULT NULL,
  parvar07 varchar(10) DEFAULT NULL,
  parvar08 varchar(10) DEFAULT NULL,
  parvar09 varchar(10) DEFAULT NULL,
  parvar10 varchar(10) DEFAULT NULL,
  parint01 varchar(10) DEFAULT NULL,
  parint02 varchar(10) DEFAULT NULL,
  parint03 varchar(10) DEFAULT NULL,
  parint04 varchar(10) DEFAULT NULL,
  parint05 varchar(10) DEFAULT NULL,
  parint06 varchar(10) DEFAULT NULL,
  parint07 varchar(10) DEFAULT NULL,
  parint08 varchar(10) DEFAULT NULL,
  parint09 varchar(10) DEFAULT NULL,
  parint10 varchar(10) DEFAULT NULL,
  pardat01 date DEFAULT NULL,
  pardat02 date DEFAULT NULL,
  pardat03 date DEFAULT NULL,
  pardat04 date DEFAULT NULL,
  pardat05 date DEFAULT NULL,
  pardat06 date DEFAULT NULL,
  pardat07 date DEFAULT NULL,
  pardat08 date DEFAULT NULL,
  pardat09 date DEFAULT NULL,
  pardat10 date DEFAULT NULL,
  operatore int(11) NOT NULL DEFAULT '0',
  modifica datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtri_parametri_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtri_parametri_v`;
CREATE TABLE `filtri_parametri_v` (
`ges_id` int(10)
,`id_filtro` int(10)
,`par_conn` int(10)
,`parvar01` varchar(10)
,`parvar02` varchar(10)
,`parvar03` varchar(10)
,`parvar04` varchar(10)
,`parvar05` varchar(10)
,`parvar06` varchar(10)
,`parvar07` varchar(10)
,`parvar08` varchar(10)
,`parvar09` varchar(10)
,`parvar10` varchar(10)
,`parint01` varchar(10)
,`parint02` varchar(10)
,`parint03` varchar(10)
,`parint04` varchar(10)
,`parint05` varchar(10)
,`parint06` varchar(10)
,`parint07` varchar(10)
,`parint08` varchar(10)
,`parint09` varchar(10)
,`parint10` varchar(10)
,`pardat01` date
,`pardat02` date
,`pardat03` date
,`pardat04` date
,`pardat05` date
,`pardat06` date
,`pardat07` date
,`pardat08` date
,`pardat09` date
,`pardat10` date
,`operatore` int(11)
,`modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0000_parametri_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0000_parametri_v`;
CREATE TABLE `filtro0000_parametri_v` (
`ges_id` int(10)
,`id_filtro` int(10)
,`par_conn` int(10)
,`parvar01` varchar(10)
,`parvar02` varchar(10)
,`parvar03` varchar(10)
,`parvar04` varchar(10)
,`parvar05` varchar(10)
,`parvar06` varchar(10)
,`parvar07` varchar(10)
,`parvar08` varchar(10)
,`parvar09` varchar(10)
,`parvar10` varchar(10)
,`parint01` varchar(10)
,`parint02` varchar(10)
,`parint03` varchar(10)
,`parint04` varchar(10)
,`parint05` varchar(10)
,`parint06` varchar(10)
,`parint07` varchar(10)
,`parint08` varchar(10)
,`parint09` varchar(10)
,`parint10` varchar(10)
,`pardat01` date
,`pardat02` date
,`pardat03` date
,`pardat04` date
,`pardat05` date
,`pardat06` date
,`pardat07` date
,`pardat08` date
,`pardat09` date
,`pardat10` date
,`operatore` int(11)
,`modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0001_debitori_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0001_debitori_v`;
CREATE TABLE `filtro0001_debitori_v` (
`deb_id` int(10)
,`deb_des` varchar(112)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0001_get_datafat_a_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0001_get_datafat_a_v`;
CREATE TABLE `filtro0001_get_datafat_a_v` (
`datafat_a_v` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0001_get_datafat_da_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0001_get_datafat_da_v`;
CREATE TABLE `filtro0001_get_datafat_da_v` (
`datafat_da_v` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0001_get_tipodoc_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0001_get_tipodoc_v`;
CREATE TABLE `filtro0001_get_tipodoc_v` (
`td_id` bigint(11)
,`td_des` varchar(5)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0001_parametri_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0001_parametri_v`;
CREATE TABLE `filtro0001_parametri_v` (
`ges_id` int(10)
,`id_filtro` int(10)
,`par_conn` int(10)
,`parvar01` varchar(10)
,`parvar02` varchar(10)
,`parvar03` varchar(10)
,`parvar04` varchar(10)
,`parvar05` varchar(10)
,`parvar06` varchar(10)
,`parvar07` varchar(10)
,`parvar08` varchar(10)
,`parvar09` varchar(10)
,`parvar10` varchar(10)
,`parint01` varchar(10)
,`parint02` varchar(10)
,`parint03` varchar(10)
,`parint04` varchar(10)
,`parint05` varchar(10)
,`parint06` varchar(10)
,`parint07` varchar(10)
,`parint08` varchar(10)
,`parint09` varchar(10)
,`parint10` varchar(10)
,`pardat01` date
,`pardat02` date
,`pardat03` date
,`pardat04` date
,`pardat05` date
,`pardat06` date
,`pardat07` date
,`pardat08` date
,`pardat09` date
,`pardat10` date
,`operatore` int(11)
,`modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0001_societa_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0001_societa_v`;
CREATE TABLE `filtro0001_societa_v` (
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0001_statolav_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0001_statolav_v`;
CREATE TABLE `filtro0001_statolav_v` (
`sl_id` bigint(11)
,`sl_des` varchar(30)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0001_tipodoc_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0001_tipodoc_v`;
CREATE TABLE `filtro0001_tipodoc_v` (
`td_id` bigint(11)
,`td_des` varchar(5)
);

-- --------------------------------------------------------

--
-- Struttura della tabella filtro0002_eleord
--

DROP TABLE IF EXISTS filtro0002_eleord;
CREATE TABLE filtro0002_eleord (
  eleord_id int(2) NOT NULL,
  eleord_cod int(2) NOT NULL,
  eleord_des varchar(50) DEFAULT NULL,
  eleord_campo varchar(50) NOT NULL,
  eleord_ope varchar(100) NOT NULL,
  operatore varchar(50) NOT NULL DEFAULT 'SYSTEM',
  modifica datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0002_liv1_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0002_liv1_v`;
CREATE TABLE `filtro0002_liv1_v` (
`lv1_id` bigint(11)
,`lv1_des` varchar(50)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0002_liv2_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0002_liv2_v`;
CREATE TABLE `filtro0002_liv2_v` (
`lv2_id` bigint(11)
,`lv2_des` varchar(50)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0002_liv3_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0002_liv3_v`;
CREATE TABLE `filtro0002_liv3_v` (
`lv3_id` bigint(11)
,`lv3_des` varchar(50)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_elenco_pagine_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_elenco_pagine_v`;
CREATE TABLE `filtro0003_elenco_pagine_v` (
`seq` bigint(20)
,`valore` varchar(8)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_get_limfin_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_get_limfin_v`;
CREATE TABLE `filtro0003_get_limfin_v` (
`numpagesp` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_get_limini_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_get_limini_v`;
CREATE TABLE `filtro0003_get_limini_v` (
`limini` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_get_numpagcorr_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_get_numpagcorr_v`;
CREATE TABLE `filtro0003_get_numpagcorr_v` (
`numpagcorr` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_get_numpagesp_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_get_numpagesp_v`;
CREATE TABLE `filtro0003_get_numpagesp_v` (
`numpagesp` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_get_numpagtot_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_get_numpagtot_v`;
CREATE TABLE `filtro0003_get_numpagtot_v` (
`numpagtot` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_get_numposcorr_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_get_numposcorr_v`;
CREATE TABLE `filtro0003_get_numposcorr_v` (
`numposcorr` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_get_numrectot_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_get_numrectot_v`;
CREATE TABLE `filtro0003_get_numrectot_v` (
`numrectot` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_get_recperpag_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_get_recperpag_v`;
CREATE TABLE `filtro0003_get_recperpag_v` (
`recperpag` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura della tabella filtro0003_pagine
--

DROP TABLE IF EXISTS filtro0003_pagine;
CREATE TABLE filtro0003_pagine (
  id_pagina int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_parametri_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_parametri_v`;
CREATE TABLE `filtro0003_parametri_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`recperpag` varchar(10)
,`numrectot` varchar(10)
,`numpagtot` varchar(10)
,`numpagcorr` varchar(10)
,`numpagesp` varchar(10)
,`limini` varchar(10)
,`limfin` varchar(10)
,`numposcorr` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_parconn_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_parconn_v`;
CREATE TABLE `filtro0003_parconn_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`recperpag` varchar(10)
,`numrectot` varchar(10)
,`numpagtot` varchar(10)
,`numpagcorr` varchar(10)
,`numpagesp` varchar(10)
,`limini` varchar(10)
,`limfin` varchar(10)
,`numposcorr` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro0003_range_pagine_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro0003_range_pagine_v`;
CREATE TABLE `filtro0003_range_pagine_v` (
`valore` int(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste filtro003_elenco_pagine_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `filtro003_elenco_pagine_v`;
CREATE TABLE `filtro003_elenco_pagine_v` (
`pag` bigint(20)
);

-- --------------------------------------------------------

--
-- Struttura della tabella fosuser_gestori
--

DROP TABLE IF EXISTS fosuser_gestori;
CREATE TABLE fosuser_gestori (
  fosuser_username varchar(50) NOT NULL,
  fosuser_gestid int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella fos_user
--

DROP TABLE IF EXISTS fos_user;
CREATE TABLE fos_user (
  id int(11) NOT NULL,
  username varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  username_canonical varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  email_canonical varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  enabled tinyint(1) NOT NULL,
  salt varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  password varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  last_login datetime DEFAULT NULL,
  confirmation_token varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  password_requested_at datetime DEFAULT NULL,
  roles longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  name varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste gestione_scadenziario_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `gestione_scadenziario_v`;
CREATE TABLE `gestione_scadenziario_v` (
`id` int(10)
,`ft_soc` int(10)
,`ft_deb_id` int(10)
,`ft_ced_id` int(10)
,`ft_attribuzione` varchar(50)
,`ft_num_fat` varchar(20)
,`ft_td_id` int(10)
,`ft_td` varchar(100)
,`ft_id_stato_lav` int(10)
,`ft_des_stato_lav` varchar(50)
,`ft_testo` varchar(500)
,`ft_data_fat` varchar(10)
,`ft_data_scad` varchar(10)
,`ft_imp_fat` decimal(10,2)
,`ft_imp_aperto` decimal(10,2)
,`ft_data_ins` date
);

-- --------------------------------------------------------

--
-- Struttura della tabella gestori
--

DROP TABLE IF EXISTS gestori;
CREATE TABLE gestori (
  ges_id int(10) NOT NULL,
  ges_cognome varchar(50) DEFAULT NULL,
  ges_nome varchar(50) DEFAULT NULL,
  ges_mail varchar(50) DEFAULT NULL,
  ges_tel varchar(50) DEFAULT NULL,
  ges_ind varchar(50) DEFAULT NULL,
  ges_citta varchar(30) DEFAULT NULL,
  ges_regione varchar(50) DEFAULT NULL,
  ges_pwd varchar(10) NOT NULL,
  ges_ruolo varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella gest_debitore
--

DROP TABLE IF EXISTS gest_debitore;
CREATE TABLE gest_debitore (
  gd_ges_id int(10) NOT NULL,
  gd_deb_id int(10) NOT NULL,
  gd_data_associazione date DEFAULT NULL,
  gd_data_dissociazione date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste gest_debitore_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `gest_debitore_v`;
CREATE TABLE `gest_debitore_v` (
`ges_id` int(10)
,`ges_cognome` varchar(50)
,`ges_nome` varchar(50)
,`ges_ruolo` varchar(10)
,`deb_id` int(10)
,`deb_soc` int(10)
,`beb_cod` int(10)
,`des_des` varchar(100)
);

-- --------------------------------------------------------

--
-- Struttura della tabella immagini
--

DROP TABLE IF EXISTS immagini;
CREATE TABLE immagini (
  id int(10) NOT NULL,
  immagine blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella parametri_procedura
--

DROP TABLE IF EXISTS parametri_procedura;
CREATE TABLE parametri_procedura (
  sistema_operativo char(50) DEFAULT NULL,
  directory_fatture char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella prova
--

DROP TABLE IF EXISTS prova;
CREATE TABLE prova (
  id int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella sessions
--

DROP TABLE IF EXISTS sessions;
CREATE TABLE sessions (
  sess_id varchar(128) COLLATE utf8_bin NOT NULL,
  sess_data blob NOT NULL,
  sess_time int(10) UNSIGNED NOT NULL,
  sess_lifetime mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella societa
--

DROP TABLE IF EXISTS societa;
CREATE TABLE societa (
  soc_cod int(10) NOT NULL,
  soc_des varchar(50) DEFAULT NULL,
  soc_data_ini date DEFAULT NULL,
  soc_indirizzo varchar(50) DEFAULT NULL,
  soc_citta varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella statist_movimenti
--

DROP TABLE IF EXISTS statist_movimenti;
CREATE TABLE statist_movimenti (
  stmov_id int(11) NOT NULL,
  stmov_gesid int(10) NOT NULL,
  stmov_opeid int(10) NOT NULL,
  stmod_datetime datetime DEFAULT NULL,
  operatore int(11) NOT NULL DEFAULT '0',
  modifica datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste statist_movimenti_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `statist_movimenti_v`;
CREATE TABLE `statist_movimenti_v` (
`stmov_gesid` int(10)
,`stmov_username` varchar(50)
,`cognome` varchar(50)
,`ges_nome` varchar(50)
,`stmov_opeid` int(10)
,`stmov_des` varchar(50)
,`stmod_datetime` datetime
,`operatore` varchar(180)
,`modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura della tabella statist_operazioni
--

DROP TABLE IF EXISTS statist_operazioni;
CREATE TABLE statist_operazioni (
  statope_id int(10) NOT NULL,
  statope_desc varchar(50) NOT NULL,
  operatore int(11) NOT NULL DEFAULT '0',
  modifica datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella stato_ft
--

DROP TABLE IF EXISTS stato_ft;
CREATE TABLE stato_ft (
  st_id int(10) NOT NULL,
  st_ft_id int(10) NOT NULL,
  st_data_nota date NOT NULL,
  st_stato_lavorazione int(10) NOT NULL,
  st_note varchar(1000) DEFAULT NULL,
  st_operatore int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella stato_lavorazione
--

DROP TABLE IF EXISTS stato_lavorazione;
CREATE TABLE stato_lavorazione (
  id int(10) NOT NULL,
  sl_soc int(10) NOT NULL,
  sl_cod varchar(4) NOT NULL,
  sl_descrizione varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella tab_procedure
--

DROP TABLE IF EXISTS tab_procedure;
CREATE TABLE tab_procedure (
  id_proc int(10) NOT NULL,
  descrizione char(50) DEFAULT NULL,
  modifica datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella tab_procedure_host
--

DROP TABLE IF EXISTS tab_procedure_host;
CREATE TABLE tab_procedure_host (
  id_host int(10) NOT NULL,
  descrizione char(50) DEFAULT NULL,
  modifica datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella tab_procedure_parametri
--

DROP TABLE IF EXISTS tab_procedure_parametri;
CREATE TABLE tab_procedure_parametri (
  id_proc int(10) NOT NULL,
  id_host int(2) NOT NULL,
  parint1 int(10) DEFAULT NULL,
  parint2 int(10) DEFAULT NULL,
  parint3 int(10) DEFAULT NULL,
  parint4 int(10) DEFAULT NULL,
  parint5 int(10) DEFAULT NULL,
  parcar1 char(100) DEFAULT NULL,
  parcar2 char(100) DEFAULT NULL,
  parcar3 char(100) DEFAULT NULL,
  parcar4 char(100) DEFAULT NULL,
  parcar5 char(100) DEFAULT NULL,
  modifica datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste tab_procedure_parametri_v
-- (Vedi sotto per la vista effettiva)
--
DROP VIEW IF EXISTS `tab_procedure_parametri_v`;
CREATE TABLE `tab_procedure_parametri_v` (
`id_proc` int(10)
,`des_proc` char(50)
,`id_host` int(2)
,`des_host` char(50)
,`directory_file_load` char(100)
);

-- --------------------------------------------------------

--
-- Struttura della tabella tipo_doc
--

DROP TABLE IF EXISTS tipo_doc;
CREATE TABLE tipo_doc (
  td_id int(10) NOT NULL,
  td_soc int(10) NOT NULL,
  td_cod varchar(5) NOT NULL,
  td_des varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura per vista fatture_v
--
DROP TABLE IF EXISTS `fatture_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.fatture_v  AS  select a.id AS id,a.ft_soc AS soc_cod,b.soc_des AS soc_des,c.deb_id AS deb_id,a.ft_deb_id AS cod_deb,c.deb_des AS deb_des,a.ft_ced_id AS ced_cod,d.ced_des AS ced_des,a.ft_num_fat AS num_fat,a.ft_attribuzione AS attribuzione,a.ft_td AS tipo_doc_cod,e.td_des AS tipo_doc_des,a.ft_testo AS testo,a.ft_data_fat AS data_fat,a.ft_data_scad AS data_scad,a.ft_imp_fat AS imp_fat,a.ft_imp_aperto AS imp_aperto,a.ft_data_ins AS data_ins,a.ft_id_stato_lav AS id_stato_lav_cod from ((((vs28ahbz_gecotest.fatture a join vs28ahbz_gecotest.societa b on((b.soc_cod = a.ft_soc))) join vs28ahbz_gecotest.debitori c on(((a.ft_soc = c.deb_soc) and (a.ft_deb_id = c.deb_cod)))) join vs28ahbz_gecotest.cedenti d on(((a.ft_soc = d.ced_soc) and (a.ft_ced_id = d.ced_cod)))) join vs28ahbz_gecotest.tipo_doc e on(((a.ft_soc = e.td_soc) and (a.ft_td = e.td_cod)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtri_parametri_v
--
DROP TABLE IF EXISTS `filtri_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtri_parametri_v  AS  select vs28ahbz_gecotest.filtri_parametri.ges_id AS ges_id,vs28ahbz_gecotest.filtri_parametri.id_filtro AS id_filtro,vs28ahbz_gecotest.filtri_parametri.par_conn AS par_conn,vs28ahbz_gecotest.filtri_parametri.parvar01 AS parvar01,vs28ahbz_gecotest.filtri_parametri.parvar02 AS parvar02,vs28ahbz_gecotest.filtri_parametri.parvar03 AS parvar03,vs28ahbz_gecotest.filtri_parametri.parvar04 AS parvar04,vs28ahbz_gecotest.filtri_parametri.parvar05 AS parvar05,vs28ahbz_gecotest.filtri_parametri.parvar06 AS parvar06,vs28ahbz_gecotest.filtri_parametri.parvar07 AS parvar07,vs28ahbz_gecotest.filtri_parametri.parvar08 AS parvar08,vs28ahbz_gecotest.filtri_parametri.parvar09 AS parvar09,vs28ahbz_gecotest.filtri_parametri.parvar10 AS parvar10,vs28ahbz_gecotest.filtri_parametri.parint01 AS parint01,vs28ahbz_gecotest.filtri_parametri.parint02 AS parint02,vs28ahbz_gecotest.filtri_parametri.parint03 AS parint03,vs28ahbz_gecotest.filtri_parametri.parint04 AS parint04,vs28ahbz_gecotest.filtri_parametri.parint05 AS parint05,vs28ahbz_gecotest.filtri_parametri.parint06 AS parint06,vs28ahbz_gecotest.filtri_parametri.parint07 AS parint07,vs28ahbz_gecotest.filtri_parametri.parint08 AS parint08,vs28ahbz_gecotest.filtri_parametri.parint09 AS parint09,vs28ahbz_gecotest.filtri_parametri.parint10 AS parint10,vs28ahbz_gecotest.filtri_parametri.pardat01 AS pardat01,vs28ahbz_gecotest.filtri_parametri.pardat02 AS pardat02,vs28ahbz_gecotest.filtri_parametri.pardat03 AS pardat03,vs28ahbz_gecotest.filtri_parametri.pardat04 AS pardat04,vs28ahbz_gecotest.filtri_parametri.pardat05 AS pardat05,vs28ahbz_gecotest.filtri_parametri.pardat06 AS pardat06,vs28ahbz_gecotest.filtri_parametri.pardat07 AS pardat07,vs28ahbz_gecotest.filtri_parametri.pardat08 AS pardat08,vs28ahbz_gecotest.filtri_parametri.pardat09 AS pardat09,vs28ahbz_gecotest.filtri_parametri.pardat10 AS pardat10,vs28ahbz_gecotest.filtri_parametri.operatore AS operatore,vs28ahbz_gecotest.filtri_parametri.modifica AS modifica from vs28ahbz_gecotest.filtri_parametri where (vs28ahbz_gecotest.filtri_parametri.par_conn = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0000_parametri_v
--
DROP TABLE IF EXISTS `filtro0000_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0000_parametri_v  AS  select filtri_parametri_v.ges_id AS ges_id,filtri_parametri_v.id_filtro AS id_filtro,filtri_parametri_v.par_conn AS par_conn,filtri_parametri_v.parvar01 AS parvar01,filtri_parametri_v.parvar02 AS parvar02,filtri_parametri_v.parvar03 AS parvar03,filtri_parametri_v.parvar04 AS parvar04,filtri_parametri_v.parvar05 AS parvar05,filtri_parametri_v.parvar06 AS parvar06,filtri_parametri_v.parvar07 AS parvar07,filtri_parametri_v.parvar08 AS parvar08,filtri_parametri_v.parvar09 AS parvar09,filtri_parametri_v.parvar10 AS parvar10,filtri_parametri_v.parint01 AS parint01,filtri_parametri_v.parint02 AS parint02,filtri_parametri_v.parint03 AS parint03,filtri_parametri_v.parint04 AS parint04,filtri_parametri_v.parint05 AS parint05,filtri_parametri_v.parint06 AS parint06,filtri_parametri_v.parint07 AS parint07,filtri_parametri_v.parint08 AS parint08,filtri_parametri_v.parint09 AS parint09,filtri_parametri_v.parint10 AS parint10,filtri_parametri_v.pardat01 AS pardat01,filtri_parametri_v.pardat02 AS pardat02,filtri_parametri_v.pardat03 AS pardat03,filtri_parametri_v.pardat04 AS pardat04,filtri_parametri_v.pardat05 AS pardat05,filtri_parametri_v.pardat06 AS pardat06,filtri_parametri_v.pardat07 AS pardat07,filtri_parametri_v.pardat08 AS pardat08,filtri_parametri_v.pardat09 AS pardat09,filtri_parametri_v.pardat10 AS pardat10,filtri_parametri_v.operatore AS operatore,filtri_parametri_v.modifica AS modifica from vs28ahbz_gecotest.filtri_parametri_v where (filtri_parametri_v.id_filtro = 0) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0001_debitori_v
--
DROP TABLE IF EXISTS `filtro0001_debitori_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0001_debitori_v  AS  select b.gd_deb_id AS deb_id,concat(c.deb_cod,'-',c.deb_des) AS deb_des from ((vs28ahbz_gecotest.filtro0001_parametri_v a join vs28ahbz_gecotest.gest_debitore b on((b.gd_ges_id = a.ges_id))) join vs28ahbz_gecotest.debitori c on(((c.deb_soc = ifnull(a.parint01,c.deb_soc)) and (c.deb_id = b.gd_deb_id)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0001_get_datafat_a_v
--
DROP TABLE IF EXISTS `filtro0001_get_datafat_a_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0001_get_datafat_a_v  AS  select ifnull(b.`data`,'gg/mm/aaaa') AS datafat_a_v from (vs28ahbz_gecotest.filtro0001_parametri_v a join vs28ahbz_gecotest.calendario b on((b.id = ifnull(a.parint06,0)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0001_get_datafat_da_v
--
DROP TABLE IF EXISTS `filtro0001_get_datafat_da_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0001_get_datafat_da_v  AS  select ifnull(b.`data`,'gg/mm/aaaa') AS datafat_da_v from (vs28ahbz_gecotest.filtro0001_parametri_v a join vs28ahbz_gecotest.calendario b on((b.id = ifnull(a.parint05,0)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0001_get_tipodoc_v
--
DROP TABLE IF EXISTS `filtro0001_get_tipodoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0001_get_tipodoc_v  AS  select b.td_id AS td_id,b.td_des AS td_des from (vs28ahbz_gecotest.filtro0001_parametri_v a join vs28ahbz_gecotest.filtro0001_tipodoc_v b on((b.td_id = ifnull(a.parint02,0)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0001_parametri_v
--
DROP TABLE IF EXISTS `filtro0001_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0001_parametri_v  AS  select filtri_parametri_v.ges_id AS ges_id,filtri_parametri_v.id_filtro AS id_filtro,filtri_parametri_v.par_conn AS par_conn,filtri_parametri_v.parvar01 AS parvar01,filtri_parametri_v.parvar02 AS parvar02,filtri_parametri_v.parvar03 AS parvar03,filtri_parametri_v.parvar04 AS parvar04,filtri_parametri_v.parvar05 AS parvar05,filtri_parametri_v.parvar06 AS parvar06,filtri_parametri_v.parvar07 AS parvar07,filtri_parametri_v.parvar08 AS parvar08,filtri_parametri_v.parvar09 AS parvar09,filtri_parametri_v.parvar10 AS parvar10,filtri_parametri_v.parint01 AS parint01,filtri_parametri_v.parint02 AS parint02,filtri_parametri_v.parint03 AS parint03,filtri_parametri_v.parint04 AS parint04,filtri_parametri_v.parint05 AS parint05,filtri_parametri_v.parint06 AS parint06,filtri_parametri_v.parint07 AS parint07,filtri_parametri_v.parint08 AS parint08,filtri_parametri_v.parint09 AS parint09,filtri_parametri_v.parint10 AS parint10,filtri_parametri_v.pardat01 AS pardat01,filtri_parametri_v.pardat02 AS pardat02,filtri_parametri_v.pardat03 AS pardat03,filtri_parametri_v.pardat04 AS pardat04,filtri_parametri_v.pardat05 AS pardat05,filtri_parametri_v.pardat06 AS pardat06,filtri_parametri_v.pardat07 AS pardat07,filtri_parametri_v.pardat08 AS pardat08,filtri_parametri_v.pardat09 AS pardat09,filtri_parametri_v.pardat10 AS pardat10,filtri_parametri_v.operatore AS operatore,filtri_parametri_v.modifica AS modifica from vs28ahbz_gecotest.filtri_parametri_v where (filtri_parametri_v.id_filtro = 1) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0001_societa_v
--
DROP TABLE IF EXISTS `filtro0001_societa_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0001_societa_v  AS  select c.soc_cod AS soc_cod,c.soc_des AS soc_des from ((((vs28ahbz_gecotest.filtri_parametri_v upd join vs28ahbz_gecotest.fosuser_gestori fug on((fug.fosuser_gestid = upd.ges_id))) join vs28ahbz_gecotest.gest_debitore a on((a.gd_ges_id = fug.fosuser_gestid))) join vs28ahbz_gecotest.debitori b on((b.deb_id = a.gd_deb_id))) join vs28ahbz_gecotest.societa c on((c.soc_cod = b.deb_soc))) group by c.soc_cod,c.soc_des ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0001_statolav_v
--
DROP TABLE IF EXISTS `filtro0001_statolav_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0001_statolav_v  AS  select 0 AS sl_id,'Tutti' AS sl_des union all select b.id AS sl_id,substr(concat(b.sl_cod,'-',b.sl_descrizione),1,30) AS sl_des from (vs28ahbz_gecotest.filtro0001_parametri_v a join vs28ahbz_gecotest.stato_lavorazione b on((b.sl_soc = ifnull(a.parint01,b.sl_soc)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0001_tipodoc_v
--
DROP TABLE IF EXISTS `filtro0001_tipodoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0001_tipodoc_v  AS  select 0 AS td_id,'Tutti' AS td_des union all select b.td_id AS td_id,b.td_cod AS td_des from (vs28ahbz_gecotest.filtro0001_parametri_v a join vs28ahbz_gecotest.tipo_doc b on((b.td_soc = ifnull(a.parint01,b.td_soc)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0002_liv1_v
--
DROP TABLE IF EXISTS `filtro0002_liv1_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0002_liv1_v  AS  select 0 AS lv1_id,'Tutti' AS lv1_des union all select a.eleord_id AS lv1_id,a.eleord_des AS lv1_des from vs28ahbz_gecotest.filtro0002_eleord a ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0002_liv2_v
--
DROP TABLE IF EXISTS `filtro0002_liv2_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0002_liv2_v  AS  select 0 AS lv2_id,'Tutti' AS lv2_des union all select a.eleord_id AS lv2_id,a.eleord_des AS lv2_des from vs28ahbz_gecotest.filtro0002_eleord a ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0002_liv3_v
--
DROP TABLE IF EXISTS `filtro0002_liv3_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0002_liv3_v  AS  select 0 AS lv3_id,'Tutti' AS lv3_des union all select a.eleord_id AS lv3_id,a.eleord_des AS lv3_des from vs28ahbz_gecotest.filtro0002_eleord a ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_elenco_pagine_v
--
DROP TABLE IF EXISTS `filtro0003_elenco_pagine_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_elenco_pagine_v  AS  select 1 AS seq,'&laquo;' AS valore union select 2 AS seq,'&lsaquo;' AS valore union select 3 AS seq,'1' AS valore union select 4 AS seq,'2' AS valore union select 5 AS seq,'3' AS valore union select 6 AS seq,'4' AS valore union select 7 AS seq,'5' AS valore union select 8 AS seq,'&rsaquo;' AS valore union select 9 AS seq,'&raquo;' AS valore ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_get_limfin_v
--
DROP TABLE IF EXISTS `filtro0003_get_limfin_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_get_limfin_v  AS  select vs28ahbz_gecotest.filtro0003_parconn_v.numpagesp AS numpagesp from vs28ahbz_gecotest.filtro0003_parconn_v ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_get_limini_v
--
DROP TABLE IF EXISTS `filtro0003_get_limini_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_get_limini_v  AS  select vs28ahbz_gecotest.filtro0003_parconn_v.limini AS limini from vs28ahbz_gecotest.filtro0003_parconn_v ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_get_numpagcorr_v
--
DROP TABLE IF EXISTS `filtro0003_get_numpagcorr_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_get_numpagcorr_v  AS  select vs28ahbz_gecotest.filtro0003_parconn_v.numpagcorr AS numpagcorr from vs28ahbz_gecotest.filtro0003_parconn_v ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_get_numpagesp_v
--
DROP TABLE IF EXISTS `filtro0003_get_numpagesp_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_get_numpagesp_v  AS  select vs28ahbz_gecotest.filtro0003_parconn_v.numpagesp AS numpagesp from vs28ahbz_gecotest.filtro0003_parconn_v ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_get_numpagtot_v
--
DROP TABLE IF EXISTS `filtro0003_get_numpagtot_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_get_numpagtot_v  AS  select vs28ahbz_gecotest.filtro0003_parconn_v.numpagtot AS numpagtot from vs28ahbz_gecotest.filtro0003_parconn_v ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_get_numposcorr_v
--
DROP TABLE IF EXISTS `filtro0003_get_numposcorr_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_get_numposcorr_v  AS  select vs28ahbz_gecotest.filtro0003_parconn_v.numposcorr AS numposcorr from vs28ahbz_gecotest.filtro0003_parconn_v ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_get_numrectot_v
--
DROP TABLE IF EXISTS `filtro0003_get_numrectot_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_get_numrectot_v  AS  select vs28ahbz_gecotest.filtro0003_parconn_v.numrectot AS numrectot from vs28ahbz_gecotest.filtro0003_parconn_v ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_get_recperpag_v
--
DROP TABLE IF EXISTS `filtro0003_get_recperpag_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_get_recperpag_v  AS  select vs28ahbz_gecotest.filtro0003_parconn_v.recperpag AS recperpag from vs28ahbz_gecotest.filtro0003_parconn_v ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_parametri_v
--
DROP TABLE IF EXISTS `filtro0003_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_parametri_v  AS  select b.ges_id AS ges_id,c.ges_nome AS ges_nome,a.id_filtro AS id_filtro,a.descrizione AS des_filtro,b.par_conn AS par_conn,b.parint01 AS recperpag,b.parint02 AS numrectot,b.parint03 AS numpagtot,b.parint04 AS numpagcorr,b.parint05 AS numpagesp,b.parint06 AS limini,b.parint07 AS limfin,b.parint08 AS numposcorr from ((vs28ahbz_gecotest.filtri a join vs28ahbz_gecotest.filtri_parametri b on((b.id_filtro = a.id_filtro))) join vs28ahbz_gecotest.gestori c on((c.ges_id = b.ges_id))) where (a.id_filtro = 3) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_parconn_v
--
DROP TABLE IF EXISTS `filtro0003_parconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_parconn_v  AS  select filtro0003_parametri_v.ges_id AS ges_id,filtro0003_parametri_v.ges_nome AS ges_nome,filtro0003_parametri_v.id_filtro AS id_filtro,filtro0003_parametri_v.des_filtro AS des_filtro,filtro0003_parametri_v.par_conn AS par_conn,filtro0003_parametri_v.recperpag AS recperpag,filtro0003_parametri_v.numrectot AS numrectot,filtro0003_parametri_v.numpagtot AS numpagtot,filtro0003_parametri_v.numpagcorr AS numpagcorr,filtro0003_parametri_v.numpagesp AS numpagesp,filtro0003_parametri_v.limini AS limini,filtro0003_parametri_v.limfin AS limfin,filtro0003_parametri_v.numposcorr AS numposcorr from vs28ahbz_gecotest.filtro0003_parametri_v where (filtro0003_parametri_v.par_conn = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro0003_range_pagine_v
--
DROP TABLE IF EXISTS `filtro0003_range_pagine_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro0003_range_pagine_v  AS  select a.id_pagina AS valore from ((vs28ahbz_gecotest.filtro0003_pagine a join vs28ahbz_gecotest.filtro0003_get_numpagcorr_v b) join vs28ahbz_gecotest.filtro0003_get_numpagesp_v c) where (a.id_pagina between b.numpagcorr and ((b.numpagcorr + c.numpagesp) - 1)) ;

-- --------------------------------------------------------

--
-- Struttura per vista filtro003_elenco_pagine_v
--
DROP TABLE IF EXISTS `filtro003_elenco_pagine_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.filtro003_elenco_pagine_v  AS  select 1 AS pag union select 2 AS pag union select 3 AS pag union select 4 AS pag union select 5 AS pag ;

-- --------------------------------------------------------

--
-- Struttura per vista gestione_scadenziario_v
--
DROP TABLE IF EXISTS `gestione_scadenziario_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.gestione_scadenziario_v  AS  select c.id AS id,c.ft_soc AS ft_soc,c.ft_deb_id AS ft_deb_id,c.ft_ced_id AS ft_ced_id,c.ft_attribuzione AS ft_attribuzione,c.ft_num_fat AS ft_num_fat,c.ft_td_id AS ft_td_id,td.td_des AS ft_td,c.ft_id_stato_lav AS ft_id_stato_lav,sl.sl_descrizione AS ft_des_stato_lav,c.ft_testo AS ft_testo,df.databar AS ft_data_fat,ds.databar AS ft_data_scad,c.ft_imp_fat AS ft_imp_fat,c.ft_imp_aperto AS ft_imp_aperto,c.ft_data_ins AS ft_data_ins from ((((((((vs28ahbz_gecotest.filtro0001_parametri_v a join vs28ahbz_gecotest.gest_debitore b on((b.gd_ges_id = a.ges_id))) join vs28ahbz_gecotest.debitori d on(((d.deb_id = b.gd_deb_id) and (d.deb_id = ifnull(a.parint03,d.deb_id))))) join vs28ahbz_gecotest.societa soc on((soc.soc_cod = ifnull(a.parint01,soc.soc_cod)))) join vs28ahbz_gecotest.tipo_doc td on(((td.td_soc = ifnull(a.parint01,soc.soc_cod)) and (td.td_id = ifnull(a.parint02,td.td_id))))) join vs28ahbz_gecotest.stato_lavorazione sl on((sl.id = ifnull(a.parint04,sl.id)))) join vs28ahbz_gecotest.calendario df on((df.id between ifnull(a.parint05,0) and ifnull(a.parint06,9999999)))) join vs28ahbz_gecotest.calendario ds on((ds.id between ifnull(a.parint07,0) and ifnull(a.parint08,9999999)))) join vs28ahbz_gecotest.fatture c on(((c.ft_deb_id = d.deb_id) and (c.ft_soc = soc.soc_cod) and (c.ft_td_id = td.td_id) and (c.ft_id_stato_lav = sl.id) and (c.ft_id_data_fat = df.id) and (c.ft_id_data_scad = ds.id) and (c.ft_num_fat = ifnull(a.parint09,c.ft_num_fat))))) ;

-- --------------------------------------------------------

--
-- Struttura per vista gest_debitore_v
--
DROP TABLE IF EXISTS `gest_debitore_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.gest_debitore_v  AS  select a.ges_id AS ges_id,a.ges_cognome AS ges_cognome,a.ges_nome AS ges_nome,a.ges_ruolo AS ges_ruolo,c.deb_id AS deb_id,c.deb_soc AS deb_soc,c.deb_cod AS beb_cod,c.deb_des AS des_des from ((vs28ahbz_gecotest.gestori a left join vs28ahbz_gecotest.gest_debitore b on((b.gd_ges_id = a.ges_id))) join vs28ahbz_gecotest.debitori c on((c.deb_id = (case a.ges_ruolo when 'admin' then c.deb_id else b.gd_deb_id end)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista statist_movimenti_v
--
DROP TABLE IF EXISTS `statist_movimenti_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.statist_movimenti_v  AS  select a.stmov_gesid AS stmov_gesid,f.fosuser_username AS stmov_username,g.ges_cognome AS cognome,g.ges_nome AS ges_nome,a.stmov_opeid AS stmov_opeid,b.statope_desc AS stmov_des,a.stmod_datetime AS stmod_datetime,d.username AS operatore,a.modifica AS modifica from ((((vs28ahbz_gecotest.statist_movimenti a join vs28ahbz_gecotest.statist_operazioni b on((b.statope_id = a.stmov_id))) join vs28ahbz_gecotest.gestori g on((g.ges_id = a.stmov_gesid))) join vs28ahbz_gecotest.fosuser_gestori f on((f.fosuser_gestid = g.ges_id))) join vs28ahbz_gecotest.fos_user d on((d.id = a.operatore))) ;

-- --------------------------------------------------------

--
-- Struttura per vista tab_procedure_parametri_v
--
DROP TABLE IF EXISTS `tab_procedure_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=vs28ahbz_cris@`88.147.43.61` SQL SECURITY DEFINER VIEW vs28ahbz_gecotest.tab_procedure_parametri_v  AS  select a.id_proc AS id_proc,b.descrizione AS des_proc,a.id_host AS id_host,c.descrizione AS des_host,a.parcar1 AS directory_file_load from ((vs28ahbz_gecotest.tab_procedure_parametri a join vs28ahbz_gecotest.tab_procedure b on((b.id_proc = a.id_proc))) join vs28ahbz_gecotest.tab_procedure_host c on((c.id_host = a.id_host))) ;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle agenda
--
ALTER TABLE agenda
  ADD PRIMARY KEY (id);

--
-- Indici per le tabelle calendario
--
ALTER TABLE calendario
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY calendario_1 (datachar),
  ADD UNIQUE KEY calendario_2 (databar);

--
-- Indici per le tabelle cedenti
--
ALTER TABLE cedenti
  ADD PRIMARY KEY (ced_id),
  ADD UNIQUE KEY cedenti_1 (ced_soc,ced_cod);

--
-- Indici per le tabelle debitori
--
ALTER TABLE debitori
  ADD PRIMARY KEY (deb_id),
  ADD UNIQUE KEY debitori_1 (deb_soc,deb_cod);

--
-- Indici per le tabelle fatture
--
ALTER TABLE fatture
  ADD PRIMARY KEY (id,ft_soc,ft_deb_id,ft_ced_id,ft_num_fat,ft_data_fat,ft_imp_fat),
  ADD UNIQUE KEY fatture_2 (ft_nomefile_csv,ft_id_csv),
  ADD KEY FK_FATTURE_TIPODOC (ft_td_id),
  ADD KEY FK_FATTURE_CEDENTI (ft_ced_id),
  ADD KEY FK_FATTURE_DEBITORI (ft_deb_id),
  ADD KEY FK_FATTURE_SOCIETA (ft_soc),
  ADD KEY FK_FATTURE_STATOLAV (ft_id_stato_lav);

--
-- Indici per le tabelle fatture_fastweb_csv
--
ALTER TABLE fatture_fastweb_csv
  ADD PRIMARY KEY (ft_id_csv);

--
-- Indici per le tabelle fatture_mps_csv
--
ALTER TABLE fatture_mps_csv
  ADD PRIMARY KEY (ft_id_csv);

--
-- Indici per le tabelle filtri
--
ALTER TABLE filtri
  ADD PRIMARY KEY (id_filtro),
  ADD KEY fk_filtri_fosuser (operatore);

--
-- Indici per le tabelle filtri_parametri
--
ALTER TABLE filtri_parametri
  ADD PRIMARY KEY (ges_id,id_filtro),
  ADD KEY fk_filtriparametri_fosuser (operatore);

--
-- Indici per le tabelle filtro0002_eleord
--
ALTER TABLE filtro0002_eleord
  ADD PRIMARY KEY (eleord_id);

--
-- Indici per le tabelle filtro0003_pagine
--
ALTER TABLE filtro0003_pagine
  ADD PRIMARY KEY (id_pagina);

--
-- Indici per le tabelle fosuser_gestori
--
ALTER TABLE fosuser_gestori
  ADD PRIMARY KEY (fosuser_username);

--
-- Indici per le tabelle fos_user
--
ALTER TABLE fos_user
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY UNIQ_957A647992FC23A8 (username_canonical),
  ADD UNIQUE KEY UNIQ_957A6479A0D96FBF (email_canonical),
  ADD UNIQUE KEY UNIQ_957A6479C05FB297 (confirmation_token);

--
-- Indici per le tabelle gestori
--
ALTER TABLE gestori
  ADD PRIMARY KEY (ges_id);

--
-- Indici per le tabelle gest_debitore
--
ALTER TABLE gest_debitore
  ADD PRIMARY KEY (gd_ges_id,gd_deb_id),
  ADD KEY FK_GESTDEBITORE_DEBITORI (gd_deb_id);

--
-- Indici per le tabelle immagini
--
ALTER TABLE immagini
  ADD PRIMARY KEY (id);

--
-- Indici per le tabelle sessions
--
ALTER TABLE sessions
  ADD PRIMARY KEY (sess_id);

--
-- Indici per le tabelle societa
--
ALTER TABLE societa
  ADD PRIMARY KEY (soc_cod);

--
-- Indici per le tabelle statist_movimenti
--
ALTER TABLE statist_movimenti
  ADD PRIMARY KEY (stmov_id),
  ADD KEY fk_stmovimenti_fosuser (operatore),
  ADD KEY fk_stmovimenti_gestori (stmov_gesid),
  ADD KEY fk_stmovimenti_stoperazioni (stmov_opeid);

--
-- Indici per le tabelle statist_operazioni
--
ALTER TABLE statist_operazioni
  ADD PRIMARY KEY (statope_id),
  ADD KEY fk_stoperazioni_fosuser (operatore);

--
-- Indici per le tabelle stato_ft
--
ALTER TABLE stato_ft
  ADD PRIMARY KEY (st_id),
  ADD UNIQUE KEY stato_ft_1 (st_ft_id);

--
-- Indici per le tabelle stato_lavorazione
--
ALTER TABLE stato_lavorazione
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY stato_lavorazione_1 (sl_soc,sl_cod);

--
-- Indici per le tabelle tab_procedure
--
ALTER TABLE tab_procedure
  ADD PRIMARY KEY (id_proc);

--
-- Indici per le tabelle tab_procedure_host
--
ALTER TABLE tab_procedure_host
  ADD PRIMARY KEY (id_host);

--
-- Indici per le tabelle tab_procedure_parametri
--
ALTER TABLE tab_procedure_parametri
  ADD PRIMARY KEY (id_proc),
  ADD KEY fk_tabprochost_tabhost (id_host);

--
-- Indici per le tabelle tipo_doc
--
ALTER TABLE tipo_doc
  ADD PRIMARY KEY (td_id),
  ADD KEY FK_TIPODOC_SOCIETA (td_soc);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella agenda
--
ALTER TABLE agenda
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella calendario
--
ALTER TABLE calendario
  MODIFY id int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella cedenti
--
ALTER TABLE cedenti
  MODIFY ced_id int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella debitori
--
ALTER TABLE debitori
  MODIFY deb_id int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella fatture
--
ALTER TABLE fatture
  MODIFY id int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella fatture_fastweb_csv
--
ALTER TABLE fatture_fastweb_csv
  MODIFY ft_id_csv int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella fatture_mps_csv
--
ALTER TABLE fatture_mps_csv
  MODIFY ft_id_csv int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella fos_user
--
ALTER TABLE fos_user
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella immagini
--
ALTER TABLE immagini
  MODIFY id int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella statist_movimenti
--
ALTER TABLE statist_movimenti
  MODIFY stmov_id int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella stato_lavorazione
--
ALTER TABLE stato_lavorazione
  MODIFY id int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella tab_procedure
--
ALTER TABLE tab_procedure
  MODIFY id_proc int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella tipo_doc
--
ALTER TABLE tipo_doc
  MODIFY td_id int(10) NOT NULL AUTO_INCREMENT;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella cedenti
--
ALTER TABLE cedenti
  ADD CONSTRAINT FK_CEDENTI_SOCIETA FOREIGN KEY (ced_soc) REFERENCES societa (soc_cod);

--
-- Limiti per la tabella debitori
--
ALTER TABLE debitori
  ADD CONSTRAINT FK_debitori_SOCIETA FOREIGN KEY (deb_soc) REFERENCES societa (soc_cod);

--
-- Limiti per la tabella fatture
--
ALTER TABLE fatture
  ADD CONSTRAINT FK_FATTURE_CEDENTI FOREIGN KEY (ft_ced_id) REFERENCES cedenti (ced_id),
  ADD CONSTRAINT FK_FATTURE_DEBITORI FOREIGN KEY (ft_deb_id) REFERENCES debitori (deb_id),
  ADD CONSTRAINT FK_FATTURE_SOCIETA FOREIGN KEY (ft_soc) REFERENCES societa (soc_cod),
  ADD CONSTRAINT FK_FATTURE_STATOLAV FOREIGN KEY (ft_id_stato_lav) REFERENCES stato_lavorazione (id);

--
-- Limiti per la tabella filtri
--
ALTER TABLE filtri
  ADD CONSTRAINT fk_filtri_fosuser FOREIGN KEY (operatore) REFERENCES fos_user (id);

--
-- Limiti per la tabella filtri_parametri
--
ALTER TABLE filtri_parametri
  ADD CONSTRAINT fk_filtriparametri_fosuser FOREIGN KEY (operatore) REFERENCES fos_user (id);

--
-- Limiti per la tabella gest_debitore
--
ALTER TABLE gest_debitore
  ADD CONSTRAINT FK_GESTDEBITORE_DEBITORI FOREIGN KEY (gd_deb_id) REFERENCES debitori (deb_id),
  ADD CONSTRAINT FK_GESTEDBITORI_GESTORI FOREIGN KEY (gd_ges_id) REFERENCES gestori (ges_id);

--
-- Limiti per la tabella statist_movimenti
--
ALTER TABLE statist_movimenti
  ADD CONSTRAINT fk_stmovimenti_fosuser FOREIGN KEY (operatore) REFERENCES fos_user (id),
  ADD CONSTRAINT fk_stmovimenti_gestori FOREIGN KEY (stmov_gesid) REFERENCES gestori (ges_id),
  ADD CONSTRAINT fk_stmovimenti_stoperazioni FOREIGN KEY (stmov_opeid) REFERENCES statist_operazioni (statope_id);

--
-- Limiti per la tabella statist_operazioni
--
ALTER TABLE statist_operazioni
  ADD CONSTRAINT fk_stoperazioni_fosuser FOREIGN KEY (operatore) REFERENCES fos_user (id);

--
-- Limiti per la tabella stato_lavorazione
--
ALTER TABLE stato_lavorazione
  ADD CONSTRAINT FK_STATOLAVORAZIONE_SOCIETA FOREIGN KEY (sl_soc) REFERENCES societa (soc_cod);

--
-- Limiti per la tabella tab_procedure_parametri
--
ALTER TABLE tab_procedure_parametri
  ADD CONSTRAINT fk_tabprochost_tabhost FOREIGN KEY (id_host) REFERENCES tab_procedure_host (id_host),
  ADD CONSTRAINT fk_tabprocpar_tabprocedure FOREIGN KEY (id_proc) REFERENCES tab_procedure (id_proc);

--
-- Limiti per la tabella tipo_doc
--
ALTER TABLE tipo_doc
  ADD CONSTRAINT FK_TIPODOC_SOCIETA FOREIGN KEY (td_soc) REFERENCES societa (soc_cod);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
