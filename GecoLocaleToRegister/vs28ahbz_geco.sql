-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Feb 28, 2018 alle 23:35
-- Versione del server: 5.6.35-cll-lve
-- Versione PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vs28ahbz_gecotest`
--

DELIMITER $$
--
-- Procedure
--
$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

--
-- Funzioni
--
$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura della tabella `agenda`
--

CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `descrizione` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_debitore` bigint(255) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `stato` int(11) DEFAULT NULL,
  `id_societa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `calendario`
--

CREATE TABLE `calendario` (
  `id` int(10) NOT NULL,
  `data` date DEFAULT NULL,
  `gg` int(2) DEFAULT NULL,
  `mese` int(2) DEFAULT NULL,
  `anno` int(4) DEFAULT NULL,
  `databar` varchar(10) DEFAULT NULL,
  `datanum` int(8) DEFAULT NULL,
  `datachar` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `calendario2`
--

CREATE TABLE `calendario2` (
  `id` int(10) NOT NULL DEFAULT '0',
  `data` date DEFAULT NULL,
  `gg` int(2) DEFAULT NULL,
  `mese` int(2) DEFAULT NULL,
  `anno` int(4) DEFAULT NULL,
  `databar` varchar(10) DEFAULT NULL,
  `datanum` int(8) DEFAULT NULL,
  `datachar` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `cedenti`
--

CREATE TABLE `cedenti` (
  `ced_id` int(10) NOT NULL,
  `ced_soc` int(10) NOT NULL,
  `ced_cod` int(10) NOT NULL,
  `ced_des` varchar(50) DEFAULT NULL,
  `ced_citta` varchar(50) DEFAULT NULL,
  `ced_ind` varchar(50) DEFAULT NULL,
  `ced_cap` varchar(5) DEFAULT NULL,
  `ced_pro` varchar(2) DEFAULT NULL,
  `ced_regione` varchar(20) DEFAULT NULL,
  `ced_tel` varchar(10) DEFAULT NULL,
  `ced_mail` varchar(50) DEFAULT NULL,
  `ced_data_affido` date DEFAULT NULL,
  `ced_data_disaffido` date DEFAULT NULL,
  `idold` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `debann_arcdoc`
--

CREATE TABLE `debann_arcdoc` (
  `debandoc_id` int(11) NOT NULL,
  `debandoc_idann` int(11) NOT NULL,
  `debandoc_iddoc` int(11) NOT NULL,
  `debandoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `debandoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `debann_arcdoc_checksino_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `debann_arcdoc_checksino_v` (
`debandoc_idann` int(11)
,`debandoc_numdoc` bigint(21)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `debann_arcdoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `debann_arcdoc_v` (
`debandoc_id` int(11)
,`debandoc_idann` int(11)
,`debandoc_iddoc` int(11)
,`debandoc_estid` int(11)
,`debandoc_estcod` varchar(5)
,`debandoc_estdes` varchar(50)
,`debandoc_tipoid` int(11)
,`debandoc_tipocod` decimal(10,0)
,`debandoc_tipodes` varchar(50)
,`debandoc_desc` varchar(50)
,`debandoc_size` decimal(10,0)
,`debandoc_nomefile` varchar(50)
,`debandoc_ges_id` int(11)
,`debandoc_ges_nominativo` char(100)
,`debandoc_modifica` datetime
,`debandoc_modbar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `debann_checksino_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `debann_checksino_v` (
`debann_debid` int(11)
,`debann_numann` bigint(21)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `debann_tipi_annotazione`
--

CREATE TABLE `debann_tipi_annotazione` (
  `dantpan_id` int(11) NOT NULL,
  `dantpan_idsoc` int(11) NOT NULL,
  `dantpan_cod` decimal(5,0) NOT NULL,
  `dantpan_des` varchar(50) NOT NULL,
  `dantpan_ges_id` int(11) NOT NULL DEFAULT '0',
  `dantpan_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `debann_tipi_contatto`
--

CREATE TABLE `debann_tipi_contatto` (
  `dantpcon_id` int(11) NOT NULL,
  `dantpcon_idsoc` int(11) NOT NULL,
  `dantpcon_cod` decimal(5,0) NOT NULL,
  `dantpcon_des` varchar(50) NOT NULL,
  `dantpcon_ges_id` int(11) NOT NULL DEFAULT '0',
  `dantpcon_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `debitori`
--

CREATE TABLE `debitori` (
  `deb_id` int(10) NOT NULL,
  `deb_soc` int(10) NOT NULL,
  `deb_cod` decimal(10,0) NOT NULL,
  `deb_des` varchar(100) DEFAULT NULL,
  `deb_cf_piva` varchar(16) DEFAULT NULL,
  `deb_citta` varchar(50) DEFAULT NULL,
  `deb_ind` varchar(50) DEFAULT NULL,
  `deb_cap` varchar(5) DEFAULT NULL,
  `deb_pro` varchar(2) DEFAULT NULL,
  `deb_regione` varchar(20) DEFAULT NULL,
  `deb_tel` varchar(30) DEFAULT NULL,
  `deb_mail` varchar(50) DEFAULT NULL,
  `deb_data_affido` date DEFAULT NULL,
  `deb_data_disaffido` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `debitori_annotazioni`
--

CREATE TABLE `debitori_annotazioni` (
  `debann_id` int(11) NOT NULL,
  `debann_debid` int(11) NOT NULL,
  `debann_idtpann` int(11) NOT NULL,
  `debann_idtpcon` int(11) NOT NULL,
  `debann_iddata` int(11) NOT NULL,
  `debann_annot` varchar(2000) DEFAULT NULL,
  `debann_interlocutore` varchar(2000) DEFAULT NULL,
  `debann_ges_id` int(11) NOT NULL DEFAULT '0',
  `debann_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `debitori_annotazioni_csv`
--

CREATE TABLE `debitori_annotazioni_csv` (
  `debann_id` int(11) NOT NULL,
  `debann_debcod` varchar(10) DEFAULT NULL,
  `debann_debid` int(11) DEFAULT NULL,
  `debann_idtpann` int(11) DEFAULT NULL,
  `debann_destpann` varchar(50) DEFAULT NULL,
  `debann_idtpcon` int(11) DEFAULT NULL,
  `debann_destpconn` varchar(50) DEFAULT NULL,
  `debann_iddata` int(11) DEFAULT NULL,
  `debann_desdata` varchar(10) DEFAULT NULL,
  `debann_annot` varchar(2000) DEFAULT NULL,
  `debann_interlocutore` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `debitori_annotazioni_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `debitori_annotazioni_v` (
`debann_id` int(11)
,`debann_debid` int(11)
,`debann_idsoc` int(10)
,`debann_debcod` decimal(10,0)
,`debann_debdes` varchar(100)
,`debann_idtpann` int(11)
,`debann_anncod` decimal(5,0)
,`debann_anndes` varchar(50)
,`debann_idtpcon` int(11)
,`debann_tpcod` decimal(5,0)
,`debann_tpdes` varchar(50)
,`debann_iddata` int(11)
,`debann_datanum` int(8)
,`debann_databar` varchar(10)
,`debann_annot` varchar(2000)
,`debann_inter` varchar(2000)
,`debann_ges_id` int(11)
,`debanno_nominativo` char(100)
,`debann_modifica` datetime
,`debann_modbar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `debitori_fastweb_201710`
--

CREATE TABLE `debitori_fastweb_201710` (
  `deb_id` int(10) NOT NULL DEFAULT '0',
  `deb_soc` int(10) NOT NULL,
  `deb_cod` decimal(10,0) NOT NULL,
  `deb_des` varchar(100) DEFAULT NULL,
  `deb_cf_piva` varchar(16) DEFAULT NULL,
  `deb_citta` varchar(50) DEFAULT NULL,
  `deb_ind` varchar(50) DEFAULT NULL,
  `deb_cap` varchar(5) DEFAULT NULL,
  `deb_pro` varchar(2) DEFAULT NULL,
  `deb_regione` varchar(20) DEFAULT NULL,
  `deb_tel` varchar(30) DEFAULT NULL,
  `deb_mail` varchar(50) DEFAULT NULL,
  `deb_data_affido` date DEFAULT NULL,
  `deb_data_disaffido` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `debitori_fastweb_csv`
--

CREATE TABLE `debitori_fastweb_csv` (
  `deb_id` int(10) NOT NULL DEFAULT '0',
  `deb_soc` int(10) NOT NULL,
  `deb_cod` decimal(10,0) NOT NULL,
  `deb_des` varchar(100) DEFAULT NULL,
  `deb_cf_piva` varchar(16) DEFAULT NULL,
  `deb_citta` varchar(50) DEFAULT NULL,
  `deb_ind` varchar(50) DEFAULT NULL,
  `deb_cap` varchar(5) DEFAULT NULL,
  `deb_pro` varchar(2) DEFAULT NULL,
  `deb_regione` varchar(20) DEFAULT NULL,
  `deb_tel` varchar(30) DEFAULT NULL,
  `deb_mail` varchar(50) DEFAULT NULL,
  `deb_data_affido` date DEFAULT NULL,
  `deb_data_disaffido` date DEFAULT NULL,
  `deb_zona` decimal(2,0) DEFAULT NULL,
  `deb_nomefile_csv` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `estensioni_arcdoc`
--

CREATE TABLE `estensioni_arcdoc` (
  `estarcdoc_id` int(11) NOT NULL,
  `estarcdoc_cod` varchar(5) NOT NULL,
  `estarcdoc_des` varchar(50) NOT NULL,
  `estarcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `estarcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture`
--

CREATE TABLE `fatture` (
  `id` int(10) NOT NULL,
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` int(10) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL,
  `ft_arcdoc_check_sino` decimal(1,0) DEFAULT NULL,
  `ft_tabsr_check_sino` decimal(1,0) DEFAULT NULL,
  `last_ftstlav_id` int(11) DEFAULT NULL,
  `last_ftstlav_id_stato_lav` int(11) NOT NULL DEFAULT '20',
  `last_ftstlav_cod_stato_lav` varchar(4) DEFAULT NULL,
  `last_ftstlav_des_stato_lav` varchar(50) DEFAULT NULL,
  `last_ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `last_fsf_id` int(11) DEFAULT NULL,
  `last_fsf_id_stato` int(11) NOT NULL DEFAULT '1',
  `last_fsf_ac` int(11) NOT NULL DEFAULT '1',
  `ft_forecast_mese` decimal(2,0) DEFAULT NULL,
  `ft_forecast_anno` decimal(4,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_201710`
--

CREATE TABLE `fatture_201710` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` int(10) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL,
  `ft_arcdoc_check_sino` decimal(1,0) DEFAULT NULL,
  `ft_tabsr_check_sino` decimal(1,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_201711a`
--

CREATE TABLE `fatture_201711a` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` int(10) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL,
  `ft_arcdoc_check_sino` decimal(1,0) DEFAULT NULL,
  `ft_tabsr_check_sino` decimal(1,0) DEFAULT NULL,
  `last_ftstlav_id` int(11) DEFAULT NULL,
  `last_ftstlav_id_stato_lav` int(11) NOT NULL DEFAULT '20',
  `last_ftstlav_cod_stato_lav` varchar(4) DEFAULT NULL,
  `last_ftstlav_des_stato_lav` varchar(50) DEFAULT NULL,
  `last_ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `last_fsf_id` int(11) DEFAULT NULL,
  `last_fsf_id_stato` int(11) NOT NULL DEFAULT '1',
  `last_fsf_ac` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_arcdoc`
--

CREATE TABLE `fatture_arcdoc` (
  `ftarcdoc_id` int(11) NOT NULL,
  `ftarcdoc_idfatt` int(11) NOT NULL,
  `ftarcdoc_iddoc` int(11) NOT NULL,
  `ftarcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `ftarcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_arcdoc_201710`
--

CREATE TABLE `fatture_arcdoc_201710` (
  `ftarcdoc_id` int(11) NOT NULL DEFAULT '0',
  `ftarcdoc_idfatt` int(11) NOT NULL,
  `ftarcdoc_iddoc` int(11) NOT NULL,
  `ftarcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `ftarcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_arcdoc_201711a`
--

CREATE TABLE `fatture_arcdoc_201711a` (
  `ftarcdoc_id` int(11) NOT NULL DEFAULT '0',
  `ftarcdoc_idfatt` int(11) NOT NULL,
  `ftarcdoc_iddoc` int(11) NOT NULL,
  `ftarcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `ftarcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_arcdoc_pinna`
--

CREATE TABLE `fatture_arcdoc_pinna` (
  `ftarcdoc_id` int(11) NOT NULL DEFAULT '0',
  `ftarcdoc_idfatt` int(11) NOT NULL,
  `ftarcdoc_iddoc` int(11) NOT NULL,
  `ftarcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `ftarcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `fatture_arcdoc_sino_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `fatture_arcdoc_sino_v` (
`ftarcdoc_idfatt` int(10)
,`ftarcdoc_sino` int(0)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_arcdoc_storico`
--

CREATE TABLE `fatture_arcdoc_storico` (
  `ftarcdoc_id` int(11) NOT NULL DEFAULT '0',
  `ftarcdoc_idfatt` int(11) NOT NULL,
  `ftarcdoc_iddoc` int(11) NOT NULL,
  `ftarcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `ftarcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `fatture_arcdoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `fatture_arcdoc_v` (
`ftarcdoc_id` int(11)
,`ftarcdoc_idfatt` int(11)
,`ftarcdoc_iddoc` int(11)
,`ftarcdoc_estid` int(11)
,`ftarcdoc_estcod` varchar(5)
,`ftarcdoc_estdes` varchar(50)
,`ftarcdoc_tipoid` int(11)
,`ftarcdoc_tipocod` decimal(10,0)
,`ftarcdoc_tipodes` varchar(50)
,`ftarcdoc_desc` varchar(50)
,`ftarcdoc_size` decimal(10,0)
,`ftarcdoc_nomefile` varchar(50)
,`ftarcdoc_ges_id` int(11)
,`ftarcdoc_ges_nominativo` varchar(101)
,`ftarcdoc_modifica` datetime
,`ftarcdoc_modbar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_datesettembre_csv`
--

CREATE TABLE `fatture_datesettembre_csv` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` int(10) NOT NULL,
  `ft_deb_cod` varchar(20) DEFAULT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_data_fat` varchar(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_data_reg` varchar(10) DEFAULT NULL,
  `ft_data_scad` varchar(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_date_settembre`
--

CREATE TABLE `fatture_date_settembre` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` int(10) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_fastweb_csv`
--

CREATE TABLE `fatture_fastweb_csv` (
  `ft_id_csv` int(10) NOT NULL,
  `ft_zona` int(10) DEFAULT NULL,
  `ft_soc` int(10) NOT NULL DEFAULT '1',
  `ft_ced_cod` int(10) DEFAULT NULL,
  `ft_ced_id` int(10) DEFAULT NULL,
  `ft_ced_denom` varchar(100) DEFAULT NULL,
  `ft_deb_cod` decimal(10,0) DEFAULT NULL,
  `ft_deb_id` int(10) DEFAULT NULL,
  `ft_deb_denom` varchar(100) DEFAULT NULL,
  `ft_deb_citta` varchar(50) DEFAULT NULL,
  `ft_deb_prov` varchar(2) DEFAULT NULL,
  `ft_num_fat` varchar(20) DEFAULT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_td_id` int(10) DEFAULT NULL,
  `ft_data_fat` varchar(10) DEFAULT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_data_scad` varchar(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_stato_lav` varchar(100) DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_data_ins` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_fastweb_csv_anomalie`
--

CREATE TABLE `fatture_fastweb_csv_anomalie` (
  `ft_id_csv` int(10) NOT NULL DEFAULT '0',
  `ft_zona` int(10) DEFAULT NULL,
  `ft_soc` int(10) DEFAULT NULL,
  `ft_ced_cod` int(10) DEFAULT NULL,
  `ft_ced_id` int(10) DEFAULT NULL,
  `ft_ced_denom` varchar(100) DEFAULT NULL,
  `ft_deb_cod` int(10) DEFAULT NULL,
  `ft_deb_id` int(10) DEFAULT NULL,
  `ft_deb_denom` varchar(100) DEFAULT NULL,
  `ft_deb_citta` varchar(50) DEFAULT NULL,
  `ft_deb_prov` varchar(2) DEFAULT NULL,
  `ft_num_fat` varchar(20) DEFAULT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_td_id` int(10) DEFAULT NULL,
  `ft_data_fat` varchar(10) DEFAULT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_data_scad` varchar(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_stato_lav` varchar(100) DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_data_ins` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_fastweb_notecrgest`
--

CREATE TABLE `fatture_fastweb_notecrgest` (
  `ftnote_id_nota` int(10) NOT NULL,
  `ftnote_id_fattura` int(10) NOT NULL,
  `ftnote_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftnote_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftnote_id_nota_csv` int(10) NOT NULL,
  `ftnote_nomefile_csv` varchar(100) NOT NULL,
  `ftnote_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_fastweb_notecrgest_csv`
--

CREATE TABLE `fatture_fastweb_notecrgest_csv` (
  `ftnote_id` int(10) NOT NULL,
  `ftnote_idfatt` int(10) DEFAULT NULL,
  `ftnote_societa` int(10) DEFAULT NULL,
  `ftnote_ced_id` int(10) DEFAULT NULL,
  `ftnote_ced_cod` int(10) DEFAULT NULL,
  `ftnote_deb_id` int(10) DEFAULT NULL,
  `ftnote_deb_cod` int(10) DEFAULT NULL,
  `ftnote_id_data_fat` int(10) DEFAULT NULL,
  `ftnote_data_fat` varchar(10) DEFAULT NULL,
  `ftnote_num_fat` varchar(20) DEFAULT NULL,
  `ftnote_attribuzione` varchar(50) DEFAULT NULL,
  `ftnote_imp_fat` decimal(10,2) DEFAULT NULL,
  `ftnote_stato_lav` varchar(100) DEFAULT NULL,
  `ftnote_id_stato_lav` int(10) DEFAULT NULL,
  `ftnote_nota` varchar(1000) DEFAULT NULL,
  `ftnote_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftnote_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_fastweb_statilavnote_csv`
--

CREATE TABLE `fatture_fastweb_statilavnote_csv` (
  `ftstlav_id` int(10) NOT NULL,
  `ftstlav_idfatt` int(10) DEFAULT NULL,
  `ftstlav_societa` int(10) DEFAULT NULL,
  `ftstlav_ced_id` int(10) DEFAULT NULL,
  `ftstlav_ced_cod` decimal(10,0) DEFAULT NULL,
  `ftstlav_deb_id` int(10) DEFAULT NULL,
  `ftstlav_deb_cod` decimal(10,0) DEFAULT NULL,
  `ftstlav_id_data_fat` int(10) DEFAULT NULL,
  `ftstlav_data_fat` varchar(10) DEFAULT NULL,
  `ftstlav_num_fat` varchar(20) DEFAULT NULL,
  `ftstlav_attribuzione` varchar(50) DEFAULT NULL,
  `ftstlav_imp_fat` decimal(10,2) DEFAULT NULL,
  `ftstlav_stato_lav` varchar(100) DEFAULT NULL,
  `ftstlav_id_stato_lav` int(10) DEFAULT NULL,
  `ftstlav_nota` varchar(1000) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_idfatt_temp`
--

CREATE TABLE `fatture_idfatt_temp` (
  `id_fattura` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_mps_csv`
--

CREATE TABLE `fatture_mps_csv` (
  `ft_id_csv` int(10) NOT NULL,
  `ft_ced_cod` int(10) DEFAULT NULL,
  `ft_ced_id` int(10) DEFAULT NULL,
  `ft_ced_denom` varchar(100) DEFAULT NULL,
  `ft_deb_cod` int(10) DEFAULT NULL,
  `ft_deb_id` int(10) DEFAULT NULL,
  `ft_deb_denom` varchar(100) DEFAULT NULL,
  `ft_num_fat` varchar(20) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_td_id` int(10) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` varchar(10) DEFAULT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_data_scad` varchar(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) DEFAULT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_notecrgest`
--

CREATE TABLE `fatture_notecrgest` (
  `ftnote_id_nota` int(10) NOT NULL,
  `ftnote_id_fattura` int(10) NOT NULL,
  `ftnote_id_stato_lav` int(10) NOT NULL,
  `ftnote_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftnote_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftnote_id_nota_csv` int(10) DEFAULT NULL,
  `ftnote_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftnote_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_notecrgest_aprile`
--

CREATE TABLE `fatture_notecrgest_aprile` (
  `ftnote_id_nota` int(10) NOT NULL DEFAULT '0',
  `ftnote_id_fattura` int(10) NOT NULL,
  `ftnote_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftnote_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftnote_id_nota_csv` int(10) DEFAULT NULL,
  `ftnote_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftnote_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `fatture_notecrgest_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `fatture_notecrgest_v` (
`ftnote_id_nota` int(10)
,`ftnote_id_fattura` int(10)
,`ftnote_nota_crgest` varchar(1000)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_ori`
--

CREATE TABLE `fatture_ori` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` int(10) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_pinna`
--

CREATE TABLE `fatture_pinna` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` int(10) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL,
  `ft_arcdoc_check_sino` decimal(1,0) DEFAULT NULL,
  `ft_tabsr_check_sino` decimal(1,0) DEFAULT NULL,
  `last_ftstlav_id` int(11) DEFAULT NULL,
  `last_ftstlav_id_stato_lav` int(11) NOT NULL DEFAULT '20',
  `last_ftstlav_cod_stato_lav` varchar(4) DEFAULT NULL,
  `last_ftstlav_des_stato_lav` varchar(50) DEFAULT NULL,
  `last_ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `last_fsf_id` int(11) DEFAULT NULL,
  `last_fsf_id_stato` int(11) NOT NULL DEFAULT '1',
  `last_fsf_ac` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_pre_ftdebid`
--

CREATE TABLE `fatture_pre_ftdebid` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` decimal(10,0) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL,
  `ft_arcdoc_check_sino` decimal(1,0) DEFAULT NULL,
  `ft_tabsr_check_sino` decimal(1,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_stati`
--

CREATE TABLE `fatture_stati` (
  `fs_id` int(3) NOT NULL,
  `fs_soc` int(10) NOT NULL,
  `fs_cod` int(3) DEFAULT NULL,
  `fs_desc` varchar(50) DEFAULT NULL,
  `fs_ges_id` int(10) NOT NULL DEFAULT '0',
  `fs_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_statilavnote_storico`
--

CREATE TABLE `fatture_statilavnote_storico` (
  `ftstlav_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_fattura` int(10) NOT NULL,
  `ftstlav_id_stato_lav` int(10) NOT NULL,
  `ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftstlav_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_nota_csv` int(10) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `fatture_statilavnote_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `fatture_statilavnote_v` (
`ftstlav_id` int(10)
,`ftstlav_id_fattura` int(10)
,`ftstlav_id_stato_lav` int(10)
,`ftstlav_slcod` varchar(4)
,`ftstlav_sldes` varchar(50)
,`ftstlav_nota_crgest` varchar(1000)
,`ftstlav_data_bar` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_statilav_note`
--

CREATE TABLE `fatture_statilav_note` (
  `ftstlav_id` int(10) NOT NULL,
  `ftstlav_id_fattura` int(10) NOT NULL,
  `ftstlav_id_stato_lav` int(10) NOT NULL,
  `ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftstlav_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_nota_csv` int(10) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_statilav_note_201710`
--

CREATE TABLE `fatture_statilav_note_201710` (
  `ftstlav_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_fattura` int(10) NOT NULL,
  `ftstlav_id_stato_lav` int(10) NOT NULL,
  `ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftstlav_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_nota_csv` int(10) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_statilav_note_201711a`
--

CREATE TABLE `fatture_statilav_note_201711a` (
  `ftstlav_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_fattura` int(10) NOT NULL,
  `ftstlav_id_stato_lav` int(10) NOT NULL,
  `ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftstlav_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_nota_csv` int(10) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_statilav_note_pinna`
--

CREATE TABLE `fatture_statilav_note_pinna` (
  `ftstlav_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_fattura` int(10) NOT NULL,
  `ftstlav_id_stato_lav` int(10) NOT NULL,
  `ftstlav_nota_crgest` varchar(1000) DEFAULT NULL,
  `ftstlav_ges_id` int(10) NOT NULL DEFAULT '0',
  `ftstlav_id_nota_csv` int(10) DEFAULT NULL,
  `ftstlav_nomefile_csv` varchar(100) DEFAULT NULL,
  `ftstlav_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `fatture_stati_fattura_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `fatture_stati_fattura_v` (
`fsf_id` int(3)
,`fsf_soc` int(10)
,`fsf_id_fattura` int(10)
,`fsf_id_stato` int(3)
,`fsf_cod` int(3)
,`fsf_desc` varchar(50)
,`fsf_ges_id` int(10)
,`fsf_modifica` datetime
,`fsf_nomefile_csv` varchar(100)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_statofattura_storico`
--

CREATE TABLE `fatture_statofattura_storico` (
  `fsf_id` int(3) NOT NULL DEFAULT '0',
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `fatture_stato_aperta_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `fatture_stato_aperta_v` (
`fsfa_id_fattura` int(10)
,`fsfa_num_fat` varchar(20)
,`fsfa_imp_fat` decimal(10,2)
,`fsfa_id_stato` int(3)
,`fsfa_ac` int(0)
,`fsfa_cod_stato` int(3)
,`fsfa_des_stato` varchar(50)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_stato_fattura`
--

CREATE TABLE `fatture_stato_fattura` (
  `fsf_id` int(3) NOT NULL,
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_stato_fattura_201710`
--

CREATE TABLE `fatture_stato_fattura_201710` (
  `fsf_id` int(3) NOT NULL DEFAULT '0',
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_stato_fattura_201711a`
--

CREATE TABLE `fatture_stato_fattura_201711a` (
  `fsf_id` int(3) NOT NULL DEFAULT '0',
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_stato_fattura_fastweb_csv`
--

CREATE TABLE `fatture_stato_fattura_fastweb_csv` (
  `fsf_id` int(3) NOT NULL,
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `fatture_stato_fattura_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `fatture_stato_fattura_v` (
`fsf_id` int(3)
,`fsf_soc` int(10)
,`fsf_cod` int(3)
,`fsf_id_fattura` int(10)
,`fsf_id_stato` int(3)
,`fsf_ac` int(0)
,`fsf_desc` varchar(50)
,`fsf_ges_id` int(10)
,`fsf_modifica` datetime
,`fsf_nomefile_csv` varchar(100)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_stato_fatura_maggio`
--

CREATE TABLE `fatture_stato_fatura_maggio` (
  `fsf_id` int(3) NOT NULL DEFAULT '0',
  `fsf_id_fattura` int(10) NOT NULL,
  `fsf_id_stato` int(3) NOT NULL,
  `fsf_ges_id` int(10) NOT NULL DEFAULT '0',
  `fsf_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fsf_nomefile_csv` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_storico`
--

CREATE TABLE `fatture_storico` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ft_soc` int(10) NOT NULL,
  `ft_deb_id` decimal(10,0) NOT NULL,
  `ft_ced_id` int(10) NOT NULL,
  `ft_num_fat` varchar(20) NOT NULL,
  `ft_attribuzione` varchar(50) DEFAULT NULL,
  `ft_td` varchar(5) DEFAULT NULL,
  `ft_testo` varchar(500) DEFAULT NULL,
  `ft_data_fat` date NOT NULL,
  `ft_data_scad` date DEFAULT NULL,
  `ft_imp_fat` decimal(10,2) NOT NULL,
  `ft_imp_aperto` decimal(10,2) DEFAULT NULL,
  `ft_data_ins` date DEFAULT NULL,
  `ft_id_stato_lav` int(10) DEFAULT NULL,
  `ft_td_id` int(10) NOT NULL,
  `ft_id_data_fat` int(10) DEFAULT NULL,
  `ft_id_data_scad` int(10) DEFAULT NULL,
  `ft_nomefile_csv` varchar(100) DEFAULT NULL,
  `ft_id_csv` int(10) DEFAULT NULL,
  `ft_datetime_csv` datetime DEFAULT NULL,
  `ft_id_data_chius` int(10) DEFAULT NULL,
  `ft_data_chius` datetime DEFAULT NULL,
  `ft_arcdoc_check_sino` decimal(1,0) DEFAULT NULL,
  `ft_tabsr_check_sino` decimal(1,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_tabsr`
--

CREATE TABLE `fatture_tabsr` (
  `fttbsr_id` int(11) NOT NULL,
  `fttbsr_idfatt` int(11) NOT NULL,
  `fttbsr_idsr` int(11) NOT NULL,
  `fttbsr_ges_id` int(11) NOT NULL DEFAULT '0',
  `fttbsr_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_tabsr_201710`
--

CREATE TABLE `fatture_tabsr_201710` (
  `fttbsr_id` int(11) NOT NULL DEFAULT '0',
  `fttbsr_idfatt` int(11) NOT NULL,
  `fttbsr_idsr` int(11) NOT NULL,
  `fttbsr_ges_id` int(11) NOT NULL DEFAULT '0',
  `fttbsr_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fatture_tabsr_storico`
--

CREATE TABLE `fatture_tabsr_storico` (
  `fttbsr_id` int(11) NOT NULL DEFAULT '0',
  `fttbsr_idfatt` int(11) NOT NULL,
  `fttbsr_idsr` int(11) NOT NULL,
  `fttbsr_ges_id` int(11) NOT NULL DEFAULT '0',
  `fttbsr_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `fatture_tabsr_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `fatture_tabsr_v` (
`fttbsr_id` int(11)
,`fttbsr_idfatt` int(11)
,`fttbsr_idsr` int(11)
,`fttbsr_numero` varchar(20)
,`fttbsr_nota` varchar(2000)
,`fttbsr_ges_nominativo` char(100)
,`fttbsr_modifica` datetime
,`fttbsr_modbar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `fatture_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `fatture_v` (
`id` int(10)
,`soc_cod` int(10)
,`soc_des` varchar(50)
,`deb_id` int(10)
,`cod_deb` int(10)
,`deb_cod` decimal(10,0)
,`deb_des` varchar(100)
,`ced_id` int(10)
,`ced_des` varchar(50)
,`num_fat` varchar(20)
,`attribuzione` varchar(50)
,`tipo_doc_cod` varchar(5)
,`tipo_doc_des` varchar(100)
,`testo` varchar(500)
,`data_fat` date
,`id_data_fat` int(10)
,`data_scad` date
,`id_data_scad` int(10)
,`imp_fat` decimal(10,2)
,`imp_aperto` decimal(10,2)
,`data_ins` date
,`id_stato_lav_cod` int(10)
,`cod_stato_fat` bigint(11)
,`des_stato_fat` varchar(50)
,`data_chius` datetime
,`id_data_chius` int(10)
,`ft_nomefile_csv` varchar(100)
,`fsf_nomefile_csv` varchar(100)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `filtri`
--

CREATE TABLE `filtri` (
  `id_filtro` int(10) NOT NULL,
  `descrizione` varchar(50) DEFAULT NULL,
  `operatore` int(11) NOT NULL DEFAULT '0',
  `modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `filtri_parametri`
--

CREATE TABLE `filtri_parametri` (
  `ges_id` int(10) NOT NULL,
  `id_filtro` int(10) NOT NULL,
  `par_conn` int(10) DEFAULT NULL,
  `parvar01` varchar(50) DEFAULT NULL,
  `parvar02` varchar(50) DEFAULT NULL,
  `parvar03` varchar(50) DEFAULT NULL,
  `parvar04` varchar(50) DEFAULT NULL,
  `parvar05` varchar(50) DEFAULT NULL,
  `parvar06` varchar(50) DEFAULT NULL,
  `parvar07` varchar(50) DEFAULT NULL,
  `parvar08` varchar(50) DEFAULT NULL,
  `parvar09` varchar(50) DEFAULT NULL,
  `parvar10` varchar(50) DEFAULT NULL,
  `parvar11` varchar(50) DEFAULT NULL,
  `parvar12` varchar(50) DEFAULT NULL,
  `parvar13` varchar(50) DEFAULT NULL,
  `parvar14` varchar(50) DEFAULT NULL,
  `parvar15` varchar(50) DEFAULT NULL,
  `parvar16` varchar(50) DEFAULT NULL,
  `parvar17` varchar(50) DEFAULT NULL,
  `parvar18` varchar(50) DEFAULT NULL,
  `parvar19` varchar(50) DEFAULT NULL,
  `parvar20` varchar(50) DEFAULT NULL,
  `parvar21` varchar(50) DEFAULT NULL,
  `parvar22` varchar(50) DEFAULT NULL,
  `parvar23` varchar(50) DEFAULT NULL,
  `parvar24` varchar(50) DEFAULT NULL,
  `parvar25` varchar(50) DEFAULT NULL,
  `parvar26` varchar(50) DEFAULT NULL,
  `parvar27` varchar(50) DEFAULT NULL,
  `parvar28` varchar(50) DEFAULT NULL,
  `parvar29` varchar(50) DEFAULT NULL,
  `parvar30` varchar(50) DEFAULT NULL,
  `parint01` decimal(10,0) DEFAULT NULL,
  `parint02` decimal(10,0) DEFAULT NULL,
  `parint03` decimal(10,0) DEFAULT NULL,
  `parint04` decimal(10,0) DEFAULT NULL,
  `parint05` decimal(10,0) DEFAULT NULL,
  `parint06` decimal(10,0) DEFAULT NULL,
  `parint07` decimal(10,0) DEFAULT NULL,
  `parint08` decimal(10,0) DEFAULT NULL,
  `parint09` decimal(10,0) DEFAULT NULL,
  `parint10` decimal(10,0) DEFAULT NULL,
  `parint11` decimal(10,0) DEFAULT NULL,
  `parint12` decimal(10,0) DEFAULT NULL,
  `parint13` decimal(10,0) DEFAULT NULL,
  `parint14` decimal(10,0) DEFAULT NULL,
  `parint15` decimal(10,0) DEFAULT NULL,
  `parint16` decimal(10,0) DEFAULT NULL,
  `parint17` decimal(10,0) DEFAULT NULL,
  `parint18` decimal(10,0) DEFAULT NULL,
  `parint19` decimal(10,0) DEFAULT NULL,
  `parint20` decimal(10,0) DEFAULT NULL,
  `parint21` decimal(10,0) DEFAULT NULL,
  `parint22` decimal(10,0) DEFAULT NULL,
  `parint23` decimal(10,0) DEFAULT NULL,
  `parint24` decimal(10,0) DEFAULT NULL,
  `parint25` decimal(10,0) DEFAULT NULL,
  `parint26` decimal(10,0) DEFAULT NULL,
  `parint27` decimal(10,0) DEFAULT NULL,
  `parint28` decimal(10,0) DEFAULT NULL,
  `parint29` decimal(10,0) DEFAULT NULL,
  `parint30` decimal(10,0) DEFAULT NULL,
  `pardat01` date DEFAULT NULL,
  `pardat02` date DEFAULT NULL,
  `pardat03` date DEFAULT NULL,
  `pardat04` date DEFAULT NULL,
  `pardat05` date DEFAULT NULL,
  `pardat06` date DEFAULT NULL,
  `pardat07` date DEFAULT NULL,
  `pardat08` date DEFAULT NULL,
  `pardat09` date DEFAULT NULL,
  `pardat10` date DEFAULT NULL,
  `operatore` int(11) NOT NULL DEFAULT '0',
  `modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtri_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtri_parametri_v` (
`ges_id` int(10)
,`id_filtro` int(10)
,`par_conn` int(10)
,`parvar01` varchar(50)
,`parvar02` varchar(50)
,`parvar03` varchar(50)
,`parvar04` varchar(50)
,`parvar05` varchar(50)
,`parvar06` varchar(50)
,`parvar07` varchar(50)
,`parvar08` varchar(50)
,`parvar09` varchar(50)
,`parvar10` varchar(50)
,`parvar11` varchar(50)
,`parvar12` varchar(50)
,`parvar13` varchar(50)
,`parvar14` varchar(50)
,`parvar15` varchar(50)
,`parvar16` varchar(50)
,`parvar17` varchar(50)
,`parvar18` varchar(50)
,`parvar19` varchar(50)
,`parvar20` varchar(50)
,`parvar21` varchar(50)
,`parvar22` varchar(50)
,`parvar23` varchar(50)
,`parvar24` varchar(50)
,`parvar25` varchar(50)
,`parvar26` varchar(50)
,`parvar27` varchar(50)
,`parvar28` varchar(50)
,`parvar29` varchar(50)
,`parvar30` varchar(50)
,`parint01` decimal(10,0)
,`parint02` decimal(10,0)
,`parint03` decimal(10,0)
,`parint04` decimal(10,0)
,`parint05` decimal(10,0)
,`parint06` decimal(10,0)
,`parint07` decimal(10,0)
,`parint08` decimal(10,0)
,`parint09` decimal(10,0)
,`parint10` decimal(10,0)
,`parint11` decimal(10,0)
,`parint12` decimal(10,0)
,`parint13` decimal(10,0)
,`parint14` decimal(10,0)
,`parint15` decimal(10,0)
,`parint16` decimal(10,0)
,`parint17` decimal(10,0)
,`parint18` decimal(10,0)
,`parint19` decimal(10,0)
,`parint20` decimal(10,0)
,`parint21` decimal(10,0)
,`parint22` decimal(10,0)
,`parint23` decimal(10,0)
,`parint24` decimal(10,0)
,`parint25` decimal(10,0)
,`parint26` decimal(10,0)
,`parint27` decimal(10,0)
,`parint28` decimal(10,0)
,`parint29` decimal(10,0)
,`parint30` decimal(10,0)
,`pardat01` date
,`pardat02` date
,`pardat03` date
,`pardat04` date
,`pardat05` date
,`pardat06` date
,`pardat07` date
,`pardat08` date
,`pardat09` date
,`pardat10` date
,`operatore` int(11)
,`modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0000_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0000_parametri_v` (
`ges_id` int(10)
,`id_filtro` int(10)
,`par_conn` int(10)
,`parvar01` varchar(50)
,`parvar02` varchar(50)
,`parvar03` varchar(50)
,`parvar04` varchar(50)
,`parvar05` varchar(50)
,`parvar06` varchar(50)
,`parvar07` varchar(50)
,`parvar08` varchar(50)
,`parvar09` varchar(50)
,`parvar10` varchar(50)
,`parint01` decimal(10,0)
,`parint02` decimal(10,0)
,`parint03` decimal(10,0)
,`parint04` decimal(10,0)
,`parint05` decimal(10,0)
,`parint06` decimal(10,0)
,`parint07` decimal(10,0)
,`parint08` decimal(10,0)
,`parint09` decimal(10,0)
,`parint10` decimal(10,0)
,`pardat01` date
,`pardat02` date
,`pardat03` date
,`pardat04` date
,`pardat05` date
,`pardat06` date
,`pardat07` date
,`pardat08` date
,`pardat09` date
,`pardat10` date
,`operatore` int(11)
,`modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_debitori_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_debitori_v` (
`deb_id` bigint(11)
,`deb_des` varchar(112)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_get_datafat_a_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_get_datafat_a_v` (
`datafat_a_v` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_get_datafat_da_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_get_datafat_da_v` (
`datafat_da_v` varchar(10)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_get_debitore_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_get_debitore_v` (
`deb_id` int(1)
,`deb_des` int(1)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_get_statolav_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_get_statolav_v` (
`sl_id` int(1)
,`sl_des` int(1)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_get_tipodoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_get_tipodoc_v` (
`td_id` bigint(11)
,`td_des` varchar(5)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_parametri_v` (
`ges_id` int(10)
,`id_filtro` int(10)
,`par_conn` int(10)
,`parvar01` varchar(50)
,`parvar02` varchar(50)
,`parvar03` varchar(50)
,`parvar04` varchar(50)
,`parvar05` varchar(50)
,`parvar06` varchar(50)
,`parvar07` varchar(50)
,`parvar08` varchar(50)
,`parvar09` varchar(50)
,`parvar10` varchar(50)
,`parvar11` varchar(50)
,`parvar12` varchar(50)
,`parvar13` varchar(50)
,`parvar14` varchar(50)
,`parvar15` varchar(50)
,`parvar16` varchar(50)
,`parvar17` varchar(50)
,`parvar18` varchar(50)
,`parvar19` varchar(50)
,`parvar20` varchar(50)
,`parvar21` varchar(50)
,`parvar22` varchar(50)
,`parvar23` varchar(50)
,`parvar24` varchar(50)
,`parvar25` varchar(50)
,`parvar26` varchar(50)
,`parvar27` varchar(50)
,`parvar28` varchar(50)
,`parvar29` varchar(50)
,`parvar30` varchar(50)
,`parint01` decimal(10,0)
,`parint02` decimal(10,0)
,`parint03` decimal(10,0)
,`parint04` decimal(10,0)
,`parint05` decimal(10,0)
,`parint06` decimal(10,0)
,`parint07` decimal(10,0)
,`parint08` decimal(10,0)
,`parint09` decimal(10,0)
,`parint10` decimal(10,0)
,`parint11` decimal(10,0)
,`parint12` decimal(10,0)
,`parint13` decimal(10,0)
,`parint14` decimal(10,0)
,`parint15` decimal(10,0)
,`parint16` decimal(10,0)
,`parint17` decimal(10,0)
,`parint18` decimal(10,0)
,`parint19` decimal(10,0)
,`parint20` decimal(10,0)
,`parint21` decimal(10,0)
,`parint22` decimal(10,0)
,`parint23` decimal(10,0)
,`parint24` decimal(10,0)
,`parint25` decimal(10,0)
,`parint26` decimal(10,0)
,`parint27` decimal(10,0)
,`parint28` decimal(10,0)
,`parint29` decimal(10,0)
,`parint30` decimal(10,0)
,`pardat01` date
,`pardat02` date
,`pardat03` date
,`pardat04` date
,`pardat05` date
,`pardat06` date
,`pardat07` date
,`pardat08` date
,`pardat09` date
,`pardat10` date
,`operatore` int(11)
,`modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_societa_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_societa_v` (
`soc_cod` int(10)
,`soc_des` varchar(50)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_statolav_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_statolav_v` (
`sl_id` bigint(11)
,`sl_des` varchar(25)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_tipoarcdoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_tipoarcdoc_v` (
`tparcdoc_id` int(11)
,`tparcdoc_soc` int(11)
,`tparcdoc_socdes` varchar(50)
,`tparcdoc_cod` decimal(10,0)
,`tparcdoc_des` varchar(50)
,`tparcdoc_ges_id` int(11)
,`tparcdoc_gesnome` varchar(101)
,`tparcdoc_modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0001_tipodoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0001_tipodoc_v` (
`td_id` bigint(11)
,`td_des` varchar(5)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `filtro0002_eleord`
--

CREATE TABLE `filtro0002_eleord` (
  `eleord_id` int(2) NOT NULL,
  `eleord_cod` int(2) NOT NULL,
  `eleord_des` varchar(50) DEFAULT NULL,
  `eleord_campo` varchar(50) NOT NULL,
  `eleord_ope` varchar(100) NOT NULL,
  `operatore` varchar(50) NOT NULL DEFAULT 'SYSTEM',
  `modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0002_liv1_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0002_liv1_v` (
`lv1_id` bigint(11)
,`lv1_des` varchar(50)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0002_liv2_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0002_liv2_v` (
`lv2_id` bigint(11)
,`lv2_des` varchar(50)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0002_liv3_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0002_liv3_v` (
`lv3_id` bigint(11)
,`lv3_des` varchar(50)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `filtro0003_elementi_pagina`
--

CREATE TABLE `filtro0003_elementi_pagina` (
  `id` int(3) NOT NULL,
  `valore` int(3) DEFAULT NULL,
  `descrizione` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_elenco_pagine_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_elenco_pagine_v` (
`seq` bigint(20)
,`valore` varchar(8)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_get_idrecperpag_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_get_idrecperpag_v` (
`idrecperpag` int(3)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_get_limfin_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_get_limfin_v` (
`numpagesp` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_get_limini_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_get_limini_v` (
`limini` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_get_numpagcorr_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_get_numpagcorr_v` (
`numpagcorr` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_get_numpagesp_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_get_numpagesp_v` (
`numpagesp` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_get_numpagtot_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_get_numpagtot_v` (
`numpagtot` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_get_numposcorr_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_get_numposcorr_v` (
`numposcorr` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_get_numrectot_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_get_numrectot_v` (
`numrectot` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_get_recperpag_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_get_recperpag_v` (
`valrecperpag` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_get_valrecperpag_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_get_valrecperpag_v` (
`valrecperpag` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `filtro0003_pagine`
--

CREATE TABLE `filtro0003_pagine` (
  `id_pagina` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_parametri_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`idrecperpag` int(3)
,`valrecperpag` decimal(10,0)
,`desrecperpag` varchar(10)
,`numrectot` decimal(10,0)
,`numpagtot` decimal(10,0)
,`numpagcorr` decimal(10,0)
,`numpagesp` decimal(10,0)
,`limini` decimal(10,0)
,`limfin` decimal(10,0)
,`numposcorr` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_parconn_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_parconn_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`idrecperpag` int(3)
,`valrecperpag` decimal(10,0)
,`desrecperpag` varchar(10)
,`numrectot` decimal(10,0)
,`numpagtot` decimal(10,0)
,`numpagcorr` decimal(10,0)
,`numpagesp` decimal(10,0)
,`limini` decimal(10,0)
,`limfin` decimal(10,0)
,`numposcorr` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0003_range_pagine_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0003_range_pagine_v` (
`valore` int(10)
,`numpagcorr` decimal(10,0)
,`numposcorr` decimal(10,0)
,`sinistra` decimal(11,0)
,`destra` decimal(11,0)
,`valsin` decimal(12,0)
,`valdes` decimal(12,0)
,`numpagtot` decimal(10,0)
,`numpagesp` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `filtro0003_valori_default`
--

CREATE TABLE `filtro0003_valori_default` (
  `idrecperpag` decimal(4,0) NOT NULL,
  `numpagesp` decimal(4,0) NOT NULL,
  `numpagcorr` decimal(4,0) NOT NULL,
  `numposcorr` decimal(4,0) NOT NULL,
  `maxrecperpag` decimal(9,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0004_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0004_parametri_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_lastid` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0004_parconn_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0004_parconn_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_lastid` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0005_elearcdoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0005_elearcdoc_v` (
`elearcdoc_id` int(11)
,`elearcdoc_idfatt` int(11)
,`elearcdoc_iddoc` int(11)
,`elearcdoc_estid` int(11)
,`elearcdoc_estcod` varchar(5)
,`elearcdoc_estdes` varchar(50)
,`elearcdoc_tipoid` int(11)
,`elearcdoc_tipocod` decimal(10,0)
,`elearcdoc_tipodes` varchar(50)
,`elearcdoc_desc` varchar(50)
,`elearcdoc_size` decimal(10,0)
,`elearcdoc_nomefile` varchar(50)
,`elearcdoc_ges_id` int(11)
,`elearcdoc_ges_nominativo` varchar(101)
,`elearcdoc_modifica` datetime
,`elearcdoc_modbar` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0005_fatture_arcdoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0005_fatture_arcdoc_v` (
`ftarcdoc_id` int(11)
,`ftarcdoc_idfatt` int(11)
,`ftarcdoc_iddoc` int(11)
,`ftarcdoc_estid` int(11)
,`ftarcdoc_estcod` varchar(5)
,`ftarcdoc_estdes` varchar(50)
,`ftarcdoc_tipoid` int(11)
,`ftarcdoc_tipocod` decimal(10,0)
,`ftarcdoc_tipodes` varchar(50)
,`ftarcdoc_desc` varchar(50)
,`ftarcdoc_size` decimal(10,0)
,`ftarcdoc_nomefile` varchar(50)
,`ftarcdoc_ges_id` int(11)
,`ftarcdoc_ges_nominativo` varchar(101)
,`ftarcdoc_modifica` datetime
,`ftarcdoc_modbar` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0005_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0005_parametri_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_idfatt` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0005_parconn_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0005_parconn_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_idfatt` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0006_debann_tpann_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0006_debann_tpann_v` (
`dantpan_id` int(11)
,`dantpan_idsoc` int(11)
,`dantpan_cod` decimal(5,0)
,`dantpan_des` varchar(50)
,`dantpan_gesid` int(11)
,`dantpan_nominativo` char(100)
,`dantpan_modifica` datetime
,`dantpan_modbar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0006_debann_tpcon_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0006_debann_tpcon_v` (
`dantpcon_id` int(11)
,`dantpcon_idsoc` int(11)
,`dantpcon_cod` decimal(5,0)
,`dantpcon_des` varchar(50)
,`dantpcon_gesid` int(11)
,`dantpcon_nominativo` char(100)
,`dantpcon_modifica` datetime
,`dantpcon_modbar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0006_debann_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0006_debann_v` (
`debann_id` int(11)
,`deann_debid` int(11)
,`debann_idsoc` int(10)
,`debann_debcod` decimal(10,0)
,`debann_debdes` varchar(100)
,`debann_idtpann` int(11)
,`debann_anncod` decimal(5,0)
,`debann_anndes` varchar(50)
,`debann_idtpcon` int(11)
,`debann_tpcod` decimal(5,0)
,`debann_tpdes` varchar(50)
,`debann_iddata` int(11)
,`debann_datanum` int(8)
,`debann_databar` varchar(10)
,`debann_annot` varchar(2000)
,`debann_inter` varchar(2000)
,`debann_ges_id` int(11)
,`debanno_nominativo` char(100)
,`debann_modifica` datetime
,`debann_modbar` char(19)
,`debann_numdoc` bigint(21)
,`debann_doc_checkdocsino` varchar(1)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0006_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0006_parametri_v` (
`ges_id` int(10)
,`ges_nominativo` char(100)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_debid` decimal(10,0)
,`par_lastid` decimal(10,0)
,`ope_nominativo` char(100)
,`modibar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0006_parconn_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0006_parconn_v` (
`ges_id` int(10)
,`ges_nominativo` char(100)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_debid` decimal(10,0)
,`par_lastid` decimal(10,0)
,`ope_nominativo` char(100)
,`modibar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0007_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0007_parametri_v` (
`ges_id` int(10)
,`ges_nominativo` char(100)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_debid` decimal(10,0)
,`ope_nominativo` char(100)
,`modibar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0007_parconn_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0007_parconn_v` (
`ges_id` int(10)
,`ges_nominativo` char(100)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_debid` decimal(10,0)
,`ope_nominativo` char(100)
,`modibar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0008_debann_arcdoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0008_debann_arcdoc_v` (
`debandoc_id` int(11)
,`debandoc_idann` int(11)
,`debandoc_iddoc` int(11)
,`debandoc_estid` int(11)
,`debandoc_estcod` varchar(5)
,`debandoc_estdes` varchar(50)
,`debandoc_tipoid` int(11)
,`debandoc_tipocod` decimal(10,0)
,`debandoc_tipodes` varchar(50)
,`debandoc_desc` varchar(50)
,`debandoc_size` decimal(10,0)
,`debandoc_nomefile` varchar(50)
,`debandoc_ges_id` int(11)
,`debandoc_ges_nominativo` char(100)
,`debandoc_modifica` datetime
,`debandoc_modbar` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0008_elearcdoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0008_elearcdoc_v` (
`elearcdoc_id` int(11)
,`elearcdoc_idann` int(11)
,`elearcdoc_iddoc` int(11)
,`elearcdoc_estid` int(11)
,`elearcdoc_estcod` varchar(5)
,`elearcdoc_estdes` varchar(50)
,`elearcdoc_tipoid` int(11)
,`elearcdoc_tipocod` decimal(10,0)
,`elearcdoc_tipodes` varchar(50)
,`elearcdoc_desc` varchar(50)
,`elearcdoc_size` decimal(10,0)
,`elearcdoc_nomefile` varchar(50)
,`elearcdoc_ges_id` int(11)
,`elearcdoc_ges_nominativo` char(100)
,`elearcdoc_modifica` datetime
,`elearcdoc_modbar` datetime
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0008_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0008_parametri_v` (
`ges_id` int(10)
,`ges_nominativo` char(100)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_debannid` decimal(10,0)
,`ope_nominativo` char(100)
,`modibar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0008_parconn_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0008_parconn_v` (
`ges_id` int(10)
,`ges_nominativo` char(100)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_debannid` decimal(10,0)
,`ope_nominativo` char(100)
,`modibar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0009_debitori_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0009_debitori_v` (
`deb_id` int(10)
,`deb_cod` decimal(10,0)
,`deb_des` varchar(100)
,`deb_citta` varchar(50)
,`deb_pro` varchar(2)
,`deb_tel` varchar(30)
,`deb_mail` varchar(50)
,`deb_numann` bigint(21)
,`deb_ann_checksino` varchar(1)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0009_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0009_parametri_v` (
`ges_id` int(10)
,`ges_nominativo` char(100)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_debid` decimal(10,0)
,`ope_nominativo` char(100)
,`modibar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0009_parconn_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0009_parconn_v` (
`ges_id` int(10)
,`ges_nominativo` char(100)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_debid` decimal(10,0)
,`ope_nominativo` char(100)
,`modibar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0010_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0010_parametri_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_lastid` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0010_parconn_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0010_parconn_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_lastid` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0011_eletabsr_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0011_eletabsr_v` (
`fttbsr_numero` varchar(20)
,`fttbsr_nota` varchar(2000)
,`fttbsr_ges_nominativo` char(100)
,`fttbsr_modifica` datetime
,`fttbsr_modbar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0011_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0011_parametri_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_idfatt` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro0011_parconn_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro0011_parconn_v` (
`ges_id` int(10)
,`ges_nome` varchar(50)
,`id_filtro` int(10)
,`des_filtro` varchar(50)
,`par_conn` int(10)
,`par_idfatt` decimal(10,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `filtro003_elenco_pagine_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `filtro003_elenco_pagine_v` (
`pag` bigint(20)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `fosuser_gestori`
--

CREATE TABLE `fosuser_gestori` (
  `fosuser_username` varchar(50) NOT NULL,
  `fosuser_gestid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `gestdebitore_fastweb_csv`
--

CREATE TABLE `gestdebitore_fastweb_csv` (
  `gd_id` int(11) NOT NULL,
  `gd_ges_cod` decimal(10,0) NOT NULL,
  `gd_ges_id` int(11) NOT NULL,
  `gd_deb_cod` decimal(10,0) NOT NULL,
  `gd_deb_id` int(11) NOT NULL,
  `gd_data_associazione` varchar(10) NOT NULL,
  `gd_id_dataassoc` int(11) NOT NULL,
  `gd_data_dissociazione` varchar(10) DEFAULT NULL,
  `gd_id_datadissoc` int(11) NOT NULL,
  `gd_operatore` int(11) NOT NULL DEFAULT '0',
  `gd_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `gestione_scadenziario_migliorata_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `gestione_scadenziario_migliorata_v` (
`id` int(10)
,`ft_soc` int(10)
,`ft_deb_id` int(10)
,`ft_ac` int(11)
,`ft_id_debitore` int(10)
,`ft_deb_cod` decimal(10,0)
,`ft_deb_des` varchar(100)
,`ft_deb_citta` varchar(50)
,`ft_ced_id` int(10)
,`ft_attribuzione` varchar(49)
,`ft_num_fat` varchar(20)
,`ft_td_id` int(10)
,`ft_td` varchar(5)
,`ft_testo` varchar(500)
,`ft_data_fat` varchar(4)
,`ft_data_scad` varchar(4)
,`ft_imp_fat` decimal(10,2)
,`ft_imp_aperto` decimal(10,2)
,`ft_data_ins` date
,`ft_id_data_chius` int(10)
,`ft_nota_crgest` varchar(3)
,`ft_data_nota` varchar(3)
,`ft_arcdoc_sino` decimal(1,0)
,`ft_tabsr_sino` decimal(1,0)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `gestione_scadenziario_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `gestione_scadenziario_v` (
`id` int(10)
,`ft_soc` int(10)
,`ft_deb_id` int(10)
,`ft_ac` int(0)
,`ft_id_debitore` int(10)
,`ft_deb_cod` decimal(10,0)
,`ft_deb_des` varchar(100)
,`ft_deb_citta` varchar(50)
,`ft_ced_id` int(10)
,`ft_attribuzione` varchar(49)
,`ft_num_fat` varchar(20)
,`ft_td_id` int(10)
,`ft_td` varchar(5)
,`ft_id_stato_lav` int(10)
,`ft_des_stato_lav` varchar(50)
,`ft_sl_coddes` varchar(55)
,`ft_testo` varchar(500)
,`ft_data_fat` varchar(10)
,`ft_data_scad` varchar(10)
,`ft_imp_fat` decimal(10,2)
,`ft_imp_aperto` decimal(10,2)
,`ft_data_ins` date
,`ft_id_data_fat` int(10)
,`ft_id_data_chius` int(10)
,`ft_nota_crgest` varchar(1000)
,`ft_data_nota` varchar(10)
,`ft_arcdoc_sino` decimal(1,0)
,`ft_tabsr_sino` decimal(1,0)
,`ft_forecast_mese` decimal(2,0)
,`ft_forecast_anno` decimal(4,0)
,`ft_forecast_scad` varchar(9)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `gestori`
--

CREATE TABLE `gestori` (
  `ges_id` int(10) NOT NULL,
  `ges_cognome` varchar(50) DEFAULT NULL,
  `ges_nome` varchar(50) DEFAULT NULL,
  `ges_mail` varchar(50) DEFAULT NULL,
  `ges_tel` varchar(50) DEFAULT NULL,
  `ges_ind` varchar(50) DEFAULT NULL,
  `ges_citta` varchar(30) DEFAULT NULL,
  `ges_regione` varchar(50) DEFAULT NULL,
  `ges_pwd` varchar(10) NOT NULL,
  `ges_ruolo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `gest_debitore`
--

CREATE TABLE `gest_debitore` (
  `gd_ges_id` int(10) NOT NULL,
  `gd_deb_id` int(10) NOT NULL,
  `gd_data_associazione` date DEFAULT NULL,
  `gd_data_dissociazione` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `gest_debitore_201710`
--

CREATE TABLE `gest_debitore_201710` (
  `gd_ges_id` int(10) NOT NULL,
  `gd_deb_id` int(10) NOT NULL,
  `gd_data_associazione` date DEFAULT NULL,
  `gd_data_dissociazione` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `gest_debitore_20171005`
--

CREATE TABLE `gest_debitore_20171005` (
  `gd_ges_id` int(10) NOT NULL,
  `gd_deb_id` int(10) NOT NULL,
  `gd_data_associazione` date DEFAULT NULL,
  `gd_data_dissociazione` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `gest_debitore_attivi_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `gest_debitore_attivi_v` (
`gd_ges_id` int(10)
,`gd_deb_id` int(10)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `gest_debitore_periodi_temp`
--

CREATE TABLE `gest_debitore_periodi_temp` (
  `gd_id` int(11) NOT NULL DEFAULT '0',
  `gd_ges_id` int(11) NOT NULL,
  `gd_deb_id` int(11) NOT NULL,
  `gd_id_dataassoc` int(11) NOT NULL,
  `gd_id_datadissoc` int(11) NOT NULL,
  `gd_operatore` int(11) NOT NULL DEFAULT '0',
  `gd_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `gest_debitore_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `gest_debitore_v` (
`ges_id` int(10)
,`ges_cognome` varchar(50)
,`ges_nome` varchar(50)
,`ges_ruolo` varchar(10)
,`deb_id` int(10)
,`deb_soc` int(10)
,`beb_cod` decimal(10,0)
,`des_des` varchar(100)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `gest_debitori_periodi`
--

CREATE TABLE `gest_debitori_periodi` (
  `gd_id` int(11) NOT NULL,
  `gd_ges_id` int(11) NOT NULL,
  `gd_deb_id` int(11) NOT NULL,
  `gd_id_dataassoc` int(11) NOT NULL,
  `gd_id_datadissoc` int(11) NOT NULL DEFAULT '43829',
  `gd_operatore` int(11) NOT NULL DEFAULT '0',
  `gd_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `parametri_procedura`
--

CREATE TABLE `parametri_procedura` (
  `sistema_operativo` char(50) DEFAULT NULL,
  `directory_fatture` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `pippo`
--

CREATE TABLE `pippo` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `pippo_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `pippo_v` (
`pippo` varchar(1)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `save_debitori_annotazioni`
--

CREATE TABLE `save_debitori_annotazioni` (
  `debann_id` int(11) NOT NULL DEFAULT '0',
  `debann_debid` int(11) NOT NULL,
  `debann_idtpann` int(11) NOT NULL,
  `debann_idtpcon` int(11) NOT NULL,
  `debann_iddata` int(11) NOT NULL,
  `debann_annot` varchar(2000) DEFAULT NULL,
  `debann_interlocutore` varchar(2000) DEFAULT NULL,
  `debann_ges_id` int(11) NOT NULL DEFAULT '0',
  `debann_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `save_gest_debitore`
--

CREATE TABLE `save_gest_debitore` (
  `gd_ges_id` int(10) NOT NULL,
  `gd_deb_id` int(10) NOT NULL,
  `gd_data_associazione` date DEFAULT NULL,
  `gd_data_dissociazione` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `sessions`
--

CREATE TABLE `sessions` (
  `sess_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `sess_data` blob NOT NULL,
  `sess_time` int(10) UNSIGNED NOT NULL,
  `sess_lifetime` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `societa`
--

CREATE TABLE `societa` (
  `soc_cod` int(10) NOT NULL,
  `soc_des` varchar(50) DEFAULT NULL,
  `soc_data_ini` date DEFAULT NULL,
  `soc_indirizzo` varchar(50) DEFAULT NULL,
  `soc_citta` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `statist_movimenti`
--

CREATE TABLE `statist_movimenti` (
  `stmov_id` int(11) NOT NULL,
  `stmov_gesid` int(10) NOT NULL,
  `stmov_opeid` int(10) NOT NULL,
  `stmod_datetime` datetime DEFAULT NULL,
  `operatore` int(11) NOT NULL DEFAULT '0',
  `modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `statist_movimenti_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `statist_movimenti_v` (
`stmov_gesid` int(10)
,`stmov_username` varchar(50)
,`cognome` varchar(50)
,`ges_nome` varchar(50)
,`stmov_opeid` int(10)
,`stmov_des` varchar(50)
,`stmod_datetime` datetime
,`operatore` varchar(180)
,`modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura della tabella `statist_operazioni`
--

CREATE TABLE `statist_operazioni` (
  `statope_id` int(10) NOT NULL,
  `statope_desc` varchar(50) NOT NULL,
  `operatore` int(11) NOT NULL DEFAULT '0',
  `modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_ft`
--

CREATE TABLE `stato_ft` (
  `st_id` int(10) NOT NULL,
  `st_ft_id` int(10) NOT NULL,
  `st_data_nota` date NOT NULL,
  `st_stato_lavorazione` int(10) NOT NULL,
  `st_note` varchar(1000) DEFAULT NULL,
  `st_operatore` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_lavorazione`
--

CREATE TABLE `stato_lavorazione` (
  `id` int(10) NOT NULL,
  `sl_soc` int(10) NOT NULL,
  `sl_cod` varchar(4) NOT NULL,
  `sl_descrizione` varchar(50) DEFAULT NULL,
  `sl_ins` decimal(1,0) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tabsr_stati`
--

CREATE TABLE `tabsr_stati` (
  `tbsrst_id` int(11) NOT NULL,
  `tbsrst_soc` int(11) NOT NULL,
  `tbsrst_cod` varchar(1) NOT NULL,
  `tbsrst_desc` varchar(50) NOT NULL,
  `tbsrst_ges_id` int(11) NOT NULL DEFAULT '0',
  `tbsrst_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `tabsr_stati_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `tabsr_stati_v` (
`tbsrst_id` int(11)
,`tbsrst_soccod` int(11)
,`tbsrst_socdes` varchar(50)
,`tbsrst_cod` varchar(1)
,`tbsrst_desc` varchar(50)
,`tbsrst_ges_id` int(11)
,`tbsrst_ges_nominativo` char(100)
,`tbsrst_modifica` datetime
,`tbsrst_modbar` char(19)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `tabsr_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `tabsr_v` (
`tbsr_id` int(11)
,`tbsr_numero` varchar(20)
,`tbsr_nota` varchar(2000)
,`tbsr_idstato` int(11)
,`tbsr_codstato` varchar(1)
,`tbsr_desstato` varchar(50)
,`tbsr_ges_id` int(11)
,`tbsr_modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura della tabella `tab_arcdoc`
--

CREATE TABLE `tab_arcdoc` (
  `tbarcdoc_id` int(11) NOT NULL,
  `tbarcdoc_soc` int(11) NOT NULL,
  `tbarcdoc_est` int(11) NOT NULL,
  `tbarcdoc_tipo` int(11) NOT NULL,
  `tbarcdoc_size` decimal(10,0) NOT NULL,
  `tbarcdoc_nomefile` varchar(50) DEFAULT NULL,
  `tbarcdoc_desc` varchar(50) DEFAULT NULL,
  `tbarcdoc_blob` mediumblob,
  `tbarcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `tbarcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `tab_arcdoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `tab_arcdoc_v` (
`tbarcdoc_id` int(11)
,`tbarcdoc_soc` int(11)
,`tbarcdoc_estid` int(11)
,`tbarcdoc_estcod` varchar(5)
,`tbarcdoc_estdes` varchar(50)
,`tbarcdoc_tipoid` int(11)
,`tbarcdoc_tipocod` decimal(10,0)
,`tbarcdoc_tipodes` varchar(50)
,`tbarcdoc_size` decimal(10,0)
,`tbarcdoc_nomefile` varchar(50)
,`tbarcdoc_desc` varchar(50)
,`tbarcdoc_blob` mediumblob
,`tbarcdoc_ges_id` int(11)
,`tbarcdoc_ges_nominativo` varchar(101)
,`tbarcdoc_modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura della tabella `tab_procedure`
--

CREATE TABLE `tab_procedure` (
  `id_proc` int(10) NOT NULL,
  `descrizione` char(50) DEFAULT NULL,
  `modifica` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tab_procedure_host`
--

CREATE TABLE `tab_procedure_host` (
  `id_host` int(10) NOT NULL,
  `descrizione` char(50) DEFAULT NULL,
  `modifica` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tab_procedure_parametri`
--

CREATE TABLE `tab_procedure_parametri` (
  `id_proc` int(10) NOT NULL,
  `id_host` int(2) NOT NULL,
  `parint1` int(10) DEFAULT NULL,
  `parint2` int(10) DEFAULT NULL,
  `parint3` int(10) DEFAULT NULL,
  `parint4` int(10) DEFAULT NULL,
  `parint5` int(10) DEFAULT NULL,
  `parcar1` char(100) DEFAULT NULL,
  `parcar2` char(100) DEFAULT NULL,
  `parcar3` char(100) DEFAULT NULL,
  `parcar4` char(100) DEFAULT NULL,
  `parcar5` char(100) DEFAULT NULL,
  `modifica` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `tab_procedure_parametri_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `tab_procedure_parametri_v` (
`id_proc` int(10)
,`des_proc` char(50)
,`id_host` int(2)
,`des_host` char(50)
,`directory_file_load` char(100)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `tab_sr`
--

CREATE TABLE `tab_sr` (
  `tbsr_id` int(11) NOT NULL,
  `tbsr_numero` varchar(20) NOT NULL,
  `tbsr_nota` varchar(2000) DEFAULT NULL,
  `tbsr_stato` int(11) NOT NULL,
  `tbsr_ges_id` int(11) NOT NULL DEFAULT '0',
  `tbsr_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `temp_debitori_annotazioni`
--

CREATE TABLE `temp_debitori_annotazioni` (
  `debann_id` int(11) NOT NULL DEFAULT '0',
  `debann_debid` int(11) NOT NULL,
  `debann_idtpann` int(11) NOT NULL,
  `debann_idtpcon` int(11) NOT NULL,
  `debann_iddata` int(11) NOT NULL,
  `debann_annot` varchar(2000) DEFAULT NULL,
  `debann_interlocutore` varchar(2000) DEFAULT NULL,
  `debann_ges_id` int(11) NOT NULL DEFAULT '0',
  `debann_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `test`
--

CREATE TABLE `test` (
  `tnumgg` int(11) DEFAULT NULL,
  `tgg` int(11) DEFAULT NULL,
  `tggfine` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tipi_arcdoc`
--

CREATE TABLE `tipi_arcdoc` (
  `tparcdoc_id` int(11) NOT NULL,
  `tparcdoc_soc` int(11) NOT NULL,
  `tparcdoc_cod` decimal(10,0) NOT NULL,
  `tparcdoc_des` varchar(50) DEFAULT NULL,
  `tparcdoc_ges_id` int(11) NOT NULL DEFAULT '0',
  `tparcdoc_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `tipo_arcdoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `tipo_arcdoc_v` (
`tparcdoc_id` int(11)
,`tparcdoc_soc` int(11)
,`tparcdoc_socdes` varchar(50)
,`tparcdoc_cod` decimal(10,0)
,`tparcdoc_des` varchar(50)
,`tparcdoc_ges_id` int(11)
,`tparcdoc_gesnome` varchar(101)
,`tparcdoc_modifica` datetime
);

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_doc`
--

CREATE TABLE `tipo_doc` (
  `td_id` int(10) NOT NULL,
  `td_soc` int(10) NOT NULL,
  `td_cod` varchar(5) NOT NULL,
  `td_des` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `vs28ahbz_gecotestlog`
--

CREATE TABLE `vs28ahbz_gecotestlog` (
  `id` int(10) NOT NULL,
  `orario` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descrizione` varchar(100) DEFAULT NULL,
  `ges_id` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `webapp_selsoc_updconn_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `webapp_selsoc_updconn_v` (
`username` int(1)
,`ges_id` int(1)
,`nominativo` int(1)
,`soc_cod` int(1)
,`soc_des` int(1)
);

-- --------------------------------------------------------

--
-- Struttura stand-in per le viste `webapp_selsoc_v`
-- (Vedi sotto per la vista effettiva)
--
CREATE TABLE `webapp_selsoc_v` (
`username` int(1)
,`ges_id` int(1)
,`nominativo` int(1)
,`soc_cod` int(1)
,`soc_des` int(1)
);

-- --------------------------------------------------------

--
-- Struttura della tabella `www`
--

CREATE TABLE `www` (
  `id` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura per vista `debann_arcdoc_checksino_v`
--
DROP TABLE IF EXISTS `debann_arcdoc_checksino_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `debann_arcdoc_checksino_v`  AS  select `debann_arcdoc`.`debandoc_idann` AS `debandoc_idann`,count(0) AS `debandoc_numdoc` from `debann_arcdoc` group by `debann_arcdoc`.`debandoc_idann` ;

-- --------------------------------------------------------

--
-- Struttura per vista `debann_arcdoc_v`
--
DROP TABLE IF EXISTS `debann_arcdoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `debann_arcdoc_v`  AS  select `a`.`debandoc_id` AS `debandoc_id`,`a`.`debandoc_idann` AS `debandoc_idann`,`b`.`tbarcdoc_id` AS `debandoc_iddoc`,`b`.`tbarcdoc_estid` AS `debandoc_estid`,`b`.`tbarcdoc_estcod` AS `debandoc_estcod`,`b`.`tbarcdoc_estdes` AS `debandoc_estdes`,`b`.`tbarcdoc_tipoid` AS `debandoc_tipoid`,`b`.`tbarcdoc_tipocod` AS `debandoc_tipocod`,`b`.`tbarcdoc_tipodes` AS `debandoc_tipodes`,`b`.`tbarcdoc_desc` AS `debandoc_desc`,`b`.`tbarcdoc_size` AS `debandoc_size`,`b`.`tbarcdoc_nomefile` AS `debandoc_nomefile`,`a`.`debandoc_ges_id` AS `debandoc_ges_id`,`utypkg_gestid2gestnomin`(`b`.`tbarcdoc_ges_id`) AS `debandoc_ges_nominativo`,`a`.`debandoc_modifica` AS `debandoc_modifica`,`utypkg_datanum2databar`(`a`.`debandoc_modifica`) AS `debandoc_modbar` from (`debann_arcdoc` `a` join `tab_arcdoc_v` `b` on((`b`.`tbarcdoc_id` = `a`.`debandoc_iddoc`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `debann_checksino_v`
--
DROP TABLE IF EXISTS `debann_checksino_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `debann_checksino_v`  AS  select `debitori_annotazioni`.`debann_debid` AS `debann_debid`,count(0) AS `debann_numann` from `debitori_annotazioni` group by `debitori_annotazioni`.`debann_debid` ;

-- --------------------------------------------------------

--
-- Struttura per vista `debitori_annotazioni_v`
--
DROP TABLE IF EXISTS `debitori_annotazioni_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz`@`localhost` SQL SECURITY DEFINER VIEW `debitori_annotazioni_v`  AS  select `a`.`debann_id` AS `debann_id`,`a`.`debann_debid` AS `debann_debid`,`b`.`deb_soc` AS `debann_idsoc`,`b`.`deb_cod` AS `debann_debcod`,`b`.`deb_des` AS `debann_debdes`,`a`.`debann_idtpann` AS `debann_idtpann`,`c`.`dantpan_cod` AS `debann_anncod`,`c`.`dantpan_des` AS `debann_anndes`,`a`.`debann_idtpcon` AS `debann_idtpcon`,`d`.`dantpcon_cod` AS `debann_tpcod`,`d`.`dantpcon_des` AS `debann_tpdes`,`a`.`debann_iddata` AS `debann_iddata`,`e`.`datanum` AS `debann_datanum`,`e`.`databar` AS `debann_databar`,`a`.`debann_annot` AS `debann_annot`,`a`.`debann_interlocutore` AS `debann_inter`,`a`.`debann_ges_id` AS `debann_ges_id`,`utypkg_gestid2gestnomin`(`a`.`debann_ges_id`) AS `debanno_nominativo`,`a`.`debann_modifica` AS `debann_modifica`,`utypkg_datadate2databar`(`a`.`debann_modifica`) AS `debann_modbar` from ((((`debitori_annotazioni` `a` join `debitori` `b` on((`b`.`deb_id` = `a`.`debann_debid`))) join `debann_tipi_annotazione` `c` on((`c`.`dantpan_id` = `a`.`debann_idtpann`))) join `debann_tipi_contatto` `d` on((`d`.`dantpcon_id` = `a`.`debann_idtpcon`))) join `calendario` `e` on((`e`.`id` = `a`.`debann_iddata`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `fatture_arcdoc_sino_v`
--
DROP TABLE IF EXISTS `fatture_arcdoc_sino_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `fatture_arcdoc_sino_v`  AS  select `ft`.`id` AS `ftarcdoc_idfatt`,(case ifnull(min(`fa`.`ftarcdoc_iddoc`),0) when 0 then 0 else 1 end) AS `ftarcdoc_sino` from (`fatture` `ft` left join `fatture_arcdoc` `fa` on((`fa`.`ftarcdoc_idfatt` = `ft`.`id`))) group by `ft`.`id` ;

-- --------------------------------------------------------

--
-- Struttura per vista `fatture_arcdoc_v`
--
DROP TABLE IF EXISTS `fatture_arcdoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `fatture_arcdoc_v`  AS  select `a`.`ftarcdoc_id` AS `ftarcdoc_id`,`a`.`ftarcdoc_idfatt` AS `ftarcdoc_idfatt`,`b`.`tbarcdoc_id` AS `ftarcdoc_iddoc`,`b`.`tbarcdoc_estid` AS `ftarcdoc_estid`,`b`.`tbarcdoc_estcod` AS `ftarcdoc_estcod`,`b`.`tbarcdoc_estdes` AS `ftarcdoc_estdes`,`b`.`tbarcdoc_tipoid` AS `ftarcdoc_tipoid`,`b`.`tbarcdoc_tipocod` AS `ftarcdoc_tipocod`,`b`.`tbarcdoc_tipodes` AS `ftarcdoc_tipodes`,`b`.`tbarcdoc_desc` AS `ftarcdoc_desc`,`b`.`tbarcdoc_size` AS `ftarcdoc_size`,`b`.`tbarcdoc_nomefile` AS `ftarcdoc_nomefile`,`b`.`tbarcdoc_ges_id` AS `ftarcdoc_ges_id`,`b`.`tbarcdoc_ges_nominativo` AS `ftarcdoc_ges_nominativo`,`b`.`tbarcdoc_modifica` AS `ftarcdoc_modifica`,`utypkg_datanum2databar`(`b`.`tbarcdoc_modifica`) AS `ftarcdoc_modbar` from (`fatture_arcdoc` `a` join `tab_arcdoc_v` `b` on((`b`.`tbarcdoc_id` = `a`.`ftarcdoc_iddoc`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `fatture_notecrgest_v`
--
DROP TABLE IF EXISTS `fatture_notecrgest_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `fatture_notecrgest_v`  AS  select `a`.`ftnote_id_nota` AS `ftnote_id_nota`,`a`.`ftnote_id_fattura` AS `ftnote_id_fattura`,`a`.`ftnote_nota_crgest` AS `ftnote_nota_crgest` from `fatture_notecrgest` `a` where (`a`.`ftnote_id_nota` = (select max(`b`.`ftnote_id_nota`) from `fatture_notecrgest` `b` where (`b`.`ftnote_id_fattura` = `a`.`ftnote_id_fattura`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `fatture_statilavnote_v`
--
DROP TABLE IF EXISTS `fatture_statilavnote_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `fatture_statilavnote_v`  AS  select `a`.`ftstlav_id` AS `ftstlav_id`,`a`.`ftstlav_id_fattura` AS `ftstlav_id_fattura`,`a`.`ftstlav_id_stato_lav` AS `ftstlav_id_stato_lav`,`b`.`sl_cod` AS `ftstlav_slcod`,`b`.`sl_descrizione` AS `ftstlav_sldes`,`a`.`ftstlav_nota_crgest` AS `ftstlav_nota_crgest`,date_format(`a`.`ftstlav_modifica`,'%d/%m/%Y') AS `ftstlav_data_bar` from (`fatture_statilav_note` `a` join `stato_lavorazione` `b` on((`b`.`id` = `a`.`ftstlav_id_stato_lav`))) where (`a`.`ftstlav_id` = (select max(`c`.`ftstlav_id`) from `fatture_statilav_note` `c` where (`c`.`ftstlav_id_fattura` = `a`.`ftstlav_id_fattura`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `fatture_stati_fattura_v`
--
DROP TABLE IF EXISTS `fatture_stati_fattura_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `fatture_stati_fattura_v`  AS  select `a`.`fsf_id` AS `fsf_id`,`b`.`fs_soc` AS `fsf_soc`,`a`.`fsf_id_fattura` AS `fsf_id_fattura`,`a`.`fsf_id_stato` AS `fsf_id_stato`,`b`.`fs_cod` AS `fsf_cod`,`b`.`fs_desc` AS `fsf_desc`,`a`.`fsf_ges_id` AS `fsf_ges_id`,`a`.`fsf_modifica` AS `fsf_modifica`,`a`.`fsf_nomefile_csv` AS `fsf_nomefile_csv` from (`fatture_stato_fattura` `a` join `fatture_stati` `b` on((`b`.`fs_id` = `a`.`fsf_id_stato`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `fatture_stato_aperta_v`
--
DROP TABLE IF EXISTS `fatture_stato_aperta_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `fatture_stato_aperta_v`  AS  select `a`.`id` AS `fsfa_id_fattura`,`a`.`ft_num_fat` AS `fsfa_num_fat`,`a`.`ft_imp_fat` AS `fsfa_imp_fat`,`b`.`fsf_id_stato` AS `fsfa_id_stato`,`b`.`fsf_ac` AS `fsfa_ac`,`c`.`fs_cod` AS `fsfa_cod_stato`,`c`.`fs_desc` AS `fsfa_des_stato` from ((`fatture` `a` join `fatture_stato_fattura_v` `b` on((`b`.`fsf_id_fattura` = `a`.`id`))) join `fatture_stati` `c` on((`c`.`fs_id` = `b`.`fsf_id_stato`))) where (`b`.`fsf_ac` = 'A') ;

-- --------------------------------------------------------

--
-- Struttura per vista `fatture_stato_fattura_v`
--
DROP TABLE IF EXISTS `fatture_stato_fattura_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `fatture_stato_fattura_v`  AS  select `a`.`fsf_id` AS `fsf_id`,`a`.`fsf_soc` AS `fsf_soc`,`a`.`fsf_cod` AS `fsf_cod`,`a`.`fsf_id_fattura` AS `fsf_id_fattura`,`a`.`fsf_id_stato` AS `fsf_id_stato`,(case `a`.`fsf_id_stato` when 1 then 0 when 2 then 1 when 3 then 0 when 4 then 1 end) AS `fsf_ac`,`a`.`fsf_desc` AS `fsf_desc`,`a`.`fsf_ges_id` AS `fsf_ges_id`,`a`.`fsf_modifica` AS `fsf_modifica`,`a`.`fsf_nomefile_csv` AS `fsf_nomefile_csv` from `fatture_stati_fattura_v` `a` where (`a`.`fsf_id_fattura`,`a`.`fsf_id`) in (select `b`.`fsf_id_fattura`,max(`b`.`fsf_id`) from `fatture_stato_fattura` `b` group by `b`.`fsf_id_fattura`) ;

-- --------------------------------------------------------

--
-- Struttura per vista `fatture_tabsr_v`
--
DROP TABLE IF EXISTS `fatture_tabsr_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `fatture_tabsr_v`  AS  select `a`.`fttbsr_id` AS `fttbsr_id`,`a`.`fttbsr_idfatt` AS `fttbsr_idfatt`,`a`.`fttbsr_idsr` AS `fttbsr_idsr`,`b`.`tbsr_numero` AS `fttbsr_numero`,`b`.`tbsr_nota` AS `fttbsr_nota`,`UTYPKG_GESTID2GESTNOMIN`(`a`.`fttbsr_ges_id`) AS `fttbsr_ges_nominativo`,`a`.`fttbsr_modifica` AS `fttbsr_modifica`,`UTYPKG_DATADATE2DATABAR`(`a`.`fttbsr_modifica`) AS `fttbsr_modbar` from ((`fatture_tabsr` `a` join `tab_sr` `b` on((`b`.`tbsr_id` = `a`.`fttbsr_idsr`))) join `tabsr_stati` `c` on((`c`.`tbsrst_id` = `b`.`tbsr_stato`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `fatture_v`
--
DROP TABLE IF EXISTS `fatture_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.228.251` SQL SECURITY DEFINER VIEW `fatture_v`  AS  select `a`.`id` AS `id`,`a`.`ft_soc` AS `soc_cod`,`b`.`soc_des` AS `soc_des`,`c`.`deb_id` AS `deb_id`,`a`.`ft_deb_id` AS `cod_deb`,`c`.`deb_cod` AS `deb_cod`,`c`.`deb_des` AS `deb_des`,`a`.`ft_ced_id` AS `ced_id`,`d`.`ced_des` AS `ced_des`,`a`.`ft_num_fat` AS `num_fat`,`a`.`ft_attribuzione` AS `attribuzione`,`a`.`ft_td` AS `tipo_doc_cod`,`e`.`td_des` AS `tipo_doc_des`,`a`.`ft_testo` AS `testo`,`a`.`ft_data_fat` AS `data_fat`,`a`.`ft_id_data_fat` AS `id_data_fat`,`a`.`ft_data_scad` AS `data_scad`,`a`.`ft_id_data_scad` AS `id_data_scad`,`a`.`ft_imp_fat` AS `imp_fat`,`a`.`ft_imp_aperto` AS `imp_aperto`,`a`.`ft_data_ins` AS `data_ins`,`a`.`ft_id_stato_lav` AS `id_stato_lav_cod`,ifnull(`g`.`fs_cod`,0) AS `cod_stato_fat`,ifnull(`g`.`fs_desc`,'Nessuno') AS `des_stato_fat`,`a`.`ft_data_chius` AS `data_chius`,`a`.`ft_id_data_chius` AS `id_data_chius`,`a`.`ft_nomefile_csv` AS `ft_nomefile_csv`,`f`.`fsf_nomefile_csv` AS `fsf_nomefile_csv` from ((((((`fatture` `a` join `societa` `b` on((`b`.`soc_cod` = `a`.`ft_soc`))) join `debitori` `c` on((`c`.`deb_id` = `a`.`ft_deb_id`))) join `cedenti` `d` on((`d`.`ced_id` = `a`.`ft_ced_id`))) join `tipo_doc` `e` on((`e`.`td_id` = `a`.`ft_td_id`))) left join `fatture_stato_fattura_v` `f` on((`f`.`fsf_id_fattura` = `a`.`id`))) left join `fatture_stati` `g` on((`g`.`fs_id` = `f`.`fsf_id_stato`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtri_parametri_v`
--
DROP TABLE IF EXISTS `filtri_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtri_parametri_v`  AS  select `filtri_parametri`.`ges_id` AS `ges_id`,`filtri_parametri`.`id_filtro` AS `id_filtro`,`filtri_parametri`.`par_conn` AS `par_conn`,`filtri_parametri`.`parvar01` AS `parvar01`,`filtri_parametri`.`parvar02` AS `parvar02`,`filtri_parametri`.`parvar03` AS `parvar03`,`filtri_parametri`.`parvar04` AS `parvar04`,`filtri_parametri`.`parvar05` AS `parvar05`,`filtri_parametri`.`parvar06` AS `parvar06`,`filtri_parametri`.`parvar07` AS `parvar07`,`filtri_parametri`.`parvar08` AS `parvar08`,`filtri_parametri`.`parvar09` AS `parvar09`,`filtri_parametri`.`parvar10` AS `parvar10`,`filtri_parametri`.`parvar11` AS `parvar11`,`filtri_parametri`.`parvar12` AS `parvar12`,`filtri_parametri`.`parvar13` AS `parvar13`,`filtri_parametri`.`parvar14` AS `parvar14`,`filtri_parametri`.`parvar15` AS `parvar15`,`filtri_parametri`.`parvar16` AS `parvar16`,`filtri_parametri`.`parvar17` AS `parvar17`,`filtri_parametri`.`parvar18` AS `parvar18`,`filtri_parametri`.`parvar19` AS `parvar19`,`filtri_parametri`.`parvar20` AS `parvar20`,`filtri_parametri`.`parvar21` AS `parvar21`,`filtri_parametri`.`parvar22` AS `parvar22`,`filtri_parametri`.`parvar23` AS `parvar23`,`filtri_parametri`.`parvar24` AS `parvar24`,`filtri_parametri`.`parvar25` AS `parvar25`,`filtri_parametri`.`parvar26` AS `parvar26`,`filtri_parametri`.`parvar27` AS `parvar27`,`filtri_parametri`.`parvar28` AS `parvar28`,`filtri_parametri`.`parvar29` AS `parvar29`,`filtri_parametri`.`parvar30` AS `parvar30`,`filtri_parametri`.`parint01` AS `parint01`,`filtri_parametri`.`parint02` AS `parint02`,`filtri_parametri`.`parint03` AS `parint03`,`filtri_parametri`.`parint04` AS `parint04`,`filtri_parametri`.`parint05` AS `parint05`,`filtri_parametri`.`parint06` AS `parint06`,`filtri_parametri`.`parint07` AS `parint07`,`filtri_parametri`.`parint08` AS `parint08`,`filtri_parametri`.`parint09` AS `parint09`,`filtri_parametri`.`parint10` AS `parint10`,`filtri_parametri`.`parint11` AS `parint11`,`filtri_parametri`.`parint12` AS `parint12`,`filtri_parametri`.`parint13` AS `parint13`,`filtri_parametri`.`parint14` AS `parint14`,`filtri_parametri`.`parint15` AS `parint15`,`filtri_parametri`.`parint16` AS `parint16`,`filtri_parametri`.`parint17` AS `parint17`,`filtri_parametri`.`parint18` AS `parint18`,`filtri_parametri`.`parint19` AS `parint19`,`filtri_parametri`.`parint20` AS `parint20`,`filtri_parametri`.`parint21` AS `parint21`,`filtri_parametri`.`parint22` AS `parint22`,`filtri_parametri`.`parint23` AS `parint23`,`filtri_parametri`.`parint24` AS `parint24`,`filtri_parametri`.`parint25` AS `parint25`,`filtri_parametri`.`parint26` AS `parint26`,`filtri_parametri`.`parint27` AS `parint27`,`filtri_parametri`.`parint28` AS `parint28`,`filtri_parametri`.`parint29` AS `parint29`,`filtri_parametri`.`parint30` AS `parint30`,`filtri_parametri`.`pardat01` AS `pardat01`,`filtri_parametri`.`pardat02` AS `pardat02`,`filtri_parametri`.`pardat03` AS `pardat03`,`filtri_parametri`.`pardat04` AS `pardat04`,`filtri_parametri`.`pardat05` AS `pardat05`,`filtri_parametri`.`pardat06` AS `pardat06`,`filtri_parametri`.`pardat07` AS `pardat07`,`filtri_parametri`.`pardat08` AS `pardat08`,`filtri_parametri`.`pardat09` AS `pardat09`,`filtri_parametri`.`pardat10` AS `pardat10`,`filtri_parametri`.`operatore` AS `operatore`,`filtri_parametri`.`modifica` AS `modifica` from `filtri_parametri` where (`filtri_parametri`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0000_parametri_v`
--
DROP TABLE IF EXISTS `filtro0000_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0000_parametri_v`  AS  select `filtri_parametri_v`.`ges_id` AS `ges_id`,`filtri_parametri_v`.`id_filtro` AS `id_filtro`,`filtri_parametri_v`.`par_conn` AS `par_conn`,`filtri_parametri_v`.`parvar01` AS `parvar01`,`filtri_parametri_v`.`parvar02` AS `parvar02`,`filtri_parametri_v`.`parvar03` AS `parvar03`,`filtri_parametri_v`.`parvar04` AS `parvar04`,`filtri_parametri_v`.`parvar05` AS `parvar05`,`filtri_parametri_v`.`parvar06` AS `parvar06`,`filtri_parametri_v`.`parvar07` AS `parvar07`,`filtri_parametri_v`.`parvar08` AS `parvar08`,`filtri_parametri_v`.`parvar09` AS `parvar09`,`filtri_parametri_v`.`parvar10` AS `parvar10`,`filtri_parametri_v`.`parint01` AS `parint01`,`filtri_parametri_v`.`parint02` AS `parint02`,`filtri_parametri_v`.`parint03` AS `parint03`,`filtri_parametri_v`.`parint04` AS `parint04`,`filtri_parametri_v`.`parint05` AS `parint05`,`filtri_parametri_v`.`parint06` AS `parint06`,`filtri_parametri_v`.`parint07` AS `parint07`,`filtri_parametri_v`.`parint08` AS `parint08`,`filtri_parametri_v`.`parint09` AS `parint09`,`filtri_parametri_v`.`parint10` AS `parint10`,`filtri_parametri_v`.`pardat01` AS `pardat01`,`filtri_parametri_v`.`pardat02` AS `pardat02`,`filtri_parametri_v`.`pardat03` AS `pardat03`,`filtri_parametri_v`.`pardat04` AS `pardat04`,`filtri_parametri_v`.`pardat05` AS `pardat05`,`filtri_parametri_v`.`pardat06` AS `pardat06`,`filtri_parametri_v`.`pardat07` AS `pardat07`,`filtri_parametri_v`.`pardat08` AS `pardat08`,`filtri_parametri_v`.`pardat09` AS `pardat09`,`filtri_parametri_v`.`pardat10` AS `pardat10`,`filtri_parametri_v`.`operatore` AS `operatore`,`filtri_parametri_v`.`modifica` AS `modifica` from `filtri_parametri_v` where (`filtri_parametri_v`.`id_filtro` = 0) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_debitori_v`
--
DROP TABLE IF EXISTS `filtro0001_debitori_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.157.112.27` SQL SECURITY DEFINER VIEW `filtro0001_debitori_v`  AS  select 0 AS `deb_id`,'Tutti' AS `deb_des` union all select `b`.`gd_deb_id` AS `deb_id`,concat(ifnull(`c`.`deb_cod`,' '),'-',ifnull(`c`.`deb_des`,' ')) AS `deb_des` from ((`filtro0001_parametri_v` `a` join `gest_debitore_attivi_v` `b` on((`b`.`gd_ges_id` = `a`.`ges_id`))) join `debitori` `c` on(((`c`.`deb_soc` = ifnull(`a`.`parint01`,`c`.`deb_soc`)) and (`c`.`deb_id` = `b`.`gd_deb_id`)))) where (`a`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_get_datafat_a_v`
--
DROP TABLE IF EXISTS `filtro0001_get_datafat_a_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0001_get_datafat_a_v`  AS  select ifnull(`b`.`data`,'gg/mm/aaaa') AS `datafat_a_v` from (`filtro0001_parametri_v` `a` join `calendario` `b` on((`b`.`id` = ifnull(`a`.`parint06`,0)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_get_datafat_da_v`
--
DROP TABLE IF EXISTS `filtro0001_get_datafat_da_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0001_get_datafat_da_v`  AS  select ifnull(`b`.`data`,'gg/mm/aaaa') AS `datafat_da_v` from (`filtro0001_parametri_v` `a` join `calendario` `b` on((`b`.`id` = ifnull(`a`.`parint05`,0)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_get_debitore_v`
--
DROP TABLE IF EXISTS `filtro0001_get_debitore_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`95.246.198.146` SQL SECURITY DEFINER VIEW `filtro0001_get_debitore_v`  AS  select 1 AS `deb_id`,1 AS `deb_des` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_get_statolav_v`
--
DROP TABLE IF EXISTS `filtro0001_get_statolav_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`95.246.198.146` SQL SECURITY DEFINER VIEW `filtro0001_get_statolav_v`  AS  select 1 AS `sl_id`,1 AS `sl_des` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_get_tipodoc_v`
--
DROP TABLE IF EXISTS `filtro0001_get_tipodoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0001_get_tipodoc_v`  AS  select `b`.`td_id` AS `td_id`,`b`.`td_des` AS `td_des` from (`filtro0001_parametri_v` `a` join `filtro0001_tipodoc_v` `b` on((`b`.`td_id` = ifnull(`a`.`parint02`,0)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_parametri_v`
--
DROP TABLE IF EXISTS `filtro0001_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.224.10` SQL SECURITY DEFINER VIEW `filtro0001_parametri_v`  AS  select `filtri_parametri_v`.`ges_id` AS `ges_id`,`filtri_parametri_v`.`id_filtro` AS `id_filtro`,`filtri_parametri_v`.`par_conn` AS `par_conn`,`filtri_parametri_v`.`parvar01` AS `parvar01`,`filtri_parametri_v`.`parvar02` AS `parvar02`,`filtri_parametri_v`.`parvar03` AS `parvar03`,`filtri_parametri_v`.`parvar04` AS `parvar04`,`filtri_parametri_v`.`parvar05` AS `parvar05`,`filtri_parametri_v`.`parvar06` AS `parvar06`,`filtri_parametri_v`.`parvar07` AS `parvar07`,`filtri_parametri_v`.`parvar08` AS `parvar08`,`filtri_parametri_v`.`parvar09` AS `parvar09`,`filtri_parametri_v`.`parvar10` AS `parvar10`,`filtri_parametri_v`.`parvar11` AS `parvar11`,`filtri_parametri_v`.`parvar12` AS `parvar12`,`filtri_parametri_v`.`parvar13` AS `parvar13`,`filtri_parametri_v`.`parvar14` AS `parvar14`,`filtri_parametri_v`.`parvar15` AS `parvar15`,`filtri_parametri_v`.`parvar16` AS `parvar16`,`filtri_parametri_v`.`parvar17` AS `parvar17`,`filtri_parametri_v`.`parvar18` AS `parvar18`,`filtri_parametri_v`.`parvar19` AS `parvar19`,`filtri_parametri_v`.`parvar20` AS `parvar20`,`filtri_parametri_v`.`parvar21` AS `parvar21`,`filtri_parametri_v`.`parvar22` AS `parvar22`,`filtri_parametri_v`.`parvar23` AS `parvar23`,`filtri_parametri_v`.`parvar24` AS `parvar24`,`filtri_parametri_v`.`parvar25` AS `parvar25`,`filtri_parametri_v`.`parvar26` AS `parvar26`,`filtri_parametri_v`.`parvar27` AS `parvar27`,`filtri_parametri_v`.`parvar28` AS `parvar28`,`filtri_parametri_v`.`parvar29` AS `parvar29`,`filtri_parametri_v`.`parvar30` AS `parvar30`,`filtri_parametri_v`.`parint01` AS `parint01`,`filtri_parametri_v`.`parint02` AS `parint02`,`filtri_parametri_v`.`parint03` AS `parint03`,`filtri_parametri_v`.`parint04` AS `parint04`,`filtri_parametri_v`.`parint05` AS `parint05`,`filtri_parametri_v`.`parint06` AS `parint06`,`filtri_parametri_v`.`parint07` AS `parint07`,`filtri_parametri_v`.`parint08` AS `parint08`,`filtri_parametri_v`.`parint09` AS `parint09`,`filtri_parametri_v`.`parint10` AS `parint10`,`filtri_parametri_v`.`parint11` AS `parint11`,`filtri_parametri_v`.`parint12` AS `parint12`,`filtri_parametri_v`.`parint13` AS `parint13`,`filtri_parametri_v`.`parint14` AS `parint14`,`filtri_parametri_v`.`parint15` AS `parint15`,`filtri_parametri_v`.`parint16` AS `parint16`,`filtri_parametri_v`.`parint17` AS `parint17`,`filtri_parametri_v`.`parint18` AS `parint18`,`filtri_parametri_v`.`parint19` AS `parint19`,`filtri_parametri_v`.`parint20` AS `parint20`,`filtri_parametri_v`.`parint21` AS `parint21`,`filtri_parametri_v`.`parint22` AS `parint22`,`filtri_parametri_v`.`parint23` AS `parint23`,`filtri_parametri_v`.`parint24` AS `parint24`,`filtri_parametri_v`.`parint25` AS `parint25`,`filtri_parametri_v`.`parint26` AS `parint26`,`filtri_parametri_v`.`parint27` AS `parint27`,`filtri_parametri_v`.`parint28` AS `parint28`,`filtri_parametri_v`.`parint29` AS `parint29`,`filtri_parametri_v`.`parint30` AS `parint30`,`filtri_parametri_v`.`pardat01` AS `pardat01`,`filtri_parametri_v`.`pardat02` AS `pardat02`,`filtri_parametri_v`.`pardat03` AS `pardat03`,`filtri_parametri_v`.`pardat04` AS `pardat04`,`filtri_parametri_v`.`pardat05` AS `pardat05`,`filtri_parametri_v`.`pardat06` AS `pardat06`,`filtri_parametri_v`.`pardat07` AS `pardat07`,`filtri_parametri_v`.`pardat08` AS `pardat08`,`filtri_parametri_v`.`pardat09` AS `pardat09`,`filtri_parametri_v`.`pardat10` AS `pardat10`,`filtri_parametri_v`.`operatore` AS `operatore`,`filtri_parametri_v`.`modifica` AS `modifica` from `filtri_parametri_v` where (`filtri_parametri_v`.`id_filtro` = 1) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_societa_v`
--
DROP TABLE IF EXISTS `filtro0001_societa_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0001_societa_v`  AS  select `c`.`soc_cod` AS `soc_cod`,`c`.`soc_des` AS `soc_des` from ((((`filtri_parametri_v` `upd` join `fosuser_gestori` `fug` on((`fug`.`fosuser_gestid` = `upd`.`ges_id`))) join `gest_debitore` `a` on((`a`.`gd_ges_id` = `fug`.`fosuser_gestid`))) join `debitori` `b` on((`b`.`deb_id` = `a`.`gd_deb_id`))) join `societa` `c` on((`c`.`soc_cod` = `b`.`deb_soc`))) group by `c`.`soc_cod`,`c`.`soc_des` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_statolav_v`
--
DROP TABLE IF EXISTS `filtro0001_statolav_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`78.134.116.59` SQL SECURITY DEFINER VIEW `filtro0001_statolav_v`  AS  select 0 AS `sl_id`,'Tutti' AS `sl_des` union all select `b`.`id` AS `sl_id`,substr(concat(`b`.`sl_cod`,'-',`b`.`sl_descrizione`),1,25) AS `sl_des` from (`filtro0001_parametri_v` `a` join `stato_lavorazione` `b` on((`b`.`sl_soc` = ifnull(`a`.`parint01`,`b`.`sl_soc`)))) where (`b`.`sl_ins` = 1) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_tipoarcdoc_v`
--
DROP TABLE IF EXISTS `filtro0001_tipoarcdoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0001_tipoarcdoc_v`  AS  select `a`.`tparcdoc_id` AS `tparcdoc_id`,`a`.`tparcdoc_soc` AS `tparcdoc_soc`,`a`.`tparcdoc_socdes` AS `tparcdoc_socdes`,`a`.`tparcdoc_cod` AS `tparcdoc_cod`,`a`.`tparcdoc_des` AS `tparcdoc_des`,`a`.`tparcdoc_ges_id` AS `tparcdoc_ges_id`,`a`.`tparcdoc_gesnome` AS `tparcdoc_gesnome`,`a`.`tparcdoc_modifica` AS `tparcdoc_modifica` from `tipo_arcdoc_v` `a` where (`a`.`tparcdoc_soc` = `filtro0001_get_societa`()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0001_tipodoc_v`
--
DROP TABLE IF EXISTS `filtro0001_tipodoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0001_tipodoc_v`  AS  select 0 AS `td_id`,'Tutti' AS `td_des` union all select `b`.`td_id` AS `td_id`,`b`.`td_cod` AS `td_des` from (`filtro0001_parametri_v` `a` join `tipo_doc` `b` on((`b`.`td_soc` = ifnull(`a`.`parint01`,`b`.`td_soc`)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0002_liv1_v`
--
DROP TABLE IF EXISTS `filtro0002_liv1_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0002_liv1_v`  AS  select 0 AS `lv1_id`,'Tutti' AS `lv1_des` union all select `a`.`eleord_id` AS `lv1_id`,`a`.`eleord_des` AS `lv1_des` from `filtro0002_eleord` `a` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0002_liv2_v`
--
DROP TABLE IF EXISTS `filtro0002_liv2_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0002_liv2_v`  AS  select 0 AS `lv2_id`,'Tutti' AS `lv2_des` union all select `a`.`eleord_id` AS `lv2_id`,`a`.`eleord_des` AS `lv2_des` from `filtro0002_eleord` `a` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0002_liv3_v`
--
DROP TABLE IF EXISTS `filtro0002_liv3_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0002_liv3_v`  AS  select 0 AS `lv3_id`,'Tutti' AS `lv3_des` union all select `a`.`eleord_id` AS `lv3_id`,`a`.`eleord_des` AS `lv3_des` from `filtro0002_eleord` `a` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_elenco_pagine_v`
--
DROP TABLE IF EXISTS `filtro0003_elenco_pagine_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0003_elenco_pagine_v`  AS  select 1 AS `seq`,'&laquo;' AS `valore` union select 2 AS `seq`,'&lsaquo;' AS `valore` union select 3 AS `seq`,'1' AS `valore` union select 4 AS `seq`,'2' AS `valore` union select 5 AS `seq`,'3' AS `valore` union select 6 AS `seq`,'4' AS `valore` union select 7 AS `seq`,'5' AS `valore` union select 8 AS `seq`,'&rsaquo;' AS `valore` union select 9 AS `seq`,'&raquo;' AS `valore` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_get_idrecperpag_v`
--
DROP TABLE IF EXISTS `filtro0003_get_idrecperpag_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.170.73.253` SQL SECURITY DEFINER VIEW `filtro0003_get_idrecperpag_v`  AS  select `filtro0003_parconn_v`.`idrecperpag` AS `idrecperpag` from `filtro0003_parconn_v` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_get_limfin_v`
--
DROP TABLE IF EXISTS `filtro0003_get_limfin_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.170.73.253` SQL SECURITY DEFINER VIEW `filtro0003_get_limfin_v`  AS  select `filtro0003_parconn_v`.`numpagesp` AS `numpagesp` from `filtro0003_parconn_v` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_get_limini_v`
--
DROP TABLE IF EXISTS `filtro0003_get_limini_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.170.73.253` SQL SECURITY DEFINER VIEW `filtro0003_get_limini_v`  AS  select `filtro0003_parconn_v`.`limini` AS `limini` from `filtro0003_parconn_v` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_get_numpagcorr_v`
--
DROP TABLE IF EXISTS `filtro0003_get_numpagcorr_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.170.73.253` SQL SECURITY DEFINER VIEW `filtro0003_get_numpagcorr_v`  AS  select `filtro0003_parconn_v`.`numpagcorr` AS `numpagcorr` from `filtro0003_parconn_v` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_get_numpagesp_v`
--
DROP TABLE IF EXISTS `filtro0003_get_numpagesp_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.170.73.253` SQL SECURITY DEFINER VIEW `filtro0003_get_numpagesp_v`  AS  select `filtro0003_parconn_v`.`numpagesp` AS `numpagesp` from `filtro0003_parconn_v` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_get_numpagtot_v`
--
DROP TABLE IF EXISTS `filtro0003_get_numpagtot_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.170.73.253` SQL SECURITY DEFINER VIEW `filtro0003_get_numpagtot_v`  AS  select `filtro0003_parconn_v`.`numpagtot` AS `numpagtot` from `filtro0003_parconn_v` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_get_numposcorr_v`
--
DROP TABLE IF EXISTS `filtro0003_get_numposcorr_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.170.73.253` SQL SECURITY DEFINER VIEW `filtro0003_get_numposcorr_v`  AS  select `filtro0003_parconn_v`.`numposcorr` AS `numposcorr` from `filtro0003_parconn_v` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_get_numrectot_v`
--
DROP TABLE IF EXISTS `filtro0003_get_numrectot_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.170.73.253` SQL SECURITY DEFINER VIEW `filtro0003_get_numrectot_v`  AS  select `filtro0003_parconn_v`.`numrectot` AS `numrectot` from `filtro0003_parconn_v` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_get_recperpag_v`
--
DROP TABLE IF EXISTS `filtro0003_get_recperpag_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0003_get_recperpag_v`  AS  select `filtro0003_parconn_v`.`valrecperpag` AS `valrecperpag` from `filtro0003_parconn_v` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_get_valrecperpag_v`
--
DROP TABLE IF EXISTS `filtro0003_get_valrecperpag_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.170.73.253` SQL SECURITY DEFINER VIEW `filtro0003_get_valrecperpag_v`  AS  select `filtro0003_parconn_v`.`valrecperpag` AS `valrecperpag` from `filtro0003_parconn_v` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_parametri_v`
--
DROP TABLE IF EXISTS `filtro0003_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0003_parametri_v`  AS  select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`d`.`id` AS `idrecperpag`,(case `d`.`valore` when 0 then (case `b`.`parint02` when (`b`.`parint02` <= `e`.`maxrecperpag`) then `b`.`parint02` else `e`.`maxrecperpag` end) else `d`.`valore` end) AS `valrecperpag`,`d`.`descrizione` AS `desrecperpag`,`b`.`parint02` AS `numrectot`,`b`.`parint03` AS `numpagtot`,`b`.`parint04` AS `numpagcorr`,`b`.`parint05` AS `numpagesp`,`b`.`parint06` AS `limini`,`b`.`parint07` AS `limfin`,`b`.`parint08` AS `numposcorr` from ((((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) join `filtro0003_valori_default` `e`) join `filtro0003_elementi_pagina` `d` on((`d`.`id` = ifnull(`b`.`parint09`,`e`.`idrecperpag`)))) where (`a`.`id_filtro` = 3) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_parconn_v`
--
DROP TABLE IF EXISTS `filtro0003_parconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0003_parconn_v`  AS  select `filtro0003_parametri_v`.`ges_id` AS `ges_id`,`filtro0003_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0003_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0003_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0003_parametri_v`.`par_conn` AS `par_conn`,`filtro0003_parametri_v`.`idrecperpag` AS `idrecperpag`,`filtro0003_parametri_v`.`valrecperpag` AS `valrecperpag`,`filtro0003_parametri_v`.`desrecperpag` AS `desrecperpag`,`filtro0003_parametri_v`.`numrectot` AS `numrectot`,`filtro0003_parametri_v`.`numpagtot` AS `numpagtot`,`filtro0003_parametri_v`.`numpagcorr` AS `numpagcorr`,`filtro0003_parametri_v`.`numpagesp` AS `numpagesp`,`filtro0003_parametri_v`.`limini` AS `limini`,`filtro0003_parametri_v`.`limfin` AS `limfin`,`filtro0003_parametri_v`.`numposcorr` AS `numposcorr` from `filtro0003_parametri_v` where (`filtro0003_parametri_v`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0003_range_pagine_v`
--
DROP TABLE IF EXISTS `filtro0003_range_pagine_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.170.73.253` SQL SECURITY DEFINER VIEW `filtro0003_range_pagine_v`  AS  select `b`.`id_pagina` AS `valore`,`a`.`numpagcorr` AS `numpagcorr`,`a`.`numposcorr` AS `numposcorr`,(`a`.`numposcorr` - 1) AS `sinistra`,(`a`.`numpagesp` - `a`.`numposcorr`) AS `destra`,(`a`.`numpagcorr` - (`a`.`numposcorr` - 1)) AS `valsin`,(`a`.`numpagcorr` + (`a`.`numpagesp` - `a`.`numposcorr`)) AS `valdes`,`a`.`numpagtot` AS `numpagtot`,`a`.`numpagesp` AS `numpagesp` from (`filtro0003_parconn_v` `a` join `filtro0003_pagine` `b` on((`b`.`id_pagina` between (`a`.`numpagcorr` - (`a`.`numposcorr` - 1)) and (`a`.`numpagcorr` + (`a`.`numpagesp` - `a`.`numposcorr`))))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0004_parametri_v`
--
DROP TABLE IF EXISTS `filtro0004_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0004_parametri_v`  AS  select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_lastid` from ((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) where (`a`.`id_filtro` = 4) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0004_parconn_v`
--
DROP TABLE IF EXISTS `filtro0004_parconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0004_parconn_v`  AS  select `filtro0004_parametri_v`.`ges_id` AS `ges_id`,`filtro0004_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0004_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0004_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0004_parametri_v`.`par_conn` AS `par_conn`,`filtro0004_parametri_v`.`par_lastid` AS `par_lastid` from `filtro0004_parametri_v` where (`filtro0004_parametri_v`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0005_elearcdoc_v`
--
DROP TABLE IF EXISTS `filtro0005_elearcdoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0005_elearcdoc_v`  AS  select `b`.`ftarcdoc_id` AS `elearcdoc_id`,`b`.`ftarcdoc_idfatt` AS `elearcdoc_idfatt`,`b`.`ftarcdoc_iddoc` AS `elearcdoc_iddoc`,`b`.`ftarcdoc_estid` AS `elearcdoc_estid`,`b`.`ftarcdoc_estcod` AS `elearcdoc_estcod`,`b`.`ftarcdoc_estdes` AS `elearcdoc_estdes`,`b`.`ftarcdoc_tipoid` AS `elearcdoc_tipoid`,`b`.`ftarcdoc_tipocod` AS `elearcdoc_tipocod`,`b`.`ftarcdoc_tipodes` AS `elearcdoc_tipodes`,`b`.`ftarcdoc_desc` AS `elearcdoc_desc`,`b`.`ftarcdoc_size` AS `elearcdoc_size`,`b`.`ftarcdoc_nomefile` AS `elearcdoc_nomefile`,`b`.`ftarcdoc_ges_id` AS `elearcdoc_ges_id`,`b`.`ftarcdoc_ges_nominativo` AS `elearcdoc_ges_nominativo`,`b`.`ftarcdoc_modifica` AS `elearcdoc_modifica`,`b`.`ftarcdoc_modifica` AS `elearcdoc_modbar` from (`filtro0005_parconn_v` `a` join `fatture_arcdoc_v` `b` on((`b`.`ftarcdoc_idfatt` = `a`.`par_idfatt`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0005_fatture_arcdoc_v`
--
DROP TABLE IF EXISTS `filtro0005_fatture_arcdoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0005_fatture_arcdoc_v`  AS  select `b`.`ftarcdoc_id` AS `ftarcdoc_id`,`b`.`ftarcdoc_idfatt` AS `ftarcdoc_idfatt`,`b`.`ftarcdoc_iddoc` AS `ftarcdoc_iddoc`,`b`.`ftarcdoc_estid` AS `ftarcdoc_estid`,`b`.`ftarcdoc_estcod` AS `ftarcdoc_estcod`,`b`.`ftarcdoc_estdes` AS `ftarcdoc_estdes`,`b`.`ftarcdoc_tipoid` AS `ftarcdoc_tipoid`,`b`.`ftarcdoc_tipocod` AS `ftarcdoc_tipocod`,`b`.`ftarcdoc_tipodes` AS `ftarcdoc_tipodes`,`b`.`ftarcdoc_desc` AS `ftarcdoc_desc`,`b`.`ftarcdoc_size` AS `ftarcdoc_size`,`b`.`ftarcdoc_nomefile` AS `ftarcdoc_nomefile`,`b`.`ftarcdoc_ges_id` AS `ftarcdoc_ges_id`,`b`.`ftarcdoc_ges_nominativo` AS `ftarcdoc_ges_nominativo`,`b`.`ftarcdoc_modifica` AS `ftarcdoc_modifica`,`b`.`ftarcdoc_modifica` AS `ftarcdoc_modbar` from (`filtro0005_parconn_v` `a` join `fatture_arcdoc_v` `b` on((`b`.`ftarcdoc_idfatt` = `a`.`par_idfatt`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0005_parametri_v`
--
DROP TABLE IF EXISTS `filtro0005_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0005_parametri_v`  AS  select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_idfatt` from ((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) where (`a`.`id_filtro` = 5) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0005_parconn_v`
--
DROP TABLE IF EXISTS `filtro0005_parconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0005_parconn_v`  AS  select `filtro0005_parametri_v`.`ges_id` AS `ges_id`,`filtro0005_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0005_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0005_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0005_parametri_v`.`par_conn` AS `par_conn`,`filtro0005_parametri_v`.`par_idfatt` AS `par_idfatt` from `filtro0005_parametri_v` where (`filtro0005_parametri_v`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0006_debann_tpann_v`
--
DROP TABLE IF EXISTS `filtro0006_debann_tpann_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0006_debann_tpann_v`  AS  select `a`.`dantpan_id` AS `dantpan_id`,`a`.`dantpan_idsoc` AS `dantpan_idsoc`,`a`.`dantpan_cod` AS `dantpan_cod`,`a`.`dantpan_des` AS `dantpan_des`,`a`.`dantpan_ges_id` AS `dantpan_gesid`,`utypkg_gestid2gestnomin`(`a`.`dantpan_ges_id`) AS `dantpan_nominativo`,`a`.`dantpan_modifica` AS `dantpan_modifica`,`utypkg_datadate2databar`(`a`.`dantpan_modifica`) AS `dantpan_modbar` from `debann_tipi_annotazione` `a` where (`a`.`dantpan_idsoc` = `filtro0001_get_societa`()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0006_debann_tpcon_v`
--
DROP TABLE IF EXISTS `filtro0006_debann_tpcon_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0006_debann_tpcon_v`  AS  select `a`.`dantpcon_id` AS `dantpcon_id`,`a`.`dantpcon_idsoc` AS `dantpcon_idsoc`,`a`.`dantpcon_cod` AS `dantpcon_cod`,`a`.`dantpcon_des` AS `dantpcon_des`,`a`.`dantpcon_ges_id` AS `dantpcon_gesid`,`utypkg_gestid2gestnomin`(`a`.`dantpcon_ges_id`) AS `dantpcon_nominativo`,`a`.`dantpcon_modifica` AS `dantpcon_modifica`,`utypkg_datadate2databar`(`a`.`dantpcon_modifica`) AS `dantpcon_modbar` from `debann_tipi_contatto` `a` where (`a`.`dantpcon_idsoc` = `filtro0001_get_societa`()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0006_debann_v`
--
DROP TABLE IF EXISTS `filtro0006_debann_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz`@`localhost` SQL SECURITY DEFINER VIEW `filtro0006_debann_v`  AS  select `a`.`debann_id` AS `debann_id`,`a`.`debann_debid` AS `deann_debid`,`a`.`debann_idsoc` AS `debann_idsoc`,`a`.`debann_debcod` AS `debann_debcod`,`a`.`debann_debdes` AS `debann_debdes`,`a`.`debann_idtpann` AS `debann_idtpann`,`a`.`debann_anncod` AS `debann_anncod`,`a`.`debann_anndes` AS `debann_anndes`,`a`.`debann_idtpcon` AS `debann_idtpcon`,`a`.`debann_tpcod` AS `debann_tpcod`,`a`.`debann_tpdes` AS `debann_tpdes`,`a`.`debann_iddata` AS `debann_iddata`,`a`.`debann_datanum` AS `debann_datanum`,`a`.`debann_databar` AS `debann_databar`,`a`.`debann_annot` AS `debann_annot`,`a`.`debann_inter` AS `debann_inter`,`a`.`debann_ges_id` AS `debann_ges_id`,`a`.`debanno_nominativo` AS `debanno_nominativo`,`a`.`debann_modifica` AS `debann_modifica`,`a`.`debann_modbar` AS `debann_modbar`,ifnull(`b`.`debandoc_numdoc`,0) AS `debann_numdoc`,(case ifnull(`b`.`debandoc_numdoc`,0) when 0 then 'N' else 'S' end) AS `debann_doc_checkdocsino` from (((((`filtro0006_parconn_v` `par` join `societa` `soc` on((`soc`.`soc_cod` = ifnull(`filtro0001_get_societa`(),`soc`.`soc_cod`)))) join `gest_debitore` `gd` on((`gd`.`gd_ges_id` = `filtro0000_get_gesid`()))) join `debitori` `d` on(((`d`.`deb_soc` = `soc`.`soc_cod`) and (`d`.`deb_id` = `gd`.`gd_deb_id`) and (`d`.`deb_id` = ifnull(`par`.`par_debid`,`d`.`deb_id`))))) join `debitori_annotazioni_v` `a` on(((`a`.`debann_idsoc` = `soc`.`soc_cod`) and (`a`.`debann_debid` = `d`.`deb_id`)))) left join `debann_arcdoc_checksino_v` `b` on((`b`.`debandoc_idann` = `a`.`debann_id`))) order by `a`.`debann_iddata` desc ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0006_parametri_v`
--
DROP TABLE IF EXISTS `filtro0006_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0006_parametri_v`  AS  select `b`.`ges_id` AS `ges_id`,`utypkg_gestid2gestnomin`(`b`.`ges_id`) AS `ges_nominativo`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_debid`,`b`.`parint02` AS `par_lastid`,`utypkg_gestid2gestnomin`(`b`.`operatore`) AS `ope_nominativo`,`UTYPKG_DATADATE2DATABAR`(`b`.`modifica`) AS `modibar` from (`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) where (`a`.`id_filtro` = 6) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0006_parconn_v`
--
DROP TABLE IF EXISTS `filtro0006_parconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0006_parconn_v`  AS  select `filtro0006_parametri_v`.`ges_id` AS `ges_id`,`filtro0006_parametri_v`.`ges_nominativo` AS `ges_nominativo`,`filtro0006_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0006_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0006_parametri_v`.`par_conn` AS `par_conn`,`filtro0006_parametri_v`.`par_debid` AS `par_debid`,`filtro0006_parametri_v`.`par_lastid` AS `par_lastid`,`filtro0006_parametri_v`.`ope_nominativo` AS `ope_nominativo`,`filtro0006_parametri_v`.`modibar` AS `modibar` from `filtro0006_parametri_v` where (`filtro0006_parametri_v`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0007_parametri_v`
--
DROP TABLE IF EXISTS `filtro0007_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0007_parametri_v`  AS  select `b`.`ges_id` AS `ges_id`,`utypkg_gestid2gestnomin`(`b`.`ges_id`) AS `ges_nominativo`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_debid`,`utypkg_gestid2gestnomin`(`b`.`operatore`) AS `ope_nominativo`,`UTYPKG_DATADATE2DATABAR`(`b`.`modifica`) AS `modibar` from (`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) where (`a`.`id_filtro` = 7) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0007_parconn_v`
--
DROP TABLE IF EXISTS `filtro0007_parconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0007_parconn_v`  AS  select `filtro0007_parametri_v`.`ges_id` AS `ges_id`,`filtro0007_parametri_v`.`ges_nominativo` AS `ges_nominativo`,`filtro0007_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0007_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0007_parametri_v`.`par_conn` AS `par_conn`,`filtro0007_parametri_v`.`par_debid` AS `par_debid`,`filtro0007_parametri_v`.`ope_nominativo` AS `ope_nominativo`,`filtro0007_parametri_v`.`modibar` AS `modibar` from `filtro0007_parametri_v` where (`filtro0007_parametri_v`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0008_debann_arcdoc_v`
--
DROP TABLE IF EXISTS `filtro0008_debann_arcdoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0008_debann_arcdoc_v`  AS  select `b`.`debandoc_id` AS `debandoc_id`,`b`.`debandoc_idann` AS `debandoc_idann`,`b`.`debandoc_iddoc` AS `debandoc_iddoc`,`b`.`debandoc_estid` AS `debandoc_estid`,`b`.`debandoc_estcod` AS `debandoc_estcod`,`b`.`debandoc_estdes` AS `debandoc_estdes`,`b`.`debandoc_tipoid` AS `debandoc_tipoid`,`b`.`debandoc_tipocod` AS `debandoc_tipocod`,`b`.`debandoc_tipodes` AS `debandoc_tipodes`,`b`.`debandoc_desc` AS `debandoc_desc`,`b`.`debandoc_size` AS `debandoc_size`,`b`.`debandoc_nomefile` AS `debandoc_nomefile`,`b`.`debandoc_ges_id` AS `debandoc_ges_id`,`b`.`debandoc_ges_nominativo` AS `debandoc_ges_nominativo`,`b`.`debandoc_modifica` AS `debandoc_modifica`,`b`.`debandoc_modifica` AS `debandoc_modbar` from (`filtro0008_parconn_v` `a` join `debann_arcdoc_v` `b` on((`b`.`debandoc_idann` = `a`.`par_debannid`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0008_elearcdoc_v`
--
DROP TABLE IF EXISTS `filtro0008_elearcdoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0008_elearcdoc_v`  AS  select `b`.`debandoc_id` AS `elearcdoc_id`,`b`.`debandoc_idann` AS `elearcdoc_idann`,`b`.`debandoc_iddoc` AS `elearcdoc_iddoc`,`b`.`debandoc_estid` AS `elearcdoc_estid`,`b`.`debandoc_estcod` AS `elearcdoc_estcod`,`b`.`debandoc_estdes` AS `elearcdoc_estdes`,`b`.`debandoc_tipoid` AS `elearcdoc_tipoid`,`b`.`debandoc_tipocod` AS `elearcdoc_tipocod`,`b`.`debandoc_tipodes` AS `elearcdoc_tipodes`,`b`.`debandoc_desc` AS `elearcdoc_desc`,`b`.`debandoc_size` AS `elearcdoc_size`,`b`.`debandoc_nomefile` AS `elearcdoc_nomefile`,`b`.`debandoc_ges_id` AS `elearcdoc_ges_id`,`b`.`debandoc_ges_nominativo` AS `elearcdoc_ges_nominativo`,`b`.`debandoc_modifica` AS `elearcdoc_modifica`,`b`.`debandoc_modifica` AS `elearcdoc_modbar` from (`filtro0008_parconn_v` `a` join `debann_arcdoc_v` `b` on((`b`.`debandoc_idann` = `a`.`par_debannid`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0008_parametri_v`
--
DROP TABLE IF EXISTS `filtro0008_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0008_parametri_v`  AS  select `b`.`ges_id` AS `ges_id`,`utypkg_gestid2gestnomin`(`b`.`ges_id`) AS `ges_nominativo`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_debannid`,`utypkg_gestid2gestnomin`(`b`.`operatore`) AS `ope_nominativo`,`UTYPKG_DATADATE2DATABAR`(`b`.`modifica`) AS `modibar` from (`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) where (`a`.`id_filtro` = 8) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0008_parconn_v`
--
DROP TABLE IF EXISTS `filtro0008_parconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0008_parconn_v`  AS  select `filtro0008_parametri_v`.`ges_id` AS `ges_id`,`filtro0008_parametri_v`.`ges_nominativo` AS `ges_nominativo`,`filtro0008_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0008_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0008_parametri_v`.`par_conn` AS `par_conn`,`filtro0008_parametri_v`.`par_debannid` AS `par_debannid`,`filtro0008_parametri_v`.`ope_nominativo` AS `ope_nominativo`,`filtro0008_parametri_v`.`modibar` AS `modibar` from `filtro0008_parametri_v` where (`filtro0008_parametri_v`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0009_debitori_v`
--
DROP TABLE IF EXISTS `filtro0009_debitori_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.157.112.27` SQL SECURITY DEFINER VIEW `filtro0009_debitori_v`  AS  select `b`.`deb_id` AS `deb_id`,`b`.`deb_cod` AS `deb_cod`,`b`.`deb_des` AS `deb_des`,`b`.`deb_citta` AS `deb_citta`,`b`.`deb_pro` AS `deb_pro`,`b`.`deb_tel` AS `deb_tel`,`b`.`deb_mail` AS `deb_mail`,ifnull(`c`.`debann_numann`,0) AS `deb_numann`,(case ifnull(`c`.`debann_numann`,0) when 0 then 'N' else 'S' end) AS `deb_ann_checksino` from (((`filtro0009_parconn_v` `a` join `gest_debitore_attivi_v` `gd` on(((`gd`.`gd_ges_id` = `a`.`ges_id`) and (`gd`.`gd_deb_id` = ifnull(`a`.`par_debid`,`gd`.`gd_deb_id`))))) join `debitori` `b` on(((`b`.`deb_soc` = `filtro0001_get_societa`()) and (`b`.`deb_id` = `gd`.`gd_deb_id`)))) left join `debann_checksino_v` `c` on((`c`.`debann_debid` = `b`.`deb_id`))) order by `b`.`deb_cod` ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0009_parametri_v`
--
DROP TABLE IF EXISTS `filtro0009_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0009_parametri_v`  AS  select `b`.`ges_id` AS `ges_id`,`utypkg_gestid2gestnomin`(`b`.`ges_id`) AS `ges_nominativo`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_debid`,`utypkg_gestid2gestnomin`(`b`.`operatore`) AS `ope_nominativo`,`UTYPKG_DATADATE2DATABAR`(`b`.`modifica`) AS `modibar` from (`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) where (`a`.`id_filtro` = 9) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0009_parconn_v`
--
DROP TABLE IF EXISTS `filtro0009_parconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0009_parconn_v`  AS  select `filtro0009_parametri_v`.`ges_id` AS `ges_id`,`filtro0009_parametri_v`.`ges_nominativo` AS `ges_nominativo`,`filtro0009_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0009_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0009_parametri_v`.`par_conn` AS `par_conn`,`filtro0009_parametri_v`.`par_debid` AS `par_debid`,`filtro0009_parametri_v`.`ope_nominativo` AS `ope_nominativo`,`filtro0009_parametri_v`.`modibar` AS `modibar` from `filtro0009_parametri_v` where (`filtro0009_parametri_v`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0010_parametri_v`
--
DROP TABLE IF EXISTS `filtro0010_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0010_parametri_v`  AS  select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_lastid` from ((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) where (`a`.`id_filtro` = 10) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0010_parconn_v`
--
DROP TABLE IF EXISTS `filtro0010_parconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0010_parconn_v`  AS  select `filtro0010_parametri_v`.`ges_id` AS `ges_id`,`filtro0010_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0010_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0010_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0010_parametri_v`.`par_conn` AS `par_conn`,`filtro0010_parametri_v`.`par_lastid` AS `par_lastid` from `filtro0010_parametri_v` where (`filtro0010_parametri_v`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0011_eletabsr_v`
--
DROP TABLE IF EXISTS `filtro0011_eletabsr_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0011_eletabsr_v`  AS  select `b`.`fttbsr_numero` AS `fttbsr_numero`,`b`.`fttbsr_nota` AS `fttbsr_nota`,`b`.`fttbsr_ges_nominativo` AS `fttbsr_ges_nominativo`,`b`.`fttbsr_modifica` AS `fttbsr_modifica`,`b`.`fttbsr_modbar` AS `fttbsr_modbar` from (`filtro0011_parconn_v` `a` join `fatture_tabsr_v` `b` on((`b`.`fttbsr_idfatt` = `a`.`par_idfatt`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0011_parametri_v`
--
DROP TABLE IF EXISTS `filtro0011_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0011_parametri_v`  AS  select `b`.`ges_id` AS `ges_id`,`c`.`ges_nome` AS `ges_nome`,`a`.`id_filtro` AS `id_filtro`,`a`.`descrizione` AS `des_filtro`,`b`.`par_conn` AS `par_conn`,`b`.`parint01` AS `par_idfatt` from ((`filtri` `a` join `filtri_parametri` `b` on((`b`.`id_filtro` = `a`.`id_filtro`))) join `gestori` `c` on((`c`.`ges_id` = `b`.`ges_id`))) where (`a`.`id_filtro` = 11) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro0011_parconn_v`
--
DROP TABLE IF EXISTS `filtro0011_parconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro0011_parconn_v`  AS  select `filtro0011_parametri_v`.`ges_id` AS `ges_id`,`filtro0011_parametri_v`.`ges_nome` AS `ges_nome`,`filtro0011_parametri_v`.`id_filtro` AS `id_filtro`,`filtro0011_parametri_v`.`des_filtro` AS `des_filtro`,`filtro0011_parametri_v`.`par_conn` AS `par_conn`,`filtro0011_parametri_v`.`par_idfatt` AS `par_idfatt` from `filtro0011_parametri_v` where (`filtro0011_parametri_v`.`par_conn` = connection_id()) ;

-- --------------------------------------------------------

--
-- Struttura per vista `filtro003_elenco_pagine_v`
--
DROP TABLE IF EXISTS `filtro003_elenco_pagine_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `filtro003_elenco_pagine_v`  AS  select 1 AS `pag` union select 2 AS `pag` union select 3 AS `pag` union select 4 AS `pag` union select 5 AS `pag` ;

-- --------------------------------------------------------

--
-- Struttura per vista `gestione_scadenziario_migliorata_v`
--
DROP TABLE IF EXISTS `gestione_scadenziario_migliorata_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.180.87` SQL SECURITY DEFINER VIEW `gestione_scadenziario_migliorata_v`  AS  select `c`.`id` AS `id`,`c`.`ft_soc` AS `ft_soc`,`c`.`ft_deb_id` AS `ft_deb_id`,`c`.`last_fsf_ac` AS `ft_ac`,`d`.`deb_id` AS `ft_id_debitore`,`d`.`deb_cod` AS `ft_deb_cod`,`d`.`deb_des` AS `ft_deb_des`,`d`.`deb_citta` AS `ft_deb_citta`,`c`.`ft_ced_id` AS `ft_ced_id`,substr(`c`.`ft_attribuzione`,2,length(`c`.`ft_attribuzione`)) AS `ft_attribuzione`,`c`.`ft_num_fat` AS `ft_num_fat`,`c`.`ft_td_id` AS `ft_td_id`,`td`.`td_cod` AS `ft_td`,`c`.`ft_testo` AS `ft_testo`,'1010' AS `ft_data_fat`,'1010' AS `ft_data_scad`,`c`.`ft_imp_fat` AS `ft_imp_fat`,`c`.`ft_imp_aperto` AS `ft_imp_aperto`,`c`.`ft_data_ins` AS `ft_data_ins`,`c`.`ft_id_data_chius` AS `ft_id_data_chius`,'ddd' AS `ft_nota_crgest`,'ddd' AS `ft_data_nota`,ifnull(`c`.`ft_arcdoc_check_sino`,0) AS `ft_arcdoc_sino`,ifnull(`c`.`ft_tabsr_check_sino`,0) AS `ft_tabsr_sino` from (((((`filtro0001_parametri_v` `a` join `gest_debitore` `b` on((`b`.`gd_ges_id` = `a`.`ges_id`))) join `debitori` `d` on(((`d`.`deb_id` = `b`.`gd_deb_id`) and (`d`.`deb_id` = ifnull(`a`.`parint03`,`d`.`deb_id`))))) join `societa` `soc` on((`soc`.`soc_cod` = ifnull(`a`.`parint01`,`soc`.`soc_cod`)))) join `tipo_doc` `td` on(((`td`.`td_soc` = ifnull(`a`.`parint01`,`soc`.`soc_cod`)) and (`td`.`td_id` = ifnull(`a`.`parint02`,`td`.`td_id`))))) join `fatture` `c` on(((`c`.`ft_deb_id` = `d`.`deb_id`) and (`c`.`ft_soc` = `soc`.`soc_cod`) and (`c`.`ft_td_id` = `td`.`td_id`) and (`c`.`last_fsf_ac` = 0)))) where (`c`.`id` <= 1000) ;

-- --------------------------------------------------------

--
-- Struttura per vista `gestione_scadenziario_v`
--
DROP TABLE IF EXISTS `gestione_scadenziario_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.157.112.27` SQL SECURITY DEFINER VIEW `gestione_scadenziario_v`  AS  select `c`.`id` AS `id`,`c`.`ft_soc` AS `ft_soc`,`c`.`ft_deb_id` AS `ft_deb_id`,`fsf`.`fsf_ac` AS `ft_ac`,`d`.`deb_id` AS `ft_id_debitore`,`d`.`deb_cod` AS `ft_deb_cod`,`d`.`deb_des` AS `ft_deb_des`,`d`.`deb_citta` AS `ft_deb_citta`,`c`.`ft_ced_id` AS `ft_ced_id`,substr(`c`.`ft_attribuzione`,2,length(`c`.`ft_attribuzione`)) AS `ft_attribuzione`,`c`.`ft_num_fat` AS `ft_num_fat`,`c`.`ft_td_id` AS `ft_td_id`,`td`.`td_cod` AS `ft_td`,`fsln`.`ftstlav_id_stato_lav` AS `ft_id_stato_lav`,`fsln`.`ftstlav_sldes` AS `ft_des_stato_lav`,concat(`fsln`.`ftstlav_slcod`,'-',`fsln`.`ftstlav_sldes`) AS `ft_sl_coddes`,`c`.`ft_testo` AS `ft_testo`,`df`.`databar` AS `ft_data_fat`,`ds`.`databar` AS `ft_data_scad`,`c`.`ft_imp_fat` AS `ft_imp_fat`,`c`.`ft_imp_aperto` AS `ft_imp_aperto`,`c`.`ft_data_ins` AS `ft_data_ins`,`c`.`ft_id_data_fat` AS `ft_id_data_fat`,`c`.`ft_id_data_chius` AS `ft_id_data_chius`,`fsln`.`ftstlav_nota_crgest` AS `ft_nota_crgest`,`fsln`.`ftstlav_data_bar` AS `ft_data_nota`,ifnull(`c`.`ft_arcdoc_check_sino`,0) AS `ft_arcdoc_sino`,ifnull(`c`.`ft_tabsr_check_sino`,0) AS `ft_tabsr_sino`,`c`.`ft_forecast_mese` AS `ft_forecast_mese`,`c`.`ft_forecast_anno` AS `ft_forecast_anno`,(case `c`.`ft_forecast_anno` when NULL then NULL else concat(`c`.`ft_forecast_mese`,'/',`c`.`ft_forecast_anno`) end) AS `ft_forecast_scad` from ((((((((((`filtro0001_parametri_v` `a` join `gest_debitore_attivi_v` `b` on((`b`.`gd_ges_id` = `a`.`ges_id`))) join `debitori` `d` on(((`d`.`deb_id` = `b`.`gd_deb_id`) and (`d`.`deb_id` = ifnull(`a`.`parint03`,`d`.`deb_id`))))) join `societa` `soc` on((`soc`.`soc_cod` = ifnull(`a`.`parint01`,`soc`.`soc_cod`)))) join `tipo_doc` `td` on(((`td`.`td_soc` = ifnull(`a`.`parint01`,`soc`.`soc_cod`)) and (`td`.`td_id` = ifnull(`a`.`parint02`,`td`.`td_id`))))) join `stato_lavorazione` `sl` on((`sl`.`id` = ifnull(`a`.`parint04`,`sl`.`id`)))) join `calendario` `df` on((`df`.`id` between ifnull(`a`.`parint05`,0) and ifnull(`a`.`parint06`,9999999)))) join `calendario` `ds` on((`ds`.`id` between ifnull(`a`.`parint07`,0) and ifnull(`a`.`parint08`,9999999)))) join `fatture` `c` on(((`c`.`ft_deb_id` = `d`.`deb_id`) and (`c`.`ft_soc` = `soc`.`soc_cod`) and (`c`.`ft_td_id` = `td`.`td_id`) and (`c`.`ft_id_data_fat` = `df`.`id`) and (`c`.`ft_id_data_scad` = `ds`.`id`) and (`c`.`ft_num_fat` = ifnull(`a`.`parvar09`,`c`.`ft_num_fat`)) and (ifnull(`c`.`ft_arcdoc_check_sino`,0) = (case ifnull(`a`.`parint11`,0) when 0 then ifnull(`c`.`ft_arcdoc_check_sino`,0) else ifnull(`a`.`parint11`,0) end))))) join `fatture_stato_fattura_v` `fsf` on(((`fsf`.`fsf_id_fattura` = `c`.`id`) and (`fsf`.`fsf_ac` = (case ifnull(`a`.`parint10`,0) when 0 then 0 else `fsf`.`fsf_ac` end))))) join `fatture_statilavnote_v` `fsln` on(((`fsln`.`ftstlav_id_fattura` = `c`.`id`) and (`fsln`.`ftstlav_id_stato_lav` = `sl`.`id`)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `gest_debitore_attivi_v`
--
DROP TABLE IF EXISTS `gest_debitore_attivi_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`5.157.112.27` SQL SECURITY DEFINER VIEW `gest_debitore_attivi_v`  AS  select `gest_debitore`.`gd_ges_id` AS `gd_ges_id`,`gest_debitore`.`gd_deb_id` AS `gd_deb_id` from `gest_debitore` where isnull(`gest_debitore`.`gd_data_dissociazione`) ;

-- --------------------------------------------------------

--
-- Struttura per vista `gest_debitore_v`
--
DROP TABLE IF EXISTS `gest_debitore_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `gest_debitore_v`  AS  select `a`.`ges_id` AS `ges_id`,`a`.`ges_cognome` AS `ges_cognome`,`a`.`ges_nome` AS `ges_nome`,`a`.`ges_ruolo` AS `ges_ruolo`,`c`.`deb_id` AS `deb_id`,`c`.`deb_soc` AS `deb_soc`,`c`.`deb_cod` AS `beb_cod`,`c`.`deb_des` AS `des_des` from ((`gestori` `a` left join `gest_debitore` `b` on((`b`.`gd_ges_id` = `a`.`ges_id`))) join `debitori` `c` on((`c`.`deb_id` = (case `a`.`ges_ruolo` when 'admin' then `c`.`deb_id` else `b`.`gd_deb_id` end)))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `pippo_v`
--
DROP TABLE IF EXISTS `pippo_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `pippo_v`  AS  select '\'' AS `pippo` ;

-- --------------------------------------------------------

--
-- Struttura per vista `statist_movimenti_v`
--
DROP TABLE IF EXISTS `statist_movimenti_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `statist_movimenti_v`  AS  select `a`.`stmov_gesid` AS `stmov_gesid`,`f`.`fosuser_username` AS `stmov_username`,`g`.`ges_cognome` AS `cognome`,`g`.`ges_nome` AS `ges_nome`,`a`.`stmov_opeid` AS `stmov_opeid`,`b`.`statope_desc` AS `stmov_des`,`a`.`stmod_datetime` AS `stmod_datetime`,`d`.`username` AS `operatore`,`a`.`modifica` AS `modifica` from ((((`statist_movimenti` `a` join `statist_operazioni` `b` on((`b`.`statope_id` = `a`.`stmov_id`))) join `gestori` `g` on((`g`.`ges_id` = `a`.`stmov_gesid`))) join `fosuser_gestori` `f` on((`f`.`fosuser_gestid` = `g`.`ges_id`))) join `fos_user` `d` on((`d`.`id` = `a`.`operatore`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `tabsr_stati_v`
--
DROP TABLE IF EXISTS `tabsr_stati_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `tabsr_stati_v`  AS  select `a`.`tbsrst_id` AS `tbsrst_id`,`a`.`tbsrst_soc` AS `tbsrst_soccod`,`b`.`soc_des` AS `tbsrst_socdes`,`a`.`tbsrst_cod` AS `tbsrst_cod`,`a`.`tbsrst_desc` AS `tbsrst_desc`,`a`.`tbsrst_ges_id` AS `tbsrst_ges_id`,`UTYPKG_GESTID2GESTNOMIN`(`a`.`tbsrst_ges_id`) AS `tbsrst_ges_nominativo`,`a`.`tbsrst_modifica` AS `tbsrst_modifica`,`UTYPKG_DATADATE2DATABAR`(`a`.`tbsrst_modifica`) AS `tbsrst_modbar` from (`tabsr_stati` `a` join `societa` `b` on((`b`.`soc_cod` = `a`.`tbsrst_soc`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `tabsr_v`
--
DROP TABLE IF EXISTS `tabsr_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `tabsr_v`  AS  select `a`.`tbsr_id` AS `tbsr_id`,`a`.`tbsr_numero` AS `tbsr_numero`,`a`.`tbsr_nota` AS `tbsr_nota`,`a`.`tbsr_stato` AS `tbsr_idstato`,`b`.`tbsrst_cod` AS `tbsr_codstato`,`b`.`tbsrst_desc` AS `tbsr_desstato`,`a`.`tbsr_ges_id` AS `tbsr_ges_id`,`a`.`tbsr_modifica` AS `tbsr_modifica` from (`tab_sr` `a` join `tabsr_stati` `b` on((`b`.`tbsrst_id` = `a`.`tbsr_stato`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `tab_arcdoc_v`
--
DROP TABLE IF EXISTS `tab_arcdoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `tab_arcdoc_v`  AS  select `a`.`tbarcdoc_id` AS `tbarcdoc_id`,`a`.`tbarcdoc_soc` AS `tbarcdoc_soc`,`a`.`tbarcdoc_est` AS `tbarcdoc_estid`,`b`.`estarcdoc_cod` AS `tbarcdoc_estcod`,`b`.`estarcdoc_des` AS `tbarcdoc_estdes`,`a`.`tbarcdoc_tipo` AS `tbarcdoc_tipoid`,`c`.`tparcdoc_cod` AS `tbarcdoc_tipocod`,`c`.`tparcdoc_des` AS `tbarcdoc_tipodes`,`a`.`tbarcdoc_size` AS `tbarcdoc_size`,`a`.`tbarcdoc_nomefile` AS `tbarcdoc_nomefile`,`a`.`tbarcdoc_desc` AS `tbarcdoc_desc`,`a`.`tbarcdoc_blob` AS `tbarcdoc_blob`,`a`.`tbarcdoc_ges_id` AS `tbarcdoc_ges_id`,concat(`d`.`ges_cognome`,' ',`d`.`ges_nome`) AS `tbarcdoc_ges_nominativo`,`a`.`tbarcdoc_modifica` AS `tbarcdoc_modifica` from (((`tab_arcdoc` `a` join `estensioni_arcdoc` `b` on((`b`.`estarcdoc_id` = `a`.`tbarcdoc_est`))) join `tipi_arcdoc` `c` on((`c`.`tparcdoc_id` = `a`.`tbarcdoc_tipo`))) join `gestori` `d` on((`d`.`ges_id` = `a`.`tbarcdoc_ges_id`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `tab_procedure_parametri_v`
--
DROP TABLE IF EXISTS `tab_procedure_parametri_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `tab_procedure_parametri_v`  AS  select `a`.`id_proc` AS `id_proc`,`b`.`descrizione` AS `des_proc`,`a`.`id_host` AS `id_host`,`c`.`descrizione` AS `des_host`,`a`.`parcar1` AS `directory_file_load` from ((`tab_procedure_parametri` `a` join `tab_procedure` `b` on((`b`.`id_proc` = `a`.`id_proc`))) join `tab_procedure_host` `c` on((`c`.`id_host` = `a`.`id_host`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `tipo_arcdoc_v`
--
DROP TABLE IF EXISTS `tipo_arcdoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`146.241.243.105` SQL SECURITY DEFINER VIEW `tipo_arcdoc_v`  AS  select `a`.`tparcdoc_id` AS `tparcdoc_id`,`a`.`tparcdoc_soc` AS `tparcdoc_soc`,`c`.`soc_des` AS `tparcdoc_socdes`,`a`.`tparcdoc_cod` AS `tparcdoc_cod`,`a`.`tparcdoc_des` AS `tparcdoc_des`,`a`.`tparcdoc_ges_id` AS `tparcdoc_ges_id`,concat(`b`.`ges_cognome`,' ',`b`.`ges_nome`) AS `tparcdoc_gesnome`,`a`.`tparcdoc_modifica` AS `tparcdoc_modifica` from ((`tipi_arcdoc` `a` join `gestori` `b` on((`b`.`ges_id` = `a`.`tparcdoc_ges_id`))) join `societa` `c` on((`c`.`soc_cod` = `a`.`tparcdoc_soc`))) ;

-- --------------------------------------------------------

--
-- Struttura per vista `webapp_selsoc_updconn_v`
--
DROP TABLE IF EXISTS `webapp_selsoc_updconn_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`95.246.198.146` SQL SECURITY DEFINER VIEW `webapp_selsoc_updconn_v`  AS  select 1 AS `username`,1 AS `ges_id`,1 AS `nominativo`,1 AS `soc_cod`,1 AS `soc_des` ;

-- --------------------------------------------------------

--
-- Struttura per vista `webapp_selsoc_v`
--
DROP TABLE IF EXISTS `webapp_selsoc_v`;

CREATE ALGORITHM=UNDEFINED DEFINER=`vs28ahbz_cris`@`95.246.198.146` SQL SECURITY DEFINER VIEW `webapp_selsoc_v`  AS  select 1 AS `username`,1 AS `ges_id`,1 AS `nominativo`,1 AS `soc_cod`,1 AS `soc_des` ;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `calendario`
--
ALTER TABLE `calendario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `calendario_1` (`datachar`),
  ADD UNIQUE KEY `calendario_2` (`databar`);

--
-- Indici per le tabelle `cedenti`
--
ALTER TABLE `cedenti`
  ADD PRIMARY KEY (`ced_id`),
  ADD UNIQUE KEY `cedenti_1` (`ced_soc`,`ced_cod`);

--
-- Indici per le tabelle `debann_arcdoc`
--
ALTER TABLE `debann_arcdoc`
  ADD PRIMARY KEY (`debandoc_id`),
  ADD KEY `debann_arcdoc_1` (`debandoc_idann`),
  ADD KEY `fk_debannarcdoc_tabarcdoc` (`debandoc_iddoc`),
  ADD KEY `fk_debannarcdoc_gestori` (`debandoc_ges_id`);

--
-- Indici per le tabelle `debann_tipi_annotazione`
--
ALTER TABLE `debann_tipi_annotazione`
  ADD PRIMARY KEY (`dantpan_id`),
  ADD UNIQUE KEY `debanntipiannotazione_1` (`dantpan_idsoc`,`dantpan_cod`),
  ADD KEY `fk_debanntipiannotazione_gestori` (`dantpan_ges_id`);

--
-- Indici per le tabelle `debann_tipi_contatto`
--
ALTER TABLE `debann_tipi_contatto`
  ADD PRIMARY KEY (`dantpcon_id`),
  ADD UNIQUE KEY `debanntipicontatto_1` (`dantpcon_idsoc`,`dantpcon_cod`),
  ADD KEY `fk_debanntipicontatto_gestori` (`dantpcon_ges_id`);

--
-- Indici per le tabelle `debitori`
--
ALTER TABLE `debitori`
  ADD PRIMARY KEY (`deb_id`),
  ADD UNIQUE KEY `debitori_1` (`deb_soc`,`deb_cod`);

--
-- Indici per le tabelle `debitori_annotazioni`
--
ALTER TABLE `debitori_annotazioni`
  ADD PRIMARY KEY (`debann_id`),
  ADD KEY `fk_debitoriannotazioni_debitori` (`debann_debid`),
  ADD KEY `fk_debitoriannotazioni_debanntipicontatto` (`debann_idtpcon`),
  ADD KEY `fk_debitoriannotazioni_debanntipiannotazione` (`debann_idtpann`),
  ADD KEY `fk_debitoriannotazioni_calendario` (`debann_iddata`);

--
-- Indici per le tabelle `debitori_annotazioni_csv`
--
ALTER TABLE `debitori_annotazioni_csv`
  ADD PRIMARY KEY (`debann_id`);

--
-- Indici per le tabelle `estensioni_arcdoc`
--
ALTER TABLE `estensioni_arcdoc`
  ADD PRIMARY KEY (`estarcdoc_id`),
  ADD UNIQUE KEY `estensioni_arcdoc_1` (`estarcdoc_cod`),
  ADD KEY `fk_estensioni_arcdoc_gestori` (`estarcdoc_ges_id`);

--
-- Indici per le tabelle `fatture`
--
ALTER TABLE `fatture`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fatture_1` (`ft_soc`,`ft_deb_id`,`ft_ced_id`,`ft_num_fat`,`ft_attribuzione`,`ft_id_data_fat`,`ft_imp_fat`),
  ADD UNIQUE KEY `fatture_2` (`ft_nomefile_csv`,`ft_id_csv`),
  ADD KEY `FK_FATDAAFAT_CALENDARIO` (`ft_id_data_fat`),
  ADD KEY `FK_FATDATASCAD_CALENDARIO` (`ft_id_data_scad`),
  ADD KEY `FK_FATTURE_CEDENTI` (`ft_ced_id`),
  ADD KEY `FK_FATTURE_DEBITORI` (`ft_deb_id`),
  ADD KEY `FK_FATTURE_STATOLAV` (`ft_id_stato_lav`),
  ADD KEY `FK_FATTURE_TIPODOC` (`ft_td_id`),
  ADD KEY `fatture_3` (`ft_id_data_chius`),
  ADD KEY `fatture_4` (`ft_data_chius`);

--
-- Indici per le tabelle `fatture_arcdoc`
--
ALTER TABLE `fatture_arcdoc`
  ADD PRIMARY KEY (`ftarcdoc_id`),
  ADD KEY `fk_fatturearcdoc_gestori` (`ftarcdoc_ges_id`);

--
-- Indici per le tabelle `fatture_datesettembre_csv`
--
ALTER TABLE `fatture_datesettembre_csv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ft_soc` (`ft_soc`,`ft_deb_id`,`ft_num_fat`,`ft_attribuzione`,`ft_id_data_fat`,`ft_imp_fat`);

--
-- Indici per le tabelle `fatture_fastweb_csv`
--
ALTER TABLE `fatture_fastweb_csv`
  ADD PRIMARY KEY (`ft_id_csv`),
  ADD KEY `fatture_fastweb_csv_1` (`ft_nomefile_csv`),
  ADD KEY `fatture_fastweb_csv_2` (`ft_soc`,`ft_deb_id`,`ft_ced_id`,`ft_num_fat`,`ft_attribuzione`,`ft_imp_fat`,`ft_id_data_fat`);

--
-- Indici per le tabelle `fatture_fastweb_notecrgest`
--
ALTER TABLE `fatture_fastweb_notecrgest`
  ADD PRIMARY KEY (`ftnote_id_nota`),
  ADD UNIQUE KEY `fatture_fastweb_notecrgest_1` (`ftnote_id_fattura`),
  ADD KEY `fk_fatturefastwebnotegrgest_gestori` (`ftnote_ges_id`);

--
-- Indici per le tabelle `fatture_fastweb_notecrgest_csv`
--
ALTER TABLE `fatture_fastweb_notecrgest_csv`
  ADD PRIMARY KEY (`ftnote_id`),
  ADD UNIQUE KEY `fatture_fastweb_notecrgest_csv_1` (`ftnote_idfatt`),
  ADD KEY `fatture_fastweb_notecrgest_csv_2` (`ftnote_societa`,`ftnote_ced_id`,`ftnote_deb_id`,`ftnote_data_fat`,`ftnote_num_fat`,`ftnote_attribuzione`,`ftnote_imp_fat`),
  ADD KEY `fk_fatturefastwebnotecrgestcsv_statolav` (`ftnote_id_stato_lav`);

--
-- Indici per le tabelle `fatture_fastweb_statilavnote_csv`
--
ALTER TABLE `fatture_fastweb_statilavnote_csv`
  ADD PRIMARY KEY (`ftstlav_id`),
  ADD UNIQUE KEY `fatture_fastweb_statilavnote_csv_1` (`ftstlav_idfatt`),
  ADD KEY `fatture_fastweb_statilavnote_csv_2` (`ftstlav_societa`,`ftstlav_ced_id`,`ftstlav_deb_id`,`ftstlav_data_fat`,`ftstlav_num_fat`,`ftstlav_attribuzione`,`ftstlav_imp_fat`),
  ADD KEY `fk_fatturefastwebstatilavnotecsv_statolav` (`ftstlav_id_stato_lav`);

--
-- Indici per le tabelle `fatture_idfatt_temp`
--
ALTER TABLE `fatture_idfatt_temp`
  ADD UNIQUE KEY `fatture_idfatt_temp_idx` (`id_fattura`);

--
-- Indici per le tabelle `fatture_mps_csv`
--
ALTER TABLE `fatture_mps_csv`
  ADD PRIMARY KEY (`ft_id_csv`);

--
-- Indici per le tabelle `fatture_notecrgest`
--
ALTER TABLE `fatture_notecrgest`
  ADD PRIMARY KEY (`ftnote_id_nota`),
  ADD KEY `fatture_notecrgest_1` (`ftnote_id_fattura`),
  ADD KEY `fk_fatturenotegrgest_gestori` (`ftnote_ges_id`);

--
-- Indici per le tabelle `fatture_stati`
--
ALTER TABLE `fatture_stati`
  ADD PRIMARY KEY (`fs_id`),
  ADD UNIQUE KEY `fatture_stati_1` (`fs_soc`,`fs_cod`);

--
-- Indici per le tabelle `fatture_statilav_note`
--
ALTER TABLE `fatture_statilav_note`
  ADD PRIMARY KEY (`ftstlav_id`),
  ADD KEY `fatture_fatture_statilav_note_1` (`ftstlav_id_fattura`),
  ADD KEY `fatture_fatture_statilav_note_2` (`ftstlav_id_fattura`,`ftstlav_id_stato_lav`),
  ADD KEY `fk_fatturestatilavnote_gestori` (`ftstlav_ges_id`),
  ADD KEY `fk_fatturestatilavnote_statolavorazione` (`ftstlav_id_stato_lav`);

--
-- Indici per le tabelle `fatture_stato_fattura`
--
ALTER TABLE `fatture_stato_fattura`
  ADD PRIMARY KEY (`fsf_id`),
  ADD KEY `fatture_stato_fattura_2` (`fsf_id_fattura`),
  ADD KEY `fatture_stato_fattura_3` (`fsf_id_stato`),
  ADD KEY `fatture_stato_fattura_1` (`fsf_id_fattura`,`fsf_id_stato`);

--
-- Indici per le tabelle `fatture_stato_fattura_fastweb_csv`
--
ALTER TABLE `fatture_stato_fattura_fastweb_csv`
  ADD PRIMARY KEY (`fsf_id`),
  ADD KEY `fatture_stato_fattura_fastweb_csv_1` (`fsf_id_fattura`,`fsf_id_stato`),
  ADD KEY `fatture_stato_fattura_fastweb_csv_2` (`fsf_id_fattura`),
  ADD KEY `fatture_stato_fattura_fastweb_csv_3` (`fsf_id_stato`),
  ADD KEY `fatture_stato_fattura_fastweb_csv_4` (`fsf_nomefile_csv`);

--
-- Indici per le tabelle `fatture_tabsr`
--
ALTER TABLE `fatture_tabsr`
  ADD PRIMARY KEY (`fttbsr_id`),
  ADD UNIQUE KEY `fatture_tabsr_idx` (`fttbsr_idfatt`,`fttbsr_idsr`),
  ADD KEY `fk_fatturetabsr_tabsr` (`fttbsr_idsr`),
  ADD KEY `fk_fatturetabsr_gestori` (`fttbsr_ges_id`);

--
-- Indici per le tabelle `filtri`
--
ALTER TABLE `filtri`
  ADD PRIMARY KEY (`id_filtro`),
  ADD KEY `fk_filtri_fosuser` (`operatore`);

--
-- Indici per le tabelle `filtri_parametri`
--
ALTER TABLE `filtri_parametri`
  ADD PRIMARY KEY (`ges_id`,`id_filtro`),
  ADD KEY `fk_filtriparametri2_gestori` (`operatore`);

--
-- Indici per le tabelle `filtro0002_eleord`
--
ALTER TABLE `filtro0002_eleord`
  ADD PRIMARY KEY (`eleord_id`);

--
-- Indici per le tabelle `filtro0003_elementi_pagina`
--
ALTER TABLE `filtro0003_elementi_pagina`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `filtro0003_pagine`
--
ALTER TABLE `filtro0003_pagine`
  ADD PRIMARY KEY (`id_pagina`);

--
-- Indici per le tabelle `fosuser_gestori`
--
ALTER TABLE `fosuser_gestori`
  ADD PRIMARY KEY (`fosuser_username`);

--
-- Indici per le tabelle `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Indici per le tabelle `gestdebitore_fastweb_csv`
--
ALTER TABLE `gestdebitore_fastweb_csv`
  ADD PRIMARY KEY (`gd_id`),
  ADD KEY `fk_gestdebitorefastwebcsv_gestori1` (`gd_ges_id`),
  ADD KEY `fk_gestdebitorefastwebcsv_debitori` (`gd_deb_id`),
  ADD KEY `fk_gestdebitorefastwebcsv_gestori2` (`gd_operatore`),
  ADD KEY `fk_gestdebitorefastwebcsv_calendario1` (`gd_id_dataassoc`),
  ADD KEY `fk_gestdebitorefastwebcsv_calendario2` (`gd_id_datadissoc`);

--
-- Indici per le tabelle `gestori`
--
ALTER TABLE `gestori`
  ADD PRIMARY KEY (`ges_id`);

--
-- Indici per le tabelle `gest_debitore`
--
ALTER TABLE `gest_debitore`
  ADD PRIMARY KEY (`gd_ges_id`,`gd_deb_id`),
  ADD KEY `FK_GESTDEBITORE_DEBITORI` (`gd_deb_id`);

--
-- Indici per le tabelle `gest_debitori_periodi`
--
ALTER TABLE `gest_debitori_periodi`
  ADD PRIMARY KEY (`gd_id`),
  ADD UNIQUE KEY `gest_debitori_periodi_1` (`gd_ges_id`,`gd_deb_id`,`gd_id_dataassoc`),
  ADD KEY `fk_gestdebitoriperiodi_debitori` (`gd_deb_id`),
  ADD KEY `fk_gestdebitoriperiodiassoc_calendario` (`gd_id_dataassoc`),
  ADD KEY `fk_gestdebitoriperiodidissoc_calendario` (`gd_id_datadissoc`);

--
-- Indici per le tabelle `pippo`
--
ALTER TABLE `pippo`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sess_id`);

--
-- Indici per le tabelle `societa`
--
ALTER TABLE `societa`
  ADD PRIMARY KEY (`soc_cod`);

--
-- Indici per le tabelle `statist_movimenti`
--
ALTER TABLE `statist_movimenti`
  ADD PRIMARY KEY (`stmov_id`),
  ADD KEY `fk_stmovimenti_fosuser` (`operatore`),
  ADD KEY `fk_stmovimenti_gestori` (`stmov_gesid`),
  ADD KEY `fk_stmovimenti_stoperazioni` (`stmov_opeid`);

--
-- Indici per le tabelle `statist_operazioni`
--
ALTER TABLE `statist_operazioni`
  ADD PRIMARY KEY (`statope_id`),
  ADD KEY `fk_stoperazioni_fosuser` (`operatore`);

--
-- Indici per le tabelle `stato_ft`
--
ALTER TABLE `stato_ft`
  ADD PRIMARY KEY (`st_id`),
  ADD UNIQUE KEY `stato_ft_1` (`st_ft_id`);

--
-- Indici per le tabelle `stato_lavorazione`
--
ALTER TABLE `stato_lavorazione`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `stato_lavorazione_1` (`sl_soc`,`sl_cod`);

--
-- Indici per le tabelle `tabsr_stati`
--
ALTER TABLE `tabsr_stati`
  ADD PRIMARY KEY (`tbsrst_id`),
  ADD UNIQUE KEY `tabsrstati_idx` (`tbsrst_soc`,`tbsrst_cod`),
  ADD KEY `fk_tabsrstati_gestori` (`tbsrst_ges_id`);

--
-- Indici per le tabelle `tab_arcdoc`
--
ALTER TABLE `tab_arcdoc`
  ADD PRIMARY KEY (`tbarcdoc_id`);

--
-- Indici per le tabelle `tab_procedure`
--
ALTER TABLE `tab_procedure`
  ADD PRIMARY KEY (`id_proc`);

--
-- Indici per le tabelle `tab_procedure_host`
--
ALTER TABLE `tab_procedure_host`
  ADD PRIMARY KEY (`id_host`);

--
-- Indici per le tabelle `tab_procedure_parametri`
--
ALTER TABLE `tab_procedure_parametri`
  ADD PRIMARY KEY (`id_proc`),
  ADD KEY `fk_tabprochost_tabhost` (`id_host`);

--
-- Indici per le tabelle `tab_sr`
--
ALTER TABLE `tab_sr`
  ADD PRIMARY KEY (`tbsr_id`),
  ADD KEY `fk_tabsr_gestori` (`tbsr_ges_id`),
  ADD KEY `fk_tabsr_tabsrstati` (`tbsr_stato`),
  ADD KEY `tab_sr_idx` (`tbsr_numero`);

--
-- Indici per le tabelle `tipi_arcdoc`
--
ALTER TABLE `tipi_arcdoc`
  ADD PRIMARY KEY (`tparcdoc_id`),
  ADD UNIQUE KEY `tipi_arcdoc_1` (`tparcdoc_soc`,`tparcdoc_cod`),
  ADD KEY `tipi_arcdoc_2` (`tparcdoc_soc`),
  ADD KEY `fk_tipiarcdoc_gestori` (`tparcdoc_ges_id`);

--
-- Indici per le tabelle `tipo_doc`
--
ALTER TABLE `tipo_doc`
  ADD PRIMARY KEY (`td_id`),
  ADD KEY `FK_TIPODOC_SOCIETA` (`td_soc`);

--
-- Indici per le tabelle `vs28ahbz_gecotestlog`
--
ALTER TABLE `vs28ahbz_gecotestlog`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT per la tabella `calendario`
--
ALTER TABLE `calendario`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43830;
--
-- AUTO_INCREMENT per la tabella `cedenti`
--
ALTER TABLE `cedenti`
  MODIFY `ced_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT per la tabella `debann_arcdoc`
--
ALTER TABLE `debann_arcdoc`
  MODIFY `debandoc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1118;
--
-- AUTO_INCREMENT per la tabella `debann_tipi_annotazione`
--
ALTER TABLE `debann_tipi_annotazione`
  MODIFY `dantpan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `debann_tipi_contatto`
--
ALTER TABLE `debann_tipi_contatto`
  MODIFY `dantpcon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT per la tabella `debitori`
--
ALTER TABLE `debitori`
  MODIFY `deb_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1746;
--
-- AUTO_INCREMENT per la tabella `debitori_annotazioni`
--
ALTER TABLE `debitori_annotazioni`
  MODIFY `debann_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5656;
--
-- AUTO_INCREMENT per la tabella `debitori_annotazioni_csv`
--
ALTER TABLE `debitori_annotazioni_csv`
  MODIFY `debann_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2070;
--
-- AUTO_INCREMENT per la tabella `estensioni_arcdoc`
--
ALTER TABLE `estensioni_arcdoc`
  MODIFY `estarcdoc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT per la tabella `fatture`
--
ALTER TABLE `fatture`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=490315;
--
-- AUTO_INCREMENT per la tabella `fatture_arcdoc`
--
ALTER TABLE `fatture_arcdoc`
  MODIFY `ftarcdoc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4719;
--
-- AUTO_INCREMENT per la tabella `fatture_fastweb_csv`
--
ALTER TABLE `fatture_fastweb_csv`
  MODIFY `ft_id_csv` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32060;
--
-- AUTO_INCREMENT per la tabella `fatture_fastweb_notecrgest`
--
ALTER TABLE `fatture_fastweb_notecrgest`
  MODIFY `ftnote_id_nota` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `fatture_fastweb_notecrgest_csv`
--
ALTER TABLE `fatture_fastweb_notecrgest_csv`
  MODIFY `ftnote_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67551;
--
-- AUTO_INCREMENT per la tabella `fatture_fastweb_statilavnote_csv`
--
ALTER TABLE `fatture_fastweb_statilavnote_csv`
  MODIFY `ftstlav_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=574;
--
-- AUTO_INCREMENT per la tabella `fatture_mps_csv`
--
ALTER TABLE `fatture_mps_csv`
  MODIFY `ft_id_csv` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9928;
--
-- AUTO_INCREMENT per la tabella `fatture_notecrgest`
--
ALTER TABLE `fatture_notecrgest`
  MODIFY `ftnote_id_nota` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `fatture_stati`
--
ALTER TABLE `fatture_stati`
  MODIFY `fs_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT per la tabella `fatture_statilav_note`
--
ALTER TABLE `fatture_statilav_note`
  MODIFY `ftstlav_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=515504;
--
-- AUTO_INCREMENT per la tabella `fatture_stato_fattura`
--
ALTER TABLE `fatture_stato_fattura`
  MODIFY `fsf_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1399303;
--
-- AUTO_INCREMENT per la tabella `fatture_stato_fattura_fastweb_csv`
--
ALTER TABLE `fatture_stato_fattura_fastweb_csv`
  MODIFY `fsf_id` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `fatture_tabsr`
--
ALTER TABLE `fatture_tabsr`
  MODIFY `fttbsr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1428;
--
-- AUTO_INCREMENT per la tabella `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT per la tabella `gestdebitore_fastweb_csv`
--
ALTER TABLE `gestdebitore_fastweb_csv`
  MODIFY `gd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1831;
--
-- AUTO_INCREMENT per la tabella `gest_debitori_periodi`
--
ALTER TABLE `gest_debitori_periodi`
  MODIFY `gd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1158;
--
-- AUTO_INCREMENT per la tabella `pippo`
--
ALTER TABLE `pippo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `statist_movimenti`
--
ALTER TABLE `statist_movimenti`
  MODIFY `stmov_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT per la tabella `stato_lavorazione`
--
ALTER TABLE `stato_lavorazione`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT per la tabella `tabsr_stati`
--
ALTER TABLE `tabsr_stati`
  MODIFY `tbsrst_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `tab_arcdoc`
--
ALTER TABLE `tab_arcdoc`
  MODIFY `tbarcdoc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1845;
--
-- AUTO_INCREMENT per la tabella `tab_procedure`
--
ALTER TABLE `tab_procedure`
  MODIFY `id_proc` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la tabella `tab_sr`
--
ALTER TABLE `tab_sr`
  MODIFY `tbsr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=338;
--
-- AUTO_INCREMENT per la tabella `tipi_arcdoc`
--
ALTER TABLE `tipi_arcdoc`
  MODIFY `tparcdoc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT per la tabella `tipo_doc`
--
ALTER TABLE `tipo_doc`
  MODIFY `td_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT per la tabella `vs28ahbz_gecotestlog`
--
ALTER TABLE `vs28ahbz_gecotestlog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1143;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `cedenti`
--
ALTER TABLE `cedenti`
  ADD CONSTRAINT `FK_CEDENTI_SOCIETA` FOREIGN KEY (`ced_soc`) REFERENCES `societa` (`soc_cod`);

--
-- Limiti per la tabella `debann_arcdoc`
--
ALTER TABLE `debann_arcdoc`
  ADD CONSTRAINT `fk_debannarcdoc_debitoriannotazioni` FOREIGN KEY (`debandoc_idann`) REFERENCES `debitori_annotazioni` (`debann_id`),
  ADD CONSTRAINT `fk_debannarcdoc_gestori` FOREIGN KEY (`debandoc_ges_id`) REFERENCES `gestori` (`ges_id`),
  ADD CONSTRAINT `fk_debannarcdoc_tabarcdoc` FOREIGN KEY (`debandoc_iddoc`) REFERENCES `tab_arcdoc` (`tbarcdoc_id`);

--
-- Limiti per la tabella `debann_tipi_annotazione`
--
ALTER TABLE `debann_tipi_annotazione`
  ADD CONSTRAINT `fk_debanntipiannotazione_gestori` FOREIGN KEY (`dantpan_ges_id`) REFERENCES `gestori` (`ges_id`),
  ADD CONSTRAINT `fk_debanntipiannotazione_societa` FOREIGN KEY (`dantpan_idsoc`) REFERENCES `societa` (`soc_cod`);

--
-- Limiti per la tabella `debann_tipi_contatto`
--
ALTER TABLE `debann_tipi_contatto`
  ADD CONSTRAINT `fk_debanntipicontatto_gestori` FOREIGN KEY (`dantpcon_ges_id`) REFERENCES `gestori` (`ges_id`),
  ADD CONSTRAINT `fk_debanntipicontatto_societa` FOREIGN KEY (`dantpcon_idsoc`) REFERENCES `societa` (`soc_cod`);

--
-- Limiti per la tabella `debitori`
--
ALTER TABLE `debitori`
  ADD CONSTRAINT `FK_debitori_SOCIETA` FOREIGN KEY (`deb_soc`) REFERENCES `societa` (`soc_cod`);

--
-- Limiti per la tabella `debitori_annotazioni`
--
ALTER TABLE `debitori_annotazioni`
  ADD CONSTRAINT `fk_debitoriannotazioni_calendario` FOREIGN KEY (`debann_iddata`) REFERENCES `calendario` (`id`),
  ADD CONSTRAINT `fk_debitoriannotazioni_debanntipiannotazione` FOREIGN KEY (`debann_idtpann`) REFERENCES `debann_tipi_annotazione` (`dantpan_id`),
  ADD CONSTRAINT `fk_debitoriannotazioni_debanntipicontatto` FOREIGN KEY (`debann_idtpcon`) REFERENCES `debann_tipi_contatto` (`dantpcon_id`),
  ADD CONSTRAINT `fk_debitoriannotazioni_debitori` FOREIGN KEY (`debann_debid`) REFERENCES `debitori` (`deb_id`);

--
-- Limiti per la tabella `estensioni_arcdoc`
--
ALTER TABLE `estensioni_arcdoc`
  ADD CONSTRAINT `fk_estensioni_arcdoc_gestori` FOREIGN KEY (`estarcdoc_ges_id`) REFERENCES `gestori` (`ges_id`);

--
-- Limiti per la tabella `fatture`
--
ALTER TABLE `fatture`
  ADD CONSTRAINT `FK_FATDAAFAT_CALENDARIO` FOREIGN KEY (`ft_id_data_fat`) REFERENCES `calendario` (`id`),
  ADD CONSTRAINT `FK_FATDATASCAD_CALENDARIO` FOREIGN KEY (`ft_id_data_scad`) REFERENCES `calendario` (`id`),
  ADD CONSTRAINT `FK_FATTURE_CEDENTI` FOREIGN KEY (`ft_ced_id`) REFERENCES `cedenti` (`ced_id`),
  ADD CONSTRAINT `FK_FATTURE_DEBITORI` FOREIGN KEY (`ft_deb_id`) REFERENCES `debitori` (`deb_id`),
  ADD CONSTRAINT `FK_FATTURE_SOCIETA` FOREIGN KEY (`ft_soc`) REFERENCES `societa` (`soc_cod`),
  ADD CONSTRAINT `FK_FATTURE_STATOLAV` FOREIGN KEY (`ft_id_stato_lav`) REFERENCES `stato_lavorazione` (`id`),
  ADD CONSTRAINT `FK_FATTURE_TIPODOC` FOREIGN KEY (`ft_td_id`) REFERENCES `tipo_doc` (`td_id`);

--
-- Limiti per la tabella `fatture_arcdoc`
--
ALTER TABLE `fatture_arcdoc`
  ADD CONSTRAINT `fk_fatturearcdoc_gestori` FOREIGN KEY (`ftarcdoc_ges_id`) REFERENCES `gestori` (`ges_id`);

--
-- Limiti per la tabella `fatture_fastweb_notecrgest`
--
ALTER TABLE `fatture_fastweb_notecrgest`
  ADD CONSTRAINT `fk_fatturefastwebnotegrgest_fatture` FOREIGN KEY (`ftnote_id_fattura`) REFERENCES `fatture` (`id`),
  ADD CONSTRAINT `fk_fatturefastwebnotegrgest_gestori` FOREIGN KEY (`ftnote_ges_id`) REFERENCES `gestori` (`ges_id`);

--
-- Limiti per la tabella `fatture_fastweb_notecrgest_csv`
--
ALTER TABLE `fatture_fastweb_notecrgest_csv`
  ADD CONSTRAINT `fk_fatturefastwebnotecrgestcsv_fatture` FOREIGN KEY (`ftnote_idfatt`) REFERENCES `fatture` (`id`),
  ADD CONSTRAINT `fk_fatturefastwebnotecrgestcsv_statolav` FOREIGN KEY (`ftnote_id_stato_lav`) REFERENCES `stato_lavorazione` (`id`);

--
-- Limiti per la tabella `fatture_fastweb_statilavnote_csv`
--
ALTER TABLE `fatture_fastweb_statilavnote_csv`
  ADD CONSTRAINT `fk_fatturefastwebstatilavnotecsv_fatture` FOREIGN KEY (`ftstlav_idfatt`) REFERENCES `fatture` (`id`),
  ADD CONSTRAINT `fk_fatturefastwebstatilavnotecsv_statolav` FOREIGN KEY (`ftstlav_id_stato_lav`) REFERENCES `stato_lavorazione` (`id`);

--
-- Limiti per la tabella `fatture_notecrgest`
--
ALTER TABLE `fatture_notecrgest`
  ADD CONSTRAINT `fk_fatturenotegrgest_fatture` FOREIGN KEY (`ftnote_id_fattura`) REFERENCES `fatture` (`id`),
  ADD CONSTRAINT `fk_fatturenotegrgest_gestori` FOREIGN KEY (`ftnote_ges_id`) REFERENCES `gestori` (`ges_id`);

--
-- Limiti per la tabella `fatture_stati`
--
ALTER TABLE `fatture_stati`
  ADD CONSTRAINT `fk_fatturestati_societa` FOREIGN KEY (`fs_soc`) REFERENCES `societa` (`soc_cod`);

--
-- Limiti per la tabella `fatture_statilav_note`
--
ALTER TABLE `fatture_statilav_note`
  ADD CONSTRAINT `fk_fatturestatilavnote_fatture` FOREIGN KEY (`ftstlav_id_fattura`) REFERENCES `fatture` (`id`),
  ADD CONSTRAINT `fk_fatturestatilavnote_gestori` FOREIGN KEY (`ftstlav_ges_id`) REFERENCES `gestori` (`ges_id`),
  ADD CONSTRAINT `fk_fatturestatilavnote_statolavorazione` FOREIGN KEY (`ftstlav_id_stato_lav`) REFERENCES `stato_lavorazione` (`id`);

--
-- Limiti per la tabella `fatture_stato_fattura`
--
ALTER TABLE `fatture_stato_fattura`
  ADD CONSTRAINT `fk_fatturestatofatture_fatture` FOREIGN KEY (`fsf_id_fattura`) REFERENCES `fatture` (`id`),
  ADD CONSTRAINT `fk_fatturestatofatture_fatturestati` FOREIGN KEY (`fsf_id_stato`) REFERENCES `fatture_stati` (`fs_id`);

--
-- Limiti per la tabella `fatture_stato_fattura_fastweb_csv`
--
ALTER TABLE `fatture_stato_fattura_fastweb_csv`
  ADD CONSTRAINT `fk_fatturestatofatturefastweb_csv_fatture` FOREIGN KEY (`fsf_id_fattura`) REFERENCES `fatture` (`id`);

--
-- Limiti per la tabella `fatture_tabsr`
--
ALTER TABLE `fatture_tabsr`
  ADD CONSTRAINT `fk_fatturetabsr_fatture` FOREIGN KEY (`fttbsr_idfatt`) REFERENCES `fatture` (`id`),
  ADD CONSTRAINT `fk_fatturetabsr_gestori` FOREIGN KEY (`fttbsr_ges_id`) REFERENCES `gestori` (`ges_id`),
  ADD CONSTRAINT `fk_fatturetabsr_tabsr` FOREIGN KEY (`fttbsr_idsr`) REFERENCES `tab_sr` (`tbsr_id`);

--
-- Limiti per la tabella `filtri`
--
ALTER TABLE `filtri`
  ADD CONSTRAINT `fk_filtri_fosuser` FOREIGN KEY (`operatore`) REFERENCES `fos_user` (`id`);

--
-- Limiti per la tabella `filtri_parametri`
--
ALTER TABLE `filtri_parametri`
  ADD CONSTRAINT `fk_filtriparametri_gestori` FOREIGN KEY (`ges_id`) REFERENCES `gestori` (`ges_id`);

--
-- Limiti per la tabella `gest_debitore`
--
ALTER TABLE `gest_debitore`
  ADD CONSTRAINT `FK_GESTDEBITORE_DEBITORI` FOREIGN KEY (`gd_deb_id`) REFERENCES `debitori` (`deb_id`),
  ADD CONSTRAINT `FK_GESTEDBITORI_GESTORI` FOREIGN KEY (`gd_ges_id`) REFERENCES `gestori` (`ges_id`);

--
-- Limiti per la tabella `gest_debitori_periodi`
--
ALTER TABLE `gest_debitori_periodi`
  ADD CONSTRAINT `fk_gestdebitoriperiodi_debitori` FOREIGN KEY (`gd_deb_id`) REFERENCES `debitori` (`deb_id`),
  ADD CONSTRAINT `fk_gestdebitoriperiodi_gestori` FOREIGN KEY (`gd_ges_id`) REFERENCES `gestori` (`ges_id`),
  ADD CONSTRAINT `fk_gestdebitoriperiodiassoc_calendario` FOREIGN KEY (`gd_id_dataassoc`) REFERENCES `calendario` (`id`),
  ADD CONSTRAINT `fk_gestdebitoriperiodidissoc_calendario` FOREIGN KEY (`gd_id_datadissoc`) REFERENCES `calendario` (`id`);

--
-- Limiti per la tabella `statist_movimenti`
--
ALTER TABLE `statist_movimenti`
  ADD CONSTRAINT `fk_stmovimenti_fosuser` FOREIGN KEY (`operatore`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `fk_stmovimenti_gestori` FOREIGN KEY (`stmov_gesid`) REFERENCES `gestori` (`ges_id`),
  ADD CONSTRAINT `fk_stmovimenti_stoperazioni` FOREIGN KEY (`stmov_opeid`) REFERENCES `statist_operazioni` (`statope_id`);

--
-- Limiti per la tabella `statist_operazioni`
--
ALTER TABLE `statist_operazioni`
  ADD CONSTRAINT `fk_stoperazioni_fosuser` FOREIGN KEY (`operatore`) REFERENCES `fos_user` (`id`);

--
-- Limiti per la tabella `stato_lavorazione`
--
ALTER TABLE `stato_lavorazione`
  ADD CONSTRAINT `FK_STATOLAVORAZIONE_SOCIETA` FOREIGN KEY (`sl_soc`) REFERENCES `societa` (`soc_cod`);

--
-- Limiti per la tabella `tabsr_stati`
--
ALTER TABLE `tabsr_stati`
  ADD CONSTRAINT `fk_tabsrstati_gestori` FOREIGN KEY (`tbsrst_ges_id`) REFERENCES `gestori` (`ges_id`),
  ADD CONSTRAINT `fk_tabsrstati_societa` FOREIGN KEY (`tbsrst_soc`) REFERENCES `societa` (`soc_cod`);

--
-- Limiti per la tabella `tab_procedure_parametri`
--
ALTER TABLE `tab_procedure_parametri`
  ADD CONSTRAINT `fk_tabprochost_tabhost` FOREIGN KEY (`id_host`) REFERENCES `tab_procedure_host` (`id_host`),
  ADD CONSTRAINT `fk_tabprocpar_tabprocedure` FOREIGN KEY (`id_proc`) REFERENCES `tab_procedure` (`id_proc`);

--
-- Limiti per la tabella `tab_sr`
--
ALTER TABLE `tab_sr`
  ADD CONSTRAINT `fk_tabsr_gestori` FOREIGN KEY (`tbsr_ges_id`) REFERENCES `gestori` (`ges_id`),
  ADD CONSTRAINT `fk_tabsr_tabsrstati` FOREIGN KEY (`tbsr_stato`) REFERENCES `tabsr_stati` (`tbsrst_id`);

--
-- Limiti per la tabella `tipi_arcdoc`
--
ALTER TABLE `tipi_arcdoc`
  ADD CONSTRAINT `fk_tipiarcdoc_gestori` FOREIGN KEY (`tparcdoc_ges_id`) REFERENCES `gestori` (`ges_id`),
  ADD CONSTRAINT `fk_tipiarcdoc_societa` FOREIGN KEY (`tparcdoc_soc`) REFERENCES `societa` (`soc_cod`);

--
-- Limiti per la tabella `tipo_doc`
--
ALTER TABLE `tipo_doc`
  ADD CONSTRAINT `FK_TIPODOC_SOCIETA` FOREIGN KEY (`td_soc`) REFERENCES `societa` (`soc_cod`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
