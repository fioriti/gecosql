DROP PROCEDURE IF EXISTS FILTRO0001_UNLOCK;
create procedure FILTRO0001_UNLOCK()
begin
update filtri_parametri set par_conn = null where id_filtro = 1;
end;

drop   procedure if exists FILTRO0001_CHECK_EXISTS; 
create procedure FILTRO0001_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 1;
if (pexists = 0)
then 
insert 
  into filtri 
select 1,'Gestione scadenzario',0,now();
end if;
end;
DROP PROCEDURE IF EXISTS FILTRO0001_RESET_ALL;
create procedure FILTRO0001_RESET_ALL()
begin
call FILTRO0001_RESET_TIPODOC();
call FILTRO0001_RESET_DEBITORE();
call FILTRO0001_RESET_STATOLAV();
call FILTRO0001_RESET_DATAFAT_DA();
call FILTRO0001_RESET_DATAFAT_A();
call FILTRO0001_RESET_DATASCAD_DA();
call FILTRO0001_RESET_DATASCAD_A();
call FILTRO0001_RESET_NUMFAT();
call FILTRO0001_RESET_CHIUSE();
call FILTRO0001_RESET_ARCDOC_SINO();
end;

DROP PROCEDURE IF EXISTS FILTRO0001_RESET_SOCIETA;
create procedure FILTRO0001_RESET_SOCIETA()
begin
  update filtri_parametri 
     set parint01  = null
   where par_conn  = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_SET_SOCIETA;
create procedure FILTRO0001_SET_SOCIETA(in psocieta int)
begin
  update filtri_parametri 
     set parint01 = psocieta
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_RESET_TIPODOC;
create procedure FILTRO0001_RESET_TIPODOC()
begin
  update filtri_parametri 
     set parint02 = null
   where par_conn = connection_id()
    and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_SET_TIPODOC;
create procedure FILTRO0001_SET_TIPODOC(in ptd_id int)
begin
  update filtri_parametri 
     set parint02 = ptd_id
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP FUNCTION IF EXISTS FILTRO0001_GET_TIPODOC;
create function FILTRO0001_GET_TIPODOC() RETURNS int
begin
  declare ptd_id int(10);
   select parint02 
     into ptd_id
     from filtri_parametri
    where par_conn = connection_id()
      and id_filtro = 1;
  return ptd_id;
end;

DROP FUNCTION IF EXISTS FILTRO0001_GET_SOCIETA;
create function FILTRO0001_GET_SOCIETA() RETURNS int
begin
  declare psoc_cod int(10);
   select parint01 
     into psoc_cod
     from filtri_parametri
   where par_conn = connection_id()
     and id_filtro = 1;
  return psoc_cod;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_RESET_DEBITORE;
create procedure FILTRO0001_RESET_DEBITORE()
begin
  update filtri_parametri 
     set parint03  = null
   where par_conn  = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_SET_DEBITORE;
create procedure FILTRO0001_SET_DEBITORE(in pdebitore int)
begin
  update filtri_parametri 
     set parint03 = pdebitore
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_RESET_STATOLAV;
create procedure FILTRO0001_RESET_STATOLAV()
begin
  update filtri_parametri 
     set parint04  = null
   where par_conn  = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_SET_STATOLAV;
create procedure FILTRO0001_SET_STATOLAV(in pstatolav int)
begin
  update filtri_parametri 
     set parint04 = pstatolav
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_RESET_DATAFAT_DA;
create procedure FILTRO0001_RESET_DATAFAT_DA()
begin
  update filtri_parametri as a
     set a.parint05 = null
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_SET_DATAFAT_DA;
create procedure FILTRO0001_SET_DATAFAT_DA(in pdatafat_da varchar(10))
begin
  update filtri_parametri as a
     set a.parint05 = (select ifnull(b.id,0) 
                         from calendario as b 
                        where b.datachar = case substring(pdatafat_da,3,1) when '/' then concat(substring(pdatafat_da,1,2),substring(pdatafat_da,4,2),substring(pdatafat_da,7,4))
                                                                                    else concat(substring(pdatafat_da,9,2),substring(pdatafat_da,6,2),substring(pdatafat_da,1,4)) end)
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_RESET_DATAFAT_A;
create procedure FILTRO0001_RESET_DATAFAT_A()
begin
  update filtri_parametri as a
     set a.parint06 = null
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_SET_DATAFAT_A;
create procedure FILTRO0001_SET_DATAFAT_A(in pdatafat_a varchar(10))
begin
  update filtri_parametri as a
     set a.parint06 = (select ifnull(b.id,0) 
                         from calendario as b 
                        where b.datachar = case substring(pdatafat_a,3,1) when '/' then concat(substring(pdatafat_a,1,2),substring(pdatafat_a,4,2),substring(pdatafat_a,7,4))
                                                                                   else concat(substring(pdatafat_a,9,2),substring(pdatafat_a,6,2),substring(pdatafat_a,1,4)) end)
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_RESET_DATASCAD_DA;
create procedure FILTRO0001_RESET_DATASCAD_DA()
begin
  update filtri_parametri as a
     set a.parint07 = null
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_SET_DATASCAD_DA;
create procedure FILTRO0001_SET_DATASCAD_DA(in pdatascad_da varchar(10))
begin
  update filtri_parametri as a
     set a.parint07 = (select ifnull(b.id,0) 
                         from calendario as b 
                        where b.datachar = case substring(pdatascad_da,3,1) when '/' then concat(substring(pdatascad_da,1,2),substring(pdatascad_da,4,2),substring(pdatascad_da,7,4))
                                                                                     else concat(substring(pdatascad_da,9,2),substring(pdatascad_da,6,2),substring(pdatascad_da,1,4)) end)
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_RESET_DATASCAD_A;
create procedure FILTRO0001_RESET_DATASCAD_A()
begin
  update filtri_parametri as a
     set a.parint08 = null
   where par_conn = connection_id()
     and id_filtro = 1;
end;

DROP PROCEDURE IF EXISTS FILTRO0001_SET_DATASCAD_A;
create procedure FILTRO0001_SET_DATASCAD_A(in pdatascad_a varchar(10))
begin
  update filtri_parametri as a
     set a.parint08 = (select ifnull(b.id,0) 
                         from calendario as b 
                        where b.datachar = case substring(pdatascad_a,3,1) when '/' then concat(substring(pdatascad_a,1,2),substring(pdatascad_a,4,2),substring(pdatascad_a,7,4))
                                                                                    else concat(substring(pdatascad_a,9,2),substring(pdatascad_a,6,2),substring(pdatascad_a,1,4)) end)
   where par_conn = connection_id()
     and id_filtro = 1;
end;
DROP PROCEDURE IF EXISTS FILTRO0001_RESET_NUMFAT;
create procedure FILTRO0001_RESET_NUMFAT()
begin
  update filtri_parametri as a
     set a.parvar09 = null
   where par_conn = connection_id()
     and id_filtro = 1;
end;
DROP PROCEDURE IF EXISTS FILTRO0001_SET_NUMFAT;
create procedure FILTRO0001_SET_NUMFAT(in pnumfat varchar(10))
begin
  update filtri_parametri as a
     set a.parvar09 = pnumfat
   where par_conn = connection_id()
     and id_filtro = 1;
end;
DROP PROCEDURE IF EXISTS FILTRO0001_RESET_CHIUSE;
create procedure FILTRO0001_RESET_CHIUSE()
begin
  update filtri_parametri as a
     set a.parint10 = 0
   where par_conn   = connection_id()
     and id_filtro  = 1;
end;
DROP PROCEDURE IF EXISTS FILTRO0001_SET_CHIUSE;
create procedure FILTRO0001_SET_CHIUSE()
begin
  update filtri_parametri as a
     set a.parint10 = 1
   where par_conn = connection_id()
     and id_filtro = 1;
end;
DROP PROCEDURE IF EXISTS FILTRO0001_RESET_ARCDOC_SINO;
create procedure FILTRO0001_RESET_ARCDOC_SINO()
begin
  update filtri_parametri as a
     set a.parint11 = 0
   where par_conn   = connection_id()
     and id_filtro  = 1;
end;
DROP PROCEDURE IF EXISTS FILTRO0001_SET_ARCDOC_SINO;
create procedure FILTRO0001_SET_ARCDOC_SINO()
begin
  update filtri_parametri as a
     set a.parint11 = 1
   where par_conn = connection_id()
     and id_filtro = 1;
end;
DROP FUNCTION IF EXISTS FILTRO0001_GET_ARCDOC_SINO;
create function FILTRO0001_GET_ARCDOC_SINO() RETURNS int
begin
  declare pfatt_arcdoc_sino int(10);
   select parint11
     into pfatt_arcdoc_sino
     from filtri_parametri
    where par_conn = connection_id()
      and id_filtro = 1;
  return pfatt_arcdoc_sino;
end;
DROP PROCEDURE IF EXISTS FILTRO0001_SET_FATTURE_STATILAVNOTE;
create procedure FILTRO0001_SET_FATTURE_STATILAVNOTE( in pid_fattura int(10),
                                                      in pid_stato_lav int(10),
                                                      in pid_nota varchar(1000) )
begin
  insert
    into fatture_statilav_note
   (
    ftstlav_id_fattura,
    ftstlav_id_stato_lav,
    ftstlav_nota_crgest,
    ftstlav_ges_id,
    ftstlav_modifica)
    select 
    pid_fattura,
    case ifnull(pid_stato_lav,0)
         when 0 then a.ftstlav_id_stato_lav
         else pid_stato_lav
    end,     
    pid_nota,
    filtro0000_get_gesid(),
    now()
 from fatture_statilavnote_v as a
where a.ftstlav_id_fattura = pid_fattura;
update fatture
   set last_ftstlav_id = last_insert_id()
 where id = pid_fattura;
end;