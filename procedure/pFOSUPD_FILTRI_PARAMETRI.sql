DROP PROCEDURE IF EXISTS FOSUPD_FILTRI_PARAMETRI;
create procedure FOSUPD_FILTRI_PARAMETRI(in pfosuser_username varchar(50), in pfiltro int)
begin
  declare pgest_id int(10);
  select fosuser_gestid
    into pgest_id
    from fosuser_gestori
   where fosuser_username = pfosuser_username;
  call upd_filtri_parametri(pgest_id,pfiltro);
end;