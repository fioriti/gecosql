drop procedure if exists updins_gestdebitore_fastweb;
create procedure updins_gestdebitore_fastweb()
begin
update gestdebitore_fastweb_csv set gd_ges_id = gd_ges_cod;
update gestdebitore_fastweb_csv set gd_deb_id = (select deb_id from debitori where deb_soc = 1 and deb_cod = gd_deb_cod);
update gestdebitore_fastweb_csv set gd_id_dataassoc = (select id from calendario where databar = gd_data_associazione) where gd_data_associazione is not null;
update gestdebitore_fastweb_csv set gd_id_datadissoc = (select id from calendario where databar = gd_data_dissociazione) where gd_data_dissociazione is not null;
end;