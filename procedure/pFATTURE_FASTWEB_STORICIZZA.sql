create table fatture_storico as select * from fatture where 1=2;
create table fatture_statofattura_storico as select * from fatture_stato_fattura where 1=2;
create table fatture_statilavnote_storico as select * from fatture_statilav_note where 1=2; 
create table fatture_tabsr_storico as select * from fatture_tabsr where 1=2; 
create table fatture_arcdoc_storico as select * from fatture_arcdoc where 1=2; 
create table fatture_idfatt_temp (id_fattura integer); 
select count(*),sum(ifnull(imp_fat,0))+23.09 from fatture_v where cod_stato_fat in (1,3);
-- 28169 52117287,06 
 
select * from  fatture_idfatt_temp;
 
truncate table fatture_idfatt_temp;
-- Inserisco gli id fattura da eliminare nella tabella fatture_idfatt_temp
insert into fatture_idfatt_temp 
select fsf_id_fattura
  from fatture_stato_fattura_v
 where fsf_ac = 1
   and fsf_nomefile_csv = '201505.csv';
commit;
-- Storicizzo fatture
insert into fatture_storico 
select * 
  from fatture
 where id in (select id_fattura
                from fatture_idfatt_temp);
commit;
-- Storicizzo fatture_stato_fattura
insert 
  into fatture_statofattura_storico 
(fsf_ges_id,
 fsf_id,
 fsf_id_fattura,
 fsf_id_stato,
 fsf_modifica,
 fsf_nomefile_csv)
select 
fsf_ges_id,
 fsf_id,
 fsf_id_fattura,
 fsf_id_stato,
 fsf_modifica,
 fsf_nomefile_csv
  from fatture_stato_fattura
 where fsf_id_fattura in (select id_fattura
                            from fatture_idfatt_temp);
commit;
-- Storicizzo fatture_statilav_note
insert into fatture_statilavnote_storico 
select *
  from fatture_statilav_note
 where ftstlav_id_fattura in (select id_fattura
                                from fatture_idfatt_temp);
                                
commit;
-- Storicizzo fatture_arcdoc
insert into fatture_arcdoc_storico
select * 
  from fatture_arcdoc
 where ftarcdoc_idfatt in (select id_fattura
                             from fatture_idfatt_temp);
commit;
-- Storicizzo fatture_tabsr
insert into fatture_tabsr_storico 
select * 
  from fatture_tabsr
 where fttbsr_idfatt in (select id_fattura
                           from fatture_idfatt_temp);
commit;
-- FASE DI ELIMINAZIONE DOPO STORICIZZAZIONE --
-- Elimino fatture_stato_fattura
delete 
  from fatture_stato_fattura
 where fsf_id_fattura in (select id_fattura
                            from fatture_idfatt_temp);
commit;
-- Elimino fatture_statilav_note
delete 
  from fatture_statilav_note
 where ftstlav_id_fattura in (select id_fattura
                                from fatture_idfatt_temp);
commit;
-- Elimino fatture_arcdoc
delete 
  from fatture_arcdoc
 where ftarcdoc_idfatt in (select id_fattura
                             from fatture_idfatt_temp);
commit;
-- Elimino fatture_tabsr
delete
  from fatture_tabsr
 where fttbsr_idfatt in (select id_fattura
                           from fatture_idfatt_temp);
commit;
-- Eliminazione fatture
delete from fatture
 where id in (select id_fattura
                from fatture_idfatt_temp);
commit;