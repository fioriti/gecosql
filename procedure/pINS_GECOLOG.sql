DROP PROCEDURE IF EXISTS INS_GECOLOG;
CREATE PROCEDURE INS_GECOLOG(in pdescrizione varchar(100))
begin
insert into gecolog(descrizione,ges_id) select pdescrizione, ifnull(filtro0000_get_gesid(),0);
end;