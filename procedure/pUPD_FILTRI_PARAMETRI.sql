DROP PROCEDURE IF EXISTS geco.UPD_FILTRI_PARAMETRI;
CREATE PROCEDURE geco.UPD_FILTRI_PARAMETRI(in pges_id int,in pid_filtro int)
begin
  declare pexist_idfiltro int(1);
  declare pexist_gesid    int(1);
  declare pexist_filpara  int(1);
  if (pid_filtro = 0)
     then call filtro0000_check_exists();
  end if;
  if (pid_filtro =  1)
     then call filtro0001_check_exists();
  end if;
  if (pid_filtro =  2)
     then call filtro0002_check_exists();
  end if;  
  if (pid_filtro =  3)
     then call filtro0003_check_exists();
  end if;  
  if (pid_filtro =  4)
     then call filtro0004_check_exists();
  end if;
  if (pid_filtro =  5)
     then call filtro0005_check_exists();
  end if;
  if (pid_filtro =  6)
     then call filtro0006_check_exists();
  end if;
  if (pid_filtro =  7)
     then call filtro0007_check_exists();
  end if;
  if (pid_filtro =  8)
     then call filtro0008_check_exists();
  end if;  
  if (pid_filtro =  9)
     then call filtro0009_check_exists();
  end if;
  if (pid_filtro = 10)
     then call filtro0010_check_exists();
  end if;
  if (pid_filtro = 11)
     then call filtro0011_check_exists();
  end if;
  select case count(*) when 0 then 0 else 1 end
    into pexist_idfiltro
    from filtri 
   where id_filtro = pid_filtro;
  select case count(*) when 0 then 0 else 1 end
    into pexist_gesid
    from gestori
   where ges_id = pges_id;
  select case count(*) when 0 then 0 else 1 end
    into pexist_filpara
    from filtri_parametri
   where id_filtro = pid_filtro
     and ges_id    = pges_id;
  if (pexist_idfiltro = 1 and
      pexist_gesid    = 1 and
      pexist_filpara  = 0)
      then 
       insert 
         into filtri_parametri (ges_id,id_filtro,par_conn,modifica)
       select pges_id,pid_filtro,null,now();
  end if;
  update filtri_parametri 
     set par_conn   = connection_id(),
         modifica   = now()
   where ges_id     = pges_id
     and id_filtro  = pid_filtro;
end;