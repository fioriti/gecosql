drop function if exists FILTRO0000_GET_GESID;
create function FILTRO0000_GET_GESID() returns int(10)
begin
declare pgesid int(10);
select ges_id
  into pgesid
  from filtro0000_parametri_v;
return pgesid;
end;
