drop   function if exists UTYPKG_DATANUM2DATABAR; 
create function UTYPKG_DATANUM2DATABAR(pdata numeric(14)) returns char(19)
begin
declare pdatabar varchar(19);
 select concat(substr(pdata,7,2),'/',substr(pdata,5,2),'/',substr(pdata,1,4),'-',substr(pdata,9,2),':',substr(pdata,11,2),':',substr(pdata,13,2)) as aaa
   into pdatabar;
return pdatabar;
end;
