drop   function if exists UTYPKG_DATANUM2DATAID; 
create function UTYPKG_DATANUM2DATAID(pdata char(8)) returns integer
begin
declare pid integer;
 select id 
   into pid
   from calendario
  where datachar = pdata;
return pid;
end;

select UTYPKG_DATANUM2DATAID('02011980');

select * from calendario limit 50;
