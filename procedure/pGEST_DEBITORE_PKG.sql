drop procedure if exists gdppkg_associa_gesdeb;
create procedure gdppkg_associa_gesdeb(in psoccod     int,
                                         in pfosuser    varchar(50),
                                         in pdebcod     varchar(20),
                                         in pdatainizio varchar(10))
gdppkg_associa_gesdeb: begin
  declare pcheckgeslog     integer;
  declare pchecksoccod     integer;
  declare pcheckgesid      integer;
  declare pcheckdebid      integer;
  declare pcheckdatainizio integer;
  declare pcheckrecdoppio  integer;
-- Verifico che l'utente abbia passato tutti i parametri alla procedure
if psoccod is null
   then select 'Parametro societa'' vuoto' as messages;
        leave gdppkg_associa_gesdeb;
end if;
if pfosuser is null
   then select 'Parametro gestore vuoto' as messages;
        leave gdppkg_associa_gesdeb;
end if;
if pdebcod is null
   then select 'Parametro debitore vuoto' as messages;
        leave gdppkg_associa_gesdeb;
end if;
if pdatainizio is null
   then select 'Parametro Data inizio vuoto' as messages;
        leave gdppkg_associa_gesdeb;
end if;

-- Verifico che l'utente sia correttamente loggato
select ifnull(filtro0000_get_gesid(),0)
  into pcheckgeslog;
if pcheckgeslog = 0
   then select 'Utente non loggato' as messages;
        leave gdppkg_associa_gesdeb;
end if;

-- Verifico esistenza di societa
select ifnull(max(soc_cod),0)
  into pchecksoccod
  from societa
 where soc_cod = psoccod;
if pchecksoccod = 0
   then select concat('Societa'' ',psoccod,' inesistente') as messages;
        leave gdppkg_associa_gesdeb;
end if;
-- Verifico esistenza di gesid
select ifnull(max(fosuser_gestid),0)
  into pcheckgesid
  from fosuser_gestori
 where fosuser_username = 'Bernabei';
if pcheckgesid = 0
   then select concat('Gestore ',pfosuser,' inesistente') as messages;
        leave gdppkg_associa_gesdeb;
end if;
-- Verifico esistenza di debid
select ifnull(max(deb_id),0)
  into pcheckdebid
  from debitori
 where deb_soc = psoccod 
   and deb_cod = pdebcod;
if pcheckdebid = 0
  then select concat('Debitore ',pdebcod,' inesistente') as messages;
       leave gdppkg_associa_gesdeb;
end if;
-- Verifico correttezza di pdatainizio
select ifnull(max(id),0)
  into pcheckdatainizio
  from calendario
 where databar = pdatainizio;
if pcheckdatainizio = 0
  then select concat('Data inizio',pdatainizio,' inesistente') as messages;
       leave gdppkg_associa_gesdeb;
end if;

-- Verifico che non ci sia gi� una riga con stesso Gestore Debitore attiva
select ifnull(max(gd_id),0)
  into pcheckrecdoppio
  from gest_debitori_periodi
 where gd_ges_id        = pcheckgesid 
   and gd_deb_id        = pcheckdebid
   and gd_id_datadissoc = 43829;
 if pcheckrecdoppio > 0
  then select concat('Esiste gia'' una riga per questo Gestore/Debitore(gd_id=',pcheckrecdoppio,')') as messages;
       leave gdppkg_associa_gesdeb;
end if;
-- Passati tutti i controlli inserisco la riga
insert 
  into gest_debitori_periodi 
  (gd_ges_id,
   gd_deb_id,
   gd_id_dataassoc,
   gd_id_datadissoc,
   gd_operatore,
   gd_modifica)
select
   pcheckgesid,
   pcheckdebid,
   pcheckdatainizio,
   43829,
   filtro0000_get_gesid(),
   now();
select 'Riga inserita correttamente';
end;

drop procedure if exists gdppkg_dissocia_gesdeb;
create procedure gdppkg_dissocia_gesdeb(in psoccod     int,
                                        in pfosuser    varchar(50),
                                        in pdebcod     varchar(20),
                                        in pdatafine varchar(10))
gdppkg_dissocia_gesdeb: begin
  declare pcheckgeslog     integer;
  declare pchecksoccod     integer;
  declare pcheckgesid      integer;
  declare pcheckdebid      integer;
  declare pcheckdatafine   integer;
    declare pcheckexistrec integer;
-- Verifico che l'utente abbia passato tutti i parametri alla procedure
if psoccod is null
   then select 'Parametro societa'' vuoto' as messages;
        leave gdppkg_dissocia_gesdeb;
end if;
if pfosuser is null
   then select 'Parametro gestore vuoto' as messages;
        leave gdppkg_dissocia_gesdeb;
end if;
if pdebcod is null
   then select 'Parametro debitore vuoto' as messages;
        leave gdppkg_dissocia_gesdeb;
end if;
if pdatafine is null
   then select 'Parametro Data fine vuoto' as messages;
        leave gdppkg_dissocia_gesdeb;
end if;

-- Verifico che l'utente sia correttamente loggato
select ifnull(filtro0000_get_gesid(),0)
  into pcheckgeslog;
if pcheckgeslog = 0
   then select 'Utente non loggato' as messages;
        leave gdppkg_dissocia_gesdeb;
end if;

-- Verifico esistenza di societa
select ifnull(max(soc_cod),0)
  into pchecksoccod
  from societa
 where soc_cod = psoccod;
if pchecksoccod = 0
   then select concat('Societa'' ',psoccod,' inesistente') as messages;
        leave gdppkg_dissocia_gesdeb;
end if;
-- Verifico esistenza di gesid
select ifnull(max(fosuser_gestid),0)
  into pcheckgesid
  from fosuser_gestori
 where fosuser_username = 'Bernabei';
if pcheckgesid = 0
   then select concat('Gestore ',pfosuser,' inesistente') as messages;
        leave gdppkg_dissocia_gesdeb;
end if;
-- Verifico esistenza di debid
select ifnull(max(deb_id),0)
  into pcheckdebid
  from debitori
 where deb_soc = psoccod 
   and deb_cod = pdebcod;
if pcheckdebid = 0
  then select concat('Debitore ',pdebcod,' inesistente') as messages;
       leave gdppkg_dissocia_gesdeb;
end if;
-- Verifico correttezza di pdatainizio
select ifnull(max(id),0)
  into pcheckdatafine
  from calendario
 where databar = pdatafine;
if pcheckdatafine = 0
  then select concat('Data inizio',pdatafine,' inesistente') as messages;
       leave gdppkg_dissocia_gesdeb;
end if;

-- Verifico che ci sia una riga con questo Gestore Debitore attiva
select ifnull(max(gd_id),0)
  into pcheckexistrec
  from gest_debitori_periodi
 where gd_ges_id        = pcheckgesid 
   and gd_deb_id        = pcheckdebid
   and gd_id_datadissoc = 43829;
 if pcheckexistrec = 0
  then select concat('Non esiste una riga per questo Gestore/Debitore(gd_id=',pcheckexistrec,')') as messages;
       leave gdppkg_dissocia_gesdeb;
end if;
-- Passati tutti i controlli inserisco la riga
insert 
  into gest_debitori_periodi 
  (gd_ges_id,
   gd_deb_id,
   gd_id_dataassoc,
   gd_id_datadissoc,
   gd_operatore,
   gd_modifica)
select
   pcheckgesid,
   pcheckdebid,
   pcheckdatainizio,
   43829,
   filtro0000_get_gesid(),
   now();
select 'Riga inserita correttamente';
end;
