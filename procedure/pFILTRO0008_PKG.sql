drop   procedure if exists FILTRO0008_CHECK_EXISTS; 
create procedure FILTRO0008_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 8;
if (pexists = 0)
then 
insert 
  into filtri 
select 8,'Gestione anagrafica debitori',0,now();
end if;
end;
drop   procedure if exists FILTRO0008_RESET_ALL; 
create procedure FILTRO0008_RESET_ALL()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 8;
end;
drop procedure if exists FILTRO0005_INS_FATARCDOC;
create procedure FILTRO0005_INS_FATARCDOC (in pidfatt int,
                                           in piddoc  int)
begin
insert into fatture_arcdoc 
(
ftarcdoc_idfatt,
ftarcdoc_iddoc,
ftarcdoc_ges_id
)
values
(
pidfatt,
piddoc,
filtro0000_get_gesid()
);
update fatture 
   set ft_arcdoc_check_sino = 1
 where id = pidfatt;
end;
drop procedure if exists FILTRO0008_RESET_DEBANNID;
create procedure FILTRO0008_RESET_DEBANNID ()
begin
update filtri_parametri
   set parint01 = null
 where ges_id     = FILTRO0000_GET_GESID()
   and id_filtro  = 8;
end;
drop procedure if exists FILTRO0008_SET_DEBANNID;
create procedure FILTRO0008_SET_DEBANNID (in pdebannid int)
begin
update filtri_parametri
   set parint01 = pdebannid
 where ges_id     = FILTRO0000_GET_GESID()
   and id_filtro  = 8;
end;

drop function if exists FILTRO0008_GET_DEBANNID;
create function FILTRO0008_GET_DEBANNID() returns int
begin
declare pdebannid int;
select par_debannid
  into pdebannid
  from filtro0008_parconn_v;
return pdebannid;
end;

drop procedure if exists FILTRO0008_INS_DEBANNARCDOC;
create procedure FILTRO0008_INS_DEBANNARCDOC (in pdebannid int,
                                              in piddoc    int)
begin
insert into debann_arcdoc
(
debandoc_idann,
debandoc_iddoc,
debandoc_ges_id
)
values
(
pdebannid,
piddoc,
filtro0000_get_gesid()
);
end;

