drop procedure if exists fatturempscsv_2_fatture;
create procedure fatturempscsv_2_fatture(in pnomefile_csv varchar(100)) 
begin
insert into fatture (
  ft_soc
  ,ft_deb_id
  ,ft_ced_id
  ,ft_num_fat
  ,ft_attribuzione
  ,ft_td
  ,ft_testo
  ,ft_data_fat
  ,ft_data_scad
  ,ft_imp_fat
  ,ft_imp_aperto
  ,ft_data_ins
  ,ft_id_stato_lav
  ,ft_td_id
  ,ft_id_data_fat
  ,ft_id_data_scad
  ,ft_nomefile_csv
  ,ft_id_csv
  ,ft_datetime_csv) 
select 
   2
  ,ft_deb_id
  ,ft_ced_id
  ,ft_num_fat
  ,ft_attribuzione
  ,ft_td
  ,null
  ,ft_data_fat
  ,ft_data_scad
  ,ft_imp_fat
  ,ft_imp_aperto
  ,now()
  ,9
  ,ft_td_id
  ,ft_id_data_fat
  ,ft_id_data_scad 
  ,ft_nomefile_csv
  ,ft_id_csv
  ,now()
 from fatture_mps_csv
where ft_nomefile_csv = pnomefile_csv and
      ft_ced_id       is not null and
      ft_deb_id       is not null and
      ft_td_id        is not null and
      ft_id_data_fat  is not null and
      ft_id_data_scad is not null
  and (2,ft_ced_id,ft_deb_id,ft_num_fat,ft_id_data_fat) not in (select ft_soc,ft_ced_id,ft_deb_id,ft_num_fat,ft_id_data_fat from fatture);
end;
