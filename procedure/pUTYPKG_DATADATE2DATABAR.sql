drop   function if exists UTYPKG_DATADATE2DATABAR; 
create function UTYPKG_DATADATE2DATABAR(pdate datetime) returns char(19)
begin
declare pdata    numeric(14);
declare pdatabar varchar(19);
select date_format(pdate,'%Y%m%d%H%i%s') 
  into pdata;
 select concat(substr(pdata,7,2),'/',substr(pdata,5,2),'/',substr(pdata,1,4),'-',substr(pdata,9,2),':',substr(pdata,11,2),':',substr(pdata,13,2)) as aaa
   into pdatabar;
return pdatabar;
end;
