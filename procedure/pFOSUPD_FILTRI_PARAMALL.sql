DROP PROCEDURE IF EXISTS FOSUPD_FILTRI_PARAMALL;
create procedure FOSUPD_FILTRI_PARAMALL(in pfosuser_username varchar(50))
begin
  declare pgest_id int(10);
  select fosuser_gestid
    into pgest_id
    from fosuser_gestori
   where fosuser_username = pfosuser_username;
  call upd_filtri_parametri(pgest_id, 0);
  call upd_filtri_parametri(pgest_id, 1);
  call upd_filtri_parametri(pgest_id, 2);
  call upd_filtri_parametri(pgest_id, 3);
  call upd_filtri_parametri(pgest_id, 4);
  call upd_filtri_parametri(pgest_id, 5);
  call upd_filtri_parametri(pgest_id, 6);
  call upd_filtri_parametri(pgest_id, 7);
  call upd_filtri_parametri(pgest_id, 8);
  call upd_filtri_parametri(pgest_id, 9);
  call upd_filtri_parametri(pgest_id,10);
  call upd_filtri_parametri(pgest_id,11);
end;