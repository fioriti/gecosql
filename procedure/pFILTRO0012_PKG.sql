drop   procedure if exists FILTRO0012_CHECK_EXISTS; 
create procedure FILTRO0012_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 12;
if (pexists = 0)
then 
insert 
  into filtri 
select 12,'Forecast',0,now();
end if;
end;
drop   procedure if exists FILTRO0012_RESET_ALL; 
create procedure FILTRO0012_RESET_ALL()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 12;
end;
drop procedure if exists FILTRO0012_UPDFORECAST;
create procedure FILTRO0012_UPDFORECAST (in pfrcst_mese numeric(2),
                                         in pfrcst_anno numeric(4),
                                         in pid_fattura integer)
begin
update fatture 
   set ft_forecast_mese = pfrcst_mese,
       ft_forecast_anno = pfrcst_anno
 where id = pid_fattura;
end;
drop procedure if exists FILTRO0012_DELFORECAST;
create procedure FILTRO0012_DELFORECAST (in pid_fattura integer)
begin
update fatture 
   set ft_forecast_mese = null,
       ft_forecast_anno = null
 where id = pid_fattura;
end;