drop procedure if exists agg_gest_debitore;
create procedure agg_gest_debitore()
begin
  delete from gest_debitore
   where gd_ges_id in (select b.ges_id from gestori as b where b.ges_ruolo = 'admin');
  insert into gest_debitore 
  select b.ges_id,
         a.deb_id,
         null,
         null
    from debitori as a
    join gestori  as b
   where a.deb_soc = 1 
     and b.ges_ruolo = 'admin'
     and b.ges_id   != 0
   group by b.ges_id,
            a.deb_id;
end;
call agg_gest_debitore();