drop   procedure if exists FILTRO0005_CHECK_EXISTS; 
create procedure FILTRO0005_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 5;
if (pexists = 0)
then 
insert 
  into filtri 
select 5,'Tabella Fatture_arcdoc',0,now();
end if;
end;
drop   procedure if exists FILTRO0005_RESET_ALL; 
create procedure FILTRO0005_RESET_ALL()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 5;
end;
drop procedure if exists FILTRO0005_INS_FATARCDOC;
create procedure FILTRO0005_INS_FATARCDOC (in pidfatt int,
                                           in piddoc  int)
begin
insert into fatture_arcdoc 
(
ftarcdoc_idfatt,
ftarcdoc_iddoc,
ftarcdoc_ges_id
)
values
(
pidfatt,
piddoc,
filtro0000_get_gesid()
);
update fatture 
   set ft_arcdoc_check_sino = 1
 where id = pidfatt;
end;

drop procedure if exists FILTRO0005_SET_IDFATTURA;
create procedure FILTRO0005_SET_IDFATTURA (in pidfatt int)
begin
update filtri_parametri
   set parint01 = pidfatt
 where ges_id     = FILTRO0000_GET_GESID()
   and id_filtro  = 5;
end;

drop function if exists FILTRO0005_GET_IDFATTURA;
create function FILTRO0005_GET_IDFATTURA() returns int
begin
declare pid_fatt int;
select par_idfatt
  into pid_fatt
  from filtro0005_parconn_v;
return pid_fatt;
end;