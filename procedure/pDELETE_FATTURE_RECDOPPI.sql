drop procedure if exists fatture_delete_recdoppi;
create procedure fatture_delete_recdoppi()
begin
delete b 
   from fatture as a,
        fatture as b
  where b.ft_soc      = a.ft_soc      and
        b.ft_deb_id   = a.ft_deb_id   and 
        b.ft_ced_id   = a.ft_ced_id   and
        b.ft_num_fat  = a.ft_num_fat  and
        b.ft_data_fat = a.ft_data_fat and 
        b.ft_imp_fat  = a.ft_imp_fat  and
        b.id          < a.id;  
end;