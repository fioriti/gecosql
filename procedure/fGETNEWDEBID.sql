drop function if exists getnewdebid;
create function getnewdebid(pdeb_soc int) returns int
begin
declare pnewdebid int(10);
select ifnull(max(deb_id),pdeb_soc*1000000000)+1
  into pnewdebid
  from debitori
 where deb_soc = pdeb_soc;
return pnewdebid;
end;
