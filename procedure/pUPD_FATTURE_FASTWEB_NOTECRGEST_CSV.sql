drop procedure if exists upd_fatture_fastweb_notecrgest_csv;
create procedure upd_fatture_fastweb_notecrgest_csv(in pnomefile_csv varchar(100)) 
begin

update fatture_fastweb_notecrgest_csv set ftnote_nomefile_csv = pnomefile_csv;

update fatture_fastweb_notecrgest_csv set ftnote_ced_id = (select min(ced_id) from cedenti where ced_soc = 1);

update fatture_fastweb_notecrgest_csv set ftnote_societa = 1;

-- Assegno l'id debitore in base a societ� e codice
update fatture_fastweb_notecrgest_csv 
   set ftnote_deb_id = (select b.deb_id from debitori as b where b.deb_soc = 1 and b.deb_cod = ftnote_deb_cod);    
   
-- Assegno l'id data fattura
update fatture_fastweb_notecrgest_csv 
   set ftnote_id_data_fat = (select b.id from calendario as b where b.databar=ftnote_data_fat);

-- Assegno l'id stato lavorazione
update fatture_fastweb_notecrgest_csv 
   set ftnote_id_stato_lav = (select b.id
                                from stato_lavorazione as b 
                               where b.sl_soc = 1 
                                 and upper(sl_descrizione) = upper(ftnote_stato_lav))
 where ftnote_nomefile_csv = pnomefile_csv;

-- Assegno l'id fattura
update fatture_fastweb_notecrgest_csv as a
   set a.ftnote_idfatt = (select b.id 
                            from fatture as b 
                           where b.ft_soc              = a.ftnote_societa 
                             and b.ft_ced_id           = a.ftnote_ced_id
                             and b.ft_deb_id           = a.ftnote_deb_id
                             and b.ft_id_data_fat      = a.ftnote_id_data_fat
                             and b.ft_num_fat          = a.ftnote_num_fat
                             and b.ft_attribuzione     = a.ftnote_attribuzione
                             and b.ft_imp_fat          = a.ftnote_imp_fat);

-- Inserisco nella tabella delle anomali le fatture doppie
 insert into fatture_fastweb_csv_anomalie
 select b.*
 from fatture_fastweb_csv  as a,
        fatture_fastweb_csv  as b
  where b.ft_soc          = a.ft_soc
    and b.ft_ced_id       = a.ft_ced_id
    and b.ft_deb_id       = a.ft_deb_id
    and b.ft_data_fat     = a.ft_data_fat
    and b.ft_num_fat      = a.ft_num_fat
    and b.ft_attribuzione = a.ft_attribuzione
    and b.ft_imp_fat      = a.ft_imp_fat
    and b.ft_id_csv       < a.ft_id_csv;
    
 delete b 
   from fatture_fastweb_csv  as a,
        fatture_fastweb_csv  as b
  where b.ft_soc          = a.ft_soc
    and b.ft_ced_id       = a.ft_ced_id
    and b.ft_deb_id       = a.ft_deb_id
    and b.ft_data_fat     = a.ft_data_fat
    and b.ft_num_fat      = a.ft_num_fat
    and b.ft_attribuzione = a.ft_attribuzione
    and b.ft_imp_fat      = a.ft_imp_fat
    and b.ft_id_csv       < a.ft_id_csv;


-- Elimino le fatture doppie
delete b 
   from fatture_fastweb_notecrgest_csv  as a,
        fatture_fastweb_notecrgest_csv  as b
  where b.ftnote_societa      = a.ftnote_societa 
    and b.ftnote_ced_id       = a.ftnote_ced_id
    and b.ftnote_deb_id       = a.ftnote_deb_id
    and b.ftnote_data_fat     = a.ftnote_data_fat
    and b.ftnote_num_fat      = a.ftnote_num_fat
    and b.ftnote_attribuzione = a.ftnote_attribuzione
    and b.ftnote_imp_fat      = a.ftnote_imp_fat
    and b.ftnote_id           < a.ftnote_id;
                             
-- Elimino le fatture senza nota - non pi�, le fatture senza nota vanno comunque trattate per lo stato lavorazione
delete from fatture_fastweb_notecrgest_csv where ftnote_nota is null;

-- Elimino le fatture senza id_fattura - no, le fatture per le quali non ho un id fattura ricondotto vanno inserite nella tabella delle anomalie
delete from fatture_fastweb_notecrgest_csv where ftnote_idfatt is null;

end;