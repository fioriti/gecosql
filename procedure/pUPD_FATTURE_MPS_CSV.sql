drop procedure if exists upd_fatture_mps_csv;
create procedure upd_fatture_mps_csv(in pnomefile_csv varchar(100)) 
begin

update fatture_mps_csv set ft_nomefile_csv = pnomefile_csv;

-- Aggiorno la tabella dei cedenti con eventuali nuovi
insert into cedenti (ced_soc,ced_cod,ced_des )
select 2,ft_ced_cod,min(ft_ced_denom) 
  from fatture_mps_csv
 where (2,ft_ced_cod) not in (select b.ced_soc,b.ced_cod from cedenti as b)
 group by ft_ced_cod;

-- Aggiorno la tabella dei debitori con eventuali nuovi
insert into debitori (deb_soc,deb_cod,deb_des)
select 2,ft_deb_cod,min(ft_deb_denom) 
  from fatture_mps_csv
 where (2,ft_deb_cod) not in (select b.deb_soc,b.deb_cod from debitori as b)
 group by ft_deb_cod;

-- Assegno l'id tipo_documento in base a societ� e codice
update fatture_mps_csv 
   set ft_td_id = (select b.td_id from tipo_doc as b where b.td_soc = 2 and b.td_cod = ft_td);

-- Assegno l'id cedente in base a societ� e codice
update fatture_mps_csv 
   set ft_ced_id = (select b.ced_id from cedenti as b where b.ced_soc = 2 and b.ced_cod = ft_ced_cod);

-- Assegno l'id debitore in base a societ� e codice
update fatture_mps_csv 
   set ft_deb_id = (select b.deb_id from debitori as b where b.deb_soc = 2 and b.deb_cod = ft_deb_cod);    
   
-- Assegno l'id data fattura
update fatture_mps_csv 
   set ft_id_data_fat = (select b.id from calendario as b where b.databar=ft_data_fat);
   
-- Assegno l'id data scadenza
update fatture_mps_csv 
   set ft_id_data_scad = (select b.id from calendario as b where b.databar=ft_data_scad);
end;
