DROP PROCEDURE IF EXISTS geco.crea_calendario;
CREATE PROCEDURE geco.crea_calendario()
begin
declare pgg         int(10);
declare pnumgg      int(10);
set pgg = 0;
set pnumgg = 36500;
truncate table calendario;
 label1: LOOP
    set pgg = pgg+1;
    insert 
      into calendario (data,gg,mese,anno,databar,datachar,datanum) 
    select date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%m'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%Y'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d/%m/%Y'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d%m%Y'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d%m%Y');
    if pgg > pnumgg
       then LEAVE label1;
    end if;
  END LOOP label1;
end;