drop   procedure if exists FILTRO0010_CHECK_EXISTS; 
create procedure FILTRO0010_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 10;
if (pexists = 0)
then 
insert 
  into filtri 
select 10,'Tabella TAB_SR',0,now();
end if;
end;
drop   procedure if exists FILTRO0010_RESET_ALL; 
create procedure FILTRO0010_RESET_ALL()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 10;
end;
drop procedure if exists FILTRO0010_INS_TABSR;
create procedure FILTRO0010_INS_TABSR (in ptbsr_numero varchar(20),
                                       in ptbsr_nota   varchar(2000))
begin
start transaction;
insert into tab_sr (
tbsr_numero,
tbsr_nota,
tbsr_stato,
tbsr_ges_id
)
values
(
 ptbsr_numero,
 ptbsr_nota,
 1,
 filtro0000_get_gesid()
);
commit;
call filtro0010_SET_LASTID();
end;

drop procedure if exists filtro0010_SET_LASTID;
create procedure filtro0010_SET_LASTID()
begin
update filtri_parametri 
   set parint01 = LAST_INSERT_ID()
 where id_filtro = 10
   and par_conn  = connection_ID();
end;

drop procedure if exists filtro0010_RESET_LASTID;
create procedure filtro0010_RESET_LASTID()
begin
update filtri_parametri 
   set parint01 = null
 where id_filtro = 10
   and par_conn  = connection_ID();
end;

drop function if exists filtro0010_GET_LASTID;
create function filtro0010_GET_LASTID() returns int
begin
declare ppar_lastid int;
select par_lastid
  into ppar_lastid
  from filtro0010_parconn_v;
return ppar_lastid;
end;