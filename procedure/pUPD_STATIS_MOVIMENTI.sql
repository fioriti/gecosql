DROP PROCEDURE IF EXISTS UPD_STATIST_MOVIMENTI;
create procedure UPD_STATIST_MOVIMENTI(in pstatope_id int)
begin
  insert 
    into statist_movimenti 
        (stmov_gesid,
         stmov_opeid,
         stmod_datetime) 
  select a.ges_id,
         pstatope_id,
         now()
    from filtro0000_parametri_v as a;
end;