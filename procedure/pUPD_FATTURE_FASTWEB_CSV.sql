drop procedure if exists upd_fatture_fastweb_csv;
create procedure upd_fatture_fastweb_csv(in pnomefile_csv varchar(100)) 
begin
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase1 Inizio'));
update fatture_fastweb_csv 
   set ft_nomefile_csv = pnomefile_csv
 where ft_nomefile_csv is null;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase1 Fine'));


call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase2 Inizio'));
update fatture_fastweb_csv 
   set ft_ced_id = (select min(ced_id) from cedenti where ced_soc = 1) 
 where ft_nomefile_csv = pnomefile_csv;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase2 Fine')); 

call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase3 Inizio'));
-- Aggiorno la tabella dei debitori con eventuali nuovi
insert into debitori (deb_soc,deb_cod,deb_des,deb_citta,deb_pro)
select 1,ft_deb_cod,min(ft_deb_denom),min(ft_deb_citta),min(ft_deb_prov) 
  from fatture_fastweb_csv
 where ft_nomefile_csv = pnomefile_csv
   and (1,ft_deb_cod) not in (select b.deb_soc,b.deb_cod from debitori as b)
 group by ft_deb_cod;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase3 Fine'));
 
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase4 Inizio'));
-- Assegno l'id tipo_documento in base a societ� e codice
update fatture_fastweb_csv 
   set ft_td_id = (select b.td_id from tipo_doc as b where b.td_soc = 1 and b.td_cod = ft_td)
 where ft_nomefile_csv = pnomefile_csv;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase4 Fine'));

call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase5 Inizio'));
-- Assegno l'id debitore in base a societ� e codice
update fatture_fastweb_csv 
   set ft_deb_id = (select b.deb_id from debitori as b where b.deb_soc = 1 and b.deb_cod = ft_deb_cod)
 where ft_nomefile_csv = pnomefile_csv;    
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase5 Fine')); 
   
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase6 Inizio'));   
-- Assegno l'id data fattura
update fatture_fastweb_csv 
   set ft_id_data_fat = (select b.id from calendario as b where b.databar=ft_data_fat)
   where ft_nomefile_csv = pnomefile_csv;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase6 Fine'));   
   
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase7 Inizio'));   
-- Assegno l'id data scadenza
update fatture_fastweb_csv 
   set ft_id_data_scad = (select b.id from calendario as b where b.databar=ft_data_scad)
 where ft_nomefile_csv = pnomefile_csv;
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase7 Fine')); 
   
call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase8 Inizio'));   
-- Assegno l'id stato lavorazione
update fatture_fastweb_csv 
   set ft_id_stato_lav = (select b.id
                            from stato_lavorazione as b 
                           where b.sl_soc = 1 
                             and upper(sl_descrizione) = upper(ft_stato_lav))
 where ft_nomefile_csv = pnomefile_csv;
 call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase8 Fine'));
 
 
 call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase9 Inizio'));
 insert into fatture_fastweb_csv_anomalie
 select b.*
 from fatture_fastweb_csv  as a,
        fatture_fastweb_csv  as b
  where b.ft_soc          = a.ft_soc
    and b.ft_ced_id       = a.ft_ced_id
    and b.ft_deb_id       = a.ft_deb_id
    and b.ft_data_fat     = a.ft_data_fat
    and b.ft_num_fat      = a.ft_num_fat
    and b.ft_attribuzione = a.ft_attribuzione
    and b.ft_imp_fat      = a.ft_imp_fat
    and b.ft_id_csv       < a.ft_id_csv;
 call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase9 Fine'));
    
 call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase10 Inizio'));   
 delete b 
   from fatture_fastweb_csv  as a,
        fatture_fastweb_csv  as b
  where b.ft_soc          = a.ft_soc
    and b.ft_ced_id       = a.ft_ced_id
    and b.ft_deb_id       = a.ft_deb_id
    and b.ft_data_fat     = a.ft_data_fat
    and b.ft_num_fat      = a.ft_num_fat
    and b.ft_attribuzione = a.ft_attribuzione
    and b.ft_imp_fat      = a.ft_imp_fat
    and b.ft_id_csv       < a.ft_id_csv;
 call INS_GECOLOG(concat('upd_fatture_fastweb_csv: ',pnomefile_csv,' -> Fase10 Fine'));       
 
end;