drop   procedure if exists FILTRO0007_CHECK_EXISTS; 
create procedure FILTRO0007_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 7;
if (pexists = 0)
then 
insert 
  into filtri 
select 7,'Gestione documenti attivita'' debitori',0,now();
end if;
end;
drop   procedure if exists FILTRO0007_RESET_ALL; 
create procedure FILTRO0007_RESET_ALL()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 7;
end;
drop   procedure if exists FILTRO0007_RESET_DEBID; 
create procedure FILTRO0007_RESET_DEBID()
begin
update filtri_parametri 
   set parint01  = null
 where par_conn  = connection_id()
   and id_filtro = 7;
end;
drop   procedure if exists FILTRO0007_SET_DEBID; 
create procedure FILTRO0007_SET_DEBID(in pdebid integer)
begin
update filtri_parametri 
   set parint01  = pdebid
 where par_conn  = connection_id()
   and id_filtro = 7;
end;
drop function if exists FILTRO0007_GET_DEBID;
create function FILTRO0007_GET_DEBID() RETURNS int
begin
  declare pdebid int(10);
   select parint01
     into pdebid
     from filtri_parametri
    where par_conn = connection_id()
      and id_filtro = 7;
  return pdebid;
end;