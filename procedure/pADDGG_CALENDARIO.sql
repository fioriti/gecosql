DROP PROCEDURE IF EXISTS addgg_calendario;
CREATE PROCEDURE addgg_calendario(in pnumgg integer)
begin
declare pgg     int(10);
declare pggfine int(10);
select ifnull(max(id),0)  
  into pgg
  from calendario;
set pggfine = pgg + pnumgg;
truncate table test;
 label1: LOOP
    set pgg = pgg+1;
    insert into test (tgg,tnumgg,tggfine) select pgg,pnumgg,pggfine;
    insert 
      into calendario (data,gg,mese,anno,databar,datachar,datanum) 
    select date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%m'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%Y'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d/%m/%Y'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d%m%Y'),
           date_format(date_add(str_to_date('01011980','%d%m%Y'),INTERVAL pgg DAY),'%d%m%Y');
    if pgg > pggfine
       then LEAVE label1;
    end if;
  END LOOP label1;
end;