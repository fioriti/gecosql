drop   procedure if exists FILTRO0002_CHECK_EXISTS; 
create procedure FILTRO0002_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 2;
if (pexists = 0)
then 
insert 
  into filtri 
select 2,'Gestione ordinamenti',0,now();
end if;
end;
drop   procedure if exists FILTRO0002_RESET_ALL; 
create procedure FILTRO0002_RESET_ALL()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 2;
end;

drop procedure if exists filtro0002_reset_liv1;
create procedure filtro0002_reset_liv1()
begin
  update filtri_parametri 
     set parint01  = null
   where par_conn  = connection_id()
     and id_filtro = 2;
end;

drop procedure if exists filtro0002_set_liv1;
create procedure filtro0002_set_liv1 (in pliv1 int)
begin
  update filtri_parametri 
     set parint1  = pliv1
   where par_conn  = connection_id()
     and id_filtro = 2;
end;

drop procedure if exists filtro0002_reset_liv2;
create procedure filtro0002_reset_liv2()
begin
  update filtri_parametri 
     set parint02  = null
   where par_conn  = connection_id()
     and id_filtro = 2;
end;

drop procedure if exists filtro0002_set_liv2;
create procedure filtro0002_set_liv2 (in pliv2 int)
begin
  update filtri_parametri 
     set parint02  = pliv2
   where par_conn  = connection_id()
     and id_filtro = 2;
end;  

drop procedure if exists filtro0002_reset_liv3;
create procedure filtro0002_reset_liv3()
begin
  update filtri_parametri 
     set parint03  = null
   where par_conn  = connection_id()
     and id_filtro = 2;
end;

drop procedure if exists filtro0002_set_liv3;
create procedure filtro0002_set_liv3 (in pliv3 int)
begin
  update filtri_parametri 
     set parint03  = pliv3
   where par_conn  = connection_id()
     and id_filtro = 2;
end;  

drop procedure if exists filtro0002_reset_liv4;
create procedure filtro0002_reset_liv4()
begin
  update filtri_parametri 
     set parint04  = null
   where par_conn  = connection_id()
     and id_filtro = 2;
end;

drop procedure if exists filtro0002_set_liv4;
create procedure filtro0002_set_liv4 (in pliv4 int)
begin
  update filtri_parametri 
     set parint04  = pliv4
   where par_conn  = connection_id()
     and id_filtro = 2;
end;  