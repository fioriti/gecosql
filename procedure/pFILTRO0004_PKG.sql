drop   procedure if exists FILTRO0004_CHECK_EXISTS; 
create procedure FILTRO0004_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 4;
if (pexists = 0)
then 
insert 
  into filtri 
select 4,'Tabella Tab_arcdoc',0,now();
end if;
end;
drop   procedure if exists FILTRO0004_RESET_ALL; 
create procedure FILTRO0004_RESET_ALL()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 4;
end;
drop procedure if exists FILTRO0004_INS_TABARCDOCID;
create procedure FILTRO0004_INS_TABARCDOCID (in pestcod   varchar(5),
                                             in ptipo     int,
                                             in psize     numeric,
                                             in pnomefile varchar(50),
                                             in pdesc     varchar(50))
begin
start transaction;
insert into tab_arcdoc (
tbarcdoc_soc,
tbarcdoc_est,
tbarcdoc_tipo,
tbarcdoc_size,
tbarcdoc_nomefile,
tbarcdoc_desc,
tbarcdoc_blob,
tbarcdoc_ges_id
)
values
(
filtro0001_get_societa(),
ifnull(filtro0004_find_idest(pestcod),0),
ptipo,
psize,
pnomefile,
pdesc,
null,
filtro0000_get_gesid()
);
commit;
call filtro0004_SET_LASTID();
end;

drop procedure if exists filtro0004_SET_LASTID;
create procedure filtro0004_SET_LASTID()
begin
update filtri_parametri 
   set parint01 = LAST_INSERT_ID()
 where id_filtro = 4
   and par_conn  = connection_ID();
end;

drop procedure if exists filtro0004_RESET_LASTID;
create procedure filtro0004_RESET_LASTID()
begin
update filtri_parametri 
   set parint01 = null
 where id_filtro = 4
   and par_conn  = connection_ID();
end;

drop function if exists filtro0004_GET_LASTID;
create function filtro0004_GET_LASTID() returns int
begin
declare ppar_lastid int;
select par_lastid
  into ppar_lastid
  from filtro0004_parconn_v;
return ppar_lastid;
end;

drop function if exists filtro0004_find_idest;
create function filtro0004_find_idest(pparest varchar(5)) returns int
begin
declare ppar_estid int;
select estarcdoc_id
  into ppar_estid
  from estensioni_arcdoc
 where lower(estarcdoc_cod) = lower(ifnull(pparest,'err'));
return ppar_estid;
end;

drop procedure if exists filtro0004_UPD_BLOB;
create procedure filtro0004_UPD_BLOB(in pblob MEDIUMBLOB)
begin
update tab_arcdoc
   set tbarcdoc_blob = pblob
 where tbarcdoc_id = filtro0004_GET_LASTID();
end;
