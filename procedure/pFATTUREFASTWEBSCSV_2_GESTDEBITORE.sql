drop procedure if exists fatturefastwebcsv_2_gestdebitore;
create procedure fatturefastwebcsv_2_gestdebitore(in pnomefile_csv varchar(100)) 
begin
insert into gest_debitore (
  gd_ges_id,
  gd_deb_id)
select 
  ft_zona,
  ft_deb_id
 from fatture_fastweb_csv
where ft_nomefile_csv = pnomefile_csv and
      ft_ced_id       is not null and
      ft_deb_id       is not null and
      ft_td_id        is not null and
      ft_id_data_fat  is not null and
      ft_id_data_scad is not null
  and (ft_zona,ft_deb_id) not in (select gd_ges_id,gd_deb_id from gest_debitore)
  group by ft_zona,ft_deb_id;
end;