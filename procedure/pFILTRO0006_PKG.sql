drop   procedure if exists FILTRO0006_CHECK_EXISTS; 
create procedure FILTRO0006_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 6;
if (pexists = 0)
then 
insert 
  into filtri 
select 6,'Gestione attivita'' debitori',0,now();
end if;
end;
drop   procedure if exists FILTRO0006_RESET_ALL; 
create procedure FILTRO0006_RESET_ALL()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 6;
end;
drop   procedure if exists FILTRO0006_RESET_DEBID; 
create procedure FILTRO0006_RESET_DEBID()
begin
update filtri_parametri 
   set parint01  = null
 where par_conn  = connection_id()
   and id_filtro = 6;
end;
drop   procedure if exists FILTRO0006_SET_DEBID; 
create procedure FILTRO0006_SET_DEBID(in pdebid integer)
begin
update filtri_parametri 
   set parint01  = pdebid
 where par_conn  = connection_id()
   and id_filtro = 6;
end;
drop function if exists FILTRO0006_GET_DEBID;
create function FILTRO0006_GET_DEBID() RETURNS int
begin
  declare pdebid int(10);
   select parint01
     into pdebid
     from filtri_parametri
    where par_conn = connection_id()
      and id_filtro = 6;
  return pdebid;
end;
drop   procedure if exists FILTRO0006_INS_ANNOTAZIONE; 
create procedure FILTRO0006_INS_ANNOTAZIONE(in ptpann integer,
                                            in ptpcon integer,
                                            in pdata  char(10),
                                            in pnota  TEXT,
                                            in pinter TEXT)
begin
start transaction;
insert into debitori_annotazioni (
debann_debid, 
debann_idtpann, 
debann_idtpcon, 
debann_iddata, 
debann_annot, 
debann_interlocutore, 
debann_ges_id
)
select 
filtro0006_get_debid(),
ptpann,
ptpcon,
id,
pnota,
pinter,
filtro0000_get_gesid()
 from calendario as a
where a.databar = pdata ;
commit;
call filtro0006_SET_LASTID();
end;

drop procedure if exists filtro0006_SET_LASTID;
create procedure filtro0006_SET_LASTID()
begin
update filtri_parametri 
   set parint02 = LAST_INSERT_ID()
 where id_filtro = 6
   and par_conn  = connection_ID();
end;

drop procedure if exists filtro0006_RESET_LASTID;
create procedure filtro0006_RESET_LASTID()
begin
update filtri_parametri 
   set parint02 = null
 where id_filtro = 6
   and par_conn  = connection_ID();
end;

drop function if exists filtro0006_GET_LASTID;
create function filtro0006_GET_LASTID() returns int
begin
declare ppar_lastid int;
select par_lastid
  into ppar_lastid
  from filtro0006_parconn_v;
return ppar_lastid;
end;
