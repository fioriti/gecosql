DROP PROCEDURE IF EXISTS fatturefastwebcsv_2_fatture;
CREATE PROCEDURE fatturefastwebcsv_2_fatture(in pnomefile_csv varchar(100))
begin
-- ********   RIAPERTURA DELLE FATTURE INIZIO                    **********
-- A) Imposto a Riaperto le fatture chiuse che sono da riaprire, a prescindere dal valore che hanno attualmente in fatture_stato_fatture. Uso la stessa select della fase B
 call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseA Inizio'));   
insert into fatture_stato_fattura 
      (fsf_id_fattura, 
       fsf_id_stato, 
       fsf_ges_id, 
       fsf_modifica,
       fsf_nomefile_csv)
select a.id,       
       3,  
       ifnull(FILTRO0000_GET_GESID(),0),
       now(),
       pnomefile_csv
  from fatture as a
  join fatture_stato_fattura_v as b on (b.fsf_id_fattura = a.id)
 where (b.fsf_cod = 2 or b.fsf_cod = 4)
    and (a.ft_soc,a.ft_deb_id,a.ft_ced_id,a.ft_num_fat,a.ft_attribuzione,a.ft_id_data_fat,a.ft_imp_fat) in
 (select b.ft_soc,b.ft_deb_id,b.ft_ced_id,b.ft_num_fat,b.ft_attribuzione,b.ft_id_data_fat,b.ft_imp_fat
   from fatture_fastweb_csv as b
  where ft_nomefile_csv = pnomefile_csv);
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseA Fine'));   

-- B) Riapro i record che ci sono sul file fatture che hanno la data di chiusura e sono presenti sul nuovo file
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseB Inizio'));   
update fatture as a
   set a.ft_id_data_chius = null,
       a.ft_data_chius    = null
 where a.ft_soc = 1
   and a.ft_id_data_chius is not null 
   and (a.ft_soc,a.ft_deb_id,a.ft_ced_id,a.ft_num_fat,a.ft_attribuzione,a.ft_id_data_fat,a.ft_imp_fat) in
   (select b.ft_soc,b.ft_deb_id,b.ft_ced_id,b.ft_num_fat,b.ft_attribuzione,b.ft_id_data_fat,b.ft_imp_fat
      from fatture_fastweb_csv as b
     where ft_nomefile_csv = pnomefile_csv);
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseB Fine'));   
-- ********   RIAPERTURA DELLE FATTURE FINE                      **********

-- ********   CHIUSURA/RICHIUSURA DELLE FATTURE APERTE INIZIO    **********
-- C) Imposto a Chiusa/Richiusa in base al valore in fatture_stato_fattura. Uso la stessa select della fase D
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseC Inizio'));   
insert into fatture_stato_fattura 
      (fsf_id_fattura, 
       fsf_id_stato, 
       fsf_ges_id, 
       fsf_modifica,
       fsf_nomefile_csv)
select a.id,       
       case ifnull(b.fsf_id_stato,1) when 1 then 2  
                                     when 3 then 4
                                     else 2
       end, 
       ifnull(FILTRO0000_GET_GESID(),0),
       now(),
       pnomefile_csv
  from fatture as a
  left outer join fatture_stato_fattura_v as b on (b.fsf_id_fattura =a.id)
where a.ft_soc = 1
   and ifnull(b.fsf_id_stato,1) in (1,3)
   -- OldVersion a.ft_id_data_chius is null 
   and not exists (select null
                     from fatture_fastweb_csv as b
                    where b.ft_soc          = a.ft_soc
                      and b.ft_deb_id       = a.ft_deb_id
                      and b.ft_ced_id       = a.ft_ced_id
                      and b.ft_num_fat      = a.ft_num_fat
                      and b.ft_attribuzione = a.ft_attribuzione
                      and b.ft_id_data_fat  = a.ft_id_data_fat
                      and b.ft_imp_fat      = a.ft_imp_fat
                      and b.ft_nomefile_csv = pnomefile_csv);
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseC Fine'));                         
                      
-- D) Chiudo i record che non sono presenti sul nuovo file 
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseD Inizio'));                         
update fatture as a
   set a.ft_data_chius = now()
 where a.ft_soc = 1
   and a.ft_id_data_chius is null 
   and not exists (select null
                     from fatture_fastweb_csv as b
                    where b.ft_soc          = a.ft_soc
                      and b.ft_deb_id       = a.ft_deb_id
                      and b.ft_ced_id       = a.ft_ced_id
                      and b.ft_num_fat      = a.ft_num_fat
                      and b.ft_attribuzione = a.ft_attribuzione
                      and b.ft_id_data_fat  = a.ft_id_data_fat
                      and b.ft_imp_fat      = a.ft_imp_fat
                      and b.ft_nomefile_csv = pnomefile_csv);
update fatture as a
   set a.ft_id_data_chius = (select b.id from calendario as b where b.databar=date_format(a.ft_data_chius,'%d/%m/%Y'))
   where a.ft_data_chius is not null;
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseD Fine'));                            
-- ********   CHIUSURA/RICHIUSURA DELLE FATTURE APERTE FINE      **********

-- ********   APERTURA DELLE FATTURE NUOVE INIZIO                **********                     
-- E) Carico i record del nuovo file che non sono presenti in fatture
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseE Inizio'));                         
insert into fatture (
  ft_soc
  ,ft_deb_id
  ,ft_ced_id
  ,ft_num_fat
  ,ft_attribuzione
  ,ft_td
  ,ft_testo
  ,ft_data_fat
  ,ft_data_scad
  ,ft_imp_fat
  ,ft_data_ins
  ,ft_id_stato_lav
  ,ft_td_id
  ,ft_id_data_fat
  ,ft_id_data_scad
  ,ft_nomefile_csv
  ,ft_id_csv
  ,ft_datetime_csv) 
select 
   a.ft_soc
  ,a.ft_deb_id
  ,a.ft_ced_id
  ,a.ft_num_fat
  ,a.ft_attribuzione
  ,a.ft_td
  ,a.ft_testo
  ,a.ft_data_fat
  ,a.ft_data_scad
  ,a.ft_imp_fat
  ,now()
  ,a.ft_id_stato_lav
  ,a.ft_td_id
  ,a.ft_id_data_fat
  ,a.ft_id_data_scad 
  ,a.ft_nomefile_csv
  ,a.ft_id_csv
  ,now()
 from fatture_fastweb_csv as a
where a.ft_nomefile_csv = pnomefile_csv
  and a.ft_ced_id       is not null
  and a.ft_deb_id       is not null
  and a.ft_td_id        is not null
  and a.ft_id_data_fat  is not null
  and a.ft_id_data_scad is not null
  and a.ft_id_stato_lav is not null
  and not exists (select null
                    from fatture as b
                   where b.ft_soc          = a.ft_soc
                     and b.ft_deb_id       = a.ft_deb_id
                     and b.ft_ced_id       = a.ft_ced_id
                     and b.ft_num_fat      = a.ft_num_fat
                     and b.ft_attribuzione = a.ft_attribuzione
                     and b.ft_id_data_fat  = a.ft_id_data_fat
                     and b.ft_imp_fat      = a.ft_imp_fat)
  and not exists (select null
                    from (select ft_soc,ft_deb_id,ft_ced_id,ft_num_fat,ft_attribuzione,ft_id_data_fat,ft_imp_fat
                            from fatture_fastweb_csv
                           where ft_nomefile_csv = pnomefile_csv
                           group by ft_soc,ft_deb_id,ft_ced_id,ft_num_fat,ft_attribuzione,ft_id_data_fat,ft_imp_fat
                           having count(1) > 1) as c
                   where c.ft_soc          = a.ft_soc
                     and c.ft_deb_id       = a.ft_deb_id
                     and c.ft_ced_id       = a.ft_ced_id
                     and c.ft_num_fat      = a.ft_num_fat
                     and c.ft_attribuzione = a.ft_attribuzione
                     and c.ft_id_data_fat  = a.ft_id_data_fat
                     and c.ft_imp_fat      = a.ft_imp_fat);
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseE Fine'));                                              
                     
-- F) Imposto con Stato Aperta tutte le fatture inserite.                     
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseF Inizio'));                                              
insert into fatture_stato_fattura 
      (fsf_id_fattura, 
       fsf_id_stato, 
       fsf_ges_id, 
       fsf_modifica,
       fsf_nomefile_csv)
select a.id,
        1,  
        ifnull(FILTRO0000_GET_GESID(),0),
        now(),
        b.ft_nomefile_csv
   from fatture as a
   join fatture_fastweb_csv as b on (    b.ft_soc          = a.ft_soc
                                     and b.ft_deb_id       = a.ft_deb_id
                                     and b.ft_ced_id       = a.ft_ced_id
                                     and b.ft_num_fat      = a.ft_num_fat
                                     and b.ft_attribuzione = a.ft_attribuzione
                                     and b.ft_id_data_fat  = a.ft_id_data_fat
                                     and b.ft_imp_fat      = a.ft_imp_fat
                                     and b.ft_nomefile_csv = pnomefile_csv)
  where a.id not in (select fsf_id_fattura
                       from fatture_stato_fattura);              
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseF Fine'));                                                  
-- ********   APERTURA DELLE FATTURE NUOVE FINE                  **********
-- ********   IMPOSTAZIONE DELLO STATO LAVORAZIONE A 20 PER LE FATTURE  NUOVE INIZIO **********
call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseG Inizio'));                                                  
insert into fatture_statilav_note
(ftstlav_id_fattura, 
 ftstlav_id_stato_lav, 
 ftstlav_nota_crgest, 
 ftstlav_ges_id, 
 ftstlav_id_nota_csv, 
 ftstlav_nomefile_csv, 
 ftstlav_modifica)             
select a.id,
       20,
       null,
       ifnull(FILTRO0000_GET_GESID(),0),
       b.ft_id_csv,
       b.ft_nomefile_csv,
       now()
  from fatture             as a 
  join fatture_fastweb_csv as b on (    b.ft_soc          = a.ft_soc
                                    and b.ft_deb_id       = a.ft_deb_id
                                    and b.ft_ced_id       = a.ft_ced_id
                                    and b.ft_num_fat      = a.ft_num_fat
                                    and b.ft_attribuzione = a.ft_attribuzione
                                    and b.ft_id_data_fat  = a.ft_id_data_fat
                                    and b.ft_imp_fat      = a.ft_imp_fat
                                    and b.ft_nomefile_csv = pnomefile_csv)
  where a.id not in (select ftstlav_id_fattura
                       from fatture_statilav_note);


call INS_GECOLOG(concat('fatturefastwebcsv_2_fatture: ',pnomefile_csv,' -> FaseG Fine'));                                                  
-- ********   IMPOSTAZIONE DELLO STATO LAVORAZIONE A 20 PER LE FATTURE  NUOVE FINE   **********


end;

