drop   procedure if exists FILTRO0003_CHECK_EXISTS; 
create procedure FILTRO0003_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 3;
if (pexists = 0)
then 
insert 
  into filtri 
select 3,'Paginazione',0,now();
end if;
end;
drop  procedure if exists FILTRO0003_RESET_ALL; 
create procedure FILTRO0003_RESET_ALL()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 3;
end;
drop  procedure if exists FILTRO0003_CHECK_DEFAULT; 
create procedure FILTRO0003_CHECK_DEFAULT()
begin
declare pnumpagesp   int(10);
declare pnumpagcorr  int(10);
declare pnumposcorr  int(10);
declare pidrecperpag int(10);
select ifnull(numpagesp,0), ifnull(numpagcorr,0), ifnull(numposcorr,0), ifnull(idrecperpag,0)
  into pnumpagesp,          pnumpagcorr,          pnumposcorr,          pidrecperpag
  from filtro0003_parconn_v;
if (pnumpagesp    = 0 or
    pnumpagcorr   = 0 or
    pnumposcorr   = 0 or
    pidrecperpag  = 0)
    then select numpagesp,  numpagcorr,  numposcorr,  idrecperpag
           into pnumpagesp, pnumpagcorr, pnumposcorr, pidrecperpag
           from filtro0003_valori_default;
         update filtri_parametri 
            set parint05 = pnumpagesp,
                parint04 = pnumpagcorr,
                parint08 = pnumposcorr,
                parint09 = pidrecperpag
         where par_conn  = connection_id()
           and id_filtro = 3;               
end if;
end;
drop  procedure if exists FILTRO0003_SET_IDRECPERPAG; 
create procedure FILTRO0003_SET_IDRECPERPAG(in pidrecperpag int)
begin
update filtri_parametri 
   set parint09  = pidrecperpag
 where par_conn  = connection_id()
   and id_filtro = 3;
end;
drop view if exists FILTRO0003_GET_IDRECPERPAG_V ;
create view FILTRO0003_GET_IDRECPERPAG_V as
select idrecperpag
  from filtro0003_parconn_v;
  
drop view if exists FILTRO0003_GET_VALRECPERPAG_V ;
create view FILTRO0003_GET_VALRECPERPAG_V as
select valrecperpag
  from filtro0003_parconn_v;

drop  procedure if exists FILTRO0003_SET_NUMRECTOT; 
create procedure FILTRO0003_SET_NUMRECTOT()
begin
declare pnumrectot int(10);
-- select count(1) 
--  into pnumrectot
--  from gestione_scadenziario_v;
select count(1)
  into pnumrectot
  from filtro0001_parametri_v   as a 
  join gest_debitore            as b   on (b.gd_ges_id    = a.ges_id)
  join debitori                 as d   on (d.deb_id       = b.gd_deb_id                     and
                                           d.deb_id       = ifnull(a.parint03,d.deb_id))
  join stato_lavorazione        as sl  on (sl.id          = ifnull(a.parint04,sl.id))
  join fatture                  as c   on (c.ft_deb_id        = d.deb_id                    and 
                                           c.ft_soc           = a.parint01                  and 
                                           c.ft_td_id         = ifnull(a.parint02,ft_td_id) and
                                           c.ft_id_data_fat   between ifnull(a.parint05,0) and ifnull(a.parint06,9999999) and
                                           c.ft_id_data_scad  between ifnull(a.parint07,0) and ifnull(a.parint08,9999999) and
                                           c.ft_num_fat       = ifnull(parvar09,c.ft_num_fat) and
                                           ifnull(c.ft_arcdoc_check_sino,0) = case ifnull(parint11,0)
                                                                                when 0 then ifnull(c.ft_arcdoc_check_sino,0)
                                                                                else        ifnull(parint11,0)
                                                                              end)
  join fatture_stato_fattura_v  as fsf  on (fsf.fsf_id_fattura = c.id and 
                                            fsf.fsf_ac = case ifnull(parint10,0) when 0 then 0 else fsf.fsf_ac end)
  join fatture_statilavnote_v   as fsln on (ftstlav_id_fattura   = c.id and 
                                            ftstlav_id_stato_lav = sl.id);
update filtri_parametri 
   set parint02 = pnumrectot
 where par_conn = connection_id()
   and id_filtro = 3;
end;
drop view if exists FILTRO0003_GET_NUMRECTOT_V ;
create view FILTRO0003_GET_NUMRECTOT_V as
select numrectot 
  from filtro0003_parconn_v;  
drop  procedure if exists FILTRO0003_SET_NUMPAGTOT; 
create procedure FILTRO0003_SET_NUMPAGTOT()
begin
update filtri_parametri as a
  join filtro0003_parconn_v as b
   set a.parint03 = case truncate((a.parint02/b.valrecperpag)+0.999999,0) when 0 then 1 else truncate((a.parint02/b.valrecperpag)+0.999999,0) end
 where a.par_conn = connection_id()
   and a.id_filtro = 3;
end;
drop view if exists FILTRO0003_GET_NUMPAGTOT_V ;
create view FILTRO0003_GET_NUMPAGTOT_V as
select numpagtot 
  from filtro0003_parconn_v;
drop  procedure if exists FILTRO0003_SET_NUMPAGCORR; 
create procedure FILTRO0003_SET_NUMPAGCORR(in pidpagcorr int)
begin
declare poldpos int(10);
declare poldpag int(10);
declare pnewpos int(10);
declare ppagtot int(10);
declare ppagesp int(10);
select a.numpagcorr,a.numposcorr, a.numpagtot, a.numpagesp
  into poldpag,     poldpos,      ppagtot, ppagesp
  from filtro0003_parconn_v as a;
set pnewpos = poldpos;
if (pidpagcorr > poldpag) then set pnewpos = poldpos + (pidpagcorr-poldpag); end if;
if (pidpagcorr < poldpag) then set pnewpos = poldpos - (poldpag-pidpagcorr); end if;
if (pidpagcorr = ppagtot) then set pnewpos = ppagesp; end if;
if (pidpagcorr = 1)       then set pnewpos = 1; end if;
update filtri_parametri 
   set parint08 = pnewpos,
       parint04 = pidpagcorr                
 where par_conn = connection_id()
   and id_filtro = 3;
end;

drop view if exists FILTRO0003_GET_NUMPAGCORR_V ;
create view FILTRO0003_GET_NUMPAGCORR_V as
select numpagcorr
  from filtro0003_parconn_v;
drop  procedure if exists FILTRO0003_SET_NUMPOSCORR; 
create procedure FILTRO0003_SET_NUMPOSCORR(in pnumposcorr int)
begin
update filtri_parametri 
   set parint08 = pnumposcorr
 where par_conn = connection_id()
   and id_filtro = 3;
end;
drop view if exists FILTRO0003_GET_NUMPOSCORR_V ;
create view FILTRO0003_GET_NUMPOSCORR_V as
select numposcorr
  from filtro0003_parconn_v;  
drop  procedure if exists FILTRO0003_SET_NUMPAGSUCC; 
create procedure FILTRO0003_SET_NUMPAGSUCC()
begin
update filtri_parametri 
   set parint04 = case ifnull(parint04,1) when parint03 then parint03 else ifnull(parint04,1)+1 end,
       parint08 = case ifnull(parint08,1) when parint05 then parint05 else ifnull(parint08,1)+1 end
 where par_conn = connection_id()
   and id_filtro = 3;
end;
drop  procedure if exists FILTRO0003_SET_NUMPAGPREC; 
create procedure FILTRO0003_SET_NUMPAGPREC()
begin
update filtri_parametri 
   set parint04 = case ifnull(parint04,1) when 1 then 1 else ifnull(parint04,1)-1 end,
       parint08 = case ifnull(parint08,1) when 1 then 1 else ifnull(parint08,1)-1 end
 where par_conn = connection_id()
   and id_filtro = 3;
end;
drop  procedure if exists FILTRO0003_SET_NUMPAGPRI; 
create procedure FILTRO0003_SET_NUMPAGPRI()
begin
update filtri_parametri 
   set parint04 = 1,
       parint08 = 1
 where par_conn = connection_id()
   and id_filtro = 3;
end;
drop  procedure if exists FILTRO0003_SET_NUMPAGULT; 
create procedure FILTRO0003_SET_NUMPAGULT()
begin
declare pultpag int(10);
declare pnumpagesp int(10);
select numpagtot 
  into pultpag
  from filtro0003_get_numpagtot_v;
select numpagesp 
  into pnumpagesp
  from filtro0003_get_numpagesp_v;  
update filtri_parametri 
   set parint04 = pultpag,
       parint08 = pnumpagesp
 where par_conn = connection_id()
   and id_filtro = 3;
end;
drop  procedure if exists FILTRO0003_SET_NUMPAGESP; 
create procedure FILTRO0003_SET_NUMPAGESP(in pnumpagesp int)
begin
update filtri_parametri 
   set parint05 = pnumpagesp
 where par_conn = connection_id()
   and id_filtro = 3;
end;
drop  procedure if exists FILTRO0003_SET_LIMINI; 
create procedure FILTRO0003_SET_LIMINI()
begin
declare pvalrecperpag int(10);
select valrecperpag
  into pvalrecperpag
  from filtro0003_parconn_v;
update filtri_parametri
   set parint06 = ((ifnull(parint04,1)-1)*pvalrecperpag)
 where par_conn = connection_id()
   and id_filtro = 3;
end;
drop  procedure if exists FILTRO0003_SET_LIMFIN; 
create procedure FILTRO0003_SET_LIMFIN()
begin
update filtri_parametri 
   set parint07 = (ifnull(parint04,1)*ifnull(parint01,25))
 where par_conn = connection_id()
   and id_filtro = 3;
end;
drop view if exists FILTRO0003_GET_LIMINI_V ;
create view FILTRO0003_GET_LIMINI_V as
select limini 
  from filtro0003_parconn_v;
drop view if exists FILTRO0003_GET_LIMFIN_V ;
create view FILTRO0003_GET_LIMFIN_V as
select numpagesp
  from filtro0003_parconn_v;

drop view if exists FILTRO0003_GET_NUMPAGESP_V ;
create view FILTRO0003_GET_NUMPAGESP_V as
select numpagesp
  from filtro0003_parconn_v;
drop view if exists filtro0003_range_pagine_v;
create view filtro0003_range_pagine_v as 
select b.id_pagina               as valore,
       a.numpagcorr              as numpagcorr,
       a.numposcorr              as numposcorr,
       a.numposcorr-1            as sinistra,
       a.numpagesp-a.numposcorr  as destra,
       a.numpagcorr-(a.numposcorr-1)          as valsin,
       a.numpagcorr+(a.numpagesp-a.numposcorr) as valdes,
       a.numpagtot,a.numpagesp
  from filtro0003_parconn_v as a
  join filtro0003_pagine    as b on (b.id_pagina between a.numpagcorr-(a.numposcorr-1) and a.numpagcorr+(a.numpagesp-a.numposcorr));
