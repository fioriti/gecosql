drop   function if exists UTYPKG_GESTID2GESTNOMIN; 
create function UTYPKG_GESTID2GESTNOMIN(pges_id integer) returns char(100)
begin
declare pnominativo varchar(100);
 select concat(rtrim(ifnull(ges_cognome,' ')),
              ' ',
              rtrim(ifnull(ges_nome,' ')))
   into pnominativo
   from gestori where ges_id = pges_id;
return pnominativo;
end;