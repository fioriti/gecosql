drop procedure if exists upd_fatture_fastweb_notecrgest_csv;
create procedure upd_fatture_fastweb_notecrgest_csv(in pnomefile_csv varchar(100)) 
begin

update fatture_fastweb_statilavnote_csv set ftstlav_nomefile_csv = pnomefile_csv;

update fatture_fastweb_statilavnote_csv set ftstlav_ced_id = (select min(ced_id) from cedenti where ced_soc = 1);

update fatture_fastweb_statilavnote_csv set ftstlav_societa = 1;

-- Assegno l'id debitore in base a societ� e codice
update fatture_fastweb_statilavnote_csv 
   set ftstlav_deb_id = (select b.deb_id from debitori as b where b.deb_soc = 1 and b.deb_cod = ftstlav_deb_cod);    
   
-- Assegno l'id data fattura
update fatture_fastweb_statilavnote_csv 
   set ftstlav_id_data_fat = (select b.id from calendario as b where b.databar=ftstlav_data_fat);

-- Assegno l'id stato lavorazione
update fatture_fastweb_statilavnote_csv 
   set ftstlav_id_stato_lav = (select b.id
                                 from stato_lavorazione as b 
                                where b.sl_soc = 1 
                                  and upper(sl_descrizione) = upper(ftstlav_stato_lav))  
 where ftstlav_nomefile_csv = pnomefile_csv;

-- Inserisco nella tabella delle anomali le fatture doppie
 insert into fatture_fastweb_statilavnote_csv_anomalie
 select b.*
   from fatture_fastweb_statilavnote_csv  as a,
        fatture_fastweb_statilavnote_csv  as b
  where b.ftstlav_societa      = a.ftstlav_societa
    and b.ftstlav_ced_id       = a.ftstlav_ced_id
    and b.ftstlav_deb_id       = a.ftstlav_deb_id
    and b.ftstlav_data_fat     = a.ftstlav_data_fat
    and b.ftstlav_num_fat      = a.ftstlav_num_fat
    and b.ftstlav_attribuzione = a.ftstlav_attribuzione
    and b.ftstlav_imp_fat      = a.ftstlav_imp_fat
    and b.ftstlav_id           < a.ftstlav_id;
    
-- Elimino le fatture doppie
delete b 
   from fatture_fastweb_statilavnote_csv  as a,
        fatture_fastweb_statilavnote_csv  as b
    where b.ftstlav_societa    = a.ftstlav_societa
    and b.ftstlav_ced_id       = a.ftstlav_ced_id
    and b.ftstlav_deb_id       = a.ftstlav_deb_id
    and b.ftstlav_data_fat     = a.ftstlav_data_fat
    and b.ftstlav_num_fat      = a.ftstlav_num_fat
    and b.ftstlav_attribuzione = a.ftstlav_attribuzione
    and b.ftstlav_imp_fat      = a.ftstlav_imp_fat
    and b.ftstlav_id           < a.ftstlav_id;
    
-- Assegno l'id fattura
update fatture_fastweb_statilavnote_csv as a
   set a.ftstlav_idfatt = (select b.id 
                            from fatture as b 
                           where b.ft_soc              = a.ftstlav_societa 
                             and b.ft_ced_id           = a.ftstlav_ced_id
                             and b.ft_deb_id           = a.ftstlav_deb_id
                             and b.ft_id_data_fat      = a.ftstlav_id_data_fat
                             and b.ft_num_fat          = a.ftstlav_num_fat
                             and b.ft_attribuzione     = a.ftstlav_attribuzione
                             and b.ft_imp_fat          = a.ftstlav_imp_fat);    
end;