drop procedure if exists AGG_DEBITORI_FASTWEB;
create PROCEDURE AGG_DEBITORI_FASTWEB ()
begin
  declare psocieta     int(10);
  declare pdebitore    varchar(50);
  declare pdescrizione varchar(100);
  declare pcitta       varchar(100);
  declare pprovincia   varchar(2);
  declare exit_loop BOOLEAN; 
  declare leggi_temp_fastweb cursor for 
  select distinct 1           as societa,
                  conto       as debitore
    from temp_fastweb
    where conto   is not null
      and conto != 0
      and length(conto) > 0
      and (1,conto) not in (select deb_soc,deb_cod 
                            from debitori);
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET exit_loop = TRUE;
 OPEN  leggi_temp_fastweb;
 read_loop: LOOP
  FETCH leggi_temp_fastweb INTO psocieta,pdebitore;
  IF exit_loop THEN
         CLOSE leggi_temp_fastweb;
         LEAVE read_loop;
  END IF;
  insert 
    into debitori (deb_id,         deb_soc,  deb_cod) 
           values (getnewdebid(1), psocieta, pdebitore);
  END LOOP read_loop;
  update debitori as a
   set a.deb_des = (select min(b.descrizione) 
                      from temp_fastweb as b
                     where b.conto = a.deb_cod
                       and b.descrizione is not null)
 where a.deb_soc = 1;
 update debitori as a
   set a.deb_citta = (select min(b.citta) 
                        from temp_fastweb as b
                       where b.conto = a.deb_cod
                         and b.citta is not null)
 where a.deb_soc = 1;
 update debitori as a
    set a.deb_pro = (select min(b.provincia) 
                       from temp_fastweb as b
                      where b.conto = a.deb_cod
                        and b.provincia is not null)
 where a.deb_soc = 1;
end;


