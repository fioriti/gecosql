drop procedure if exists fatturefastwebcsv_delcaricate;
create procedure fatturefastwebcsv_delcaricate(in pnomefile_csv varchar(100)) 
begin
delete 
  from fatture_fastweb_csv
 where ft_nomefile_csv = pnomefile_csv
   and (ft_soc,ft_ced_id,ft_deb_id,ft_num_fat,ft_attribuzione,ft_id_data_fat,ft_imp_fat) in (select ft_soc,ft_ced_id,ft_deb_id,ft_num_fat,ft_attribuzione,ft_id_data_fat,ft_imp_fat from fatture);
end;
-- Cambiare chiave di lettura ft_id_data_fat
