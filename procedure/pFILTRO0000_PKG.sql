drop   procedure if exists FILTRO0000_CHECK_EXISTS; 
create procedure FILTRO0000_CHECK_EXISTS()
begin
declare pexists numeric(1);
select case count(*) when 0 then 0 else 1 end
  into pexists
  from filtri 
 where id_filtro = 0;
if (pexists = 0)
then 
insert 
  into filtri 
select 0,'Login ',0,now();
end if;
end;
drop   procedure if exists FILTRO0000_RESET_ALL; 
create procedure FILTRO0000_RESET_ALL()
begin
update filtri_parametri 
   set parint01  = null,
       parint02  = null,
       parint03  = null,
       parint04  = null,
       parint05  = null,
       parint06  = null,
       parint07  = null,
       parint08  = null,
       parint09  = null,
       parint10  = null
 where par_conn  = connection_id()
   and id_filtro = 0;
end;