DROP PROCEDURE IF EXISTS FATTURE_FASTWEB_STORICIZZA;
CREATE PROCEDURE FATTURE_FASTWEB_STORICIZZA() 
begin
-- Storicizzo fatture
insert into fatture_storico 
select * 
  from fatture
 where id in (select id_fattura
                from fatture_idfatt_temp);
-- Storicizzo fatture_stato_fattura
insert 
  into fatture_statofattura_storico 
(fsf_ges_id,
 fsf_id,
 fsf_id_fattura,
 fsf_id_stato,
 fsf_modifica,
 fsf_nomefile_csv)
select 
fsf_ges_id,
 fsf_id,
 fsf_id_fattura,
 fsf_id_stato,
 fsf_modifica,
 fsf_nomefile_csv
  from fatture_stato_fattura
 where fsf_id_fattura in (select id_fattura
                            from fatture_idfatt_temp);
-- Storicizzo fatture_statilav_note
insert into fatture_statilavnote_storico 
select *
  from fatture_statilav_note
 where ftstlav_id_fattura in (select id_fattura
                                from fatture_idfatt_temp);
-- Storicizzo fatture_arcdoc
insert into fatture_arcdoc_storico
select * 
  from fatture_arcdoc
 where ftarcdoc_idfatt in (select id_fattura
                             from fatture_idfatt_temp);
-- Storicizzo fatture_tabsr
insert into fatture_tabsr_storico 
select * 
  from fatture_tabsr
 where fttbsr_idfatt in (select id_fattura
                           from fatture_idfatt_temp);
-- FASE DI ELIMINAZIONE DOPO STORICIZZAZIONE --
-- Elimino fatture_stato_fattura
delete fatture_stato_fattura
  from fatture_stato_fattura
  join fatture_idfatt_temp on (fatture_idfatt_temp.id_fattura = fatture_stato_fattura.fsf_id_fattura);
-- Elimino fatture_statilav_note
delete fatture_statilav_note
  from fatture_statilav_note
  join fatture_idfatt_temp   on (fatture_idfatt_temp.id_fattura = fatture_statilav_note.ftstlav_id_fattura);
-- Elimino fatture_arcdoc
delete 
  from fatture_arcdoc
 where ftarcdoc_idfatt in (select id_fattura
                             from fatture_idfatt_temp);
-- Elimino fatture_tabsr
delete
  from fatture_tabsr
 where fttbsr_idfatt in (select id_fattura
                           from fatture_idfatt_temp);
-- Eliminazione fatture
delete fatture
  from fatture
  join fatture_idfatt_temp on (fatture_idfatt_temp.id_fattura = fatture.id);
end;
