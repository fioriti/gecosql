-- Generazione di alter table che vanno a droppare le foreign key di ciascuna tabella presente nel db
SELECT CONCAT('ALTER TABLE ',kcu.TABLE_NAME,' DROP FOREIGN KEY ', kcu.CONSTRAINT_NAME,';') AS ddl  
FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu, INFORMATION_SCHEMA.STATISTICS stat  
WHERE stat.table_schema = 'geco' 
AND kcu.TABLE_NAME      = stat.TABLE_NAME   
AND kcu.COLUMN_NAME     = stat.COLUMN_NAME
AND kcu.REFERENCED_TABLE_NAME IS NOT NULL
INTO OUTFILE '/tmp/ddl.sql';
