id int(10) NOT NULL AUTO_INCREMENT,
  
ft_soc int(10) NOT NULL,
  
ft_deb_id int(10) NOT NULL,
  
ft_ced_id int(10) NOT NULL,
  
ft_num_fat varchar(20) NOT NULL,
  
ft_attribuzione varchar(50) DEFAULT NULL,
  
ft_td varchar(5) DEFAULT NULL,
  
ft_testo varchar(500) DEFAULT NULL,
  
ft_data_fat date NOT NULL,
  
ft_data_scad date DEFAULT NULL,
  
ft_imp_fat decimal(10,2) NOT NULL,
  
ft_imp_aperto decimal(10,2) DEFAULT NULL,
  
ft_data_ins date DEFAULT NULL,
  
ft_id_stato_lav int(10) DEFAULT NULL,
  
ft_td_id int(10) NOT NULL,
  
ft_id_data_fat int(10) DEFAULT NULL,
  
ft_id_data_scad int(10) DEFAULT NULL,
  
ft_nomefile_csv varchar(100) DEFAULT NULL,
  
ft_id_csv int(10) DEFAULT NULL,
  
ft_datetime_csv datetime DEFAULT NULL,
  
ft_id_data_chius int(10) DEFAULT NULL,
  
ft_data_chius datetime DEFAULT NULL,
  
ft_arcdoc_check_sino decimal(1,0) DEFAULT NULL,
  
ft_tabsr_check_sino decimal(1,0) DEFAULT NULL,
  


alter table fatture add last_ftstlav_id int(11) DEFAULT NULL;
alter table fatture add last_ftstlav_id_stato_lav int(11) NOT NULL DEFAULT '20';
alter table fatture add last_ftstlav_cod_stato_lav varchar(4) DEFAULT NULL;  
alter table fatture add last_ftstlav_des_stato_lav varchar(50) DEFAULT NULL;
  
alter table fatture add last_ftstlav_nota_crgest varchar(1000) DEFAULT NULL;
alter table fatture add last_fsf_id int(11) DEFAULT NULL;  
alter table fatture add last_fsf_id_stato int(11) NOT NULL DEFAULT '1';
  
alter table fatture add last_fsf_ac int(11) NOT NULL DEFAULT '1';  
alter table fatture add ft_forecast_mese decimal(2,0) DEFAULT NULL;
  
alter table fatture add ft_forecast_anno decimal(4,0) DEFAULT NULL;
