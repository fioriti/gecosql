ALTER TABLE geco.fatture
 CHANGE ft_aperta ft_aperta DECIMAL(1,0) NOT NULL DEFAULT '0';
 
alter table fatture add index ft_gestione_scadenziario 
(ft_soc, ft_deb_id, ft_ced_id,ft_td_id, ft_id_data_fat, ft_id_data_scad,ft_num_fat, ft_arcdoc_check_sino,ft_aperta);
           
call FOSUPD_FILTRI_PARAMALL('Cristian'); 
call FILTRO0001_RESET_ALL();
select * from gestione_scadenziario_v; 


drop table temp_fatture_id;
create table temp_fatture_id (tmpft_id integer not null, primary key (tmpft_id));
alter table temp_fatture_id add constraint fk_tempfattureid_fatture foreign key(tmpft_id) references fatture(id);

insert into temp_fatture_id (tmpft_id)
select fsf_id_fattura from fatture_stato_fattura_v where fsf_ac = 0;



update fatture 
   set ft_aperta = 1;

update fatture 
   set ft_aperta = 0 where id in (select tmpft_id from temp_fatture_id);
   
   
commit;






select a.* 
  from (
select c.id                             as id,
       c.ft_soc                         as ft_soc,
       c.ft_deb_id                      as ft_deb_id,
       -- fsf.fsf_ac                       as ft_ac,
       d.deb_id                         as ft_id_debitore,
       d.deb_cod                        as ft_deb_cod,
       d.deb_des                        as ft_deb_des,
       d.deb_citta			                as ft_deb_citta,
       c.ft_ced_id                      as ft_ced_id,
       c.ft_attribuzione                as ft_attribuzione,
       c.ft_num_fat                     as ft_num_fat,
       c.ft_td_id                       as ft_td_id,
       td.td_cod                        as ft_td,
       -- fsln.ftstlav_id_stato_lav        as ft_id_stato_lav,
       -- fsln.ftstlav_sldes               as ft_des_stato_lav, 
       -- concat(fsln.ftstlav_slcod,
       --       '-',
       --       fsln.ftstlav_sldes )      as ft_sl_coddes,
       c.ft_testo                       as ft_testo,
       df.databar                       as ft_data_fat,
       ds.databar                       as ft_data_scad,
       c.ft_imp_fat                     as ft_imp_fat, 
       c.ft_imp_aperto                  as ft_imp_aperto,
       c.ft_data_ins                    as ft_data_ins,
       c.ft_id_data_chius               as ft_id_data_chius,
       -- fsln.ftstlav_nota_crgest         as ft_nota_crgest,
       -- fsln.ftstlav_data_bar            as ft_data_nota,
       ifnull(c.ft_arcdoc_check_sino,0) as ft_arcdoc_sino,
       ifnull(c.ft_tabsr_check_sino,0)  as ft_tabsr_sino
  from filtro0001_parametri_v   as a 
  join gest_debitore            as b   on (b.gd_ges_id    = a.ges_id)
  join debitori                 as d   on (d.deb_id       = b.gd_deb_id                     and
                                           d.deb_id       = ifnull(a.parint03,d.deb_id))
  join societa                  as soc on (soc.soc_cod    = ifnull(a.parint01,soc.soc_cod))
  join tipo_doc                 as td  on (td.td_soc      = ifnull(a.parint01,soc.soc_cod)  and
                                           td.td_id       = ifnull(a.parint02,td.td_id))
  -- join stato_lavorazione        as sl  on (sl.id          = ifnull(a.parint04,sl.id))
  join calendario               as df  on (df.id between ifnull(a.parint05,0) and ifnull(a.parint06,9999999))
  join calendario               as ds  on (ds.id between ifnull(a.parint07,0) and ifnull(a.parint08,9999999))
  join fatture                  as c   on (c.ft_soc           = soc.soc_cod                   and 
                                           c.ft_deb_id        = d.deb_id                      and 
                                           c.ft_td_id         = td.td_id                      and                                        
                                           c.ft_id_data_fat   = df.id                         and
                                           c.ft_id_data_scad  = ds.id                         and
                                           c.ft_num_fat       = ifnull(parvar09,c.ft_num_fat) and
                                           ifnull(c.ft_arcdoc_check_sino,0) = case ifnull(parint11,0)
                                                                                when 0 then ifnull(c.ft_arcdoc_check_sino,0)
                                                                                else        ifnull(parint11,0)
                                                                              end             and
                                          c.ft_aperta = 0                       
                                          )
--   join fatture_stato_fattura_v  as fsf  on (fsf.fsf_id_fattura = c.id and 
--                                             fsf.fsf_ac = case ifnull(parint10,0) when 0 then 0 else fsf.fsf_ac end)
     join fatture_statilavnote_v   as fsln on (ftstlav_id_fattura   = c.id and 
                                               ftstlav_id_stato_lav = sl.id)
 ) as a;


