select substr(debann_modbar,1,10) as datains,
       debann_ges_id              as zona,
       debanno_nominativo         as gestore,
       count(*)                   as qta
  from debitori_annotazioni_v
 group by debann_ges_id, debanno_nominativo,substr(debann_modbar,1,10)
 order by substr(debann_modbar,1,10) desc,
          debann_ges_id,
          debanno_nominativo;
       
 select concat(substr(utypkg_datadate2databar(ftstlav_modifica),7,4),
               substr(utypkg_datadate2databar(ftstlav_modifica),4,2),
               substr(utypkg_datadate2databar(ftstlav_modifica),1,2)) as datains,
        ftstlav_ges_id                                          as zona,
        count(*)                                                as qta
   from fatture_statilav_note
  where ftstlav_ges_id != 0
    group by concat(substr(utypkg_datadate2databar(ftstlav_modifica),7,4),
               substr(utypkg_datadate2databar(ftstlav_modifica),4,2),
               substr(utypkg_datadate2databar(ftstlav_modifica),1,2)),
             ftstlav_ges_id
    order by concat(substr(utypkg_datadate2databar(ftstlav_modifica),7,4),
               substr(utypkg_datadate2databar(ftstlav_modifica),4,2),
               substr(utypkg_datadate2databar(ftstlav_modifica),1,2)) desc,
             ftstlav_ges_id;
             
