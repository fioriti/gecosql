call UPD_FILTRI_PARAMETRI('Coratelli',0);
call UPD_FILTRI_PARAMETRI('Coratelli',1);

select * 
  from filtri_parametri_v;
  
  
select * 
  from fatture_fastweb_csv_anomalie;
  
select count(*),sum(ifnull(a.ft_imp_fat,0)) from fatture as a join fatture_stato_fattura_v as b 
on (b.fsf_id_fattura = a.id) where b.fsf_ac = 0;





drop procedure if exists agg_gest_debitore;
create procedure agg_gest_debitore()
begin
  delete from gest_debitore
   where gd_ges_id in (select b.ges_id from gestori as b where b.ges_ruolo = 'admin');
  insert into gest_debitore 
  select b.ges_id,
         a.deb_id,
         null,
         null
    from debitori as a
    join gestori  as b
   where b.ges_ruolo = 'admin'
     and b.ges_id   != 0;
end;
