drop table societa;
create table societa (
soc_cod       int (10)    not null,
soc_des       varchar(50),
soc_data_ini  date,
soc_indirizzo varchar(50),
soc_citta     varchar(50),
primary key (soc_cod)
)
ENGINE=INNODB;