drop table if exists fatture_notecrgest;
create table fatture_notecrgest(
ftnote_id_nota      int(10)      not null auto_increment,
ftnote_id_fattura   int(10)      not null,
ftnote_nota_crgest  varchar(1000),
ftnote_ges_id       int(10)      not null default 0,
ftnote_id_nota_csv  int(10)      ,
ftnote_nomefile_csv varchar(100) ,
ftnote_modifica     datetime     not null default now(),
primary key (ftnote_id_nota)) ENGINE=INNODB;
alter table fatture_notecrgest add index fatture_notecrgest_1 (ftnote_id_fattura);
alter table fatture_notecrgest add constraint fk_fatturenotegrgest_fatture foreign key (ftnote_id_fattura) references fatture(id);
alter table fatture_notecrgest add constraint fk_fatturenotegrgest_gestori foreign key (ftnote_ges_id)     references gestori(ges_id);

-- 01062017-- Inizio: Aggiunto id_stato_lavorazione
ALTER TABLE geco.fatture_notecrgest
 ADD ftnote_id_stato_lav INT(10) NOT NULL AFTER ftnote_id_fattura;
 
alter table fatture_notecrgest add constraint fk_fatturenotecrgest_statolavorazione foreign key (ftnote_id_stato_lav) references stato_lavorazione(id); 
-- 01062017-- Fine:   Aggiunto id_stato_lavorazione