drop table if exists gest_debitori_periodi;
create table gest_debitori_periodi (
  gd_id             int(11)   NOT NULL auto_increment,
  gd_ges_id         int(11)   NOT NULL,
  gd_deb_id         int(11)   NOT NULL,
  gd_id_dataassoc   int(11)   NOT NULL,
  gd_id_datadissoc  int(11)   NOT NULL default 43829,
  gd_operatore      int(11)   NOT NULL DEFAULT 0,
  gd_modifica       datetime  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  primary key(gd_id)
) ENGINE='INNODB';
create unique index gest_debitori_periodi_1 on gest_debitori_periodi(gd_ges_id,gd_deb_id,gd_id_dataassoc);
alter table gest_debitori_periodi add constraint fk_gestdebitoriperiodi_debitori          foreign key (gd_deb_id)         references debitori   (deb_id);
alter table gest_debitori_periodi add constraint fk_gestdebitoriperiodi_gestori           foreign key (gd_ges_id)         references gestori    (ges_id);
alter table gest_debitori_periodi add constraint fk_gestdebitoriperiodiassoc_calendario   foreign key (gd_id_dataassoc)   references calendario (id);
alter table gest_debitori_periodi add constraint fk_gestdebitoriperiodidissoc_calendario  foreign key (gd_id_datadissoc)  references calendario (id);