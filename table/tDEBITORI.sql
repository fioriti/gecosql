drop table debitori;
create table debitori (
deb_id              int (10)    not null auto_increment,
deb_soc             int (10)    not null,
deb_cod             numeric(10) not null,
deb_des             varchar(100),
deb_cf_piva         varchar(16),
deb_citta           varchar(50),
deb_ind             varchar(50),
deb_cap             varchar(5),
deb_pro             varchar(2),
deb_regione         varchar(20),
deb_tel             varchar(10),
deb_mail            varchar(50),
deb_data_affido     date,
deb_data_disaffido  date,
primary key (deb_id)
)
ENGINE=INNODB;
ALTER TABLE debitori ADD UNIQUE INDEX debitori_1(deb_soc,deb_cod);
ALTER TABLE debitori ADD CONSTRAINT FK_debitori_SOCIETA FOREIGN KEY (deb_soc) REFERENCES SOCIETA(SOC_COD); 