drop table tipo_doc;
create table tipo_doc (
td_soc           int (10)     not null,
td_cod           varchar(5)   not null,
td_des           varchar(100),
primary key (td_soc,td_cod)
)
ENGINE=INNODB;
ALTER TABLE tipo_doc ADD CONSTRAINT FK_TIPODOC_SOCIETA FOREIGN KEY (td_soc) REFERENCES SOCIETA(SOC_COD); 

insert into tipo_doc select 2,'NCC','NCC';
insert into tipo_doc select 2,'NDC','NDC';
insert into tipo_doc select 2,'RTC','RTC';