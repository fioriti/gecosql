drop table gestori;
create table gestori (
ges_id        int (10)    not null,
ges_cognome   varchar(50),
ges_nome      varchar(50),
ges_mail      varchar(50),
ges_tel       varchar(50),
ges_ind       varchar(50),
ges_citta     varchar(30),
ges_regione   varchar(50),
ges_pwd       varchar(10) not null,
ges_ruolo     varchar(10) not null,
primary key (ges_id)
)
ENGINE=INNODB;