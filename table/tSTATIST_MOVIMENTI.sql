drop table if exists statist_movimenti;
create table statist_movimenti (
stmov_id        int       not null auto_increment,
stmov_gesid     int(10)   not null,
stmov_opeid     int(10)   not null,
stmod_datetime  datetime,
operatore       int(11)   not null default 0,
modifica        datetime  not null default now(),
primary key(stmov_id)
) ENGINE=INNODB;
alter table statist_movimenti add constraint fk_stmovimenti_fosuser      foreign key (operatore)   references fos_user (id);
alter table statist_movimenti add constraint fk_stmovimenti_gestori      foreign key (stmov_gesid) references gestori (ges_id);
alter table statist_movimenti add constraint fk_stmovimenti_stoperazioni foreign key (stmov_opeid) references statist_operazioni(statope_id);
