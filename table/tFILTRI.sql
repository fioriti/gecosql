drop table if exists filtri;  
create table filtri (
id_filtro    int(10)   not null,
descrizione  varchar(50),
operatore    int(11)   not null default 0,
modifica     datetime  not null default now(),
primary key (id_filtro))
ENGINE=INNODB; 

alter table filtri add constraint fk_filtri_fosuser foreign key (operatore) references fos_user(id); 

insert into filtri (id_filtro,descrizione,operatore,modifica) values ( 0,'Login');
insert into filtri (id_filtro,descrizione,operatore,modifica) values ( 1,'Gestione scadenzario');
insert into filtri (id_filtro,descrizione,operatore,modifica) values ( 2,'Gestione ordinamenti');
insert into filtri (id_filtro,descrizione,operatore,modifica) values ( 3,'Paginazione');
insert into filtri (id_filtro,descrizione,operatore,modifica) values ( 4,'Gestione TAB_ARCDOC');
insert into filtri (id_filtro,descrizione,operatore,modifica) values ( 5,'Tabella FATTURE_ARCDOC');
insert into filtri (id_filtro,descrizione,operatore,modifica) values ( 6,'Gestione attivita'' debitori');
insert into filtri (id_filtro,descrizione,operatore,modifica) values ( 7,'Gestione documenti attivita'' debitori');
insert into filtri (id_filtro,descrizione,operatore,modifica) values ( 8,'Gestione anagrafica debitori');
insert into filtri (id_filtro,descrizione,operatore,modifica) values ( 9,'Gestione generale debitori');
insert into filtri (id_filtro,descrizione,operatore,modifica) values (10,'Gestione Tabella TAB_SR');
insert into filtri (id_filtro,descrizione,operatore,modifica) values (11,'Gestione Tabella FATTURE_TABSR');
insert into filtri (id_filtro,descrizione,operatore,modifica) values (12,'Ricerca generale documenti');


