drop table tipi_arcdoc; 
create table tipi_arcdoc(
tparcdoc_id       integer     not null auto_increment,
tparcdoc_soc      integer     not null,
tparcdoc_cod      numeric(10) not null, 
tparcdoc_des      varchar(50),
tparcdoc_ges_id   integer     not null default 0,
tparcdoc_modifica datetime    not null default now(),
primary key(tparcdoc_id))
ENGINE=INNODB;
ALTER TABLE tipi_arcdoc ADD UNIQUE INDEX tipi_arcdoc_1(tparcdoc_soc,tparcdoc_cod);
ALTER TABLE tipi_arcdoc ADD INDEX        tipi_arcdoc_2(tparcdoc_soc);
ALTER TABLE tipi_arcdoc ADD CONSTRAINT   fk_tipiarcdoc_societa foreign key (tparcdoc_soc)     references societa (soc_cod);
ALTER TABLE tipi_arcdoc ADD CONSTRAINT   fk_tipiarcdoc_gestori foreign key (tparcdoc_ges_id)  references gestori (ges_id);

insert into tipi_arcdoc (tparcdoc_soc, tparcdoc_cod, tparcdoc_des) select 1,1,'Durc';
insert into tipi_arcdoc (tparcdoc_soc, tparcdoc_cod, tparcdoc_des) select 1,2,'Mandato di pagamento';
insert into tipi_arcdoc (tparcdoc_soc, tparcdoc_cod, tparcdoc_des) select 1,3,'Lettera di affido';