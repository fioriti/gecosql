drop table cedenti;
create table cedenti (
ced_id              int (10)    not null,
ced_soc             int (10)    not null,
ced_cod             int (10)    not null,
ced_des             varchar(50),
ced_citta           varchar(50),
ced_ind             varchar(50),
ced_cap             varchar(5),
ced_pro             varchar(2),
ced_regione         varchar(20),
ced_tel             varchar(10),
ced_mail            varchar(50),
ced_data_affido     date,
ced_data_disaffido  date,
primary key (ced_id)
)
ENGINE=INNODB;
ALTER TABLE cedenti ADD UNIQUE INDEX cedenti_1(ced_soc,ced_cod);
ALTER TABLE cedenti ADD CONSTRAINT FK_CEDENTI_SOCIETA FOREIGN KEY (ced_soc) REFERENCES SOCIETA(SOC_COD); 