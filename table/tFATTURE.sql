DROP TABLE fatture;
CREATE TABLE fatture (
  id                   int(10)       NOT NULL,
  ft_soc               int(10)       NOT NULL,
  ft_deb_id            int(10)       NOT NULL,
  ft_ced_id            int(10)       NOT NULL,
  ft_num_fat           varchar(20)   NOT NULL,
  ft_attribuzione      varchar(50)   DEFAULT NULL,
  ft_td                varchar(5)    DEFAULT NULL,
  ft_testo             varchar(500)  DEFAULT NULL,
  ft_data_fat          date          NOT NULL,
  ft_id_data_fat       int(10)       NOT NULL,
  ft_data_scad         date          DEFAULT NULL,
  ft_id_data_scad      int(10)       NOT NULL,
  ft_imp_fat           decimal(10,2) NOT NULL,
  ft_imp_aperto        decimal(10,2) DEFAULT NULL,
  ft_data_ins          date          DEFAULT NULL,
  ft_id_stato_lav      int(10)       DEFAULT NULL,
  ft_td_id             int(10)       NOT NULL,
  ft_nomefile_csv      varchar(100),
  ft_id_csv            int(10),
  ft_datetime_csv      datetime,
  ft_id_data_chius     int(10),
  ft_data_chius        datetime, 
  ft_arcdoc_check_sino numeric(1),
  ft_tabsr_check_sino  numeric(1),
  PRIMARY KEY (id))
  ENGINE=INNODB;
  
ALTER TABLE fatture ADD UNIQUE INDEX fatture_1(ft_soc, ft_deb_id, ft_ced_id, ft_num_fat, ft_attribuzione, ft_id_data_fat, ft_imp_fat);
ALTER TABLE fatture ADD UNIQUE INDEX fatture_2(ft_nomefile_csv,ft_id_csv);
ALTER TABLE fatture ADD INDEX fatture_3(ft_id_data_chius);
ALTER TABLE fatture ADD INDEX fatture_4(ft_data_chius);
ALTER TABLE fatture ADD CONSTRAINT FK_FATTURE_SOCIETA        FOREIGN KEY (ft_soc)           REFERENCES SOCIETA(SOC_COD);
ALTER TABLE fatture ADD CONSTRAINT FK_FATTURE_CEDENTI        FOREIGN KEY (ft_ced_id)        REFERENCES CEDENTI(CED_ID);
ALTER TABLE fatture ADD CONSTRAINT FK_FATTURE_DEBITORI       FOREIGN KEY (ft_deb_id)        REFERENCES DEBITORI(DEB_ID);
ALTER TABLE fatture ADD CONSTRAINT FK_FATTURE_TIPODOC        FOREIGN KEY (ft_td_id)         REFERENCES TIPO_DOC(TD_ID); 
ALTER TABLE fatture ADD CONSTRAINT FK_FATTURE_STATOLAV       FOREIGN KEY (ft_id_stato_lav)  REFERENCES STATO_LAVORAZIONE(ID);
ALTER TABLE fatture ADD CONSTRAINT FK_FATDAAFAT_CALENDARIO   FOREIGN KEY (ft_id_data_fat)   REFERENCES CALENDARIO(ID); 
ALTER TABLE fatture ADD CONSTRAINT FK_FATDATASCAD_CALENDARIO FOREIGN KEY (ft_id_data_scad)  REFERENCES CALENDARIO(ID); 

-- 23.03.2017 
ALTER TABLE fatture add ft_id_data_chius int(10);
ALTER TABLE fatture add ft_data_chius    date;
ALTER TABLE fatture ADD CONSTRAINT FK_FATDATACHIUS_CALENDARIO FOREIGN KEY (ft_id_data_chius)  REFERENCES CALENDARIO(ID); 

-- 13.10.2017
alter table fatture add last_ftstlav_id               integer;
alter table fatture add last_ftstlav_id_stato_lav     integer not null default 20;
alter table fatture add last_ftstlav_cod_stato_lav    varchar(4);
alter table fatture add last_ftstlav_des_stato_lav    varchar(50);
alter table fatture add last_ftstlav_nota_crgest      varchar(1000);
alter table fatture add last_fsf_id	                  integer;
alter table fatture add last_fsf_id_stato	            integer not null default 1;
alter table fatture add last_fsf_ac                   integer not null default 1;

create table fatture_pre_ftdebid as select * from fatture;



alter 

