drop table fatture_statilav_note;
create table fatture_statilav_note(
ftstlav_id           int(10)      not null auto_increment,
ftstlav_id_fattura   int(10)      not null,
ftstlav_id_stato_lav int(10)      not null,
ftstlav_nota_crgest  varchar(1000),
ftstlav_ges_id       int(10)      not null default 0,
ftstlav_id_csv       int(10)      ,
ftstlav_nomefile_csv varchar(100) ,
ftstlav_modifica     datetime     not null default now(),
primary key (ftstlav_id)) ENGINE=INNODB;
alter table fatture_statilav_note add index fatture_fatture_statilav_note_1 (ftstlav_id_fattura);
alter table fatture_statilav_note add index fatture_fatture_statilav_note_2 (ftstlav_id_fattura,ftstlav_id_stato_lav);
alter table fatture_statilav_note add constraint fk_fatturestatilavnote_fatture          foreign key (ftstlav_id_fattura)   references fatture(id);
alter table fatture_statilav_note add constraint fk_fatturestatilavnote_gestori          foreign key (ftstlav_ges_id)       references gestori(ges_id);
alter table fatture_statilav_note add constraint fk_fatturestatilavnote_statolavorazione foreign key (ftstlav_id_stato_lav) references stato_lavorazione(id);