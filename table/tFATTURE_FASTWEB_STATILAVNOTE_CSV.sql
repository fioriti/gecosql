drop table if exists fatture_fastweb_statilavnote_csv;
create table fatture_fastweb_statilavnote_csv(
ftstlav_id           int(10)     not null auto_increment,
ftstlav_idfatt       int(10),
ftstlav_societa      int(10),
ftstlav_ced_id       int(10),
ftstlav_ced_cod      int(10),
ftstlav_deb_id       int(10),
ftstlav_deb_cod      int(10),
ftstlav_id_data_fat  int(10),
ftstlav_data_fat     varchar(10),
ftstlav_num_fat      varchar(20),
ftstlav_attribuzione varchar(50),
ftstlav_imp_fat      decimal(10,2),
ftstlav_stato_lav    varchar(100),
ftstlav_id_stato_lav int(10),
ftstlav_nota         varchar(1000),
ftstlav_nomefile_csv varchar(100),
ftstlav_modifica     datetime not null default now(),
primary key (ftstlav_id)) ENGINE=INNODB;
alter table fatture_fastweb_statilavnote_csv add unique index fatture_fastweb_statilavnote_csv_1 (ftstlav_idfatt);
alter table fatture_fastweb_statilavnote_csv add index fatture_fastweb_statilavnote_csv_2 (ftstlav_societa,ftstlav_ced_id,ftstlav_deb_id,ftstlav_data_fat,ftstlav_num_fat,ftstlav_attribuzione,ftstlav_imp_fat);
alter table fatture_fastweb_statilavnote_csv add constraint fk_fatturefastwebstatilavnotecsv_fatture foreign key (ftstlav_idfatt) references fatture(id);
alter table fatture_fastweb_statilavnote_csv add constraint fk_fatturefastwebstatilavnotecsv_statolav foreign key (ftstlav_id_stato_lav) references stato_lavorazione(id);

