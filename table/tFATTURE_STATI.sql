delete from fatture_stati;

drop table fatture_stati;
create table fatture_stati
(
fs_id           int(3)  not null auto_increment,
fs_soc          int(10) not null,
fs_cod          int(3),
fs_desc         varchar(50),
fs_ges_id       int(10) not null default 0,
fs_modifica     datetime    not null default now(),
primary key (fs_id)) ENGINE=INNODB;
alter table fatture_stati add unique index fatture_stati_1 (fs_soc,fs_cod);
alter table fatture_stati add constraint fk_fatturestati_societa foreign key (fs_soc) references societa (soc_cod);

insert into fatture_stati (fs_soc, fs_cod, fs_desc, fs_ges_id ,fs_modifica) select 1,1,'Aperta'  ,0,now();
insert into fatture_stati (fs_soc, fs_cod, fs_desc, fs_ges_id ,fs_modifica) select 1,2,'Chiusa'  ,0,now();
insert into fatture_stati (fs_soc, fs_cod, fs_desc, fs_ges_id ,fs_modifica) select 1,3,'Riaperta',0,now();
insert into fatture_stati (fs_soc, fs_cod, fs_desc, fs_ges_id ,fs_modifica) select 1,4,'Richiusa',0,now();
insert into fatture_stati (fs_soc, fs_cod, fs_desc, fs_ges_id ,fs_modifica) select 2,1,'Aperta'  ,0,now();
insert into fatture_stati (fs_soc, fs_cod, fs_desc, fs_ges_id ,fs_modifica) select 2,2,'Chiusa'  ,0,now();
insert into fatture_stati (fs_soc, fs_cod, fs_desc, fs_ges_id ,fs_modifica) select 2,3,'Riaperta',0,now();
insert into fatture_stati (fs_soc, fs_cod, fs_desc, fs_ges_id ,fs_modifica) select 2,4,'Richiusa',0,now();

