drop table fatture_fastweb_csv;
CREATE TABLE fatture_fastweb_csv (
  ft_id_csv       int(10) NOT NULL AUTO_INCREMENT,
  ft_zona         int(10) ,
  ft_soc          int(10) not null default 1,
  ft_ced_cod      int(10) ,
  ft_ced_id       int(10) ,
  ft_ced_denom    varchar(100),
  ft_deb_cod      int(10),
  ft_deb_id       int(10),
  ft_deb_denom    varchar(100),
  ft_deb_citta    varchar(50),
  ft_deb_prov     varchar(2),
  ft_num_fat      varchar(20),
  ft_attribuzione varchar(50),
  ft_td           varchar(5),
  ft_td_id        int(10),
  ft_data_fat     varchar(10),
  ft_id_data_fat  int(10),
  ft_data_scad    varchar(10),
  ft_id_data_scad int(10),
  ft_imp_fat      decimal(10,2),
  ft_testo        varchar(500),
  ft_stato_lav    varchar(100),
  ft_id_stato_lav int(10),
  ft_data_ins     datetime not null default now(),
  ft_nomefile_csv varchar(100),
  PRIMARY KEY (ft_id_csv)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE geco.fatture_fastweb_csv CHANGE ft_soc ft_soc INT(10) NOT NULL DEFAULT '1';


alter table fatture_fastweb_csv add index fatture_fastweb_csv_1 (ft_nomefile_csv);
alter table fatture_fastweb_csv add index fatture_fastweb_csv_2 (ft_soc,ft_deb_id,ft_ced_id,ft_num_fat,ft_attribuzione,ft_id_data_fat,ft_imp_fat);
alter table fatture_fastweb_csv add index fatture_fastweb_csv_3 (ft_soc);
