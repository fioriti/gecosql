drop table fatture_arcdoc;
create table fatture_arcdoc
(
 ftarcdoc_id        integer not null auto_increment,
 ftarcdoc_idfatt    integer not null,
 ftarcdoc_iddoc     integer not null,
 ftarcdoc_ges_id    integer     not null default 0,
 ftarcdoc_modifica  datetime    not null default now(),
 primary key(ftarcdoc_id)
) engine = INNODB;
alter table fatture_arcdoc add index fatture_arcdoc_1 (ftarcdoc_idfatt);
alter table fatture_arcdoc add constraint fk_fatturearcdoc_fatture    foreign key (ftarcdoc_idfatt) references fatture(id);
alter table fatture_arcdoc add constraint fk_fatturearcdoc_tabarcdoc  foreign key (ftarcdoc_iddoc)  references tab_arcdoc(tbarcdoc_id);
alter table fatture_arcdoc add constraint fk_fatturearcdoc_gestori    foreign key (ftarcdoc_ges_id) references gestori (ges_id);