drop table if exists tab_procedure_parametri;
create table tab_procedure_parametri
(
 id_proc       int(10) not null,
 id_host       int(2)  not null,
 parint1       int(10),
 parint2       int(10),
 parint3       int(10),
 parint4       int(10),
 parint5       int(10),
 parcar1       char(100),
 parcar2       char(100),
 parcar3       char(100),
 parcar4       char(100),
 parcar5       char(100),
 modifica      datetime default now(),
 primary key(id_proc)
) ENGINE=INNODB;

alter table tab_procedure_parametri add constraint fk_tabprocpar_tabprocedure foreign key(id_proc) references tab_procedure(id_proc);
alter table tab_procedure_parametri add constraint fk_tabprochost_tabhost     foreign key(id_host) references tab_procedure_host(id_host);

insert into tab_procedure_parametri(id_proc,id_host,parcar1) select 1,1,'C:\\xampp\\htdocs\\geco\\sql\\ImportaFatture\\Mps\\';
insert into tab_procedure_parametri(id_proc,id_host,parcar1) select 2,1,'C:\\xampp\\htdocs\\geco\\sql\\ImportaFatture\\Fastweb\\';