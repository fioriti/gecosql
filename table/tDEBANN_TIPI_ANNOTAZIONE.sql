drop table debann_tipi_annotazione;
create table debann_tipi_annotazione
(
dantpan_id        integer     not null auto_increment,
dantpan_idsoc     integer     not null,
dantpan_cod       numeric(5)  not null,
dantpan_des       varchar(50) not null,
dantpan_ges_id    integer     not null default 0,
dantpan_modifica  datetime    not null default now(),
primary key (dantpan_id)
) ENGINE=INNODB;

alter table debann_tipi_annotazione add unique index debanntipiannotazione_1 (dantpan_idsoc,dantpan_cod);
alter table debann_tipi_annotazione add constraint fk_debanntipiannotazione_societa foreign key (dantpan_idsoc)  references societa (soc_cod);
alter table debann_tipi_annotazione add constraint fk_debanntipiannotazione_gestori foreign key (dantpan_ges_id) references gestori (ges_id);

insert into debann_tipi_annotazione (dantpan_idsoc, dantpan_cod, dantpan_des) select 1,1,'Gestione cliente';
insert into debann_tipi_annotazione (dantpan_idsoc, dantpan_cod, dantpan_des) select 1,2,'Gestione con reclamo';