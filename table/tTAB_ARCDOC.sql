drop table tab_arcdoc;
create table tab_arcdoc (
tbarcdoc_id         integer     not null auto_increment,
tbarcdoc_soc        integer     not null,
tbarcdoc_est        integer     not null,
tbarcdoc_tipo       integer     not null,
tbarcdoc_size       numeric(10) not null,
tbarcdoc_nomefile   varchar(50),
tbarcdoc_desc       varchar(50),
tbarcdoc_blob       MEDIUMBLOB,
tbarcdoc_ges_id     integer     not null default 0,
tbarcdoc_modifica   datetime    not null default now(),
primary key(tbarcdoc_id)) engine = INNODB;
alter table tab_arcdoc  add index      tab_arcdoc_1(tbarcdoc_soc);
alter table tab_arcdoc  add constraint  fk_tabarcdoc_societa            foreign key (tbarcdoc_soc)     references societa (soc_cod);
alter table tab_arcdoc  add constraint  fk_tabtarcdoc_estensionearcdoc  foreign key (tbarcdoc_est)     references estensioni_arcdoc(estarcdoc_id);
alter table tab_arcdoc  add constraint  fk_tabtarcdoc_tipiarcdoc        foreign key (tbarcdoc_tipo)    references tipi_arcdoc(tparcdoc_id);
alter table tab_arcdoc  add constraint  fk_tabarcdoc_gestori            foreign key (tbarcdoc_ges_id)  references gestori (ges_id);

