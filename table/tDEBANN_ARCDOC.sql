drop table debann_arcdoc;
create table debann_arcdoc
(
 debandoc_id        integer not null auto_increment,
 debandoc_idann     integer not null,
 debandoc_iddoc     integer not null,
 debandoc_ges_id    integer     not null default 0,
 debandoc_modifica  datetime    not null default now(),
 primary key(debandoc_id)
) engine = INNODB;
alter table debann_arcdoc add index debann_arcdoc_1(debandoc_idann);
alter table debann_arcdoc add constraint fk_debannarcdoc_debitoriannotazioni  foreign key (debandoc_idann)  references debitori_annotazioni(debann_id);
alter table debann_arcdoc add constraint fk_debannarcdoc_tabarcdoc            foreign key (debandoc_iddoc)  references tab_arcdoc(tbarcdoc_id);
alter table debann_arcdoc add constraint fk_debannarcdoc_gestori              foreign key (debandoc_ges_id) references gestori (ges_id);