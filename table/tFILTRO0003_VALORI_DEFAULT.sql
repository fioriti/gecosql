drop table filtro0003_valori_default;
create table filtro0003_valori_default(
idrecperpag  numeric(4) not null,
numpagesp    numeric(4) not null,
numpagcorr   numeric(4) not null,
numposcorr   numeric(4) not null,
maxrecperpag numeric(9) not null
);

insert into filtro0003_valori_default select 2,5,1,1,35;

select * from filtro0003_parametri_v;