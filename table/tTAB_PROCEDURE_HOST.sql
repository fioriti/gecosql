drop table if exists tab_procedure_host;

create table tab_procedure_host
(
 id_host       int(10) not null,
 descrizione   char(50),
 modifica      datetime default now(),
 primary key(id_host))
 ENGINE = INNODB;
 
 insert into tab_procedure_host (id_host,descrizione) select 1,'PcLocaleCristian';
 insert into tab_procedure_host (id_host,descrizione) select 2,'PcLocaleFederico';
 insert into tab_procedure_host (id_host,descrizione) select 3,'Hostinger';
 