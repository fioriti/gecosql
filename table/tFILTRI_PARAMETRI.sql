drop table if exists filtri_parametri;
create table filtri_parametri
(ges_id     int(10) not null,
 id_filtro  int(10) not null,
 par_conn   int(10),
 parvar01   varchar(50),
 parvar02   varchar(50),
 parvar03   varchar(50),
 parvar04   varchar(50),
 parvar05   varchar(50),
 parvar06   varchar(50),
 parvar07   varchar(50),
 parvar08   varchar(50),
 parvar09   varchar(50),
 parvar10   varchar(50),
 parvar11   varchar(50),
 parvar12   varchar(50),
 parvar13   varchar(50),
 parvar14   varchar(50),
 parvar15   varchar(50),
 parvar16   varchar(50),
 parvar17   varchar(50),
 parvar18   varchar(50),
 parvar19   varchar(50),
 parvar20   varchar(50),
 parvar21   varchar(50),
 parvar22   varchar(50),
 parvar23   varchar(50),
 parvar24   varchar(50),
 parvar25   varchar(50),
 parvar26   varchar(50),
 parvar27   varchar(50),
 parvar28   varchar(50),
 parvar29   varchar(50),
 parvar30   varchar(50),
 parint01   numeric(10),
 parint02   numeric(10),
 parint03   numeric(10),
 parint04   numeric(10),
 parint05   numeric(10),
 parint06   numeric(10),
 parint07   numeric(10),
 parint08   numeric(10),
 parint09   numeric(10),
 parint10   numeric(10),
 parint11   numeric(10),
 parint12   numeric(10),
 parint13   numeric(10),
 parint14   numeric(10),
 parint15   numeric(10),
 parint16   numeric(10),
 parint17   numeric(10),
 parint18   numeric(10),
 parint19   numeric(10),
 parint20   numeric(10), 
 parint21   numeric(10),
 parint22   numeric(10),
 parint23   numeric(10),
 parint24   numeric(10),
 parint25   numeric(10),
 parint26   numeric(10),
 parint27   numeric(10),
 parint28   numeric(10),
 parint29   numeric(10),
 parint30   numeric(10),
 pardat01   date,
 pardat02   date,
 pardat03   date,
 pardat04   date,
 pardat05   date,
 pardat06   date,
 pardat07   date,
 pardat08   date,
 pardat09   date,
 pardat10   date,
 operatore  int(11)   not null default 0,
 modifica   datetime  not null default now(),
 primary key (ges_id,id_filtro))
 ENGINE=INNODB;
 
 alter table filtri_parametri add constraint fk_filtriparametri_gestori  foreign key (ges_id)    references gestori (ges_id);
 alter table filtri_parametri add constraint fk_filtriparametri2_gestori foreign key (operatore) references gestori (ges_id);
 
 insert into filtri_parametri (ges_id,id_filtro,par_conn,parint01,parint02,parint03,parint04,parint05,parint06,parint07,parint08,parint09,parint10,parvar01,parvar02,parvar03,parvar04,parvar05,parvar06,parvar07,parvar08,parvar09,parvar10,operatore,modifica)
                       select ges_id,id_filtro,par_conn,parint01,parint02,parint03,parint04,parint05,parint06,parint07,parint08,parint09,parint10,parvar01,parvar02,parvar03,parvar04,parvar05,parvar06,parvar07,parvar08,parvar09,parvar10,operatore,modifica from save_filtri_parametri;
 
 
 insert into filtri_parametri select 31,1,null,
                                          null,null,null,null,null,null,null,null,null,null,
                                          null,null,null,null,null,null,null,null,null,null,
                                          null,null,null,null,null,null,null,null,null,null,
                                          'SYSTEM',
                                          now();
 insert into filtri_parametri select 32,1,null,
                                          null,null,null,null,null,null,null,null,null,null,
                                          null,null,null,null,null,null,null,null,null,null,
                                          null,null,null,null,null,null,null,null,null,null,
                                          'SYSTEM',
                                          now();                                          