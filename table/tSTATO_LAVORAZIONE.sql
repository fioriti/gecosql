drop table stato_lavorazione;
create table stato_lavorazione (
id                  int (10)    not null,
sl_soc              int (10)    not null,
sl_cod              int (10)    not null,
sl_descrizione      varchar(50),
primary key (id)
)
ENGINE=INNODB;
ALTER TABLE stato_lavorazione ADD UNIQUE INDEX stato_lavorazione_1(sl_soc,sl_cod);
ALTER TABLE stato_lavorazione ADD CONSTRAINT FK_STATOLAVORAZIONE_SOCIETA FOREIGN KEY (sl_soc)  REFERENCES SOCIETA(soc_cod); 
alter table stato_lavorazione add sl_ins numeric(1) not null default 1;
