drop table estensioni_arcdoc;
create table estensioni_arcdoc (
estarcdoc_id          integer     not null auto_increment,
estarcdoc_cod         varchar(5)  not null,
estarcdoc_des         varchar(50) not null,
estarcdoc_ges_id      integer     not null default 0,
estarcdoc_modifica    datetime    not null default now(),
primary key(estarcdoc_id)) engine = INNODB;

alter table estensioni_arcdoc add unique index estensioni_arcdoc_1 (estarcdoc_cod);
alter table estensioni_arcdoc add constraint fk_estensioni_arcdoc_gestori foreign key (estarcdoc_ges_id) references gestori (ges_id);

insert into estensioni_arcdoc (estarcdoc_cod, estarcdoc_des) select 'err',  'Non riconosciuto';
insert into estensioni_arcdoc (estarcdoc_cod, estarcdoc_des) select 'pdf',  'File Acrobat';
insert into estensioni_arcdoc (estarcdoc_cod, estarcdoc_des) select 'doc',  'File Word';
insert into estensioni_arcdoc (estarcdoc_cod, estarcdoc_des) select 'docx', 'File Word';
insert into estensioni_arcdoc (estarcdoc_cod, estarcdoc_des) select 'xls',  'File Excel';
insert into estensioni_arcdoc (estarcdoc_cod, estarcdoc_des) select 'xlsx', 'File Excel';
insert into estensioni_arcdoc (estarcdoc_cod, estarcdoc_des) select 'jpg',  'Immegine Ipeg';

update estensioni_arcdoc set estarcdoc_cod = 'err' where estarcdoc_cod = 'ERR';







