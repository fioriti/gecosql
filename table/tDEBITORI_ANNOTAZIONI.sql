drop table debitori_annotazioni;
create table debitori_annotazioni
(
debann_id            integer  not null auto_increment,
debann_debid         integer  not null,
debann_idtpann       integer  not null,
debann_idtpcon       integer  not null,
debann_iddata        integer  not null,
debann_annot         varchar(2000),
debann_interlocutore varchar(2000),
debann_ges_id        integer  not null default 0,
debann_modifica      datetime not null default now(),
primary key (debann_id)
) ENGINE=INNODB;

alter table debitori_annotazioni add constraint fk_debitoriannotazioni_debitori               foreign key (debann_debid)    references debitori (deb_id);
alter table debitori_annotazioni add constraint fk_debitoriannotazioni_debanntipicontatto     foreign key (debann_idtpcon)  references debann_tipi_contatto (dantpcon_id);
alter table debitori_annotazioni add constraint fk_debitoriannotazioni_debanntipiannotazione  foreign key (debann_idtpann)  references debann_tipi_annotazione (dantpan_id);
alter table debitori_annotazioni add constraint fk_debitoriannotazioni_calendario             foreign key (debann_iddata)   references calendario (id);

insert into debitori_annotazioni (debann_debid, debann_idtpann, debann_idtpcon, debann_iddata, debann_annot) select 1,1,1,1244,'Annotazione di prova';
