drop table if exists statist_operazioni;
create table statist_operazioni (
statope_id    int(10)     not null,
statope_desc  varchar(50) not null,
operatore     int(11)     not null default 0,
modifica      datetime    not null default now(),
primary key (statope_id)
) ENGINE=INNODB;

alter table statist_operazioni add constraint fk_stoperazioni_fosuser      foreign key (operatore)   references fos_user (id);

insert into statist_operazioni (statope_id,statope_desc) select 1,'Login';
