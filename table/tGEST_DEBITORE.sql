drop table gest_debitore;
create table gest_debitore (
gd_ges_id           int (10)    not null,
gd_deb_id           int (10)    not null,
gd_data_associazione   date,
gd_data_dissociazione  date,
primary key (id)
)
ENGINE=INNODB;
ALTER TABLE gest_debitore ADD UNIQUE INDEX gest_debitore_1(deb_id);
ALTER TABLE gest_debitore ADD CONSTRAINT FK_GESTDEBITORE_DEBITORI FOREIGN KEY (gd_deb_id)  REFERENCES DEBITORI(deb_id); 
ALTER TABLE gest_debitore ADD CONSTRAINT FK_GESTEDBITORI_GESTORI  FOREIGN KEY (gd_ges_id) REFERENCES GESTORI(ges_id); 