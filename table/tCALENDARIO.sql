create table calendario (
id        int(10) not null auto_increment,
data      date, 
gg        int(2),
mese      int(2),
anno      int(4),
databar   varchar(10),
datanum   int(8),
datachar  varchar(8),
primary key(id)
) ENGINE=INNODB;
alter table calendario add unique index calendario_1(datachar);
alter table calendario add unique index calendario_2(databar);