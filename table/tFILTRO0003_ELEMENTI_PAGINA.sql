drop table if exists filtro0003_elementi_pagina;
create table filtro0003_elementi_pagina (
id          int(3),
valore      int(3),
descrizione varchar(10),
primary key (id))ENGINE=INNODB;

insert into filtro0003_elementi_pagina select 1,  0,'Tutti';
insert into filtro0003_elementi_pagina select 2, 10,'10';
insert into filtro0003_elementi_pagina select 3, 25,'25';
insert into filtro0003_elementi_pagina select 4, 50,'50';
insert into filtro0003_elementi_pagina select 5,100,'100';

