drop table if exists tabsr_stati;
create table tabsr_stati
(
tbsrst_id         integer     not null auto_increment,
tbsrst_soc        integer     not null,
tbsrst_cod        varchar(1)  not null,
tbsrst_desc       varchar(50) not null,
tbsrst_ges_id     integer     not null default 0,
tbsrst_modifica   datetime    not null default now(),
primary key (tbsrst_id)
) ENGINE=INNODB;

alter table tabsr_stati add unique index tabsrstati_idx (tbsrst_soc,tbsrst_cod);
alter table tabsr_stati add constraint fk_tabsrstati_societa foreign key (tbsrst_soc)    references societa(soc_cod);
alter table tabsr_stati add constraint fk_tabsrstati_gestori foreign key (tbsrst_ges_id) references gestori(ges_id);

insert into  tabsr_stati (tbsrst_soc, tbsrst_cod , tbsrst_desc) select 1,'A','Aperto';
insert into  tabsr_stati (tbsrst_soc, tbsrst_cod , tbsrst_desc) select 1,'1','Chiuso';