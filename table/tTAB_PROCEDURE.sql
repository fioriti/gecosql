drop table if exists tab_procedure;
create table tab_procedure
(
 id_proc       int(10) not null auto_increment,
 descrizione   char(50),
 modifica      datetime default now(),
 primary key(id_proc)
) ENGINE=INNODB;

insert into tab_procedure (descrizione) select 'POPOLA_MPS';
insert into tab_procedure (descrizione) select 'POPOLA_FASTWEB';