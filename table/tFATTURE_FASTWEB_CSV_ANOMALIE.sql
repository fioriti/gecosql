CREATE TABLE fatture_fastweb_csv_anomalie (
  ft_id_csv       int(10)       NOT NULL DEFAULT '0',
  ft_zona         int(10)       DEFAULT NULL,
  ft_soc          int(10)       DEFAULT NULL,
  ft_ced_cod      int(10)       DEFAULT NULL,
  ft_ced_id       int(10)       DEFAULT NULL,
  ft_ced_denom    varchar(100)  DEFAULT NULL,
  ft_deb_cod      int(10)       DEFAULT NULL,
  ft_deb_id       int(10)       DEFAULT NULL,
  ft_deb_denom    varchar(100)  DEFAULT NULL,
  ft_deb_citta    varchar(50)   DEFAULT NULL,
  ft_deb_prov     varchar(2)    DEFAULT NULL,
  ft_num_fat      varchar(20)   DEFAULT NULL,
  ft_attribuzione varchar(50)   DEFAULT NULL,
  ft_td           varchar(5)    DEFAULT NULL,
  ft_td_id        int(10)       DEFAULT NULL,
  ft_data_fat     varchar(10)   DEFAULT NULL,
  ft_id_data_fat  int(10)       DEFAULT NULL,
  ft_data_scad    varchar(10)   DEFAULT NULL,
  ft_id_data_scad int(10)       DEFAULT NULL,
  ft_imp_fat      decimal(10,2) DEFAULT NULL,
  ft_testo        varchar(500)  DEFAULT NULL,
  ft_stato_lav    varchar(100)  DEFAULT NULL,
  ft_id_stato_lav int(10)       DEFAULT NULL,
  ft_data_ins     datetime      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ft_nomefile_csv varchar(100)  DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
