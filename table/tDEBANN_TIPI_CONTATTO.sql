drop table debann_tipi_contatto;
create table debann_tipi_contatto
(
dantpcon_id        integer     not null auto_increment,
dantpcon_idsoc     integer     not null,
dantpcon_cod       numeric(5)  not null,
dantpcon_des       varchar(50) not null,
dantpcon_ges_id    integer     not null default 0,
dantpcon_modifica  datetime    not null default now(),
primary key (dantpcon_id)
) ENGINE=INNODB;

alter table debann_tipi_contatto add unique index debanntipicontatto_1 (dantpcon_idsoc,dantpcon_cod);
alter table debann_tipi_contatto add constraint fk_debanntipicontatto_societa foreign key (dantpcon_idsoc)  references societa (soc_cod);
alter table debann_tipi_contatto add constraint fk_debanntipicontatto_gestori foreign key (dantpcon_ges_id) references gestori (ges_id);

insert into debann_tipi_contatto (dantpcon_idsoc, dantpcon_cod, dantpcon_des) select 1,1,'Telefonata';
insert into debann_tipi_contatto (dantpcon_idsoc, dantpcon_cod, dantpcon_des) select 1,2,'Fax';
insert into debann_tipi_contatto (dantpcon_idsoc, dantpcon_cod, dantpcon_des) select 1,3,'Mail';
insert into debann_tipi_contatto (dantpcon_idsoc, dantpcon_cod, dantpcon_des) select 1,4,'Visita';
insert into debann_tipi_contatto (dantpcon_idsoc, dantpcon_cod, dantpcon_des) select 1,5,'Raccomandata';