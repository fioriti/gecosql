drop table if exists fatture_fastweb_notecrgest_csv;
create table fatture_fastweb_notecrgest_csv(
ftnote_id           int(10)     not null auto_increment,
ftnote_idfatt       int(10),
ftnote_societa      int(10),
ftnote_ced_id       int(10),
ftnote_ced_cod      int(10),
ftnote_deb_id       int(10),
ftnote_deb_cod      int(10),
ftnote_id_data_fat  int(10),
ftnote_data_fat     varchar(10),
ftnote_num_fat      varchar(20),
ftnote_attribuzione varchar(50),
ftnote_imp_fat      decimal(10,2),
ftnote_stato_lav    varchar(100),
ftnote_id_stato_lav int(10),
ftnote_nota         varchar(1000),
ftnote_nomefile_csv varchar(100),
ftnote_modifica     datetime not null default now(),
primary key (ftnote_id)) ENGINE=INNODB;
alter table fatture_fastweb_notecrgest_csv add unique index fatture_fastweb_notecrgest_csv_1 (ftnote_idfatt);
alter table fatture_fastweb_notecrgest_csv add index fatture_fastweb_notecrgest_csv_2 (ftnote_societa,ftnote_ced_id,ftnote_deb_id,ftnote_data_fat,ftnote_num_fat,ftnote_attribuzione,ftnote_imp_fat);
alter table fatture_fastweb_notecrgest_csv add constraint fk_fatturefastwebnotecrgestcsv_fatture foreign key (ftnote_idfatt) references fatture(id);
alter table fatture_fastweb_notecrgest_csv add constraint fk_fatturefastwebnotecrgestcsv_statolav foreign key (ftnote_id_stato_lav) references stato_lavorazione(id);

