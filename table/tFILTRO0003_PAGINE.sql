drop table if exists filtro0003_pagine;
create table filtro0003_pagine (
id_pagina     int(10) not null,
primary key(id_pagina)) ENGINE = INNODB;

truncate table filtro0003_pagine;

insert into filtro0003_pagine select id from calendario2;

select min(id),max(id) 
  from calendario2;