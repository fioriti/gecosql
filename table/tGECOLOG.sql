drop table gecolog;
create table gecolog (
id          integer(10) not null auto_increment,
orario      datetime not null default now(),
descrizione varchar(100),
ges_id      integer(10) not null default 0,
primary key(id)
) ENGINE = INNODB;