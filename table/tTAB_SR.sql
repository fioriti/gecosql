drop table if exists tab_sr;
create table tab_sr
(
tbsr_id           integer       not null auto_increment,
tbsr_numero       varchar(10)   not null,
tbsr_nota         varchar(2000),
tbsr_stato        integer       not null,
tbsr_ges_id       integer       not null default 0,
tbsr_modifica     datetime      not null default now(),
primary key (tbsr_id)
) ENGINE=INNODB;
alter table tab_sr add unique index tab_sr_idx(tbsr_numero);
alter table tab_sr add constraint fk_tabsr_gestori    foreign key (tbsr_ges_id) references gestori (ges_id);
alter table tab_sr add constraint fk_tabsr_tabsrstati foreign key (tbsr_stato)  references tabsr_stati(tbsrst_id);
