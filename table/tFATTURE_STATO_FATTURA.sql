drop table fatture_stato_fattura;
create table fatture_stato_fattura
(
fsf_id           int(3)    not null auto_increment,
fsf_id_fattura   int(10)   not null,
fsf_id_stato     int(3)    not null,
fsf_ges_id       int(10)   not null default 0,
fsf_modifica     datetime  not null default now(),
fsf_nomefile_csv varchar(100),
primary key (fsf_id)
) ENGINE=INNODB;
alter table fatture_stato_fattura add index fatture_stato_fattura_1 (fsf_id_fattura,fsf_id_stato);
alter table fatture_stato_fattura add index fatture_stato_fattura_2 (fsf_id_fattura);
alter table fatture_stato_fattura add index fatture_stato_fattura_3 (fsf_id_stato);
alter table fatture_stato_fattura add constraint fk_fatturestatofatture_fatture      foreign key (fsf_id_fattura) references fatture (id);
alter table fatture_stato_fattura add constraint fk_fatturestatofatture_fatturestati foreign key (fsf_id_stato)   references fatture_stati (fs_id);

