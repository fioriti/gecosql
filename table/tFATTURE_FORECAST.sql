drop table if exists fatture_tabsr;
create table fatture_tabsr
(
fttbsr_id           integer       not null auto_increment,
fttbsr_idfatt       integer       not null,
fttbsr_idsr         integer       not null,
fttbsr_ges_id       integer  not null default 0,
fttbsr_modifica     datetime not null default now(),
primary key (fttbsr_id)
) ENGINE=INNODB;
alter table fatture_tabsr add unique index fatture_tabsr_idx(fttbsr_idfatt, fttbsr_idsr);
alter table fatture_tabsr add constraint fk_fatturetabsr_fatture        foreign key (fttbsr_idfatt) references fatture (id);
alter table fatture_tabsr add constraint fk_fatturetabsr_tabsr          foreign key (fttbsr_idsr)   references tab_sr (tbsr_id);
alter table fatture_tabsr add constraint fk_fatturetabsr_gestori        foreign key (fttbsr_ges_id) references gestori (ges_id);
